FROM dockerhub.ebi.ac.uk/literature-services/public-projects/xpub_base:base-node14

ENV HOME "/home/xpub"
RUN mkdir -p ${HOME}
WORKDIR ${HOME}

ARG uid
ARG gid
ENV UID=$uid
ENV GID=$gid

RUN echo "UID: $UID"
RUN echo "GID: $GID"
RUN echo "HOME: ${HOME}"
RUN echo "NODE_ENV: ${NODE_ENV}"
RUN echo "node --version"
RUN echo "python --version"

WORKDIR ${HOME}

RUN ls -l /home
RUN ls ${HOME}

# Refresh keys and update packages
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN apt-get update

# install Chrome
#RUN apt-get update && apt-get install -f -y google-chrome-stable vim less
#RUN apt-get install ca-certificates
#RUN curl -sL http://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
#RUN echo 'deb http://dl.google.com/linux/chrome/deb/ stable main' >> /etc/apt/sources.list.d/google.list

# install Firefox - apparently no debian package for firefox 57
RUN apt-get install -y libdbus-glib-1-2
RUN cd /opt && wget http://ftp.mozilla.org/pub/firefox/releases/57.0.4/linux-x86_64/en-GB/firefox-57.0.4.tar.bz2 && \
  tar xjf firefox-*.tar.bz2 && \
  ln -s /opt/firefox/firefox /usr/local/bin/
# install zip command
RUN apt-get install -y zip unzip
# RUN apt-get install -y default-jdk libstdc++6
#RUN apt-get install -y openjdk-8-jdk
RUN apt-get install -y openjdk-11-jdk
#COPY node-libxml node-libxml
#WORKDIR node-libxml
#RUN ["npm", "install"]
# WORKDIR ${HOME}

RUN npm install -g node-pre-gyp

# Install pm2
# RUN [ "yarn", "global", "add", "pm2" ]

# Remove cache and offline mirror
RUN [ "yarn", "cache", "clean"]
RUN [ "rm", "-rf", "/npm-packages-offline-cache"]

RUN groupadd -g "$GID" text && \
  useradd -u "$UID" -g text xpub

RUN chown -R xpub:users ${HOME}
RUN ls -l /home
USER xpub

COPY package.json yarn.lock ./
ENV NODE_ENV "development"

RUN ls -l ${HOME}

# We do a development install because react-styleguidist is a dev dependency and we want to run tests
RUN [ "yarn", "--ignore-platform", "install" ]

COPY app.js .babelrc .eslintignore .eslintrc .prettierrc .stylelintignore .stylelintrc ./

COPY app app
COPY config config
COPY scripts scripts
COPY static static
COPY test test
COPY webpack webpack
COPY server server
COPY ./*.sh ./
COPY ./*.js ./

RUN ls -l ${HOME}

ENV NODE_ENV ${NODE_ENV}

RUN [ "npx", "pubsweet", "build"]

CMD []
