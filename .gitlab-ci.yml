variables:
  DOCKER_IMAGE_TAG: $CI_REGISTRY_IMAGE:${CI_COMMIT_SHORT_SHA}-${CI_PIPELINE_ID}
  DOCKER_AUTH_CONFIG: '{ "auths": { "https://index.docker.io/v1/": { "auth": "echo -n "$DOCKER_HUB_USER:$DOCKER_HUB_PASSWORD" | base64" } }}'

stages:
  - build
  - lint
  - deploy_beta
  - test
  - deploy_hx_production
  - deploy_hh_production

build_beta:
  stage: build
  environment:
    name: beta
    url: https://beta.plus.europepmc.org/
  tags:
    - docker:dind 
  image: dockerhub.ebi.ac.uk/literature-services/development/docker_git:18.09-git
  services:
    - docker:18.09-dind
  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_DRIVER: overlay
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - docker version
    - docker info
  script:
    - git clone --single-branch --branch $XPUB_EPMC_CONFIG_BETA_BRANCH https://$XPUB_EPMC_CONFIG_USER:$XPUB_EPMC_CONFIG_PASS@gitlab.ebi.ac.uk/literature-services/development/xpub-epmc-config.git
    - ls -ltr
    - git branch
    - cp xpub-epmc-config/config/local-production.json config/
    - cat ./config/local-production.json
    - docker build --pull --build-arg uid=8900 --build-arg gid=1057 -t $DOCKER_IMAGE_TAG .
    - docker push $DOCKER_IMAGE_TAG
  only:
    - dev
  artifacts:
        paths:
          - xpub-epmc-config/k8s-ebi
          
build_production:
  stage: build
  environment:
    name: production
    url: https://plus.europepmc.org/
  tags:
    - docker:dind 
  image: docker:18.09-git
  services:
    - docker:18.09-dind
  variables:
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - docker version
    - docker info
  script:
    - git clone --single-branch --branch $XPUB_EPMC_CONFIG_BRANCH https://$XPUB_EPMC_CONFIG_USER:$XPUB_EPMC_CONFIG_PASS@gitlab.ebi.ac.uk/literature-services/development/xpub-epmc-config.git
    - ls -ltr
    - cp xpub-epmc-config/config/local-production.json config/
    - cat ./config/local-production.json
    - docker build --pull --build-arg uid=8900 --build-arg gid=1057 -t $DOCKER_IMAGE_TAG .
    - docker push $DOCKER_IMAGE_TAG
  only:
    - master
  artifacts:
        paths:
          - xpub-epmc-config/k8s-ebi          
            
lint:
  stage: lint
  environment:
    name: production
    url: https://plus.europepmc.org/
  tags:
    - docker:dind     
  image: $DOCKER_IMAGE_TAG
  variables:
    GIT_STRATEGY: none
  script:
    - cd ${HOME}
    - npm run lint
  only:
    - master
    - dev

deploy_beta:
  stage: deploy_beta
  environment:
    name: production-hl
    url: https://beta.plus.europepmc.org/
    kubernetes:
      namespace: mss-beta
  tags:
    - docker:dind     
  variables:
    APP_NAME: xpub-epmc
    APP_LABEL: beta
    DEPLOY_HOST: beta.plus.europepmc.org
    DATA_CENTER: HL
  image: dockerhub.ebi.ac.uk/literature-services/development/docker_git:v1.13.2
  script:
    # - kubectl create secret docker-registry gitlab-auth --docker-server=$CI_REGISTRY --docker-username=$CI_REGISTRY_USER --docker-password=$CI_REGISTRY_PASSWORD --docker-email==$GITLAB_USER_EMAIL
    - ls -ltr
    - kubectl version
    - kubectl cluster-info
    - kubectl get pods
    - kubectl delete cronjobs --all
    - >
      cat xpub-epmc-config/k8s-ebi/beta/xpub-epmc-config-common.properties xpub-epmc-config/k8s-ebi/beta/xpub-epmc-config-hh.properties
      | kubectl create configmap xpub-epmc-beta-config --from-env-file /dev/stdin -o yaml --dry-run=true
      | kubectl apply -f -
    - cat xpub-epmc-config/k8s-ebi/beta/xpub-epmc-deployment-auto.yaml | envsubst | kubectl apply -f -
    - cat xpub-epmc-config/k8s-ebi/beta/xpub-epmc-ftp-jobs.yaml | envsubst | kubectl apply -f -
    - cat xpub-epmc-config/k8s-ebi/beta/cronjobs.yaml | envsubst | kubectl apply -f -
    - sh scripts/verify-k8s-deployment.sh xpub-epmc-beta
  only:
    - dev
    
e2e_test:
  stage: test   
  script:
    - echo "Test Execution beginning"
    - "curl -X POST -F token=$TOKEN -F ref=master https://gitlab.ebi.ac.uk/api/v4/projects/494/trigger/pipeline"
  only:
    - dev

deploy_hx_prod:
  stage: deploy_hx_production
  environment:
    name: production-hx
    url: https://plus.europepmc.org/
    kubernetes:
      namespace: default
  tags:
    - docker:dind     
  variables:
    APP_NAME: xpub-epmc
    APP_LABEL: hx-production
    DEPLOY_HOST: plus.europepmc.org
    DATA_CENTER: HX
  image: dockerhub.ebi.ac.uk/literature-services/development/docker_git:v1.13.2
  script:
    - ls -ltr
    - kubectl version
    - kubectl cluster-info
    - kubectl get pods
    - kubectl delete cronjobs --all
    - >
      cat xpub-epmc-config/k8s-ebi/xpub-epmc-config-common.properties xpub-epmc-config/k8s-ebi/xpub-epmc-config-hx.properties
      | kubectl create configmap xpub-epmc-config --from-env-file /dev/stdin -o yaml --dry-run=true
      | kubectl apply -f -
    - cat xpub-epmc-config/k8s-ebi/xpub-epmc-deployment-auto.yaml | envsubst | kubectl apply -f -
    - cat xpub-epmc-config/k8s-ebi/xpub-epmc-ftp-jobs.yaml | envsubst | kubectl apply -f -
    - cat xpub-epmc-config/k8s-ebi/cronjobs-HX.yaml | envsubst | kubectl apply -f -
    - sh scripts/verify-k8s-deployment.sh xpub-epmc
  only:
    - master

deploy_hl_prod:
  stage: deploy_hh_production
  environment:
    name: production-hl
    url: https://plus.europepmc.org/
    kubernetes:
      namespace: mss-production    
  tags:
    - docker:dind     
  variables:
    APP_NAME: xpub-epmc
    APP_LABEL: hl-production
    DEPLOY_HOST: plus.europepmc.org
    DATA_CENTER: HL
  image: dockerhub.ebi.ac.uk/literature-services/development/docker_git:v1.13.2
  script:
    - ls -ltr
    - kubectl version
    - kubectl cluster-info
    - kubectl get pods
    - kubectl delete cronjobs --all
    - >
      cat xpub-epmc-config/k8s-ebi/xpub-epmc-config-common.properties xpub-epmc-config/k8s-ebi/xpub-epmc-config-hh.properties
      | kubectl create configmap xpub-epmc-config --from-env-file /dev/stdin -o yaml --dry-run=true
      | kubectl apply -f -
    - cat xpub-epmc-config/k8s-ebi/xpub-epmc-deployment-auto.yaml | envsubst | kubectl apply -f -
    - cat xpub-epmc-config/k8s-ebi/xpub-epmc-ftp-jobs.yaml | envsubst | kubectl apply -f -
    - cat xpub-epmc-config/k8s-ebi/cronjobs-HL.yaml | envsubst | kubectl apply -f -
    - sh scripts/verify-k8s-deployment.sh xpub-epmc
  only:
    - master
    
# test:
#   image: $IMAGE_ORG/$IMAGE_NAME:$CI_COMMIT_SHA
#   stage: test
#   variables:
#     GIT_STRATEGY: none
#     # setup data for postgres image
#     POSTGRES_USER: test
#     POSTGRES_PASSWORD: pw
#     # connection details for tests
#     PGUSER: test
#     PGPASSWORD: pw
#     NODE_ENV: test
#   services:
#     - postgres
#   script:
#     - cd ${HOME}
#     # specify host here else it confuses the linked postgres image
#     - PGHOST=postgres yarn jest

# test:chrome:
#   image: $IMAGE_ORG/$IMAGE_NAME:$CI_COMMIT_SHA
#   stage: test
#   variables:
#     GIT_STRATEGY: none
#     # setup data for postgres image
#     POSTGRES_USER: test
#     POSTGRES_PASSWORD: pw
#     # connection details for tests
#     PGUSER: test
#     PGPASSWORD: pw
#     NODE_ENV: test
#   services:
#     - postgres
#   script:
#     - cd ${HOME}
#     # specify host here else it confuses the linked postgres image
#     - PGHOST=postgres npm run test:e2e:ci:chrome

# test:firefox:
#   image: $IMAGE_ORG/$IMAGE_NAME:$CI_COMMIT_SHA
#   stage: test
#   variables:
#     GIT_STRATEGY: none
#     # setup data for postgres image
#     POSTGRES_USER: test
#     POSTGRES_PASSWORD: pw
#     # connection details for tests
#     PGUSER: test
#     PGPASSWORD: pw
#     NODE_ENV: test
#   services:
#     - postgres
#   script:
#     - cd ${HOME}
#     # specify host here else it confuses the linked postgres image
#     - PGHOST=postgres npm run test:e2e:ci:firefox

# push:latest:
#   image: docker:latest
#   stage: staging
#   script:
#     - if [ -z "$DOCKERHUB_USERNAME" ] || [ -z "$DOCKERHUB_PASSWORD" ]; then echo "Not pushing" && exit 0; fi
#     - docker login -u $DOCKERHUB_USERNAME -p $DOCKERHUB_PASSWORD
#     - echo "Ignore warning! Cannot perform an interactive login from a non TTY device"
#     - docker build -t $IMAGE_ORG/$IMAGE_NAME:latest --label COMMIT_SHA=$CI_COMMIT_SHA .
#     - docker push $IMAGE_ORG/$IMAGE_NAME:latest
#   only:
#   - master

# review:xpub-collabra:
#   image: pubsweet/deployer:latest
#   stage: review
#   variables:
#     PACKAGE_NAME: xpub-collabra
#     FORCE_FRESH_DB: "yes"
#     REQUIRES_PROVISIONING: "yes"
#   environment:
#     name: $PACKAGE_NAME/review/$CI_COMMIT_REF_NAME
#     # !! kube-lego will fail if domain > 64 chars
#     url: "http://${CI_ENVIRONMENT_SLUG}.${BASE_DOMAIN}"
#     on_stop: stop_review:xpub-collabra
#   except:
#   - master
#   script:
#     - source deploy.sh
#     - create_deployment

# stop_review:xpub-collabra:
#   image: pubsweet/deployer:latest
#   stage: review
#   variables:
#     PACKAGE_NAME: xpub-collabra
#     REQUIRES_PROVISIONING: "yes"
#     GIT_STRATEGY: none
#   environment:
#     name: $PACKAGE_NAME/review/$CI_COMMIT_REF_NAME
#     action: stop
#   when: manual
#   except:
#   - master
#   script:
#     - source deploy.sh
#     - delete_deployment
#     - delete_objects_in_environment pvc

# staging:xpub-collabra:
#   image: pubsweet/deployer:latest
#   stage: staging
#   variables:
#     PACKAGE_NAME: xpub-collabra
#   environment:
#     name: $PACKAGE_NAME/staging
#     url: "https://${CI_ENVIRONMENT_SLUG}.${BASE_DOMAIN}"
#   only:
#   - master
#   script:
#     - source deploy.sh
#     - create_deployment

# production:xpub-collabra:
#   image: pubsweet/deployer:latest
#   stage: production
#   variables:
#     PACKAGE_NAME: xpub-collabra
#   environment:
#     name: $PACKAGE_NAME/production
#     url: "https://${CI_ENVIRONMENT_SLUG}.${BASE_DOMAIN}"
#   when: manual
#   only:
#   - master
#   script:
#     - source deploy.sh
#     - create_deployment

# demo:xpub-collabra:
#   image: pubsweet/deployer:latest
#   stage: demo
#   variables:
#     PACKAGE_NAME: xpub-collabra
#   environment:
#     name: $PACKAGE_NAME/demo
#     url: "https://${CI_ENVIRONMENT_SLUG}.${BASE_DOMAIN}"
#   when: manual
#   script:
#     - source deploy.sh
#     - create_deployment
