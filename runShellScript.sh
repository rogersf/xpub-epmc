#!/bin/bash

cmd=$1

echo "runShellScript.sh $cmd"

# Start the process
PROCESS_RESULT=`ps aux | grep "$cmd" | grep -v "runShellScript.sh" | grep -v grep`

echo "$PROCESS_RESULT"

if [ -z "$PROCESS_RESULT" ]
then
    echo "Executing: $cmd"
    $cmd
    status=$?
    echo "status: $status"
    if [ $status -ne 0 ]; then
      echo "Failed to start $cmd: $status"
      exit $status
    fi
else
    echo "Process $cmd already running"
    exit 1
fi
