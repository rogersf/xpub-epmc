// const path = require('path')

module.exports = {
  testEnvironment: 'node',
  // rootDir: path.resolve(__dirname, '../../'),
  moduleFileExtensions: ['js', 'json'],
  // moduleNameMapper: {
  //   '^@/(.*)$': '<rootDir>/src/$1',
  // },
  transform: {
    '^.+\\.js$': 'babel-jest',
    // '\\.[jt]sx?$': 'babel-jest',
  },

  transformIgnorePatterns: [
    '<rootDir>/node_modules/(?!axios)',
    // '<rootDir>/server/',
  ],
  // add snapshotSerializers and setupFiles if necessary
  // mapCoverage: true,
  // coverageDirectory: '<rootDir>/test/unit/coverage',
  // collectCoverageFrom: [
  //   'app/**/*.js',
  //   'config/**/*.js',
  //   'scripts/**/*.js',
  //   'server/**/*.js'
  // ]
  // coverageReporters: ['json', 'lcovonly', 'text', 'clover'],
  testPathIgnorePatterns: [
    '<rootDir>/node_modules/',
    '<rootDir>/config/test.js',
  ],
  globals: {
    // config: require('./config/default.js'),
  },
}
