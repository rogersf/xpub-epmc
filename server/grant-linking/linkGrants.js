const config = require('config')
const logger = require('@pubsweet/logger')
const moment = require('moment')
const Client = require('ftp')
const { Readable } = require('stream')
const { grantEmail } = require('../email')

module.exports.createGrantLinks = async function createGrantLinks(
  manuscripts,
  removeFundingGroup,
  addFundingGroup,
) {
  try {
    const filename = `grants${
      manuscripts.length === 1 ? `.${manuscripts[0].id}` : ''
    }.${moment().format('YYYY_MM_DD-HH_mm_SS')}.xml`
    const sendManuscripts = manuscripts.reduce((list, manuscript) => {
      const articleIds =
        manuscript['meta,articleIds'] &&
        manuscript['meta,articleIds']
          .filter(a => ['pmcid', 'pmid'].includes(a.pubIdType))
          .sort((a, b) => (a.pubIdType > b.pubIdType ? 1 : -1))
      const fundingGroup =
        (manuscript['meta,fundingGroup'] &&
          manuscript['meta,fundingGroup'].length > 0 &&
          manuscript['meta,fundingGroup']) ||
        undefined
      if (
        articleIds &&
        articleIds.length > 0 &&
        (fundingGroup || removeFundingGroup)
      ) {
        list.push({
          manuscript,
          articleIds,
          fundingGroup,
        })
      } else {
        logger.info(`${manuscript.id} No grants to link or no ID to link to`)
      }
      return list
    }, [])
    if (sendManuscripts.length > 0) {
      let contents = `<?xml version="1.0" encoding="UTF-8"?>
<grants>`
      const grantsLists = await Promise.all(
        sendManuscripts.map(async each => {
          const grantList = await createGrantXML(
            each.articleIds,
            each.fundingGroup,
            removeFundingGroup,
          )
          logger.info(`${each.manuscript.id} Grant list created`)
          emailGrantPis(each.manuscript, addFundingGroup || each.fundingGroup)
          return grantList
        }),
      )
      contents += grantsLists.join('\n')
      contents += '</grants>'
      await sendXML(contents, filename)
      logger.info(`Grant list sent to EBI`)
      return { success: true, message: 'Grant list sent to EBI' }
    }
    return { success: true, message: 'No grants to link or no ID to link to' }
  } catch (err) {
    logger.error(`Unable to link grants: ${err}`)
    return { success: false, message: `Unable to link grants: ${err}` }
  }
}

async function createGrantXML(
  articleIds = [],
  fundingGroup = [],
  removeFundingGroup = [],
) {
  const aid =
    articleIds.find(a => a.pubIdType === 'pmid') ||
    articleIds.find(a => a.pubIdType === 'pmcid')
  const xml = `${removeFundingGroup
    .map(
      g =>
        `  <grant cmd="remove" id="${aid.id}" agency="${g.fundingSource}" grant-name="${g.awardId}"/>`,
    )
    .join('\n')}
${fundingGroup
  .map(
    g =>
      `  <grant cmd="add" id="${aid.id}" agency="${g.fundingSource}" grant-name="${g.awardId}"/>`,
  )
  .join('\n')}`
  return xml
}

function sendXML(contents, filename) {
  return new Promise((resolve, reject) => {
    let ftp = new Client()
    const stream = new Readable()
    stream._read = () => {}
    stream.push(contents)
    stream.push(null)
    ftp.on('ready', () => {
      ftp.put(
        stream,
        `/${config.get('grants-ftp')['send-folder']}/${filename}`,
        err => {
          ftp.end()
          ftp = null
          if (err) {
            reject(err)
          }
          resolve(true)
        },
      )
    })
    const { host, user, password } = config.get('grants-ftp')
    ftp.connect({ host, user, password })
  })
}

async function emailGrantPis(manuscript, grantList) {
  const { teams, id, status } = manuscript
  const releaseDelay = manuscript['meta,releaseDelay']
  const title = manuscript['meta,title']
  const fundingGroup = manuscript['meta,fundingGroup']
  const emails = teams.map(t => t.users[0].identities[0].email)
  const { users } =
    teams.find(t => t.roleName === 'reviewer') ||
    teams.find(t => t.roleName === 'submitter')
  const [user] = users
  if (!fundingGroup || fundingGroup.length === 0) return true
  if (fundingGroup.some(g => emails.includes(g.pi.email))) return true

  const piList = grantList.reduce((pis, curr) => {
    const exists = pis.findIndex(p => p.pi.email === curr.pi.email)
    const award = {
      awardId: curr.awardId,
      fundingSource: curr.fundingSource,
      title: curr.title,
    }
    if (exists > -1) {
      pis[exists].awards.push(award)
    } else {
      pis.push({
        pi: curr.pi,
        awards: [award],
      })
    }
    return pis
  }, [])
  const manInfo = {
    id,
    title,
    releaseDelay,
    status,
  }
  const { givenNames, surname } = user
  const linkName = { givenNames, surname }
  await Promise.all(
    piList.map(async li => grantEmail(li.pi, li.awards, manInfo, linkName)),
  )
  return true
}
