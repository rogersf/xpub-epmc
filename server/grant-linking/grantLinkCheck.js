const logger = require('@pubsweet/logger')
const { checkJobStatus, uncaughtError } = require('../job-runner')
const Manuscript = require('../xpub-model/entities/manuscript/data-access')
const { transaction } = require('objection')
const { errorDevEmail } = require('../email')
const { createGrantLinks } = require('./linkGrants')

if (!process.env.ENABLE_CRONJOB_GRANTLINKCHECK) {
  logger.info(
    'ENABLE_CRONJOB_GRANTLINKCHECK not defined. grantLinkCheck cronjob exits.',
  )
  process.exit(0)
}

;(async () => {
  await checkJobStatus('link-grants', async () => {
    const updateTime = Date.now()
    logger.info('Sending grant data')
    await grantLinkCheck()
    logger.info(
      `Grant link check and sending was completed in ${Date.now() -
        updateTime} ms`,
    )
  })
  await Manuscript.knex().destroy()
  process.exit(0)
})()

process
  .on('uncaughtException', async (err, origin) => {
    logger.error(`Uncaught Exception thrown: ${err}`)
    logger.error('Exception thrown at: ', origin)
    await uncaughtError('link-grants', `Uncaught Exception thrown: ${err}`)
    Manuscript.knex && (await Manuscript.knex().destroy())
    process.exit(1)
  })
  .on('unhandledRejection', async (reason, promise) => {
    logger.error('Unhandled Rejection: ', reason)
    logger.error('Rejected at promise: ', promise)
    await uncaughtError('link-grants', `Unhandled Rejection: ${reason}`)
    Manuscript.knex && (await Manuscript.knex().destroy())
    process.exit(1)
  })

async function grantLinkCheck() {
  const trx = await transaction.start(Manuscript.knex())
  try {
    const linkingManuscripts = await Manuscript.query()
      .where('fundingState', 'Linking grants')
      .whereNull('deleted')
      .eager('teams.users.identities')
      .modifyEager('teams', builder => {
        builder.whereNull('deleted')
      })
    if (linkingManuscripts.length > 0) {
      const grantLinkDone = await createGrantLinks(linkingManuscripts)
      if (!grantLinkDone.success) {
        throw new Error(grantLinkDone.message)
      }
      await linkingManuscripts.reduce(async (promise, m) => {
        await promise
        delete m.teams
        m.fundingState = 'Grants linked'
        await m.saveWithTrx(trx)
        return Promise.resolve()
      }, Promise.resolve())
      trx.commit()
      return true
    }
    logger.info('No grant links to send.')
    if (trx) await trx.rollback()
    return true
  } catch (e) {
    if (trx) await trx.rollback()
    await errorDevEmail('sending grants to EBI', e)
    logger.error(`Unable to get or send grant links: ${e}`)
    throw e
  }
}
