const { Job } = require('../entities/config/data-access')

exports.seed = async (knex, Promise) => {
  const midnight = new Date()
  midnight.setHours(0, 0, 0, 0)
  // Initial table data
  const seeds = [
    {
      name: 'tagging-alert',
      running: false,
      last_status: 'pass',
      last_pass: midnight,
      timeout: 1800000,
      stale: 3600000,
      frequency: '0 1 * * *',
      description: 'Sends tagging and XML QA alert/receipt emails',
    },
    {
      name: 'check-incomplete-citations',
      running: false,
      last_status: 'pass',
      last_pass: midnight,
      timeout: 1800000,
      stale: 3600000,
      frequency: '0 1 * * *',
      description: 'Attempts to complete citations of submissions with PMIDs',
    },
    {
      name: 'to-ncbi',
      running: false,
      last_status: 'pass',
      last_pass: midnight,
      timeout: 1800000,
      stale: 3600000,
      frequency: '0 3 * * * ',
      description: 'Sends complete submission packets to the NCBI',
    },
    {
      name: 'remove-ncbi',
      running: false,
      last_status: 'pass',
      last_pass: midnight,
      timeout: 1800000,
      stale: 3600000,
      frequency: '0 4 * * *',
      description: 'Sends removal requests to the NCBI',
    },
    {
      name: 'from-ncbi',
      running: false,
      last_status: 'pass',
      last_pass: midnight,
      timeout: 1800000,
      stale: 3600000,
      frequency: '0 */12 * * *',
      description:
        'Retrieves submission success and failure messages from the NCBI',
    },
    {
      name: 'link-grants',
      running: false,
      last_status: 'pass',
      last_pass: midnight,
      frequency: '0 7 * * *',
      description: 'Checks and sends grant links to the EBI',
    },
    {
      name: 'check-stalled',
      running: false,
      last_status: 'pass',
      last_pass: midnight,
      timeout: 1800000,
      stale: 3600000,
      frequency: '0 6 * * *',
      description: 'Notifies users of their stalled manuscript submissions',
    },
    {
      name: 'published-check',
      running: false,
      last_status: 'pass',
      last_pass: midnight,
      timeout: 1800000,
      stale: 3600000,
      frequency: '0 18 * * *',
      description: 'Checks whether complete submissions are live on Europe PMC',
    },
    {
      name: 'special-db-checks',
      running: false,
      last_status: 'pass',
      last_pass: midnight,
      timeout: 1800000,
      stale: 3600000,
      frequency: '0 20 * * *',
      description:
        'Checks for special, infrequent actions and performs as needed',
    },
    {
      name: 'privacy-notice',
      running: false,
      last_status: 'pass',
      last_pass: midnight,
      timeout: 1800000,
      stale: 3600000,
      frequency: '30 2 * * *',
      description: 'Checks and updates the privacy notice version number',
    },
    {
      name: 'orcid-update',
      running: false,
      last_status: 'pass',
      last_pass: midnight,
      timeout: 1800000,
      stale: 3600000,
      frequency: '0 */12 * * *',
      description: 'Checks for and mirrors changes to linked ORCID IDs',
    },
    {
      name: 'populate-journal',
      running: false,
      last_status: 'pass',
      last_pass: midnight,
      timeout: 1800000,
      stale: 3600000,
      frequency: '0 4 * * 0,3',
      description: 'Updates the local NLM Catalog-based journal list',
    },
    {
      name: 'pdf-converter',
      running: false,
      last_status: 'pass',
      last_pass: midnight,
      timeout: 1800000,
      stale: 3600000,
      frequency: '*/4 * * * *',
      description:
        'Runs checks to send and receive files from NCBI PDF converter',
    },
    {
      name: 'from-ftp-bulkupload',
      running: false,
      last_status: 'pass',
      last_pass: midnight,
      timeout: 1800000,
      stale: 3600000,
      frequency: '0 * * * *',
      description: 'Retrieves bulk upload files from FTP',
    },
    {
      name: 'from-ftp-taggerimport',
      running: false,
      last_status: 'pass',
      last_pass: midnight,
      timeout: 1800000,
      stale: 3600000,
      frequency: '0 * * * *',
      description: 'Retrieves tagger files from FTP',
    },
    {
      name: 'xml-converter',
      running: false,
      last_status: 'pass',
      last_pass: midnight,
      timeout: 1800000,
      stale: 3600000,
      frequency: '*/15 * * * *',
      description: 'Handles XML validation and conversion',
    },
  ]

  // Inserts seed entries if name not already in database
  const jobs = await Job.selectAll()

  const newJobs = seeds.reduce((arr, s) => {
    if (!jobs.some(j => j.name === s.name)) arr.push(s)
    return arr
  }, [])

  return knex('config.job').insert(newJobs)
}
