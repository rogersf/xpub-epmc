const { Property } = require('../entities/config/data-access')

exports.seed = async (knex, Promise) => {
  // Initial table data
  const seeds = [
    {
      name: 'homeNotification',
      description:
        'Login page notification settings. Checkbox must be checked for notification to appear, even when prescheduled. If dates are included, the notification will appear only after the start date and before the end date.',
      schema: JSON.stringify({
        type: 'object',
        properties: {
          display: {
            type: 'boolean',
            description: 'Show notification on login page',
          },
          kind: {
            type: 'string',
            enum: ['warning', 'error', 'info', 'success'],
            description: 'Notification type',
          },
          message: {
            type: 'string',
            format: 'block',
            description: 'Notification message text',
          },
          startDate: {
            type: 'object',
            properties: {
              date: {
                type: 'string',
                format: 'date',
                description: 'Date (YYYY-MM-DD)',
              },
              time: {
                type: 'string',
                format: 'time',
                description: 'Time (00:00:00)',
              },
            },
            required: ['date', 'time'],
            description: 'Start date for notification (optional)',
          },
          stopDate: {
            type: 'object',
            properties: {
              date: {
                type: 'string',
                format: 'date',
                formatMinimum: {
                  $data: '/value/startDate/date',
                },
                description: 'Date (YYYY-MM-DD)',
              },
              time: {
                type: 'string',
                format: 'time',
                formatMinimum: {
                  $data: '/value/startDate/time',
                },
                description: 'Time (00:00:00)',
              },
            },
            required: ['date', 'time'],
            description: 'End date for notification (optional)',
          },
        },
        dependencies: { stopDate: ['startDate'] },
        required: ['display', 'kind', 'message'],
      }),
      value: JSON.stringify({
        display: false,
        kind: 'warning',
        message:
          'We are currently experiencing technical issues affecting the ability to create and review submissions. We are working to resolve these issues, and apologise for any inconvenience.',
      }),
    },
    {
      name: 'holidayNotification',
      description:
        'Login page holiday notice settings. Start and end dates of the holiday period are required for the message text. Only the checkbox and end date are used for display calculations; if the checkbox is checked the notification will appear until the end date.',
      schema: JSON.stringify({
        type: 'object',
        properties: {
          display: { type: 'boolean', description: 'Show holiday notice' },
          startDate: {
            type: 'object',
            properties: {
              date: {
                type: 'string',
                format: 'date',
                description: 'Date (YYYY-MM-DD)',
              },
              time: {
                type: 'string',
                format: 'time',
                description: 'Time (00:00:00)',
              },
            },
            required: ['date', 'time'],
            description: 'Start date for holiday message',
          },
          stopDate: {
            type: 'object',
            properties: {
              date: {
                type: 'string',
                format: 'date',
                description: 'Date (YYYY-MM-DD)',
                formatMinimum: {
                  $data: '/value/startDate/date',
                },
              },
              time: {
                type: 'string',
                format: 'time',
                description: 'Time (00:00:00)',
                formatMinimum: {
                  $data: '/value/startDate/time',
                },
              },
            },
            required: ['date', 'time'],
            description: 'End date for holiday message',
          },
        },
        dependencies: { stopDate: ['startDate'] },
        required: ['display', 'startDate', 'stopDate'],
      }),
      value: JSON.stringify({
        display: false,
        startDate: { date: '2020-12-25', time: '00:00:00' },
        stopDate: { date: '2021-01-03', time: '00:00:00' },
      }),
    },
    {
      name: 'preprintServers',
      description:
        'NLM TAs of preprint servers in the NLM Catalog, to refuse manuscript submissions.',
      schema: JSON.stringify({
        type: 'array',
        items: { type: 'string', description: 'Server name' },
      }),
      value: JSON.stringify([
        'medRxiv',
        'bioRxiv',
        'Res Sq',
        'SSRN',
        'ArXiv',
        'ChemRxiv',
      ]),
    },
    {
      name: 'approvedServers',
      description:
        'List of preprint servers to skip submission QA. Use NLM TA or consistent exact title if no NLM TA available.',
      schema: JSON.stringify({
        type: 'array',
        items: { type: 'string', description: 'Server name' },
      }),
      value: JSON.stringify(['medRxiv', 'bioRxiv', 'Res Sq', 'SSRN', 'ArXiv']),
    },
    {
      name: 'planSFunders',
      description:
        'Plan S/Horizon Europe funder information. Check value only needed if not default date (2021-01-01T00:00:00.000Z) or category (Plan S).',
      schema: JSON.stringify({
        type: 'array',
        items: {
          type: 'object',
          properties: {
            funderName: { type: 'string', description: 'Funder name' },
            checkStrategy: {
              type: 'string',
              enum: ['grant date', 'submit date', 'category'],
              default: 'grant date',
              description: 'Grant checking strategy',
            },
            checkValue: {
              type: ['string', 'null'],
              description: 'Check value (optional)',
            },
            exceptionEmail: {
              type: 'string',
              format: 'email',
              description: 'Exception query email address',
            },
          },
          required: ['funderName', 'exceptionEmail', 'checkStrategy'],
        },
      }),
      value: JSON.stringify([
        {
          funderName: 'Wellcome Trust',
          checkStrategy: 'submit date',
          exceptionEmail: 'helpdesk@europepmc.org',
        },
      ]),
    },
  ]

  // Inserts seed entries if name not already in database
  const props = await Property.selectAll()

  const newProps = seeds.reduce((arr, s) => {
    if (!props.some(p => p.name === s.name)) arr.push(s)
    return arr
  }, [])

  return knex('config.property').insert(newProps)
}
