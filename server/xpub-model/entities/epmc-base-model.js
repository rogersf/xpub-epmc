const { transaction, snakeCaseMappers } = require('objection')
const BaseModel = require('@pubsweet/base-model')
const logger = require('@pubsweet/logger')

const knex = BaseModel.knex()

class EpmcBaseModel extends BaseModel {
  static get columnNameMappers() {
    return snakeCaseMappers()
  }

  async save(options) {
    let trx
    let saved
    try {
      trx = await transaction.start(knex)
      saved = await this.saveWithTrx(trx, options)
      await trx.commit()
    } catch (error) {
      if (trx) {
        await trx.rollback()
      }
      logger.error('Nothing was saved')
      logger.error(error)
      throw error
    }

    return saved
  }

  async saveWithTrx(
    trx,
    options = { relate: true, unrelate: true, insertMissing: true },
  ) {
    logger.debug('Objects to insert is ', this.toJSON())
    const saved = await this.constructor
      .query(trx)
      .upsertGraph(this.toJSON(), options)
    logger.debug(`Saved ${this.constructor.name} with ID ${saved.id}`)

    return saved
  }

  static async findByFieldEager(field, value, eagerExpr) {
    logger.debug('Finding', field, value)

    try {
      const results = await this.query()
        .eager(eagerExpr)
        .where(field, value)

      return results
    } catch (e) {
      logger.error(e)
    }
  }

  async findByFieldValuesIn(field, values, eagerExpr) {
    logger.debug('Finding', field, values)

    try {
      const results = await this.constructor
        .query()
        .eager(eagerExpr)
        .whereIn(field, values)

      return results
    } catch (e) {
      logger.error(e)
    }
  }

  async findByFieldIsNull(field, eagerExpr) {
    logger.debug(`Finding ${field} is null`)

    try {
      const results = await this.constructor
        .query()
        .eager(eagerExpr)
        .whereNull(field)

      return results
    } catch (e) {
      logger.error(e)
    }
  }

  // this is the change I want to see
  $beforeInsert() {
    this.created = new Date().toISOString()
    this.updated = this.created
  }

  $beforeUpdate() {
    this.updated = new Date().toISOString()
  }

  $beforeDelete() {
    this.deleted = new Date().toISOString()
    this.updated = this.deleted
  }
}

module.exports = EpmcBaseModel
