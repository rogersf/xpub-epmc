const { transaction } = require('objection')
const config = require('config')
const lodash = require('lodash')
const fetch = require('node-fetch')
const rfr = require('rfr')
const authorization = require('pubsweet-server/src/helpers/authorization')
const logger = require('@pubsweet/logger')
const ManuscriptAccess = require('./data-access')
const TeamAccess = require('../team/data-access')
const UserAccess = require('../user/data-access')
const Note = require('../note/')
const Review = require('../review/')
const File = require('../file/')
const Audit = require('../audit/')
const { PropManager } = require('../config')
const { dManuscriptUpdate, gManuscript } = require('./helpers/transform')

const { token } = rfr('server/utils/authentication')
const { createPackageForTaggers } = rfr('server/ftp-integration/toTaggers')
const { createGrantLinks } = rfr('server/grant-linking/linkGrants')
const { baseUrl } = config.get('pubsweet-server')

const {
  userMessage,
  reviewerEmail,
  submitterRejectEmail,
  removeDuplicateEmail,
  alreadyCompleteEmail,
  finalReviewEmail,
  correctedReviewEmail,
  preprintReviewEmail,
  preprintVersionEmail,
  taggerErrorEmail,
  deletedEmail,
  deletedTaggerEmail,
  finalReviewerEmail,
} = rfr('server/email')

const grantCheck = (origMan, changeMan) => {
  if (origMan.fundingState === 'Grants linked') {
    const origSet = origMan['meta,fundingGroup']
    const changeSet = changeMan['meta,fundingGroup']
    if (changeSet) {
      const added = changeSet.filter(
        p =>
          !origSet.some(
            o =>
              o.awardId === p.awardId &&
              o.fundingSource === p.fundingSource &&
              o.pi.surname === p.pi.surname,
          ),
      )
      const removed = origSet.filter(
        p =>
          !changeSet.some(
            o =>
              o.awardId === p.awardId &&
              o.fundingSource === p.fundingSource &&
              o.pi.surname === p.pi.surname,
          ),
      )
      if (added.length > 0 || removed.length > 0) {
        return { send: true, added, removed }
      }
    }
  }
  return { send: false }
}

const addReviewer = async (manuscript, userId, trx = null) => {
  const { meta, teams } = manuscript
  const { notes } = meta
  const revNote = notes.find(n => n.notesType === 'selectedReviewer')
  const reviewer = revNote && JSON.parse(revNote.content)
  const revTeam = teams.find(team => team.role === 'reviewer')
  if (reviewer && reviewer.id) {
    let team
    let commit = false
    try {
      const teamInput = {
        manuscriptId: manuscript.id,
        manuscriptVersion: manuscript.version,
        userId: reviewer.id,
        roleName: 'reviewer',
        updatedBy: userId,
      }
      if (!trx) {
        commit = true
        trx = await transaction.start(TeamAccess.knex())
      }
      if (revTeam) {
        teamInput.id = revTeam.id
        await TeamAccess.update(teamInput, userId, trx)
      } else {
        team = new TeamAccess(teamInput)
        await team.saveWithTrx(trx)
      }
      await Note.delete(revNote.id, userId, trx)
      if (commit) {
        await trx.commit()
      }
    } catch (error) {
      if (trx) {
        await trx.rollback()
      }
      logger.error('Nothing was created: ', error)
      return { reviewer, token: null, error }
    }
  }
  return { reviewer, token: revNote && revNote.id, error: null }
}

const getReviewStatus = async (input, userId, reviewUsers, trx = null) => {
  let { status } = input
  let ebiState = null
  try {
    const reviewMan = await ManuscriptAccess.selectById(input.id, true, trx)
    const manuscript = gManuscript(reviewMan)
    const { id, meta, journal, teams, organization, version } = manuscript
    const {
      title,
      releaseDelay,
      notes,
      unmatchedJournal,
      articleType,
      subjects,
    } = meta
    const revTeam = teams.find(team => team.role === 'reviewer')
    const revUser = revTeam && revTeam.teamMembers && revTeam.teamMembers[0]
    const preprint = organization.name === 'Europe PMC Preprints'
    if (input.status === 'xml-qa') {
      if (preprint) {
        // send preprints for indexing as soon as XML is ready
        status = 'repo-ready'
        // if not a removal or withdrawal, set to index only
        ebiState =
          articleType && articleType !== 'preprint'
            ? 'removal notice - release immediately'
            : 'index only'
      }
    } else if (preprint) {
      // check for auto approval
      const approvalStatus = await Note.getLatestValue(id, 'versionApproval')
      if (approvalStatus === 'approved') {
        // auto-release version
        status = 'repo-ready'
      } else {
        // set preprint reviewer
        const server = journal ? journal.meta.nlmta : unmatchedJournal
        const { reviewer, token, error } = await addReviewer(manuscript, userId)
        if (error) {
          throw new Error(`Set reviewer error: ${error}`)
        }
        const license = notes.find(n => n.notesType === 'openAccess')
        const openAccess = license && license.content
        const showVersion = version > 1 ? Math.round(version) : null
        if (reviewer) {
          // send initial email
          await preprintReviewEmail(
            reviewer,
            { id, title, openAccess, server, version: showVersion, subjects },
            token,
          )
        } else if (revUser && revUser.user && revUser.user.id) {
          // reviewer already exists, check whether correction
          const correction = reviewUsers.some(ru => ru === revUser.user.id)
          const reviewer = await UserAccess.findById(revUser.user.id)
          if (correction) {
            await correctedReviewEmail(reviewer, {
              id,
              title,
              preprint,
              releaseDelay,
            })
            // check for approval setting and send new version email
          } else if (approvalStatus) {
            await preprintVersionEmail(reviewer, { id, title, server })
          } else {
            // user has account and is reviewer, but has not set approval status
            // usually when a version was autoreleased and new version processed
            // but could also be sending the initial email a second time
            const { givenNames, surname } = reviewer
            reviewer.name = { title: reviewer.title, givenNames, surname }
            reviewer.email = reviewer.identities[0].email
            await preprintReviewEmail(
              reviewer,
              { id, title, openAccess, server, version: showVersion, subjects },
              token,
            )
          }
        } else {
          throw new Error('No reviewer note; cannot send new reviewer email')
        }
      }
    } else {
      // send final review emails for author manuscripts
      const correction = reviewUsers.some(ru => ru === revUser.user.id)
      const reviewer = await UserAccess.findById(revUser.user.id)
      // check whether correction
      if (correction) {
        await correctedReviewEmail(reviewer, {
          id,
          title,
          preprint,
          releaseDelay,
        })
      } else {
        // send standard final review email
        await finalReviewEmail(reviewer, { id, title, releaseDelay })
      }
    }
    return { error: null, status, ebiState }
  } catch (error) {
    return { error, status, ebiState }
  }
}

const checkCitation = async originalMan => {
  const idCheck = originalMan['meta,articleIds'] || []
  const hasPmid =
    idCheck.some(aid => aid.pubIdType === 'pmid') &&
    Object.values(originalMan['meta,location']).some(v => !!v) &&
    (originalMan['meta,volume'] || originalMan['meta,issue'])
  const hasPprid =
    idCheck.some(aid => aid.pubIdType === 'pprid') &&
    originalMan['meta,publicationDates']
  const hasCitation =
    originalMan['meta,volume'] &&
    Object.values(originalMan['meta,location']).some(v => !!v) &&
    originalMan['meta,publicationDates'] &&
    originalMan['meta,citerefUrl']
  const citationReady = hasPmid || hasPprid || hasCitation
  if (!idCheck.some(aid => ['pmcid', 'pprid'].includes(aid.pubIdType))) {
    const checkId = idCheck.find(aid => ['pmid', 'doi'].includes(aid.pubIdType))
    if (checkId) {
      const user = await UserAccess.findByEmail('helpdesk@europepmc.org')
      user.email = 'helpdesk@europepmc.org'
      const bearer = token.create(user)
      const headers = new fetch.Headers({ Authorization: `Bearer ${bearer}` })
      const idconv = await fetch(
        `${baseUrl}/idconv?${checkId.pubIdType}=${checkId.id}`,
        { headers },
      )
      const idJson = await idconv.json()
      const [ids] = idJson.records
      Object.keys(ids).forEach(idType => {
        if (['doi', 'pmcid', 'pmid'].includes(idType)) {
          const i = idCheck.findIndex(aid => aid.pubIdType === idType)
          if (i >= 0) {
            idCheck[i].id = ids[idType]
          } else {
            idCheck.push({ pubIdType: idType, id: ids[idType] })
          }
        }
      })
    }
  }
  return { citationReady, idCheck }
}

const triageCheck = async manuscript => {
  let status = null
  let formState = null
  const qaCheck = await Audit.qaCheck(manuscript.id, manuscript.version)
  if (qaCheck) {
    status = 'xml-corrected'
    formState = '🔎 Manuscript version has previously been through XML QA'
  }
  return { status, formState }
}

const Manuscript = {
  all: async user => {
    const Manuscripts = await ManuscriptAccess.selectAll(user)
    return Manuscripts.map(manuscript => gManuscript(manuscript))
  },

  changeClaim: async (manId, userId, unclaim = false) => {
    const originalMan = await ManuscriptAccess.selectById(manId)
    let manuscriptUpdate = {}
    const errors = []
    if (originalMan.claimedBy && unclaim) {
      if (originalMan.claimedBy === userId) {
        manuscriptUpdate = dManuscriptUpdate({ claimedBy: null }, userId)
      } else {
        errors.push({ message: 'You have not claimed this manuscript' })
      }
    } else if (originalMan.claimedBy) {
      errors.push({ message: 'Manuscript already claimed' })
    } else {
      manuscriptUpdate = dManuscriptUpdate({ claimedBy: userId }, userId)
    }
    lodash.assign(originalMan, manuscriptUpdate)
    await originalMan.save()
    const updatedMan = await ManuscriptAccess.selectById(manId, true)
    return {
      manuscript: gManuscript(updatedMan),
      errors,
    }
  },

  checkAge: async (states, days, weekdays, orgId) => {
    const older = await ManuscriptAccess.checkAge(states, days, weekdays, orgId)
    return { alert: older }
  },

  checkDuplicates: async (id, articleIds, title, user) => {
    let manuscripts = []
    if (articleIds && articleIds.length > 0) {
      const idMatches = []
      const matchLists = articleIds.map(aid =>
        ManuscriptAccess.searchArticleIds(aid, user),
      )
      await Promise.all(matchLists)
      matchLists.forEach(list => idMatches.concat(list))
      manuscripts = manuscripts.concat(
        idMatches.reduce((other, each) => {
          if (each.id !== id && !manuscripts.some(m => m.id === each.id)) {
            other.push(each)
          }
          return other
        }, []),
      )
    }
    const titleMatches = await ManuscriptAccess.searchByTitle(title, user)
    manuscripts = manuscripts.concat(
      (titleMatches &&
        titleMatches.reduce((other, each) => {
          if (each.id !== id && !manuscripts.some(m => m.id === each.id)) {
            other.push(each)
          }
          return other
        }, [])) ||
        [],
    )
    return {
      total: manuscripts.length,
      manuscripts: manuscripts.map(manuscript => gManuscript(manuscript)),
    }
  },

  countByStatus: async user => {
    const { states } = config
    const data = await ManuscriptAccess.countByStatus(user)
    const counts = states.map(type => {
      const result = data.find(d => d.status === type)
      return { type, count: (result && result.count) || 0 }
    })
    if (ManuscriptAccess.isAdmin(user)) {
      const deleted = await ManuscriptAccess.countDeleted(user)
      counts.push({ type: 'deleted', count: deleted })
    }
    return counts
  },

  countSetByStatus: async (orgId, subjCheck) => {
    const { states } = config
    const data = await ManuscriptAccess.countSetByStatus(orgId, subjCheck)
    const counts = states.map(type => {
      const result = data.find(d => d.status === type)
      return { type, count: (result && result.count) || 0 }
    })
    return counts
  },

  countPreprintsByPriority: async orgId => {
    const data = await ManuscriptAccess.prioritizePreprints(orgId)
    return data.map(row => ({ type: row.priority, count: row.count }))
  },

  create: async (data, userId, organizationId, trx = null) => {
    await authorization.can(userId, 'create', {
      id: null,
      type: 'Manuscript',
      status: config.manuscript.status.initial,
    })
    let savedMan
    let commit = false
    try {
      if (!trx) {
        commit = true
        trx = await transaction.start(ManuscriptAccess.knex())
      }
      const manuscript = new ManuscriptAccess({
        version: data.version || 0.0,
        organizationId,
        updatedBy: userId,
        status: config.manuscript.status.initial,
      })
      savedMan = await manuscript.saveWithTrx(trx)
      const input = dManuscriptUpdate(data, userId)
      lodash.assign(savedMan, input)
      await savedMan.saveWithTrx(trx)
      const team = new TeamAccess({
        manuscriptId: savedMan.id,
        manuscriptVersion: savedMan.version,
        userId,
        roleName: config.authsome.teams.submitter.name,
        updatedBy: userId,
      })
      await team.saveWithTrx(trx)
      if (commit) {
        await trx.commit()
      }
    } catch (error) {
      if (trx) {
        await trx.rollback()
      }
      logger.error('Nothing was created: ', error)
      throw error
    }
    return savedMan
  },

  delete: async (id, userId) => {
    try {
      const manuscript = await ManuscriptAccess.selectById(id)
      const teams = await TeamAccess.selectByManuscriptId(id)
      const subTeam = teams.find(t => t.roleName === 'submitter')
      if (userId === subTeam.userId) {
        const [submitter] = subTeam.users
        const emails = submitter ? [submitter.identities[0].email] : []
        const manInfo = { id, title: manuscript['meta,title'] }
        const revTeam = teams.find(t => t.roleName === 'reviewer')
        let reviewer = null
        if (revTeam && revTeam.userId !== subTeam.userId) {
          ;[reviewer] = revTeam.user
          if (reviewer) {
            emails.push(reviewer.identities[0].email)
          }
        } else {
          const notes = await Note.findByManuscriptId(id)
          const reviewerNote = notes.find(
            n => n.notesType === 'selectedReviewer',
          )
          reviewer = reviewerNote && JSON.parse(reviewerNote.content)
          if (reviewer && (!reviewer.id || reviewer.id !== subTeam.id)) {
            emails.push(reviewer.email)
          }
        }
        if (reviewer) {
          logger.info('Sending deleted notification email')
          await deletedEmail(emails, manInfo)
        }
      }
      if (manuscript.status === 'tagging') {
        await deletedTaggerEmail(id)
      }
      if (manuscript) {
        await ManuscriptAccess.delete(id, userId)
        return true
      }
      return false
    } catch (error) {
      logger.error(`Nothing was deleted: ${error}`)
      return false
    }
  },

  findByArticleId: async (id, userId, ftp = false, trx) => {
    let manuscript = null
    if (id.toUpperCase().startsWith('EMS')) {
      manuscript = await ManuscriptAccess.selectById(id.toUpperCase(), trx)
    } else {
      const searchId =
        (id.includes('doi.org/') && id.split('doi.org/').pop()) || id
      const manuscripts = await ManuscriptAccess.searchArticleIds(
        searchId,
        userId,
        ftp,
        trx,
      )
      manuscript = manuscripts.pop()
    }
    if (manuscript && !ftp) {
      try {
        await authorization.can(userId, 'read', {
          id: manuscript.id,
          type: 'Manuscript',
          status: manuscript.status,
          deleted: manuscript.deleted,
        })
      } catch (error) {
        logger.error(error)
        return {
          manuscript: null,
          errors: [
            {
              message: `The manuscript with ID ${id} has been removed from processing. Please contact the helpdesk with any questions.`,
            },
          ],
        }
      }
    }
    return {
      manuscript,
      errors: manuscript
        ? null
        : [
            {
              message: `No manuscript found with ID: ${id}. Please ensure EMSIDs begin with 'EMS' and PMCIDs begin with 'PMC'`,
            },
          ],
    }
  },

  findByDepositStates: async depositStates => {
    const manuscripts = await ManuscriptAccess.selectByPdfDepositStates(
      depositStates,
    )
    return manuscripts.map(m => gManuscript(m))
  },

  findById: async (id, userId, trx, eager = true) => {
    const manuscript = await ManuscriptAccess.selectById(id, eager, trx)
    if (manuscript) {
      await authorization.can(userId, 'read', {
        id,
        type: 'Manuscript',
        status: manuscript.status,
        deleted: manuscript.deleted,
      })
      return gManuscript(manuscript)
    }
    throw new Error(`Manuscript ${id} does not exist`)
  },

  findByPriority: async (orgId, int, page, pageSize) => {
    const res = await ManuscriptAccess.getPriorityList(
      orgId,
      int,
      page,
      pageSize,
    )
    const total = res.total || res.total === 0 ? res.total : res.length
    const manuscripts = res.results ? res.results : res
    return {
      total,
      manuscripts: manuscripts.map(manuscript => gManuscript(manuscript)),
    }
  },

  findByStatus: async (query, page, pageSize, user, claimedBy, orgId) => {
    const res =
      query[0] === 'deleted'
        ? await ManuscriptAccess.getDeleted(page, pageSize, user)
        : await ManuscriptAccess.selectByStatus(
            query,
            page,
            pageSize,
            user,
            claimedBy,
            orgId,
          )

    const total = res.total || res.total === 0 ? res.total : res.length
    const manuscripts = res.results ? res.results : res
    return {
      total,
      manuscripts: manuscripts.map(manuscript => gManuscript(manuscript)),
    }
  },

  findNeedsCitation: async (page, pageSize) => {
    const res = await ManuscriptAccess.getNeedsCitation(page, pageSize)
    const total = res.total || res.total === 0 ? res.total : res.length
    const manuscripts = res.results ? res.results : res
    return {
      total,
      manuscripts: manuscripts.map(manuscript => gManuscript(manuscript)),
    }
  },

  findDeletionReady: async () => {
    const manuscripts = await ManuscriptAccess.getDeletionReady()
    return manuscripts.map(manuscript => gManuscript(manuscript))
  },

  findIncomplete: ManuscriptAccess.getIncomplete,
  findNcbiReady: ManuscriptAccess.getNcbiReady,
  findNeedFix: ManuscriptAccess.getNeedFix,
  findPublishCheckReady: ManuscriptAccess.getPublishCheckReady,
  findStalled: ManuscriptAccess.getStalled,

  getManuscriptVersions: async (idList, trx) => {
    const manuscripts = await ManuscriptAccess.getVersions(idList, trx)
    return {
      total: (manuscripts && manuscripts.length) || 0,
      manuscripts,
    }
  },

  linkExistingEmail: async (id, userId) => {
    try {
      const manuscript = await ManuscriptAccess.selectById(id)
      const teams = await TeamAccess.selectByManuscriptId(id)
      const emailTeam =
        teams.find(t => t.roleName === 'reviewer') ||
        teams.find(t => t.roleName === 'submitter')
      const [emailPerson] = emailTeam.users
      await alreadyCompleteEmail(emailPerson, manuscript)
      return {
        success: true,
      }
    } catch (error) {
      logger.error(`Problem sending already in PMC email: ${error}`)
      return {
        success: false,
        message: `Problem sending already in PMC email: ${error}`,
      }
    }
  },

  recover: async (id, userId) => {
    const manuscript = await ManuscriptAccess.selectById(id)
    if (manuscript && manuscript.deleted) {
      lodash.assign(manuscript, { deleted: null, updatedBy: userId })
      await manuscript.save()
      return true
    }
    logger.error('Nothing to recover')
    return false
  },

  reject: async (input, userId) => {
    const originalMan = await ManuscriptAccess.selectById(input.manuscriptId)
    const teams = await TeamAccess.selectByManuscriptId(input.manuscriptId)
    const subTeam = teams.find(t => t.roleName === 'submitter')
    const revTeam = teams.find(t => t.roleName === 'reviewer')
    const [submitter] = subTeam.users
    const [reviewer] = revTeam.users
    const email = JSON.parse(input.content)
    const errors = []
    try {
      const note = Object.assign({}, input)
      if (!email.to) {
        note.content = JSON.stringify({
          to: [submitter.id],
          subject: 'Submission rejected',
          message: email,
        })
      }
      await Note.create(note, userId)
      const manUpdate = {
        claimedBy: null,
        formState: email.message ? email.message : email,
        status: 'submission-error',
        updatedBy: userId,
      }
      lodash.assign(originalMan, manUpdate)
      await originalMan.save()
    } catch (error) {
      logger.error(error)
      errors.push(error)
      return {
        manuscript: null,
        errors,
      }
    }
    if (email.to) {
      const uniqueTo = [...new Set(email.to)]
      const sendTo = uniqueTo.map(
        u =>
          (submitter.id === u && submitter.identities[0].email) ||
          (reviewer.id === u && reviewer.identities[0].email),
      )
      await userMessage(sendTo, email.subject, email.message)
    } else {
      await submitterRejectEmail({
        reviewer,
        manInfo: {
          id: originalMan.id,
          title: originalMan['meta,title'],
        },
        submitter,
        message: email,
      })
    }
    const updatedMan = await ManuscriptAccess.selectById(originalMan.id, true)
    return {
      manuscript: gManuscript(updatedMan),
      errors,
    }
  },

  replace: async (keepId, throwId, userId) => {
    const keepMan = await ManuscriptAccess.selectById(keepId)
    const throwMan = await ManuscriptAccess.selectById(throwId, true)
    const teams = await TeamAccess.selectByManuscriptId(throwId)
    const emailTeam =
      teams.find(t => t.roleName === 'reviewer') ||
      teams.find(t => t.roleName === 'submitter')
    const [user] = emailTeam.users
    const keepFunds = keepMan['meta,fundingGroup'] || []
    const throwFunds = throwMan['meta,fundingGroup'] || []
    const newFunds = keepFunds.concat(
      throwFunds.reduce((combo, t) => {
        if (
          !keepFunds.some(
            k =>
              k.pi.email === t.pi.email &&
              k.awardId === t.awardId &&
              k.fundingSource === t.fundingSource,
          )
        ) {
          combo.push(t)
        }
        return combo
      }, []),
    )
    const input = {
      'meta,fundingGroup': newFunds,
      updatedBy: userId,
    }
    const { send, added, removed } = await grantCheck(keepMan, input)
    lodash.assign(keepMan, input)
    try {
      await keepMan.save()
      if (send) {
        keepMan.teams = await TeamAccess.selectByManuscriptId(keepId)
        await createGrantLinks([keepMan], removed, added)
      }
      await ManuscriptAccess.delete(throwId, userId)
      await removeDuplicateEmail(user, throwMan, keepMan)
      return true
    } catch (error) {
      logger.error(error)
      return false
    }
  },

  retag: async (input, userId, trx) => {
    let commit = false
    if (!trx) {
      commit = true
      trx = await transaction.start(ManuscriptAccess.knex())
    }
    const originalMan = await ManuscriptAccess.selectAllEagerById(
      input.manuscriptId,
      trx,
    )
    const email = JSON.parse(input.content)
    const errors = []
    try {
      await Review.deleteByManuscriptId(input.manuscriptId, userId, trx)
      await createPackageForTaggers(gManuscript(originalMan))
      await Note.create(input, userId, trx)
      await taggerErrorEmail(
        `${input.manuscriptId}: ${email.subject}`,
        email.message,
      )
      delete originalMan.organization
      delete originalMan.notes
      delete originalMan.journal
      delete originalMan.files
      delete originalMan.teams
      delete originalMan.audits
      delete originalMan.claiming
      const manUpdate = {
        claimedBy: null,
        status: 'tagging',
        updatedBy: userId,
        formState: null,
      }
      lodash.assign(originalMan, manUpdate)
      await originalMan.saveWithTrx(trx)
      if (commit) {
        await trx.commit()
      }
    } catch (error) {
      if (trx) {
        await trx.rollback()
      }
      logger.error(error)
      errors.push(error)
    }
    const updatedMan = await ManuscriptAccess.selectById(
      input.manuscriptId,
      true,
    )
    return {
      manuscript: gManuscript(updatedMan),
      errors,
    }
  },

  review: async (input, userId, trx) => {
    if (!trx) {
      trx = await transaction.start(ManuscriptAccess.knex())
    }
    const originalMan = await ManuscriptAccess.selectById(input.id, false, trx)
    const errors = []
    if (!originalMan) {
      throw new Error('Manuscript not found')
    }
    if (input.status === 'xml-complete') {
      input.ncbiState = null
      input.formState = null
      const { idCheck, citationReady } = await checkCitation(originalMan)
      input.meta = input.meta || {}
      input.meta.articleIds = idCheck
      if (citationReady) {
        input.status = 'repo-ready'
      } else if (
        idCheck &&
        idCheck.some(aid => aid.pubIdType === 'pmid') &&
        originalMan.fundingState !== 'Grants linked'
      ) {
        input.fundingState = 'Linking grants'
      }
    }
    try {
      if (['xml-review', 'xml-qa'].includes(input.status)) {
        await Review.deleteByManuscriptId(input.id, userId, trx)
        const allReviews = await Review.getDeleted(input.id, trx)
        const allReviewUsers = allReviews.map(r => r.userId)
        const { error, status, ebiState } = await getReviewStatus(
          input,
          userId,
          allReviewUsers,
          trx,
        )
        if (error) {
          throw new Error(error)
        }
        input.status = status
        input.ebiState = ebiState
        if (input.status === 'xml-qa') {
          const { status: newStat, formState } = await triageCheck(originalMan)
          input.status = newStat || input.status
          input.formState = formState
        }
      }
      if (input.status && input.status !== originalMan.status) {
        input.claimedBy = null
      }
      const manuscriptUpdate = dManuscriptUpdate(input, userId)
      lodash.assign(originalMan, manuscriptUpdate)
      await originalMan.saveWithTrx(trx)
      await trx.commit()
    } catch (error) {
      if (trx) await trx.rollback()
      logger.error(error)
      errors.push(error)
      return {
        manuscript: null,
        errors,
      }
    }
    const updatedMan = await ManuscriptAccess.selectById(input.id, true)
    return {
      manuscript: gManuscript(updatedMan),
      errors,
    }
  },

  search: async (query, page, pageSize, user) => {
    const manuscripts = await ManuscriptAccess.searchByTitleOrUser(
      query,
      page,
      pageSize,
      user,
    )
    logger.debug('manuscripts: ', manuscripts)
    return {
      total: manuscripts.total,
      manuscripts: manuscripts.results.map(manuscript =>
        gManuscript(manuscript),
      ),
    }
  },

  selectActivityById: async id => {
    const manuscript = await ManuscriptAccess.selectActivityById(id)
    const gMan = gManuscript(manuscript)
    return gMan
  },

  submit: async (input, userId, trx = null) => {
    const errors = []
    let sendForTagging = false
    if (!trx) {
      trx = await transaction.start(ManuscriptAccess.knex())
    }
    const originalMan = await ManuscriptAccess.selectById(input.id, true, trx)
    const manuscript = gManuscript(originalMan)
    try {
      if (!originalMan) {
        throw new Error('Manuscript not found')
      }
      if (input.status === 'tagging') {
        input.formState = null
      } else if (input.status === 'xml-triage') {
        input.formState =
          '🔎 Returned from submitter/reviewer after missing materials reported. Check activity page for details.'
      } else if (input.status === 'submitted') {
        if (manuscript.organization.name === 'Europe PMC Preprints') {
          const { journal } = manuscript
          const { unmatchedJournal } = manuscript.meta
          const server = journal ? journal.meta.nlmta : unmatchedJournal
          const { value: approvedServers } = await PropManager.find(
            'approvedServers',
          )
          if (
            approvedServers
              .map(str => str.toLowerCase())
              .includes(server.toLowerCase())
          ) {
            logger.info('Preprint from approved source. Sending to taggers.')
            sendForTagging = true
          } else if (!manuscript.files.some(f => f.type === 'supplement')) {
            const content = await File.getManuscriptText(input.id, trx)
            const sups = content.match(
              /(supp(?!lemented|lementing|lementation|ort[\W]|ort(?!ing)|orting [^i]|ress|ly|li|er|ose)[\w.]*\s*[\w.]*|extended data|append[\w.]*)\s*(?!and)\w*(,?\s*(\d+|\w\d+|\w))*((?!(\s*and\s*|-|\u2013|\u2014|&)supp|extended|fig|table)(\s*and\s*|-|\u2013|\u2014|&)\w*)?/gi,
            )
            const matches = [
              ...new Set(
                sups && sups.map(match => match.replace(/\s\s+/g, ' ')),
              ),
            ]
            if (matches.length <= 0) {
              logger.info('Preprint with no supplements. Sending to taggers.')
              sendForTagging = true
            }
          }
        } else {
          let { reviewer, token, error } = await addReviewer(
            manuscript,
            userId,
            trx,
          )
          const { teams } = manuscript
          const subTeam = teams.find(team => team.role === 'submitter')
          const revTeam = teams.find(team => team.role === 'reviewer')
          const submitter =
            subTeam && subTeam.teamMembers && subTeam.teamMembers[0]
          const revUser =
            revTeam && revTeam.teamMembers && revTeam.teamMembers[0]
          let inReview = false
          if (error) {
            throw new Error(error)
          }
          if (reviewer) {
            inReview = true
            if (reviewer.id && reviewer.id === submitter.user.id) {
              inReview = false
            }
          } else if (
            manuscript.status === 'submission-error' &&
            revUser &&
            revUser.user.id !== submitter.user.id &&
            revUser.user.id !== userId
          ) {
            const { email, name } = revUser.alias
            inReview = true
            reviewer = {
              id: revUser.user.id,
              name,
              email,
            }
            token = 'correction'
          } else if (!revTeam) {
            error = 'Reviewer must be selected'
            throw new Error(error)
          }
          if (inReview) {
            input.status = 'in-review'
            await reviewerEmail({
              reviewer,
              manInfo: {
                id: input.id,
                title: originalMan['meta,title'],
              },
              submitter: submitter && submitter.alias.name,
              token,
            })
          }
        }
      }
      input.claimedBy = null
      delete originalMan.organization
      delete originalMan.notes
      delete originalMan.journal
      delete originalMan.files
      delete originalMan.teams
      delete originalMan.audits
      delete originalMan.claiming
      const manuscriptUpdate = dManuscriptUpdate(input, userId)
      lodash.assign(originalMan, manuscriptUpdate)
      await originalMan.saveWithTrx(trx)
      // Make sure manuscript passes through 'submitted', for audit logs.
      if (input.status === 'tagging') {
        const allIncluded = await ManuscriptAccess.selectAllEagerById(
          input.id,
          trx,
        )
        await createPackageForTaggers(gManuscript(allIncluded))
      }
      await trx.commit()
      trx = await transaction.start(ManuscriptAccess.knex())
      if (sendForTagging) {
        const allIncluded = await ManuscriptAccess.selectAllEagerById(
          input.id,
          trx,
        )
        await createPackageForTaggers(gManuscript(allIncluded))
        input.formState = null
        input.status = 'tagging'
        const manuscriptUpdate = dManuscriptUpdate(input, userId)
        lodash.assign(originalMan, manuscriptUpdate)
        await originalMan.saveWithTrx(trx)
      }
      await trx.commit()
    } catch (error) {
      if (trx) await trx.rollback()
      logger.error(error)
      errors.push({ message: error.message })
      return {
        manuscript: null,
        errors,
      }
    }
    const updatedMan = await ManuscriptAccess.selectById(input.id, true)
    return {
      manuscript: gManuscript(updatedMan),
      errors,
    }
  },

  setReviewer: async (id, userId) => {
    const getMan = await ManuscriptAccess.selectById(id, true)
    const manuscript = gManuscript(getMan)
    const { meta, status, teams } = manuscript
    const { title } = meta
    const manInfo = {
      id,
      title,
      status,
    }
    const sTeam = teams.find(team => team.role === 'submitter')
    const { reviewer, token, error } = await addReviewer(manuscript, userId)
    if (error) {
      logger.error(`Set reviewer error: ${error}`)
      return false
    }
    if (reviewer) {
      const submitter =
        sTeam &&
        sTeam.teamMembers[0] &&
        (sTeam.teamMembers[0].user.id !== reviewer.id || !reviewer.id) &&
        sTeam.teamMembers[0].alias.name
      await finalReviewerEmail({
        reviewer,
        manInfo,
        submitter,
        token,
      })
      return true
    }
    logger.error('No reviewer note; cannot send new reviewer email')
    return false
  },

  update: async (input, userId, trx = null) => {
    let commit = false
    if (!trx) {
      commit = true
      trx = await transaction.start(ManuscriptAccess.knex())
    }
    try {
      const originalMan = await ManuscriptAccess.selectById(
        input.id,
        false,
        trx,
      )
      if (!originalMan) {
        throw new Error('Manuscript not found')
      }
      if (input.status !== originalMan.status) {
        input.meta = input.meta || {}
        if (input.status === 'repo-ready') {
          originalMan['meta,articleIds'] =
            input.meta.articleIds || originalMan['meta,articleIds']
          const { idCheck, citationReady } = await checkCitation(originalMan)
          input.meta.articleIds = idCheck
          if (!citationReady) {
            input.status = 'xml-complete'
            input.formState =
              'Cannot set to repo-ready. Manuscript citation is not complete!'
          }
        } else if (input.status === 'repo-processing') {
          input.ncbiState = null
        } else if (input.status === 'xml-qa') {
          const { status, formState } = await triageCheck(originalMan)
          input.status = status || input.status
          input.formState = formState
        } else if (
          input.status === 'link-existing' &&
          originalMan['meta,articleIds'] &&
          originalMan['meta,articleIds'].some(
            aid => aid.pubIdType === 'pmid',
          ) &&
          originalMan.fundingState !== 'Grants linked'
        ) {
          input.fundingState = 'Linking grants'
        } else if (
          originalMan.status === 'xml-complete' &&
          input.meta.articleIds &&
          input.meta.articleIds.some(aid => aid.pubIdType === 'pmid') &&
          originalMan.fundingState !== 'Grants linked'
        ) {
          input.fundingState = 'Linking grants'
        }
      }
      const manuscriptUpdate = dManuscriptUpdate(input, userId)
      const { send, added, removed } = await grantCheck(
        originalMan,
        manuscriptUpdate,
      )
      lodash.assign(originalMan, manuscriptUpdate)
      await originalMan.saveWithTrx(trx)
      const updatedMan = await ManuscriptAccess.selectById(input.id, true, trx)
      const manuscript = gManuscript(updatedMan)
      if (send) {
        updatedMan.teams = await TeamAccess.selectByManuscriptId(input.id)
        await createGrantLinks([updatedMan], removed, added)
      }
      if (commit) {
        await trx.commit()
      }
      return manuscript
    } catch (error) {
      if (trx) {
        await trx.rollback()
      }
      logger.error('Nothing was updated: ', error)
      return false
    }
  },

  modelName: 'Manuscript',

  model: ManuscriptAccess,
}

module.exports = Manuscript
