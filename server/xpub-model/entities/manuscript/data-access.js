const { transaction, Model, raw } = require('objection')
const logger = require('@pubsweet/logger')
const config = require('config')
const EpmcBaseModel = require('../epmc-base-model')
const BaseModel = require('@pubsweet/base-model')

const knex = BaseModel.knex()

const PAGE_SIZE = config.pageSize || 50

class Manuscript extends EpmcBaseModel {
  static get tableName() {
    return 'xpub.manuscript'
  }

  static get schema() {
    return {
      properties: {
        id: { type: 'string', format: 'regex', pattern: 'EMS\\d+' },
        organizationId: { type: 'uuid' },
        journalId: { type: 'uuid' },
        created: { type: 'timestamp' },
        updated: { type: 'timestamp' },
        deleted: { type: 'timestamp' },
        updatedBy: { type: 'uuid' },
        claimedBy: { type: ['string', 'null'] },
        version: { type: 'numeric' },
        status: { type: ['string', 'null'] },
        formState: { type: ['string', 'null'] },
        decision: { type: ['string', 'null'] },
        pdfDepositId: { type: 'uuid' },
        pdfDepositState: { type: ['string', 'null'] },
        retryAttempt: { type: ['numeric'] },
        ncbiState: { type: ['string', 'null'] },
        ebiState: { type: ['string', 'null'] },
        fundingState: { type: ['string', 'null'] },
        'meta,title': { type: ['string', 'null'] },
        'meta,articleType': { type: ['string', 'null'] },
        'meta,articleIds': {
          type: ['array', 'null'],
          items: {
            type: 'object',
            properties: {
              pubIdType: { type: 'string' },
              id: { type: 'string' },
            },
          },
        },
        'meta,abstract': { type: ['string', 'null'] },
        'meta,subjects': {
          type: ['array', 'null'],
          items: { type: ['string', 'null'] },
        },
        'meta,publicationDates': {
          type: ['array', 'null'],
          items: {
            type: 'object',
            properties: {
              type: { type: 'string' },
              date: { type: 'string' },
              jatsDate: {
                type: 'object',
                properties: {
                  year: { type: 'string' },
                  month: { type: 'string' },
                  day: { type: 'string' },
                  season: { type: 'string' },
                },
              },
            },
          },
        },
        'meta,notes': {
          type: ['array', 'null'],
          items: {
            type: ['object', 'null'],
            properties: {
              id: { type: 'string' },
              content: { type: 'string' },
              notesType: { type: 'string' },
            },
          },
        },
        'meta,volume': { type: ['string', 'null'] },
        'meta,issue': { type: ['string', 'null'] },
        'meta,location': {
          type: ['object', 'null'],
          properties: {
            fpage: { type: ['string', 'null'] },
            lpage: { type: ['string', 'null'] },
            elocationId: { type: ['string', 'null'] },
          },
        },
        'meta,fundingGroup': {
          type: ['array', 'null'],
          items: {
            type: 'object',
            properties: {
              fundingSource: { type: 'string' },
              awardId: { type: 'string' },
              title: { type: ['string', 'null'] },
              categories: {
                type: ['array', 'null'],
                items: {
                  type: ['string', 'null'],
                },
              },
              pi: {
                type: 'object',
                properties: {
                  surname: { type: 'string' },
                  givenNames: { type: ['string', 'null'] },
                  title: { type: ['string', 'null'] },
                  email: { type: 'string' },
                },
              },
              startDate: { type: ['string', 'null'], format: 'date-time' },
              dateChecked: { type: ['boolean', 'null'] },
            },
          },
        },
        'meta,releaseDelay': { type: ['string', 'null'] },
        'meta,unmatchedJournal': { type: ['string', 'null'] },
        'meta,citerefUrl': { type: ['string', 'null'] },
        'meta,fulltextUrl': { type: ['string', 'null'] },
        teams: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              role: { type: 'string' },
              teamMembers: { type: 'array' },
            },
          },
        },
        files: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'string' },
              type: { type: 'string' },
              label: { type: 'string' },
              filename: { type: 'string' },
              url: { type: 'string' },
              mimeType: { type: 'string' },
            },
          },
        },
      },
    }
  }

  static get jsonAttributes() {
    return ['meta,articleIds', 'meta,publicationDates', 'meta,fundingGroup']
  }

  static get relationMappings() {
    const File = require('../file/data-access')
    const Organization = require('../organization/data-access')
    const Note = require('../note/data-access')
    const Journal = require('../journal/data-access')
    const Review = require('../review/data-access')
    const Team = require('../team/data-access')
    const User = require('../user/data-access')
    const { Audit } = require('../audit/data-access')
    return {
      reviews: {
        relation: Model.HasManyRelation,
        modelClass: Review,
        join: {
          from: ['xpub.manuscript.id', 'xpub.manuscript.version'],
          to: ['xpub.review.manuscriptId', 'xpub.review.manuscriptVersion'],
        },
      },
      files: {
        relation: Model.HasManyRelation,
        modelClass: File,
        join: {
          from: ['xpub.manuscript.id', 'xpub.manuscript.version'],
          to: ['xpub.file.manuscriptId', 'xpub.file.manuscriptVersion'],
        },
      },
      notes: {
        relation: Model.HasManyRelation,
        modelClass: Note,
        join: {
          from: 'xpub.manuscript.id',
          to: 'xpub.note.manuscriptId',
        },
      },
      journal: {
        relation: Model.BelongsToOneRelation,
        modelClass: Journal,
        join: {
          from: 'xpub.manuscript.journalId',
          to: 'xpub.journal.id',
        },
      },
      organization: {
        relation: Model.BelongsToOneRelation,
        modelClass: Organization,
        join: {
          from: 'xpub.manuscript.organizationId',
          to: 'xpub.organization.id',
        },
      },
      teams: {
        relation: Model.HasManyRelation,
        modelClass: Team,
        join: {
          from: 'xpub.manuscript.id',
          to: 'xpub.team.manuscriptId',
        },
      },
      claiming: {
        relation: Model.HasOneRelation,
        modelClass: User,
        join: {
          from: 'xpub.manuscript.claimedBy',
          to: 'xpub.users.id',
        },
      },
      audits: {
        relation: Model.HasManyRelation,
        modelClass: Audit,
        join: {
          from: 'xpub.manuscript.id',
          to: 'audit.audit_log.manuscriptId',
        },
      },
      users: {
        relation: Model.ManyToManyRelation,
        modelClass: User,
        join: {
          from: 'xpub.manuscript.id',
          // ManyToMany relation needs the `through` object
          // to describe the join table.
          through: {
            modelClass: Team,
            from: 'xpub.team.manuscriptId',
            to: 'xpub.team.userId',
          },
          to: 'xpub.users.id',
        },
      },
    }
  }

  async saveWithUserAndRole(options, userId, roleName) {
    const Team = require('../team/data-access')
    let trx
    let saved
    try {
      trx = await transaction.start(knex)
      saved = await this.saveWithTrx(trx, options)

      await new Team({
        manuscriptId: saved.id,
        userId,
        roleName,
        updatedBy: userId,
      }).saveWithTrx(trx)

      await trx.commit()
    } catch (error) {
      await trx.rollback()
      logger.error('Nothing was inserted')
      logger.error(error)
      throw error
    }

    return saved
  }

  static async selectById(id, eager = false, trx) {
    let manuscripts
    if (eager) {
      // don't whereNull deleted here
      manuscripts = await Manuscript.query(trx)
        .where('id', id)
        .eager('[journal, organization, teams.users, notes, files, claiming]')
        .modifyEager('teams', builder => {
          builder.whereNull('deleted')
        })
        .modifyEager('notes', builder => {
          builder.whereNull('deleted')
        })
        .modifyEager('files', builder => {
          builder.whereNull('deleted')
        })
    } else {
      manuscripts = await Manuscript.query().where('id', id)
    }
    return manuscripts && manuscripts.length > 0 ? manuscripts[0] : null
  }

  static async selectByStatus(
    statuses,
    page = 0,
    pageSize = PAGE_SIZE,
    user,
    claimedBy,
    orgId,
  ) {
    const { isAdmin, manuscriptIds } = await Manuscript.isAdmin(user)
    const manuscripts = await Manuscript.query()
      .with('base_query', q => {
        q.select('*')
          .from({ m: 'xpub.manuscript' })
          .modify(q => {
            if (!isAdmin) {
              q.whereIn('m.id', manuscriptIds)
            } else {
              if (claimedBy) {
                q.select(
                  raw(
                    `CASE WHEN "claimed_by" = '${user}' THEN 1 WHEN "claimed_by" is null THEN 2 ELSE 3 END as claim_group`,
                  ),
                )
              }
              if (orgId) {
                q.where('m.organization_id', '=', orgId)
              }
            }
          })
          .whereIn('m.status', statuses)
          .whereNull('m.deleted')
      })
      .select('*')
      .from(sq => {
        sq.from('base_query')
          .modify(q => {
            if (isAdmin && claimedBy) {
              q.orderBy('claim_group')
            }
          })
          .orderBy('updated', 'desc')
          .as('bq')
      })
      .modify(q => {
        if (claimedBy) {
          q.leftOuterJoin(
            sq => {
              sq.select(raw('count("id"), "claim_group"'))
                .from('base_query')
                .groupBy('claim_group')
                .as('counts')
            },
            join => {
              join.on(raw('bq.claim_group = counts.claim_group'))
            },
          )
        }
      })
      .eager('[teams.users, claiming, journal, organization, notes]')
      .modifyEager('teams', e => {
        e.whereNull('deleted')
      })
      .modifyEager('notes', e => {
        e.whereNull('deleted')
      })
      .modify(q => {
        if (page > -1) {
          q.page(page, pageSize)
        }
      })
    logger.debug('manuscripts: ', manuscripts)
    return manuscripts
  }

  static async getNeedsCitation(page = 0, pageSize = PAGE_SIZE) {
    const manuscripts = await Manuscript.query()
      .with('base_query', q => {
        q.select(
          raw(
            `m.*, CASE WHEN lower(m."meta,article_ids"::text)::jsonb @> lower('[{"pubIdType": "pmid"}]')::jsonb THEN 1 WHEN lower(m."meta,article_ids"::text)::jsonb @> lower('[{"pubIdType": "doi"}]')::jsonb THEN 2 WHEN j."meta,pubmed_status" IS NOT TRUE THEN 3 ELSE 4 END as cite_group`,
          ),
        )
          .from({ m: 'xpub.manuscript' })
          .leftJoin({ j: 'xpub.journal' }, 'm.journal_id', 'j.id')
          .where('m.status', 'xml-complete')
          .whereNull('m.deleted')
          .orderBy('m.updated', 'desc')
      })
      .select('*')
      .from(bq =>
        bq
          .select('*')
          .from('base_query')
          .orderBy([
            { column: 'cite_group', order: 'desc' },
            { column: 'updated', order: 'desc' },
          ])
          .as('bq'),
      )
      .orderBy([
        { column: 'bq.cite_group', order: 'desc' },
        { column: 'bq.updated', order: 'desc' },
      ])
      .leftOuterJoin(
        sq => {
          sq.select(raw('count("id"), "cite_group"'))
            .from('base_query')
            .groupBy('cite_group')
            .as('counts')
        },
        join => join.on(raw('bq.cite_group = counts.cite_group')),
      )
      .eager('[teams.users, journal, organization, notes]')
      .modifyEager('teams', e => {
        e.whereNull('deleted')
      })
      .modifyEager('notes', e => {
        e.whereNull('deleted')
      })
      .modify(q => {
        if (page > -1) {
          q.page(page, pageSize)
        }
      })
    logger.debug('manuscripts: ', manuscripts)
    return manuscripts
  }

  static async countByStatus(user) {
    const { isAdmin, manuscriptIds } = await Manuscript.isAdmin(user)
    return Manuscript.query()
      .select('status')
      .count('*')
      .modify(q => {
        if (!isAdmin) {
          q.whereIn('id', manuscriptIds)
        }
      })
      .whereNull('deleted')
      .groupBy('status')
  }

  static async countSetByStatus(orgId, subjCheck) {
    return Manuscript.query()
      .select('status')
      .count('*')
      .from({ m: 'xpub.manuscript' })
      .where('organizationId', '=', orgId)
      .modify(q => {
        if (subjCheck) {
          q.where(raw(subjCheck))
        }
      })
      .whereNull('deleted')
      .groupBy('status')
  }

  static async prioritizePreprints(orgId) {
    return Manuscript.query()
      .with('base', q => {
        q.select(
          raw(`m.id, CASE WHEN exists(
        select * from audit.manuscript_process_dates d
        where d.tagged_date is not null
        and d.manuscript_id=m.id and d.manuscript_version=m.version
      ) THEN 1 ELSE 0 END as corr,
      CASE WHEN exists (
        select * from xpub.note n
        where n.notes_type = 'openAccess'
        and n.manuscript_id=m.id
        and n.deleted is null
      ) THEN 1 ELSE 0 END as oa,
      CASE WHEN exists (
        select * from audit.manuscript_process_dates d
        where d.first_published_date is not null
        and d.manuscript_id=m.id
      ) THEN 1 ELSE 0 END as aa`),
        )
          .from({ m: 'xpub.manuscript' })
          .where('m.status', '=', 'tagging')
          .where('m.organizationId', '=', orgId)
          .whereNull('m.deleted')
      })
      .select(
        raw(`count(id), CASE 
      WHEN corr = 1 THEN 0
      WHEN oa = 1 and aa = 0 THEN 1
      WHEN oa = 1 and aa = 1 THEN 2
      WHEN oa = 0 and aa = 1 THEN 3
      ELSE 4 END as priority`),
      )
      .from('base')
      .groupBy('priority')
      .orderBy('priority', 'asc')
  }

  static async getPriorityList(orgId, int, page = 0, pageSize = PAGE_SIZE) {
    return Manuscript.query()
      .alias('m')
      .select('m.id', 'm.version', 'm.updated')
      .join({ d: 'audit.manuscript_process_dates' }, j => {
        j.on('m.id', 'd.manuscriptId').on('m.version', 'd.manuscriptVersion')
      })
      .modify(q => {
        const oa = raw(`select * from xpub.note n
          where n.notes_type = 'openAccess'
          and n.manuscript_id=m.id
          and n.deleted is null`)
        const aa = raw(`select * from audit.manuscript_process_dates ad
          where ad.first_published_date is not null
          and ad.manuscript_id=m.id`)
        if (int === 0) {
          q.whereNotNull('d.tagged_date')
        } else {
          q.whereNull('d.tagged_date')
          switch (int) {
            case 1:
              q.whereExists(oa).whereNotExists(aa)
              break
            case 2:
              q.whereExists(oa).whereExists(aa)
              break
            case 3:
              q.whereNotExists(oa).whereExists(aa)
              break
            default:
              q.whereNotExists(oa).whereNotExists(aa)
          }
        }
      })
      .where('m.status', '=', 'tagging')
      .where('m.organizationId', '=', orgId)
      .whereNull('m.deleted')
      .orderBy('m.updated', 'asc')
      .page(page, pageSize)
  }

  static async checkAge(statuses, days, weekdays, orgId) {
    const alert =
      (await Manuscript.query()
        .with('diffs', q => {
          q.select(
            raw(
              `id, generate_series(updated, current_date - '1 day'::interval, '1 day'::interval) as date_range`,
            ),
          )
            .from('xpub.manuscript')
            .where('organizationId', '=', orgId)
            .whereIn('status', statuses)
            .whereNull('deleted')
        })
        .with('all_diffs', q => {
          if (weekdays) {
            q.select(
              raw(
                `id, sum(case when extract ('ISODOW' from date_range) < 6 then 1 else 0 end) as diff`,
              ),
            )
              .from('diffs')
              .groupBy('id')
          } else {
            q.select(
              raw(
                `id, sum(case when extract ('ISODOW' from date_range) < 8 then 1 else 0 end) as diff`,
              ),
            )
              .from('diffs')
              .groupBy('id')
          }
        })
        .count('id')
        .from('all_diffs')
        .where('diff', '>', days)) || '0'
    return parseInt(alert[0].count, 10) > 0
  }

  static async countDeleted(user) {
    const { isAdmin } = await Manuscript.isAdmin(user)

    const count = isAdmin
      ? await Manuscript.query()
          .count('*')
          .whereNotNull('deleted')
      : [{ count: 0 }]

    return parseInt(count[0].count, 10)
  }

  static async getDeleted(page = 0, pageSize = PAGE_SIZE, user) {
    const manuscripts = await Manuscript.query()
      .distinct('id')
      .select('*')
      .eager('[teams.users, claiming, journal, organization]')
      .modifyEager('teams', builder => {
        builder.whereNull('deleted')
      })
      .whereNotNull('deleted')
      .orderBy('updated', 'desc')
      .modify(q => {
        if (page > -1) {
          q.page(page, pageSize)
        }
      })
    logger.debug('manuscripts: ', manuscripts)
    return manuscripts
  }

  static async selectActivityById(id) {
    const manuscripts = await Manuscript.query()
      .where('id', id)
      .eager(
        '[journal, organization, notes, claiming, teams.users, audits.user.identities]',
      )
      .modifyEager('notes', builder => {
        builder.whereNull('deleted')
      })
      .modifyEager('teams', builder => {
        builder.whereNull('deleted')
      })
      .modifyEager('audits', q => {
        q.orderBy('audit_log.created', 'asc')
      })
    return manuscripts && manuscripts.length > 0 ? manuscripts[0] : null
  }

  static async selectAllEagerById(id, trx) {
    const manuscripts = await Manuscript.query(trx)
      .where('id', id)
      .whereNull('deleted')
      .eager(
        '[journal, organization, teams.users, notes, files, audits, claiming]',
      )
      .modifyEager('teams', builder => {
        builder.whereNull('deleted')
      })
      .modifyEager('notes', builder => {
        builder.whereNull('deleted')
      })
      .modifyEager('files', builder => {
        builder.whereNull('deleted')
      })
      .modifyEager('audits', q => {
        q.orderBy('audit_log.created', 'asc')
      })
    return manuscripts && manuscripts.length > 0 ? manuscripts[0] : null
  }

  static async getVersions(idList, trx) {
    return Manuscript.query(trx)
      .select('id', 'version')
      .whereIn('id', idList)
      .whereNull('deleted')
      .orderBy('version')
  }

  static async selectByPdfDepositStates(depositStates) {
    const manuscripts = Manuscript.query()
      .whereIn('pdf_deposit_state', depositStates)
      .whereNull('deleted')
      .eager('[files, notes, journal]')
      .modifyEager('files', builder => {
        builder.whereNull('deleted')
      })
      .modifyEager('notes', builder => {
        builder.whereNull('deleted')
      })
    logger.debug('manuscripts: ', manuscripts)
    return manuscripts
  }

  static async selectAll(user) {
    const manuscripts = await Manuscript.query()
      .alias('m')
      .distinct('m.id')
      .select('m.*')
      .leftJoin({ t: 'xpub.team' }, 't.manuscript_id', 'm.id')
      .groupBy('m.id', 't.user_id', 't.role_name')
      .whereNull('t.deleted')
      .where('t.user_id', user)
      .whereNull('m.deleted')
      .orderBy('m.updated', 'desc')
      .eager('[teams.users, journal, organization, notes]')
      .modifyEager('teams', builder => {
        builder.whereNull('deleted')
      })
      .modifyEager('notes', builder => {
        builder.whereNull('deleted')
      })
    return manuscripts
  }

  static async searchArticleIds(id, user, excludeDeleted = true, trx) {
    const { isAdmin } = await Manuscript.isAdmin(user)

    const manuscripts = await Manuscript.query(trx)
      .where(
        raw('lower("meta,article_ids"::text)::jsonb @> lower(?)::jsonb', [
          `[{"id": "${id}"}]`,
        ]),
      )
      .modify(q => {
        if (!isAdmin) {
          // eslint-disable-next-line func-names
          q.whereExists(function() {
            this.select('*')
              .from('xpub.team')
              .whereRaw('xpub.manuscript.id = xpub.team.manuscript_id')
              .where('user_id', user)
          })
        }
        if (excludeDeleted) {
          q.whereNull('deleted')
        }
      })
      .eager('[journal, notes, organization]')
      .modifyEager('notes', builder => {
        builder.whereNull('deleted')
      })

    return manuscripts
  }

  static async searchByTitle(query, user) {
    const { isAdmin, manuscriptIds } = await Manuscript.isAdmin(user)

    const manuscripts = await Manuscript.query()
      .distinct('id')
      .select('*')
      .where(
        raw(`(? ilike concat("meta,title", '%') or "meta,title" ilike ?)`, [
          query,
          `${query}%`,
        ]),
      )
      .whereNull('deleted')
      .modify(q => {
        if (!isAdmin) {
          q.whereIn('id', manuscriptIds)
        }
      })
      .eager('[teams.users, claiming, journal, organization, notes]')
      .modifyEager('teams', builder => {
        builder.whereNull('deleted')
      })
      .modifyEager('notes', builder => {
        builder.whereNull('deleted')
      })
      .orderBy('updated', 'desc')
    return manuscripts
  }

  static async searchByTitleOrUser(
    query,
    page = 0,
    pageSize = PAGE_SIZE,
    user,
  ) {
    const { isAdmin, manuscriptIds } = await Manuscript.isAdmin(user)

    const manuscripts = await Manuscript.query()
      .alias('m')
      .distinct('m.id')
      .select('m.*')
      .modify(q => {
        if (!isAdmin) {
          q.whereIn('m.id', manuscriptIds)
        }
      })
      .whereNull('m.deleted')
      .eager('[teams.users, claiming, journal, organization, notes]')
      .modifyEager('teams', builder => {
        builder.whereNull('deleted')
      })
      .modifyEager('notes', builder => {
        builder.whereNull('deleted')
      })
      .join({ t: 'xpub.team' }, 't.manuscript_id', 'm.id')
      .join({ u: 'xpub.users' }, 't.user_id', 'u.id')
      .join({ i: 'xpub.identity' }, 'i.user_id', 'u.id')
      .where(
        raw(
          `(i."email" ilike ? or u."surname" ilike ? or m."meta,title" ilike ?)`,
          [query, `${query}%`, `${query}%`],
        ),
      )
      .orderBy('m.updated', 'desc')
      .page(page, pageSize)

    return manuscripts
  }

  static async getNcbiReady() {
    const manuscripts = await Manuscript.query()
      .where('status', 'repo-processing')
      .where('ebiState', 'EBI received')
      .whereNull('ncbiState')
      .whereNull('deleted')
      .eager('[journal, files, organization, teams.users.identities]')
    logger.debug('manuscripts: ', manuscripts)
    return manuscripts
  }

  static async getPublishCheckReady() {
    const manuscripts = await Manuscript.query()
      .distinct('id')
      .select('*')
      .eager('[users.[roles, identities], audits]')
      .where('status', 'repo-processing')
      .where('ncbiState', 'success')
      .whereNull('deleted')
      .modifyEager('audits', q => {
        q.whereRaw("changes->>'status' = 'published'")
      })

    const preprints = await Manuscript.query()
      .distinct('id')
      .select('*')
      .eager('[users.[roles, identities], audits]')
      .where('status', 'repo-processing')
      .where('ebiState', 'success')
      .whereNull('deleted')
      .modifyEager('audits', q => {
        q.whereRaw("changes->>'status' = 'published'")
      })

    return { manuscripts, preprints }
  }

  static async getDeletionReady() {
    return Manuscript.query()
      .where('status', 'being-withdrawn')
      .where('ncbiState', 'ncbi-withdraw')
      .whereNull('deleted')
      .eager('[notes]')
      .modifyEager('notes', builder => {
        builder.whereNull('deleted')
      })
  }

  static async getIncomplete() {
    return Manuscript.query()
      .whereIn('status', ['INITIAL', 'READY'])
      .whereRaw(
        '("updated"::date = current_date - interval \'1 day\' or "updated"::date = current_date - interval \'7 days\')',
      )
      .whereNull('deleted')
      .eager('[teams.users.identities]')
      .modifyEager('teams', builder => {
        builder.whereNull('deleted')
      })
  }

  static async getNeedFix() {
    return Manuscript.query()
      .where('status', 'submission-error')
      .whereRaw(
        '("updated"::date = current_date - interval \'1 week\' or "updated"::date = current_date - interval \'2 weeks\')',
      )
      .whereNull('deleted')
      .eager('[notes, teams.users.identities]')
      .modifyEager('teams', builder => {
        builder.whereNull('deleted')
      })
      .modifyEager('notes', builder => {
        builder.whereNull('deleted')
      })
  }

  static async getStalled() {
    return Manuscript.query()
      .whereIn('status', ['in-review', 'xml-review'])
      .whereRaw(
        '("updated"::date = current_date - interval \'1 week\' or "updated"::date = current_date - interval \'2 weeks\')',
      )
      .whereNull('deleted')
      .eager('[notes, teams.users.identities, organization]')
      .modifyEager('teams', builder => {
        builder.whereNull('deleted')
      })
      .modifyEager('notes', builder => {
        builder.whereNull('deleted')
      })
  }

  static async insert(manuscript) {
    const { id } = await Manuscript.query().insertGraph(manuscript)
    const manuscriptInserted = await Manuscript.query()
      .where('id', id)
      .whereNull('deleted')
      .eager()
    return manuscriptInserted[0]
  }

  static async delete(id, userId) {
    return Manuscript.query()
      .patchAndFetchById(id, {
        deleted: new Date().toISOString(),
        updatedBy: userId,
      })
      .returning('*')
  }

  static async isAdmin(user) {
    const Team = require('../team/')
    const roles = await Team.selectByUserId(user)
    const isAdmin = roles.some(r =>
      ['admin', 'external-admin', 'tagger'].includes(r.role),
    )
    const manuscriptIds = roles
      .map(r => r.objectId)
      .filter(objectId => objectId)
    return { isAdmin, manuscriptIds }
  }
}

module.exports = Manuscript
