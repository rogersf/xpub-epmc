const { Model } = require('objection')
const uuid = require('uuid')
const EpmcBaseModel = require('../epmc-base-model')
const { rowToEntity } = require('../util')
const Manuscript = require('../manuscript/data-access')
const Role = require('../role/data-access')
const User = require('../user/data-access')

class Team extends EpmcBaseModel {
  static get tableName() {
    return 'xpub.team'
  }

  static get idColumn() {
    return 'id'
  }

  static get schema() {
    return {
      properties: {
        id: { type: 'uuid' },
        created: { type: 'timestamp' },
        updated: { type: 'timestamp' },
        deleted: { type: 'timestamp' },
        manuscriptId: { anyOf: [{ type: 'string' }, { type: 'null' }] },
        manuscriptVersion: { anyOf: [{ type: 'numeric' }, { type: 'null' }] },
        userId: { type: 'uuid' },
        roleName: { type: 'string' },
        updatedBy: { type: 'uuid' },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscripts: {
        relation: Model.BelongsToOneRelation,
        modelClass: Manuscript,
        join: {
          from: 'xpub.team.manuscriptId',
          to: 'xpub.manuscript.id',
        },
      },
      roles: {
        relation: Model.BelongsToOneRelation,
        modelClass: Role,
        join: {
          from: 'xpub.team.roleName',
          to: 'xpub.role.name',
        },
      },
      users: {
        relation: Model.HasManyRelation,
        modelClass: User,
        join: {
          from: 'xpub.team.userId',
          to: 'xpub.users.id',
        },
      },
    }
  }

  static async selectById(id) {
    const rows = await Team.query()
      .where({ id })
      .whereNull('deleted')
    if (!rows.length) {
      throw new Error('manuscript_team_user not found')
    }
    return rowToEntity(rows[0])
  }

  static async selectByManuscriptId(manId, trx) {
    const manuscriptRoles = await Team.query(trx)
      .where('manuscript_id', manId)
      .whereNull('deleted')
      .eager('users.identities')
    return manuscriptRoles
  }

  static async selectByManIdAndType(manId, type, trx) {
    const rows = await Team.query(trx)
      .where('manuscript_id', manId)
      .where('role_name', type)
      .whereNull('deleted')
    if (rows.length === 0) {
      return null
    }
    if (rows.length > 1) {
      throw new Error(`Manuscript ${manId} has too many ${type}s!`)
    }
    return rowToEntity(rows[0])
  }

  static async selectByUserId(userId, manId) {
    const roles = await Team.query()
      .where('user_id', userId)
      .whereNull('xpub.team.deleted')

    return roles.map(team => ({
      id: team.id,
      created: team.created,
      updated: team.updated,
      role: team.roleName,
      objectId: team.manuscriptId ? team.manuscriptId : '',
      objectType: team.manuscriptId ? 'Manuscript' : 'Organization',
    }))
  }

  static async selectByUserIdOrManuscriptId(userId, manId) {
    let manuscriptRoles = []
    let organizationRoles = []
    if (manId) {
      manuscriptRoles = await Team.query()
        .where('manuscript_id', manId)
        .where('user_id', userId)
        .whereNull('deleted')
    }
    organizationRoles = await Team.query()
      .where('user_id', userId)
      .whereNull('manuscript_id')
      .whereNull('deleted')
    return organizationRoles.concat(manuscriptRoles)
  }

  static async selectAll() {
    const rows = await Team.query().whereNull('deleted')
    return rows.map(rowToEntity)
  }

  static async insert(team, userId, trx) {
    team.id = uuid.v4()
    team.updatedBy = userId
    await Team.query(trx).insert(team)
    return team.id
  }

  static update(team, userId, trx) {
    team.updatedBy = userId
    return Team.query(trx)
      .update(team)
      .where('id', team.id)
      .whereNull('deleted')
  }

  static delete(id, userId, trx) {
    return Team.query(trx).updateAndFetchById(id, {
      deleted: new Date().toISOString(),
      updatedBy: userId,
    })
  }
}
module.exports = Team
