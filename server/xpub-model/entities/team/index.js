const Team = require('./data-access')
const Note = require('../note/data-access')
const Manuscript = require('../manuscript')
const logger = require('@pubsweet/logger')
const { transaction } = require('objection')

const TeamManager = {
  find: Team.selectById,
  findByUserIdOrManuscriptId: Team.selectByUserIdOrManuscriptId,
  selectByUserId: Team.selectByUserId,
  selectByManuscriptId: Team.selectByManuscriptId,
  selectByManIdAndType: Team.selectByManIdAndType,
  addNewReviewer: async (noteId, userId, trx = null) => {
    let commit = false
    let message = ''
    if (!trx) {
      commit = true
      trx = await transaction.start(Team.knex())
    }
    try {
      const note = await Note.selectById(noteId, trx)
      if (note) {
        const { manuscriptId, manuscriptVersion } = note
        const existing = await Team.selectByManIdAndType(
          manuscriptId,
          'reviewer',
          trx,
        )
        if (existing) {
          if (existing.userId !== userId) {
            await Team.update({ id: existing.id, userId }, userId, trx)
          }
        } else {
          await Team.insert(
            {
              manuscriptId,
              manuscriptVersion,
              userId,
              roleName: 'reviewer',
            },
            userId,
            trx,
          )
        }
        await Note.delete(noteId, userId, trx)
        if (commit && trx) {
          await trx.commit()
          trx = await transaction.start(Team.knex())
          const submitter = await Team.selectByManIdAndType(
            manuscriptId,
            'submitter',
            trx,
          )
          const man = await Manuscript.findById(manuscriptId, userId, trx)
          message = `/submission/${manuscriptId}/${
            man.status === 'in-review' ? 'submit' : 'review'
          }`
          await trx.commit()
          if (submitter && submitter.userId === userId) {
            if (man.status === 'in-review') {
              await Manuscript.update(
                { id: manuscriptId, status: 'submitted' },
                userId,
              )
              return {
                success: false,
                message: `You have already checked and submitted ${manuscriptId}.`,
              }
            }
          }
        }
        return { success: true, message }
      }
    } catch (error) {
      if (trx) await trx.rollback()
      logger.error('Set reviewer error: ', error)
      return {
        success: false,
        message: 'Submission has already been taken for review.',
      }
    }
  },
  delete: Team.delete,
  save: async (team, userId) => {
    let { id } = team
    if (team.id) {
      const updated = await Team.update(team, userId)
      if (!updated) {
        throw new Error('team not found')
      }
    } else {
      id = await Team.insert(team, userId)
    }

    return { ...team, id }
  },
  modelName: 'EpmcTeam',

  model: Team,
}

module.exports = TeamManager
