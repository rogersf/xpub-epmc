const { Model, raw } = require('objection')
const config = require('config')
const EpmcBaseModel = require('../epmc-base-model')
const logger = require('@pubsweet/logger')
const BaseModel = require('@pubsweet/base-model')

const knex = BaseModel.knex()

class User extends EpmcBaseModel {
  static get tableName() {
    return 'xpub.users'
  }

  static get idColumn() {
    return 'id'
  }

  static get schema() {
    return {
      properties: {
        id: { type: ['uuid'] },
        defaultIdentity: { type: 'string' },
        title: { type: ['string', 'null'] },
        givenNames: { type: 'string' },
        surname: { type: 'string' },
        privacyNoticeVersion: { type: 'string' },
        privacyNoticeAgreedDate: { type: 'timestamp' },
        deleted: { type: 'timestamp' },
        updatedBy: { type: 'uuid' },
      },
    }
  }

  static get relationMappings() {
    const Identity = require('../identity/data-access')
    const Manuscript = require('../manuscript/data-access')
    const Team = require('../team/data-access')
    const Role = require('../role/data-access')

    return {
      identities: {
        relation: Model.HasManyRelation,
        modelClass: Identity,
        join: {
          from: 'xpub.users.id',
          to: 'xpub.identity.userId',
        },
      },
      teams: {
        relation: Model.HasManyRelation,
        modelClass: Team,
        join: {
          from: 'xpub.users.id',
          to: 'xpub.team.userId',
        },
      },
      manuscripts: {
        relation: Model.ManyToManyRelation,
        modelClass: Manuscript,
        join: {
          from: 'xpub.users.id',
          through: {
            modelClass: Team,
            from: 'xpub.team.userId',
            to: 'xpub.team.manuscriptId',
          },
          to: 'xpub.manuscript.userId',
        },
      },
      roles: {
        relation: Model.ManyToManyRelation,
        modelClass: Role,
        join: {
          from: 'xpub.users.id',
          // ManyToMany relation needs the `through` object
          // to describe the join table.
          through: {
            from: 'xpub.team.userId',
            to: 'xpub.team.roleName',
            modelClass: Team,
          },
          to: 'xpub.role.name',
        },
      },
    }
  }

  // We decide not to support username
  static async findByEmail(email) {
    const user = await User.query()
      .alias('u')
      .join({ i: 'xpub.identity' }, 'u.id', 'i.user_id')
      .whereNull('u.deleted')
      .where(raw('lower(i.email) = lower(?)', [email]))
      .eager('identities')
    logger.debug('findByEmail: ', user)
    return user && user.length > 0 ? user[0] : null
  }

  static async findByRole(roleName) {
    const user = await User.query()
      .alias('u')
      .join({ t: 'xpub.team' }, 'u.id', 't.user_id')
      .where('t.role_name', roleName)
      .whereNull('t.manuscript_id')
      .whereNull('t.deleted')
      .whereNull('u.deleted')
    logger.debug('findUserTagger: ', user)
    return user && user.length > 0 ? user[0] : null
  }

  static async findListByEmail(email) {
    const users = await User.query()
      .alias('u')
      .join({ i: 'xpub.identity' }, 'u.id', 'i.user_id')
      .where('i.email', 'ilike', `%${email}%`)
      .whereNull('u.deleted')
      .eager('identities')
    return users
  }
  static async findListByName(name) {
    const users = await User.query()
      .whereNull('deleted')
      .where('surname', 'ilike', `%${name}%`)
      .orWhere('given_names', 'ilike', `%${name}%`)
      .eager('identities')
    return users.filter(user => user.identities.length)
  }
  static async validateUser(email, password) {
    const Identity = require('../identity/data-access')
    const user = await User.findByEmail(email)
    if (!user) {
      throw new Error('No user with that email')
    }
    const [identity] = user.identities.filter(
      id => id.email.toLowerCase() === email.toLowerCase(),
    )
    const valid = await Identity.isPasswordSame(password, identity.passwordHash)
    if (!valid) {
      throw new Error('Incorrect password')
    }
    return { email: identity.email, id: identity.userId }
  }

  static async findById(id, relations = ['identities']) {
    const user = await User.query()
      .where('id', id)
      .whereNull('deleted')
      .eager(`[${relations.toString()}]`)
    logger.debug('user: ', user)
    return user && user.length > 0 ? user[0] : null
  }

  static async mergeUser(from, to, updatedBy) {
    // no need to "where deleted"
    const result = await knex.raw('SELECT xpub.merge(?, ?, ?)', [
      from,
      to,
      updatedBy,
    ])
    return result.rows[0].merge === true
  }

  // used in signup
  async addIdentity(email, password) {
    const Identity = require('../identity/data-access')
    this.updateProperties({
      identities: {
        email,
        passwordHash: await Identity.hashPassword(password),
        type: config.user.identity.default,
      },
    })
  }

  // unused
  async addTeam(roleName, manuscriptId) {
    this.updateProperties({
      teams: {
        roleName,
        manuscriptId,
      },
    })
  }
}

module.exports = User
