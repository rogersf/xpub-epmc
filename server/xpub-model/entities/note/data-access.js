const { Model, raw } = require('objection')
const logger = require('@pubsweet/logger')
const uuid = require('uuid')
const EpmcBaseModel = require('../epmc-base-model')
const { rowToEntity } = require('../util')

class Note extends EpmcBaseModel {
  static get tableName() {
    return 'xpub.note'
  }

  static get schema() {
    return {
      properties: {
        id: { type: 'uuid' },
        created: { type: 'timestamp' },
        updated: { type: 'timestamp' },
        deleted: { type: 'timestamp' },
        manuscriptId: { type: 'string' },
        manuscriptVersion: { type: 'numeric' },
        content: { type: 'string' },
        notesType: { type: 'string' },
        updatedBy: { type: 'uuid' },
      },
    }
  }

  static get relationMappings() {
    const Manuscript = require('../manuscript/data-access')
    const User = require('../user/data-access')
    return {
      manuscript: {
        relation: Model.BelongsToOneRelation,
        modelClass: Manuscript,
        join: {
          from: 'xpub.note.manuscriptId',
          to: 'xpub.manuscript.id',
        },
      },
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'xpub.note.updated_by',
          to: 'xpub.users.id',
        },
      },
    }
  }

  static async selectById(id, trx) {
    const rows = await Note.query(trx)
      .where({ id })
      .whereNull('deleted')
    if (!rows.length) {
      throw new Error('Note not found')
    }
    return rowToEntity(rows[0])
  }

  static async selectByManuscriptId(manId, trx) {
    const notes = await Note.query(trx)
      .where('manuscript_id', manId)
      .whereNull('deleted')
      .eager('user.identities')
    return notes
  }

  static async selectByManuscriptIdAndType(manId, type, trx) {
    return Note.query(trx)
      .where('manuscript_id', manId)
      .where('notes_type', type)
      .whereNull('deleted')
      .eager('user.identities')
  }

  static async getOrcidNotes(content, type, trx) {
    const rows = await Note.query(trx)
      .alias('n')
      .select(
        'n.id as noteId, n.manuscript_id as manuscriptId, n.content as orcId, m."meta,article_ids" as man_articleIds',
      )
      .join({ m: 'xpub.manuscript' }, 'm.id', 'n.manuscript_id')
      .where('n.content', content)
      .where('n.notesType', type)
      .whereNull('n.deleted')
      .where(
        raw('lower("meta,article_ids"::text)::jsonb @> lower(?)::jsonb', [
          `[{"pubIdType": "doi"}]`,
        ]),
      )
    if (rows.length === 0) {
      return null
    }
    return rows[0]
  }

  static async getLatestValueByMidList(list, type, trx) {
    const rows = await Note.query(trx)
      .select('content')
      .where('notes_type', type)
      .whereIn('manuscript_id', list)
      .whereNull('deleted')
      .orderByRaw('"created" desc, "updated" desc')
    if (rows.length === 0) {
      return null
    }
    return rows[0].content
  }

  static async selectAll() {
    const rows = await Note.query().whereNull('deleted')
    return rows.map(rowToEntity)
  }

  static async insert(note, userId, trx) {
    note.id = uuid.v4()
    note.updatedBy = userId
    const newNote = await Note.query(trx).insert(note)
    const manuscript = {
      id: note.manuscriptId,
      meta: {
        notes: [rowToEntity(newNote)],
      },
    }
    return manuscript
  }

  static async update(note, userId, trx) {
    note.updatedBy = userId
    const newNote = await Note.query(trx)
      .update(note)
      .returning('*')
      .where('id', note.id)
      .whereNull('deleted')
    const manuscript = {
      id: newNote[0].manuscriptId,
      meta: {
        notes: newNote,
      },
    }
    return manuscript
  }

  static async delete(id, userId, trx) {
    try {
      await Note.query(trx)
        .update({
          deleted: new Date().toISOString(),
          updatedBy: userId,
        })
        .where('id', id)
      logger.info(`Note ${id} deleted`)
      return true
    } catch (err) {
      logger.error('Note delete error: ', err)
      return false
    }
  }

  static async deleteByManuscriptId(id, userId, trx) {
    await Note.query(trx)
      .update({
        deleted: new Date().toISOString(),
        updatedBy: userId,
      })
      .where('manuscript_id', id)
      .whereNull('deleted')
  }
}
module.exports = Note
