const uuid = require('uuid')
const { Model } = require('objection')
const EpmcBaseModel = require('../epmc-base-model')
const { rowToEntity, entityToRow } = require('../util')

const columnNames = ['name']

class Organization extends EpmcBaseModel {
  static get tableName() {
    return 'xpub.organization'
  }

  static get schema() {
    return {
      properties: {
        id: { type: 'uuid' },
        created: { type: 'timestamp' },
        updated: { type: 'timestamp' },
        name: { type: 'string' },
      },
    }
  }

  static get relationMappings() {
    const Manuscript = require('../manuscript/data-access')
    return {
      manuscripts: {
        relation: Model.HasManyRelation,
        modelClass: Manuscript,
        join: {
          from: 'xpub.organization.id',
          to: 'xpub.manuscript.organizationId',
        },
      },
    }
  }

  static async selectById(id) {
    const rows = await Organization.query().where({ id })
    if (!rows.length) {
      throw new Error('Organization not found')
    }
    return rowToEntity(rows[0])
  }

  static async selectAll(trx) {
    return Organization.query(trx)
  }

  static async insert(organization) {
    const row = entityToRow(organization, columnNames)
    row.id = uuid.v4()
    await Organization.query().insert(row)
    return row.id
  }

  static update(organization) {
    const row = entityToRow(organization, columnNames)
    return Organization.query()
      .update(row)
      .where('id', organization.id)
  }

  static delete(id) {
    return Organization.query()
      .delete()
      .where({ id })
  }
}
module.exports = Organization
