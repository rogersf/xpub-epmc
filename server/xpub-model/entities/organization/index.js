const lodash = require('lodash')
const Organization = require('./data-access')
const config = require('config')

const { europepmc_plus, europepmc_preprints } = config.organization

const empty = {
  name: '',
}

let organizations

const OrganizationManager = {
  find: Organization.selectById,
  findAll: async refresh => {
    if (organizations && organizations.length > 0 && !refresh) {
      return organizations
    }

    organizations = await Organization.selectAll()
    return organizations
  },
  getOrganizationID: async (preprint, trx) => {
    const organizations = await Organization.selectAll(trx)
    if (!organizations) {
      throw new Error('Organizations cannot be found')
    }

    const id = organizations
      .filter(
        org =>
          (preprint && org.name === europepmc_preprints) ||
          org.name === europepmc_plus,
      )
      .reduce((a, b) => b.id, undefined)
    if (!id) {
      throw new Error('Organization undefined')
    }

    return id
  },
  delete: Organization.delete,
  new: (props = {}) => lodash.merge({}, empty, props),
  save: async organization => {
    let { id } = organization
    if (organization.id) {
      const updated = await Organization.update(organization)
      if (!updated) {
        throw new Error('Organization not found')
      }
    } else {
      id = await Organization.insert(organization)
    }

    return { ...organization, id }
  },
}

module.exports = OrganizationManager
