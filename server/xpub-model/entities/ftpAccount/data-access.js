const EpmcBaseModel = require('../epmc-base-model')
const logger = require('@pubsweet/logger')

class FtpAccount extends EpmcBaseModel {
  static get tableName() {
    return 'config.ftp_account'
  }

  static get schema() {
    return {
      properties: {
        name: { type: 'string' },
        description: { type: 'string' },
        username: { type: 'string' },
        password: { type: 'text' },
      },
    }
  }

  static async findByFtpUsername(value) {
    logger.debug('Finding', 'ftp_account.username', value)

    try {
      const result = await FtpAccount.query().where('username', value)
      return result && result.length > 0 ? result[0] : null
    } catch (e) {
      logger.error(e)
    }
  }

  static async selectAll() {
    const result = await FtpAccount.query()
    return result && result.length > 0 ? result : null
  }

  static async selectBulkUploaders() {
    const result = await FtpAccount.query().whereNot('name', 'tagger')
    return result && result.length > 0 ? result : null
  }

  static async insert(rows) {
    return FtpAccount.query()
      .insert(rows)
      .returning('*')
  }

  $beforeInsert() {
    delete this.created
  }
}

module.exports = FtpAccount
