const { Model, transaction } = require('objection')
const uuid = require('uuid')
const EpmcBaseModel = require('../epmc-base-model')
const { rowToEntity, entityToRow } = require('../util')
const { gJournal } = require('../manuscript/helpers/transform')
const BaseModel = require('@pubsweet/base-model')
const logger = require('@pubsweet/logger')

const knex = BaseModel.knex()

const columnNames = ['journalTitle', 'meta,publisher_name']

class Journal extends EpmcBaseModel {
  static get tableName() {
    return 'xpub.journal'
  }

  static get schema() {
    return {
      properties: {
        id: { type: 'uuid' },
        created: { type: 'timestamp' },
        updated: { type: ['timestamp', 'null'] },
        journalTitle: { type: 'string' },
        'meta,publisherName': { type: ['string', 'null'] },
        'meta,issn': {
          type: ['array', 'null', 'string'],
          items: {
            type: ['null', 'object'],
            properties: {
              type: { type: 'string' },
              id: { type: 'string' },
            },
          },
        },
        'meta,nlmta': { type: ['string', 'null'] },
        'meta,nlmuniqueid': { type: ['string', 'null'] },
        'meta,pmjrid': { type: ['string', 'null'] },
        'meta,pmcStatus': { type: 'jsonb' },
        'meta,pubmedStatus': { type: 'boolean' },
        'meta,firstYear': { type: 'string' },
        'meta,endYear': { type: ['string', 'null'] },
        other_titles: {
          type: ['array', 'null'],
        },
      },
    }
  }

  static get relationMappings() {
    const Manuscript = require('../manuscript/data-access')
    return {
      manuscripts: {
        relation: Model.HasManyRelation,
        modelClass: Manuscript,
        join: {
          from: 'xpub.journal.id',
          to: 'xpub.manuscript.journalId',
        },
      },
    }
  }

  static async selectById(id) {
    const row = await Journal.query()
      .where('id', id)
      .first()
    if (!row) {
      throw new Error('journal not found')
    }
    return gJournal(rowToEntity(row))
  }

  static async selectByNlmId(nlmId) {
    const row = await Journal.query()
      .where('meta,nlmuniqueid', nlmId)
      .first()
    if (!row) {
      throw new Error('journal not found')
    }
    return gJournal(rowToEntity(row))
  }

  static async searchByTitle(query) {
    const search = await knex.raw(`
    SELECT * FROM (
      SELECT DISTINCT ON (id) *
      FROM (
       SELECT unnest(other_titles) aliases, *
       FROM xpub.journal
      ) j
      WHERE (j.aliases ILIKE '${query}%' OR j.journal_title ILIKE '${query}%' OR j."meta,nlmta" ILIKE '${query}%')
    ) t
    ORDER BY journal_title 
    LIMIT 50;
    `)
    const { rows } = search

    if (!rows) {
      throw new Error('journal not found')
    }
    return rows.map(r => rowToEntity(r))
  }

  static async selectAll() {
    const rows = await Journal.query().select('*')
    return rows.map(r => rowToEntity(r))
  }

  static async insert(journal) {
    const row = entityToRow(journal, columnNames)
    row.id = uuid.v4()
    await Journal.query().insert(row)
    return row.id
  }

  static async upsertMulti(journals) {
    const promises = [] // array of promises
    let trx
    try {
      trx = await transaction.start(knex)
      let counter = 0
      const util = require('util')
      journals.forEach(journal => {
        const insert = knex('xpub.journal')
          .insert(journal)
          .toString()

        const update = knex('xpub.journal')
          .update(journal)
          .whereRaw(
            `xpub.journal."meta,nlmuniqueid" = '${journal['meta,nlmuniqueid']}'`,
          )
        const query = util.format(
          '%s ON CONFLICT ("meta,nlmuniqueid") DO UPDATE SET %s',
          insert.toString(),
          update.toString().replace(/^update\s.*\sset\s/i, ''),
          'RETURNING id',
        )
        counter += 1

        promises.push(knex.raw(query).transacting(trx))
      })

      return Promise.all(promises).then(() => {
        trx.commit()
        logger.info(`${counter} records were updated.`)
      })
    } catch (error) {
      if (trx) {
        await trx.rollback()
      }
      logger.error('No journals were inserted')
      logger.error(error)
      throw error
    }
  }

  static update(journal) {
    const row = entityToRow(journal, columnNames)
    return Journal.query()
      .update(row)
      .where('id', journal.id)
      .whereNull('deleted')
  }

  static delete(id) {
    return Journal.query()
      .delete()
      .where({ id })
  }
}
module.exports = Journal
