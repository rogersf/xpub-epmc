const { Model } = require('objection')
const uuid = require('uuid')
const EpmcBaseModel = require('../epmc-base-model')
const { rowToEntity, entityToRow } = require('../util')

const columnNames = [
  'quote',
  'text',
  'ranges',
  'user_id',
  'review_id',
  'file_id',
]

class Annotation extends EpmcBaseModel {
  static get tableName() {
    return 'xpub.annotation'
  }

  static get schema() {
    return {
      properties: {
        id: { type: 'uuid' },
        created: { type: 'timestamp' },
        updated: { type: 'timestamp' },
        deleted: { type: 'timestamp' },
        userId: { type: 'uuid' },
        reviewId: { type: 'uuid' },
        fileId: { type: 'uuid' },
        quote: { type: 'string' },
        text: { type: 'string' },
        ranges: {
          type: 'array',
          items: {
            type: 'object',
          },
        },
      },
    }
  }

  static get relationMappings() {
    const User = require('../user/data-access')
    const Review = require('../review/data-access')
    const File = require('../file/data-access')
    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'xpub.annotation.userId',
          to: 'xpub.users.id',
        },
      },
      review: {
        relation: Model.BelongsToOneRelation,
        modelClass: Review,
        join: {
          from: 'xpub.annotation.reviewId',
          to: 'xpub.review.id',
        },
      },
      file: {
        relation: Model.BelongsToOneRelation,
        modelClass: File,
        join: {
          from: 'xpub.annotation.fileId',
          to: 'xpub.file.id',
        },
      },
    }
  }

  static async selectById(id) {
    const rows = await Annotation.query()
      .where('id', id)
      .whereNull('deleted')
    return rowToEntity(rows[0])
  }

  static async selectByReviewId(reviewId) {
    const rows = await Annotation.query()
      .where('review_id', reviewId)
      .whereNull('deleted')
    return rows.map(rowToEntity)
  }

  static async selectByFileId(fileId) {
    const rows = await Annotation.query()
      .alias('a')
      .where('a.file_id', fileId)
      .whereNull('a.deleted')
      .joinRelation('review', { alias: 'r' })
      .whereNull('r.deleted')
    return rows.map(rowToEntity)
  }

  static async selectAll() {
    const rows = await Annotation.query().whereNull('deleted')
    return rows.map(rowToEntity)
  }

  static async insert(annotation) {
    const row = entityToRow(annotation, columnNames)
    row.id = uuid.v4()
    await Annotation.query().insert(row)
    return row.id
  }

  static delete(id) {
    return Annotation.query().updateAndFetchById(id, {
      deleted: new Date().toISOString(),
    })
  }

  static deleteByReviewId(review_id) {
    return Annotation.query()
      .update({
        deleted: new Date().toISOString(),
      })
      .where({ review_id })
      .whereNull('deleted')
  }
}
module.exports = Annotation
