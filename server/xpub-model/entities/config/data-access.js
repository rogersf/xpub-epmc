/* eslint-disable class-methods-use-this */
// method $beforeValidate does not use this
const EpmcBaseModel = require('../epmc-base-model')
const { rowToEntity } = require('../util')
const { AjvValidator } = require('objection')

class Job extends EpmcBaseModel {
  static get tableName() {
    return 'config.job'
  }

  static get schema() {
    return {
      properties: {
        name: { type: 'string' },
        description: { type: 'string' },
        running: { type: 'boolean' },
        lastStatus: { type: 'string', enum: ['pass', 'fail'] },
        lastPass: { type: 'timestamp' },
        frequency: {
          type: 'string',
          format: 'regex',
          pattern:
            '(@(annually|yearly|monthly|weekly|daily|hourly|reboot))|(@every (\\d+(ns|us|µs|ms|s|m|h))+)|((((\\d+,)+\\d+|([\\d\\*]+(\\/|-)\\d+)|\\d+|\\*) ?){5,7})',
        },
        dataCenter: { type: 'string' },
        exitMessage: { type: 'string' },
        triggered: { type: 'timestamp' },
        updated: { type: 'timestamp' },
        timeout: { type: 'integer' },
        stale: { type: 'integer' },
      },
    }
  }

  static async selectByName(name, trx) {
    const rows = await Job.query(trx).where({ name })
    if (!rows.length) {
      throw new Error('Job not found')
    }
    return rowToEntity(rows[0])
  }

  static async selectAll() {
    const rows = await Job.query()
      .whereNull('deleted')
      .orderBy('name')
    return rows.map(rowToEntity)
  }

  static async selectAllNames() {
    const rows = await Job.query()
      .select('name')
      .whereNull('deleted')
    return rows.map(r => r.name)
  }

  static async update(job, userId, trx) {
    const update = await Job.query(trx)
      .patch(job)
      .where('name', job.name)
      .returning('*')
    const [updated] = update
    return updated
  }
}

class Property extends EpmcBaseModel {
  static get tableName() {
    return 'config.property'
  }

  static createValidator() {
    return new AjvValidator({
      onCreateAjv: ajv => {
        // Add formatMinumum, formatMaximum
        require('ajv-keywords')(ajv)
        // Edit validator to allow 'block' format for text blocks in form
        ajv.addFormat('block', /[\s\S]+/)
      },
      // Allow $data JSON Pointers
      options: { allErrors: true, $data: true },
    })
  }

  static get schema() {
    return {
      properties: {
        name: { type: 'string' },
        description: { type: 'string' },
        // value will be checked against schema
        value: true,
        // schema must be string to keep $data prop name intact through GraphQL
        schema: { type: 'string' },
      },
    }
  }

  // Validate value json against schema in schema column
  $beforeValidate(jsonSchema, json, opt) {
    jsonSchema.properties.value = JSON.parse(json.schema)
    return jsonSchema
  }

  // Remove inserted updated date
  // Ensure JSON arrays are correctly formatted
  $beforeUpdate() {
    this.value = JSON.stringify(this.value)
    delete this.updated
  }

  // schema must be string to keep $data prop name intact through GraphQL
  static async selectByName(name, trx) {
    const rows = await Property.query(trx).where({ name })
    if (!rows.length) {
      throw new Error('Config property not found')
    }
    rows.map(r => (r.schema = JSON.stringify(r.schema)))
    return rowToEntity(rows[0])
  }

  static async selectAll() {
    const rows = await Property.query().orderBy('name')
    rows.map(r => (r.schema = JSON.stringify(r.schema)))
    return rows.map(rowToEntity)
  }

  static async update(prop, userId, trx) {
    const update = await Property.query(trx)
      .patch(prop)
      .where('name', prop.name)
      .returning('*')
    const [updated] = update
    updated.schema = JSON.stringify(updated.schema)
    return updated
  }
}

class XSL extends EpmcBaseModel {
  static get tableName() {
    return 'config.xslt'
  }

  static get schema() {
    return {
      properties: {
        name: { type: 'string' },
        object: { type: 'string' },
        flag: { type: 'boolean' },
        created: { type: 'timestamp' },
        updated: { type: 'timestamp' },
      },
    }
  }

  static async selectByName(name, trx) {
    const [row] = await XSL.query(trx).where({ name })
    if (row) return rowToEntity(row)
    return {}
  }

  static async insert(xsl, trx) {
    await XSL.query(trx)
      .insert(xsl)
      .returning('name')
    return xsl.name
  }

  static async update(xsl, trx) {
    const update = await XSL.query(trx)
      .patch(xsl)
      .where('name', xsl.name)
      .returning('name')
    const [updated] = update
    return updated
  }
}

module.exports = { Job, Property, XSL }
