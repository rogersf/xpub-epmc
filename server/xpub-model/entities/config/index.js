const { Job, Property, XSL } = require('./data-access')

const JobManager = {
  modelName: 'Job',
  model: Job,
  find: Job.selectByName,
  selectAll: Job.selectAll,
  listNames: Job.selectAllNames,
  update: async (data, userId, trx) => {
    const updated = await Job.update(data, userId, trx)
    if (!updated) {
      throw new Error('Job not found')
    }
    return updated
  },
}

const PropManager = {
  modelName: 'Property',
  model: Property,
  find: Property.selectByName,
  selectAll: Property.selectAll,
  update: async (data, userId, trx) => {
    const updated = await Property.update(data, userId, trx)
    if (!updated) {
      throw new Error('Config property not found')
    }
    return updated
  },
}

const XSLManager = {
  modelName: 'XSL',
  model: XSL,
  find: XSL.selectByName,
  save: async (data, trx) => {
    const exists = await XSL.selectByName(data.name, trx)
    if (exists.name) {
      const updated = await XSL.update(data, trx)
      if (!updated) {
        throw new Error('XSL not updated')
      }
      return updated
    }
    return XSL.insert(data, trx)
  },
}

module.exports = { JobManager, PropManager, XSLManager }
