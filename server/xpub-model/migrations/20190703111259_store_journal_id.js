exports.up = (knex, Promise) =>
  knex.schema.table('journal', t => {
    t.string('meta,pmjrid').unique()
    t.dropColumn('date_revised')
  })

exports.down = (knex, Promise) =>
  knex.schema.table('journal', t => {
    t.dropColumn('meta,pmjrid')
    t.timestamp('date_revised')
  })
