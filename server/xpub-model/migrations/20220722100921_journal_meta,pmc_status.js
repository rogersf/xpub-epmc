/**
 * Change column xpub.journal."meta,pmc_status" from type boolean to jsonb
 */

exports.up = async (knex, Promise) => {
  await knex.schema
    .table('xpub.journal', table => {
      table.dropColumn('meta,pmc_status')
    })
    .table('xpub.journal', table => {
      table.jsonb('meta,pmc_status')
    })
}

exports.down = async (knex, Promise) => {
  await knex.schema
    .table('xpub.journal', table => {
      table.dropColumn('meta,pmc_status')
    })
    .table('xpub.journal', table => {
      table.boolean('meta,pmc_status')
    })
}
