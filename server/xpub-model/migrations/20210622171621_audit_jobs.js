const fs = require('fs')
const path = require('path')

const dirPath = path.join(__dirname, '../../../scripts')

const addTrigger = fs
  .readFileSync(`${dirPath}/xAuditJobsTrigger.sql`)
  .toString()

exports.up = (knex, Promise) =>
  knex.schema
    .table('config.job', t => {
      t.dropColumn('last_run')
      t.timestamp('updated')
      t.timestamp('triggered')
      t.string('data_center')
      t.text('exit_message')
    })
    .then(() => knex.raw(addTrigger))

exports.down = (knex, Promise) =>
  knex.schema
    .table('config.job', t => {
      t.timestamp('last_run')
      t.dropColumn('updated')
      t.dropColumn('triggered')
      t.dropColumn('data_center')
      t.dropColumn('exit_message')
    })
    .then(() =>
      knex.raw(`
DROP TRIGGER IF EXISTS audit_trigger_row ON config.job;
DROP FUNCTION IF EXISTS audit.job_modified_func();
DROP TABLE audit.job_log;

CREATE OR REPLACE VIEW audit.tableslist AS
SELECT DISTINCT triggers.trigger_schema AS schema,
                triggers.event_object_table AS auditedtable
FROM information_schema.triggers
WHERE triggers.trigger_name::text IN ('audit_trigger_row'::text, 'audit_trigger_stm'::text)
ORDER BY schema, auditedtable;
`),
    )
