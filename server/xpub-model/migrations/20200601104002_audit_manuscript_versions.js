exports.up = (knex, Promise) =>
  knex.schema.table('audit.audit_log', t => {
    t.decimal('manuscript_version').defaultTo(0)
  })

exports.down = (knex, Promise) =>
  knex.schema.table('audit.audit_log', t => {
    t.dropColumn('manuscript_version')
  })
