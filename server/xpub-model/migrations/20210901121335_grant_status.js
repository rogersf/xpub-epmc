exports.up = (knex, Promise) =>
  knex.schema
    .table('manuscript', t => {
      t.string('funding_state')
    })
    .then(() =>
      knex.raw(
        `update manuscript set "funding_state" = 'Linking grants' where "ncbi_state" = 'linking grants'`,
      ),
    )
    .then(() =>
      knex.raw(
        `update manuscript set "funding_state" = 'Grants linked' where "status" = 'published' or "ncbi_state" = 'success'`,
      ),
    )

exports.down = (knex, Promise) =>
  knex
    .raw(
      `update manuscript set "ncbi_state" = 'linking grants' where "funding_state" = 'Linking grants'`,
    )
    .then(() =>
      knex.schema.table('manuscript', t => {
        t.string('funding_state')
      }),
    )
