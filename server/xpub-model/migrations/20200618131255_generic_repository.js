const fs = require('fs')
const path = require('path')

const dirPath = path.join(__dirname, '../../../scripts')

const newTrigger = fs
  .readFileSync(`${dirPath}/xAuditTriggerWithVersionRepo.sql`)
  .toString()
const oldTrigger = fs
  .readFileSync(`${dirPath}/xAuditTriggerWithVersion.sql`)
  .toString()

exports.up = (knex, Promise) =>
  knex.schema
    .table('manuscript', t => {
      t.string('ebi_state')
    })
    .then(() => knex.raw(newTrigger))
    .then(() =>
      knex.raw(
        `update manuscript set "status" = replace("status", 'ncbi-', 'repo-')`,
      ),
    )
    .then(() =>
      knex.raw(
        `update audit.audit_log set "original_data" = replace("original_data"::text, '"status": "ncbi-', '"status": "repo-')::jsonb`,
      ),
    )
    .then(() =>
      knex.raw(
        `update audit.audit_log set "changes" = replace("changes"::text, '"status": "ncbi-', '"status": "repo-')::jsonb`,
      ),
    )

exports.down = (knex, Promise) =>
  knex.schema
    .table('manuscript', t => {
      t.dropColumn('ebi_state')
    })
    .then(() => knex.raw(oldTrigger))
    .then(() =>
      knex.raw(
        `update manuscript set "status" = replace("status", 'repo-', 'ncbi-')`,
      ),
    )
    .then(() =>
      knex.raw(
        `update audit.audit_log set "original_data" = replace("original_data"::text, '"status": "repo-', '"status": "ncbi-')::jsonb`,
      ),
    )
    .then(() =>
      knex.raw(
        `update audit.audit_log set "changes" = replace("changes"::text, '"status": "repo-', '"status": "ncbi-')::jsonb`,
      ),
    )
