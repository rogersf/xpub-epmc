const fs = require('fs')
const path = require('path')

const dirPath = path.join(__dirname, '../../../scripts')

const newTrigger = fs
  .readFileSync(`${dirPath}/xAuditTriggerWithVersionV3.sql`)
  .toString()
const oldTrigger = fs
  .readFileSync(`${dirPath}/xAuditTriggerWithVersionV2.sql`)
  .toString()

exports.up = (knex, Promise) =>
  knex.schema
    .table('audit.manuscript_process_dates', t => {
      t.timestamp('tagging_date').nullable()
      t.timestamp('tagged_date').nullable()
    })
    .then(() => knex.raw(newTrigger))
    .then(() =>
      knex.raw(
        `update audit.manuscript_process_dates md set tagging_date = td.sent, tagged_date = td.returned from (
  select aa.manuscript_id, aa.manuscript_version, bb.sent, aa.returned from (
    select au.manuscript_id, au.manuscript_version, min(au.created) returned from audit.audit_log au
    where au.changes->>'type' = 'PMC' 
    group by au.manuscript_id, au.manuscript_version
  ) aa
  LEFT JOIN (
    select au.manuscript_id, au.manuscript_version, min(au.created) sent from audit.audit_log au
    where au.changes->>'status' = 'tagging'
    group by au.manuscript_id, au.manuscript_version
  ) bb
  ON aa.manuscript_id = bb.manuscript_id and aa.manuscript_version = bb.manuscript_version
) td
where md.manuscript_id = td.manuscript_id and md.manuscript_version = td.manuscript_version`,
      ),
    )

exports.down = (knex, Promise) =>
  knex.schema
    .table('audit.manuscript_process_dates', t => {
      t.dropColumn('tagging_date')
      t.dropColumn('tagged_date')
    })
    .then(() => knex.raw(oldTrigger))
