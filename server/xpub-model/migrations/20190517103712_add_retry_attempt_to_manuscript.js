exports.up = (knex, Promise) =>
  knex.schema.table('manuscript', t => {
    t.integer('retry_attempt').defaultTo(0)
  })

exports.down = (knex, Promise) =>
  knex.schema.table('manuscript', t => {
    t.dropColumn('retry_attempt')
  })
