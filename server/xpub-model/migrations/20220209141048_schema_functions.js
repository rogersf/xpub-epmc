exports.up = (knex, Promise) =>
  knex.raw(`CREATE OR REPLACE FUNCTION xpub.trigger_set_manuscript_id() RETURNS TRIGGER AS $$
BEGIN
  NEW.id = CONCAT('EMS', nextval('xpub.manuscript_emsid_seq'));
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION xpub.merge(secondary UUID, main UUID, updatedBy UUID) RETURNS BOOLEAN AS $$
  BEGIN
    update xpub.users set deleted = now(), updated_by = updatedBy where id = secondary;
    update xpub.annotation set user_id = main where user_id = secondary;
    update xpub.review set user_id = main where user_id = secondary;
    update xpub.manuscript set updated_by = main where updated_by = secondary;
    update xpub.file set updated_by = main where updated_by = secondary;
    update xpub.note set updated_by = main where updated_by = secondary;
    update xpub.review set updated_by = main where updated_by = secondary;
    update xpub.team set updated_by = main where updated_by = secondary; 
    delete from xpub.team where user_id = secondary and manuscript_id is null;
    delete from xpub.identity where user_id = secondary;
    update xpub.team set user_id = main where user_id = secondary and manuscript_id is not null;
    RETURN true;
  END;
  $$ LANGUAGE plpgsql;`)

exports.down = (knex, Promise) =>
  knex.raw(`CREATE OR REPLACE FUNCTION xpub.trigger_set_manuscript_id() RETURNS TRIGGER AS $$
BEGIN
  NEW.id = CONCAT('EMS', nextval('manuscript_emsid_seq'));
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION xpub.merge(secondary UUID, main UUID, updatedBy UUID) RETURNS BOOLEAN AS $$
  BEGIN
    update users set deleted = now(), updated_by = updatedBy where id = secondary;
    update annotation set user_id = main where user_id = secondary;
    update review set user_id = main where user_id = secondary;
    update manuscript set updated_by = main where updated_by = secondary;
    update file set updated_by = main where updated_by = secondary;
    update note set updated_by = main where updated_by = secondary;
    update review set updated_by = main where updated_by = secondary;
    update team set updated_by = main where updated_by = secondary; 
    delete from team where user_id = secondary and manuscript_id is null;
    delete from identity where user_id = secondary;
    update team set user_id = main where user_id = secondary and manuscript_id is not null;
    RETURN true;
  END;
  $$ LANGUAGE plpgsql;`)
