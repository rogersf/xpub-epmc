const fs = require('fs')
const path = require('path')

const dirPath = path.join(__dirname, '../../../scripts')

const newTrigger = fs
  .readFileSync(`${dirPath}/xAuditTriggerWithVersion.sql`)
  .toString()
const oldTrigger = fs
  .readFileSync(`${dirPath}/xAuditTriggerWithoutVersion.sql`)
  .toString()

exports.up = (knex, Promise) => knex.raw(newTrigger)

exports.down = (knex, Promise) => knex.raw(oldTrigger)
