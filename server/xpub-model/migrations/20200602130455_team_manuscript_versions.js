exports.up = (knex, Promise) =>
  knex.schema.table('team', t => {
    t.decimal('manuscript_version').defaultTo(0)
  })

exports.down = (knex, Promise) =>
  knex.schema.table('team', t => {
    t.dropColumn('manuscript_version')
  })
