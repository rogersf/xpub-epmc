exports.up = async (knex, Promise) =>
  knex.schema.withSchema('config').createTable('xslt', t => {
    t.string('name')
      .notNullable()
      .unique()
    t.text('object')
    t.boolean('flag')
    t.timestamp('created')
    t.timestamp('updated')
  })

exports.down = async (knex, Promise) => {
  await knex.schema.dropTable('config.xslt')
}
