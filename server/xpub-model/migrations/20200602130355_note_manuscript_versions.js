exports.up = (knex, Promise) =>
  knex.schema.table('note', t => {
    t.decimal('manuscript_version')
      .notNullable()
      .defaultTo(0)
  })

exports.down = (knex, Promise) =>
  knex.schema.table('note', t => {
    t.dropColumn('manuscript_version')
  })
