const config = require('config')
const logger = require('@pubsweet/logger')
const { FileManager } = require('../../xpub-model')
const gridfsMiddlewareImport = require('./gridFsMiddleware')

const gridfsMiddleware = gridfsMiddlewareImport.middleware()

const download_path = config.file.url.download

const downloadFile = async (req, res) => {
  if (req.gridfs.error) {
    res.status(400).json({ error: req.gridfs.error })
    return
  }

  let filename = req.gridfs.get.originalName
  let mimeType = req.gridfs.get.contentType

  if (!filename || !mimeType) {
    logger.info(
      `GridFs metadata (${filename}, ${mimeType}) not available for file ${req.params.filename}`,
    )
    ;({ filename, mimeType } = await FileManager.findByUrl(req.params.filename))
  } else {
    logger.info(
      `GridFsd metadata (${filename}, ${mimeType}) is available for file ${req.params.filename}`,
    )
  }

  res.attachment(filename)
  res.set('Content-Length', req.gridfs.get.contentLength)
  res.set('Content-Type', mimeType)

  req.gridfs.get.stream.pipe(res)
}

module.exports = app => {
  app.post(
    '/api/upload',
    gridfsMiddleware({ op: gridfsMiddlewareImport.Ops.post }),
    (req, res) => {
      if (req.gridfs.error) {
        res.status(400).json({ error: req.gridfs.error })
      } else {
        res.send(`${download_path}/${req.gridfs.post.filename}`)
      }
    },
  )

  app.get(
    `/api/files/:filename`,
    gridfsMiddleware({ op: gridfsMiddlewareImport.Ops.getStream }),
    async (req, res) => {
      if (req.gridfs.error) {
        res.status(400).json({ error: req.gridfs.error })
      } else {
        logger.info(`Getting file: ${req.params.filename}`)
        await downloadFile(req, res)
      }
    },
  )

  app.delete(
    '/api/files/:filename',
    gridfsMiddleware({ op: gridfsMiddlewareImport.Ops.delete }),
    (req, res) => {
      if (req.gridfs.error) {
        res.status(400).json({ error: req.gridfs.error })
      } else {
        res.status(200).end(req.gridfs.delete)
      }
    },
  )

  /* todo: make sure there is always a public method like this for NCBI to download the necessary
   * files for the pdf conversion stage. */

  app.get(
    `${download_path}/:filename`,
    gridfsMiddleware({ op: gridfsMiddlewareImport.Ops.getStream }),
    async (req, res) => {
      if (req.gridfs.error) {
        res.status(400).json({ error: req.gridfs.error })
      } else {
        logger.info(`Getting file: ${req.params.filename}`)
        await downloadFile(req, res)
      }
    },
  )
}
