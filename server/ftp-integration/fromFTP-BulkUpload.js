const { bulkuploadLogger: logger } = require('../utils/loggers')

const Client = require('ftp')
const config = require('config')
const fs = require('fs')
const { execSync } = require('child_process')
const ftpUser = require('../utils/ftpUser')
const { errorDevEmail } = require('../email')
const { checkJobStatus, uncaughtError } = require('../job-runner')
const { FtpAccountManager } = require('../xpub-model')
const path = require('path')

if (!process.env.ENABLE_CRONJOB_FROMFTPBULKUPLOAD) {
  logger.info(
    'ENABLE_CRONJOB_FROMFTPBULKUPLOAD not defined. fromFtp-BulkUpload cronjob exits.',
  )
  process.exit(0)
}

const BULK_UPLOAD_FILE_EXT = new RegExp(/\S+\.tar\.gz$/i)

// Check and run cronjob
;(async () => {
  await checkJobStatus(
    'from-ftp-bulkupload',
    async () => {
      const ftpBulkUploaders = await FtpAccountManager.findBulkUploaders()
      if (!ftpBulkUploaders) {
        logger.info('No FTP bulk upload users in database. Exiting.')
        return false
      }
      logger.info('Processing bulk upload users.')
      try {
        await ftpBulkUploaders.reduce(async (promise, user) => {
          await promise
          return processFolder(user)
        }, Promise.resolve())
      } catch (err) {
        throw new Error(err)
      }
      logger.info('Cron job completed. Exiting.')
    },
    // FTP healthcheck
    true,
  )
  process.exit(0)
})()

function close(ftp) {
  ftp.end()
  ftp = null
  logger.info('Ftp connection terminated')
}

process
  .on('uncaughtException', async (err, origin) => {
    logger.error(`Uncaught Exception thrown: ${err}`)
    logger.error('Exception thrown at: ', origin)
    await uncaughtError(
      'from-ftp-bulkupload',
      `Uncaught Exception thrown: ${err}`,
    )
    process.exit(1)
  })
  .on('unhandledRejection', async (reason, promise) => {
    logger.error('Unhandled Rejection: ', reason)
    logger.error('Rejected at promise: ', promise)
    await uncaughtError('from-ftp-bulkupload', `Unhandled Rejection: ${reason}`)
    process.exit(1)
  })

async function processFolder(user) {
  logger.info(
    `Bulk Upload: Initialising job for file download from FTP folder: ${user.username}`,
  )

  const ftp = new Client()
  const rootFolderLocal = `${process.env.HOME}/${config.get('ftp_directory')}`

  fs.access(rootFolderLocal, error => {
    if (error) {
      const message = `${rootFolderLocal} directory does not exist. ${error}`
      logger.error(message)
      throw new Error(message)
    }
  })

  // create local directories
  const rootPathLocal = `${rootFolderLocal}/${user.username}`
  const cmd = `mkdir -p ${rootPathLocal}`
  await execSync(cmd)

  // Clear any "tar.gz" files directly under rootPathFTPLocal
  logger.info(`Bulk upload: clear local user root folder ${rootPathLocal}`)
  try {
    const files = await fs.promises.readdir(rootPathLocal)
    files.forEach(async file => {
      if (file && file.toLowerCase().endsWith('.tar.gz')) {
        await fs.promises.unlink(path.join(rootPathLocal, file))
        logger.info(`File ${file} deleted`)
      }
    })
  } catch (err) {
    throw err
  }

  const profiler = logger.startTimer()
  try {
    const val = await checkNewFtpPackage(ftp, user, rootPathLocal)
    profiler.done({
      message: `Check new packages on ftp: ${val} file(s) downloaded to local directory ${rootPathLocal}`,
    })
    close(ftp)
    return true
  } catch (err) {
    errorDevEmail('Bulk Upload Cron Job: ', err)
    logger.error(err)
    close(ftp)
    profiler.done({
      message: `Bulk Upload error: downloading files to local directory ${rootPathLocal}`,
      level: 'error',
    })
    throw new Error(err)
  }
}

async function checkNewFtpPackage(ftp, ftpBulkUploader, rootPathLocal) {
  const rootPathFTP = '/'
  const { host } = config.get('bulk-upload-ftp')
  const { username: user, password } = await ftpUser.getFTPAccount(
    ftpBulkUploader.username,
  )

  return new Promise((resolve, reject) => {
    ftp.on('ready', async () => {
      try {
        const targzFiles = await readDirRecursive(ftp, '')
        if (!targzFiles.length) {
          resolve(0) // No files to download to local FTP space
        }
        await downloadFtpFiles(targzFiles, ftp, rootPathFTP, rootPathLocal)
        resolve(targzFiles.length)
      } catch (err) {
        logger.error('Rejection with Bulk FTP file download', err.message)
        reject(err.message)
      }
    })
    ftp.on('error', () =>
      reject(new Error('Unable to connect to Bulk Upload FTP')),
    )
    const keepalive = 10000
    const connTimeout = 60000
    const pasvTimeout = 60000
    ftp.connect({ host, user, password, keepalive, connTimeout, pasvTimeout })
  })
}

const downloadFtpFiles = (targzFiles, ftp, rootPathFTP, rootPathLocal) =>
  targzFiles.reduce(async (promise, file) => {
    const profiler = logger.startTimer()
    await promise
    const remoteFilePath = file
    return new Promise((resolve, reject) => {
      ftp.get(remoteFilePath, (err, stream) => {
        if (err) {
          logger.error(`${remoteFilePath}: ${err.message}`)
          reject(err.message)
        } else if (!stream) {
          logger.error(`${remoteFilePath}: No FTP stream`)
          reject(err.message)
        } else {
          const pathName = path.dirname(file)
          const fileName = path.basename(file)
          const filePathLocal = `${rootPathLocal}${pathName}`

          // create subfolders on MSS server
          const cmd = `mkdir -p ${filePathLocal}`
          execSync(cmd)

          const downloadFilePath = `${filePathLocal}/${fileName}`
          const writeStream = stream.pipe(
            fs.createWriteStream(downloadFilePath),
          )
          writeStream.on('finish', () => {
            const stats = fs.statSync(downloadFilePath)
            const sizeInMB = stats.size / (1024 * 1024)
            profiler.done({
              // eslint-disable-next-line prettier/prettier
              message: `FTP file downloaded to ${downloadFilePath}, size = ${sizeInMB &&
                sizeInMB.toFixed(2)} MB`,
            })
            resolve()
          })
        }
      })
    })
  }, Promise.resolve())

const readDirRecursive = (ftp, startDir) => {
  const readDirQueue = []
  const fileList = []

  const readDir = dir => {
    const getDirList = dirName =>
      new Promise((resolve, reject) => {
        ftp.list(dirName, false, async (err, dirList) => {
          if (err) {
            close(ftp)
            reject(err)
          }

          const filteredList = dirList.map(ele => ({
            objPath: `${dirName}/${ele.name}`,
            objType: ele.type,
          }))

          resolve(filteredList)
        })
      })

    // Files are added to accumulating fileList array.
    // Directories are added to readDirQueue.
    const getPackages = dirList => {
      dirList.forEach(ele => {
        if (ele.objType === 'd') {
          readDirQueue.push(ele.objPath)
        }
        if (
          BULK_UPLOAD_FILE_EXT.test(ele.objPath.split('/').pop()) &&
          ele.objType === '-'
        ) {
          // push tar.gz files
          fileList.push(ele.objPath)
        }
      })

      if (readDirQueue.length > 0) {
        return readDir(readDirQueue.shift())
      }

      return fileList
    }

    return getDirList(dir).then(getPackages)
  }

  // start reading at the top
  return readDir(startDir)
}
