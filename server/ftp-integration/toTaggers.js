const dateFormat = require('dateformat')
const fileUtils = require('../utils/files.js')
const rfr = require('rfr')
const moment = require('moment')
const { toTaggersLogger: logger } = require('../utils/loggers')
const { exec } = require('child_process')
const fetch = require('node-fetch')
const fs = require('fs')
const config = require('config')
const { createTempDirNamed } = require('../utils/unTar')
const ftpUtils = require('../utils/ebi-ftp.js')
const path = require('path')

const { taggerEmail, errorDevEmail } = rfr('server/email')
const pubsweetServer = config.get('pubsweet-server.baseUrl')
const ftpTagger = config.get('ftp_tagger')

const ftpLocation = `${process.env.HOME}/${config.get('ftp_directory')}/${
  ftpTagger.username
}/New`

module.exports.createPackageForTaggers = async function createPackageForTaggers(
  manuscript,
) {
  try {
    const preprint = manuscript.organization.name === 'Europe PMC Preprints'
    const files = manuscript.files.reduce((list, file) => {
      if (
        [
          'manuscript',
          'manuscript_secondary',
          'supplement',
          'figure',
          'table',
        ].includes(file.type)
      ) {
        if (
          file.type === 'manuscript' &&
          list.some(f => f.type === 'manuscript' && !f.label)
        ) {
          file.label = list
            .filter(f => f.type === 'manuscript')
            .length.toString()
        }
        file.filename = fileUtils.normalizeFilename(file)
        file.type =
          file.type === 'manuscript_secondary' ? 'manuscript' : file.type
      }
      list.push(file)
      return list
    }, [])
    manuscript.files = files
    const datedFolder = dateFormat(new Date(), 'yyyy-mm-dd')
    if (manuscript.id) {
      try {
        logger.info(`Sending package ${manuscript.id} to tagger FTP location.`)
        const tmpPath = await createTempDirNamed(manuscript.id)
        await writeFiles(tmpPath, manuscript)
        await createMetadataXML(tmpPath, manuscript)
        await createManifest(tmpPath, manuscript)
        const destPath = await compress(
          tmpPath,
          manuscript,
          preprint,
          datedFolder,
        )
        await updateFTP(destPath, ftpTagger.username, preprint)
        await tidyUp(tmpPath)
        if (preprint) {
          logger.info('Tagging package created')
        } else {
          logger.info('Tagging package created. Sending email to taggers.')
          await taggerEmail(
            ftpTagger.email,
            manuscript.id,
            manuscript.meta.title,
            `ftp://${process.env.PUBSWEET_FTP_ADDR}/${
              ftpTagger.username
            }/New/${datedFolder}/${manuscript.id}${
              manuscript.version > 0 ? `v${Math.round(manuscript.version)}` : ''
            }.tar.gz`,
          )
        }
      } catch (error) {
        logger.error(error)
        logger.error('Sending error dev email')
        await errorDevEmail(
          'the FTP to taggers utility',
          `Error when creating package for manuscript ${manuscript.id}
        Error: ${error}`,
        )
        throw new Error(error)
      }
    } else {
      await errorDevEmail(
        'the FTP to taggers utility',
        `Error when creating package for manuscript ${manuscript.id}
      Error: Unable to retrieve manuscript record`,
      )
      throw new Error('Error: Unable to retrieve manuscript record')
    }
  } catch (err) {
    logger.error(`Tagger package generation error: ${err}`)
    throw new Error('Unable to successfully create tagging package')
  }
}

async function createMetadataXML(tmpPath, manuscript) {
  const { id, journal, version } = manuscript
  const {
    unmatchedJournal,
    publicationDates,
    articleIds,
    notes,
    subjects,
  } = manuscript.meta
  const pprid = articleIds && articleIds.find(a => a.pubIdType === 'pprid')
  let xml = `<?xml version="1.0" encoding="UTF-8"?>
<!-- use DTD https://jats.nlm.nih.gov/publishing/1.2/JATS-journalpublishing1.dtd -->
<!-- !DOCTYPE article PUBLIC "-//NLM//DTD JATS (Z39.96) Journal Publishing DTD v1.2 20190208//EN" "JATS-journalpublishing1.dtd" -->
${pprid ? '' : `<?properties manuscript?>\n`}<?origin ukpmcpa?>
<nihms-source-xml xmlns:xlink="http://www.w3.org/1999/xlink">
  <journal-meta>`
  if (journal) {
    const { journalTitle } = journal
    const { issn, nlmta } = journal.meta
    xml += `    <journal-id journal-id-type="nlm-ta">${nlmta}</journal-id>
    <journal-title-group>
      <journal-title>${journalTitle}</journal-title>
    </journal-title-group>
    ${
      issn && issn.length > 0
        ? issn
            .map(
              n =>
                `<issn pub-type="${n.type.toLowerCase().charAt(0)}pub">${
                  n.id
                }</issn>`,
            )
            .join('\n')
        : '<issn pub-type="ppub"/>\n'
    }`
  } else {
    // else try from the unmatched journal column
    xml += `<journal-title-group>
      <journal-title>${unmatchedJournal}</journal-title>
    </journal-title-group>`
  }
  xml += `\n  </journal-meta><article-meta>
  <article-id pub-id-type="manuscript">${id}</article-id>
  ${pprid ? `<article-id pub-id-type="archive">${pprid.id}</article-id>` : ''}
  ${
    version > 0
      ? `<article-version-alternatives>
        <article-version article-version-type="status">preprint</article-version>
        <article-version article-version-type="number">${Math.round(
          version,
        )}</article-version>
      </article-version-alternatives>`
      : ''
  }
  <article-categories>
    <subj-group subj-group-type="heading">
      <subject>Article</subject>
    </subj-group>
    ${
      subjects && subjects.length > 0
        ? `<subj-group subj-group-type="europepmc-category">${subjects
            .map(s => `<subject>${s}</subject>`)
            .join('')}</subj-group>`
        : ''
    }
  </article-categories>
  ${
    publicationDates && publicationDates.length > 0
      ? publicationDates
          .map(pDate => {
            let dateStr = `<pub-date pub-type="${pDate.type}">\n`
            if (pDate.jatsDate) {
              dateStr += Object.keys(pDate.jatsDate)
                .sort()
                .map(el =>
                  pDate.jatsDate[el]
                    ? `<${el}>${pDate.jatsDate[el]}</${el}>\n`
                    : '',
                )
                .join('')
            } else {
              dateStr += moment(pDate.date).format(
                '[<day>]DD[</day>\n    <month>]MM[</month>\n    <year>]YYYY[</year>\n]',
              )
            }
            dateStr += '</pub-date>'
            return dateStr
          })
          .join('\n')
      : ''
  }`
  // #874 As discussed the NIHMS submitted date should now be the date the first manuscript file was first uploaded.
  try {
    const submittedDate = manuscript.audits.find(
      audit =>
        audit.objectType === 'file' && audit.changes.type === 'manuscript',
    ).created
    xml += `  <pub-date pub-type="nihms-submitted">
    ${moment(submittedDate).format(
      '[<day>]DD[</day>\n    <month>]MM[</month>\n    <year>]YYYY[</year>\n]',
    )}
  </pub-date>\n`
  } catch (ignored) {
    logger.debug('Did not create nihms-submitted date ')
  }

  // add permissions if available

  const planS = notes.find(n => n.notesType === 'planS')
  if (planS) {
    const licenseValue = planS.content || 'by'
    const xlink = `https://creativecommons.org/licenses/${
      licenseValue.includes('/') ? `${licenseValue}/` : `${licenseValue}/4.0/`
    }`
    xml += `<permissions>
  <ali:free_to_read xmlns:ali="http://www.niso.org/schemas/ali/1.0/"/>
  <license>
    <ali:license_ref xmlns:ali="http://www.niso.org/schemas/ali/1.0/">${xlink}</ali:license_ref>
    <license-p>This work is licensed under a <ext-link ext-link-type="uri" xlink:href="${xlink}">CC ${licenseValue
      .toUpperCase()
      .replace(/\//g, ' ')}${
      licenseValue.includes('/')
        ? `</ext-link>`
        : ` 4.0</ext-link> International`
    } license.</license-p>
  </license>
</permissions>\n`
  } else {
    try {
      const metadata = await fs.readFileSync(`${tmpPath}/metadata.xml`, 'utf8')
      xml += `${metadata}\n`
    } catch (ignored) {
      // ignored
    }
  }
  xml += `</article-meta>`
  // supplementary files Issue #290
  const suppFiles = manuscript.files
    .filter(file => file.type === 'supplement')
    .sort((a, b) => {
      if (!a.label) {
        return 1
      }
      if (!b.label) {
        return -1
      }
      const astring = a.label.replace(/\d+/g, '').toLowerCase()
      const bstring = b.label.replace(/\d+/g, '').toLowerCase()
      if (astring === bstring) {
        if (/\d/.test(a.label) && /\d/.test(b.label)) {
          if (
            parseInt(a.label.match(/\d+/)[0], 10) >
            parseInt(b.label.match(/\d+/)[0], 10)
          ) {
            return 1
          }
          return -1
        }
      }
      if (a.label.toLowerCase() > b.label.toLowerCase()) {
        return 1
      }
      return -1
    })

  if (suppFiles && suppFiles.length > 0) {
    xml += `  <sec sec-type="supplementary-material" id="SM">
    <title>Supplementary Material</title>
    ${suppFiles
      .map((file, i) => {
        const mimetypes = file.mimeType.split('/')
        return `<supplementary-material id="SD${i +
          1}" content-type="local-data">
      <label>${file.label ? file.label : ''}</label>
      <media xlink:href="${file.filename}" mimetype="${
          mimetypes[0]
        }" mime-subtype="${mimetypes[1]}"/>
    </supplementary-material>`
      })
      .join('\n')}
  </sec>`
  }
  xml += '</nihms-source-xml>'

  return new Promise((resolve, reject) => {
    fs.writeFile(`${tmpPath}/${manuscript.id}-meta.xml`, xml, err => {
      if (err) reject(err)
      resolve(`${manuscript.id}-meta.xml`)
    })
  })
}

// file types included for taggers
function filterFiles(files) {
  const allowedTypes = [
    'manuscript',
    'supplement',
    'figure',
    'table',
    'PMC',
    'IMGview',
    'IMGprint',
    'supplement_tag',
    'metadata',
  ]
  const result = files.filter(file => allowedTypes.includes(file.type))
  return result
}

function writeFiles(tmpPath, manuscript) {
  const filteredFiles = filterFiles(manuscript.files)

  return filteredFiles.reduce(async (promise, file) => {
    await promise
    const fileOnDisk = fs.createWriteStream(`${tmpPath}/${file.filename}`)

    let agent
    if (process.env.superagent_http_proxy) {
      logger.info(
        `process.env.superagent_http_proxy: ${process.env.superagent_http_proxy}`,
      )
      const HttpsProxyAgent = require('https-proxy-agent')
      const proxy = process.env.superagent_http_proxy
      agent = new HttpsProxyAgent(proxy)
    }

    const url = `${pubsweetServer}${file.url}`
    const requestOptions = {
      method: 'GET',
      agent,
    }

    return fetch(url, requestOptions).then(
      response =>
        new Promise((resolve, reject) => {
          const stream = response.body.pipe(fileOnDisk)
          response.body.on('error', reject)
          stream.on('finish', () => resolve(true))
        }),
    )
  }, Promise.resolve())
}

function createManifest(tmpPath, manuscript) {
  const order = {
    meta: 0,
    manuscript: 1,
    figure: 2,
    table: 3,
    supplement: 4,
  }

  const filteredFiles = filterFiles(manuscript.files)

  filteredFiles.sort((file1, file2) => order[file1.type] - order[file2.type])

  let text = `meta\t\t${manuscript.id}.xml\n`
  filteredFiles
    .filter(file => file.type !== 'metadata')
    .forEach(file => {
      text += `${file.type}\t\t${file.label ? file.label : ''}\t\t${
        file.filename
      }\n`
    })
  return new Promise((resolve, reject) => {
    fs.writeFile(`${tmpPath}/manifest.txt`, text, err => {
      if (err) reject(err)
      resolve(true)
    })
  })
}

function compress(tmpPath, manuscript, preprint, datedFolder) {
  return new Promise((resolve, reject) => {
    const dest = `${ftpLocation}${
      preprint ? '/Preprints' : ''
    }/${datedFolder}/${manuscript.id}${
      manuscript.version > 0 ? `v${Math.round(manuscript.version)}` : ''
    }.tar.gz`
    const cmd =
      `mkdir -p ${ftpLocation}${
        preprint ? '/Preprints' : ''
      }/${datedFolder} ;` +
      `sleep 3 ;` +
      `touch ${dest} ;` +
      `find ${tmpPath} -printf "%P\n" -type f | tar --exclude=metadata.xml --exclude=${dest} --no-recursion -czf ${dest} -C ${tmpPath} -T -`
    exec(cmd, err => {
      if (err) {
        // node couldn't execute the command
        logger.error(err)
        reject(err)
      }
      logger.info(`Created package ${dest} to tagger local FTP location.`)
      resolve(dest)
    })
  })
}

function tidyUp(tmpPath) {
  try {
    const cmd = `rm -rf ${tmpPath}`
    return new Promise((resolve, reject) => {
      exec(cmd, (err, stdout, stderr) => {
        if (err) {
          // node couldn't execute the command
          reject(err)
        }
        // logger.info(`Finished process`)
        resolve(true)
      })
    })
  } catch (e) {
    logger.error('Error while tidying up after another error!')
  }
}

async function updateFTP(packagePath, ftpUserName, preprint) {
  try {
    logger.info(`${path.basename(packagePath)}: Updating FTP server...`)
    await ftpUtils.uploadTaggersPackage(ftpUserName, packagePath, preprint)
  } catch (err) {
    logger.error('Error with updating FTP')
  }
}
