const { taggerImportLogger: logger } = require('../utils/loggers')

const Client = require('ftp')
const { execSync } = require('child_process')
const config = require('config')
const fs = require('fs')
const path = require('path')

const ftpUser = require('../utils/ftpUser')
const { errorDevEmail } = require('../email')
const { checkJobStatus, uncaughtError } = require('../job-runner')

const ftpTagger = config.get('ftp_tagger')
if (!process.env.ENABLE_CRONJOB_FROMFTPTAGGERIMPORT) {
  logger.info(
    'ENABLE_CRONJOB_FROMFTP-TAGGERIMPORT not defined. fromFtp-TaggerImport cronjob exits.',
  )
  process.exit(0)
}

const BULK_UPLOAD_FILE_EXT = new RegExp(/\S+\.tar\.gz$/i)
const ftp = new Client()

;(async () => {
  await checkJobStatus(
    'from-ftp-taggerimport',
    async () => {
      // create local directories
      const parentRootPathLocal = `${process.env.HOME}/${config.get(
        'ftp_directory',
      )}/${ftpTagger.username}`
      const rootPathLocal = `${parentRootPathLocal}/Done`
      const ignoreNewFolder = `${parentRootPathLocal}/New`
      const errorFolder = `${parentRootPathLocal}/Error`

      const cmd = `mkdir -p ${rootPathLocal} ${ignoreNewFolder} ${errorFolder}`
      execSync(cmd)

      // Clear any "tar.gz" files directly under rootPathLocal (the Done folder)
      logger.info('Tagger Import: clear local root folder')
      fs.readdir(rootPathLocal, (err, files) => {
        if (err) throw err

        files.forEach(file => {
          if (file && file.toLowerCase().endsWith('.tar.gz')) {
            logger.error(`Delete file ${file}`) // shouldn't have any file here
            fs.unlink(path.join(rootPathLocal, file), unlinkerr => {
              if (unlinkerr) {
                logger.error(`Delete file ${file}failed`)
                logger.error(unlinkerr)
              }
            })
          }
        })
      })

      logger.info('Downloading files from FTP')
      const beforeUpdate = Date.now()
      try {
        const profiler = logger.startTimer()
        const val = await checkNewFtpPackage(ftpTagger.username, rootPathLocal)
        profiler.done({
          message: `${val} file(s) downloaded to local directory ${rootPathLocal} in ${Date.now() -
            beforeUpdate} ms`,
        })
      } catch (err) {
        errorDevEmail('Tagger Import Cron Job', err)
        logger.error(err)
        close(ftp)
        throw new Error(err)
      }
      close(ftp)
    },
    // FTP healthcheck
    true,
  )
  process.exit(0)
})()

function close(ftp) {
  ftp.end()
  ftp = null
  logger.info('Ftp connection terminated')
}

process
  .on('uncaughtException', async (err, origin) => {
    logger.error(`Uncaught Exception thrown: ${err}`)
    logger.error('Exception thrown at: ', origin)
    close(ftp)
    await uncaughtError(
      'from-ftp-taggerimport',
      `Uncaught Exception thrown: ${err}`,
    )
    process.exit(1)
  })
  .on('unhandledRejection', async (reason, promise) => {
    logger.error('Unhandled Rejection: ', reason)
    logger.error('Rejected at promise: ', promise)
    close(ftp)
    await uncaughtError(
      'from-ftp-taggerimport',
      `Unhandled Rejection: ${reason}`,
    )
    process.exit(1)
  })

async function checkNewFtpPackage(ftpUsername, rootPathLocal) {
  const parentRootPathFTP = `${
    config.get('taggers-upload-ftp')['receive-folder']
  }`
  const rootPathFTP = `${parentRootPathFTP}/Done`
  const ignoreNewFolder = `${parentRootPathFTP}/New`
  const errorFolder = `${parentRootPathFTP}/Error`
  const pathArr = [rootPathFTP, ignoreNewFolder, errorFolder]

  const { host } = config.get('taggers-upload-ftp')
  const { username: user, password } = await ftpUser.getFTPAccount(ftpUsername)
  pathArr.forEach(dir => {
    ftp.mkdir(dir, true, err => {
      if (err) {
        logger.error(err)
      }
    })
  })

  return new Promise((resolve, reject) => {
    ftp.on('ready', () => {
      ftp.list(rootPathFTP, false, async (err, list) => {
        if (err) {
          close(ftp)
          reject(err)
        }
        // get array of tar.gz files only
        const targzFiles = list.filter(
          file => BULK_UPLOAD_FILE_EXT.test(file.name) && file.type === '-',
        )
        if (!targzFiles.length) {
          resolve(0) // No files to download to local FTP space
        }
        try {
          const profiler = logger.startTimer()
          await downloadFtpFiles(targzFiles, ftp, rootPathFTP, rootPathLocal)
          profiler.done({ message: `Finished check new ftp packages.` })
          resolve(targzFiles.length)
        } catch (err) {
          reject(err.message)
        }
      })
    })
    ftp.on('error', () =>
      reject(new Error('Unable to connect to Bulk Upload FTP')),
    )
    const keepalive = 10000
    const connTimeout = 60000
    const pasvTimeout = 60000
    ftp.connect({ host, user, password, keepalive, connTimeout, pasvTimeout })
  })
}

function downloadFtpFiles(targzFiles, ftp, rootPathFTP, rootPathLocal) {
  return targzFiles.reduce(async (promise, file) => {
    const profiler = logger.startTimer()
    await promise
    const remoteFilePath = `${rootPathFTP}/${file.name}`
    return new Promise((resolve, reject) => {
      ftp.get(remoteFilePath, (err, stream) => {
        if (err) {
          logger.error(`${remoteFilePath}: ${err.message}`)
          reject(err.message)
        } else if (!stream) {
          logger.error(`${remoteFilePath}: No FTP stream`)
          reject(err.message)
        } else {
          const path = `${rootPathLocal}/${file.name}`
          const writeStream = stream.pipe(fs.createWriteStream(path))
          writeStream.on('finish', () => {
            const stats = fs.statSync(path)
            const sizeInMB = stats.size / (1024 * 1024)
            profiler.done({
              // eslint-disable-next-line prettier/prettier
              message: `FTP file downloaded to ${path}, size = ${sizeInMB &&
                sizeInMB.toFixed(2)} MB`,
            })
            resolve()
          })
        }
      })
    })
  }, Promise.resolve())
}
