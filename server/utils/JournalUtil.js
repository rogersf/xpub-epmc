const isJournalSubscribedToPMC = pmcStatus => {
  if (!pmcStatus) return false
  if (!pmcStatus.start_date) return false
  const current = Date.now()
  if (current < Date.parse(pmcStatus.start_date)) return false
  if (pmcStatus.end_date) {
    if (current > Date.parse(pmcStatus.end_date)) {
      return false
    }
  }
  return true
}

module.exports = {
  isJournalSubscribedToPMC,
}
