const superagent = require('superagent')
require('superagent-proxy')(superagent)

const get = url => {
  let superagentRequest = superagent.get(url)
  if (process.env.superagent_http_proxy) {
    superagentRequest = superagentRequest.proxy(
      process.env.superagent_http_proxy,
    )
  }
  return superagentRequest
}

module.exports = {
  get,
}
