const { UserManager } = require('../xpub-model')

module.exports.getAdminUser = function getAdminUser() {
  return new Promise((resolve, reject) => {
    UserManager.findByEmail('helpdesk@europepmc.org')
      .then(user => {
        user.email = 'helpdesk@europepmc.org'
        resolve(user)
      })
      .catch(error => reject(error))
  })
}
