const Client = require('ftp')
const config = require('config')
const logger = require('@pubsweet/logger')

const checkFtpStatus = async () => {
  let status = 0

  try {
    if (config.get('ftp_healthcheck.enabled') === '1') {
      const ftp = new Client()
      try {
        status = await testFtpConnection(ftp)
      } catch (err) {
        status = 0
      } finally {
        ftp.end()
      }
    } else {
      logger.info('FTP healthcheck not enabled in config file')
    }
  } catch (e) {
    logger.error(e)
  }

  return status
}

const testFtpConnection = async ftp =>
  new Promise((resolve, reject) => {
    ftp.on('ready', () => {
      resolve(1)
    })

    ftp.on('error', err => {
      reject(new Error(`Unable to connect to FTP , error code: ${err.code}`))
    })

    const keepalive = 1000
    const connTimeout = 5000
    ftp.connect({
      host: config.get('ftp_healthcheck.host'),
      user: config.get('ftp_healthcheck.username'),
      password: config.get('ftp_healthcheck.password'),
      keepalive,
      connTimeout,
    })
  })

module.exports = {
  checkFtpStatus,
}
