const { subjCheck } = require('../util.js')
const rfr = require('rfr')

const { AuditManager, OrganizationManager } = rfr('server/xpub-model')

const resolvers = {
  Query: {
    async getMetrics(_, { startMonth, endMonth, preprint, set }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }
      const orgId = await OrganizationManager.getOrganizationID(preprint)
      return AuditManager.getMetrics(
        startMonth,
        endMonth,
        orgId,
        subjCheck(set),
      )
    },
    async publisherMetrics(_, { startMonth, endMonth }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }
      return AuditManager.publisherMetrics(startMonth, endMonth)
    },
    async weeklyMetrics(_, { preprint, set }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }
      const orgId = await OrganizationManager.getOrganizationID(preprint)
      return AuditManager.weeklyMetrics(orgId, subjCheck(set))
    },
    async jobLog(_, { name }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }
      return AuditManager.jobLog(name)
    },
  },
}

module.exports = resolvers
