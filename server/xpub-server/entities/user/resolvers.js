const rfr = require('rfr')
const logger = require('@pubsweet/logger')
const authorization = require('pubsweet-server/src/helpers/authorization')
const userUtil = require('./helpers/userUtil')

const authentication = rfr('server/utils/authentication')
const { UserManager, IdentityManager, PrivacyNoticeManager } = rfr(
  'server/xpub-model',
)

const User = UserManager.model

const resolvers = {
  Query: {
    async epmc_currentUser(_, input, ctx) {
      if (!ctx.user) {
        return null
      }
      const dbUser = await UserManager.findById(ctx.user)
      if (!dbUser) {
        throw new Error('No user authenticated!')
      }
      const privacyNotice = await PrivacyNoticeManager.findLastVersion()
      return userUtil.mapSqlToGraphql(dbUser, privacyNotice.version)
    },
    async epmc_user(_, { id }, ctx) {
      if (!ctx.user) {
        throw new Error('You are not authenticated!')
      }
      const isAuthorised = await authorization.can(ctx.user, 'read', {
        type: 'User',
      })
      if (!isAuthorised) {
        throw new Error('You are not authorized!')
      }
      const dbUser = await UserManager.findById(id, ['identities', 'teams'])
      if (!dbUser) {
        throw new Error('User does not exist!')
      }
      return userUtil.mapSqlToGraphql(dbUser)
    },
    async user(_, { id }, ctx) {
      if (!ctx.user) {
        throw new Error('You are not authenticated!')
      }
      const isAuthorised = await authorization.can(ctx.user, 'read', {
        type: 'User',
      })
      if (!isAuthorised) {
        throw new Error('You are not authorized!')
      }
      const dbUser = await UserManager.findById(id, ['identities'])
      if (!dbUser) {
        throw new Error('User does not exist!')
      }
      return userUtil.mapSqlToGraphql(dbUser)
    },
    async userByEmail(_, { email }, ctx) {
      if (!ctx.user) {
        throw new Error('You are not authenticated!')
      }
      return UserManager.findByEmail(email)
    },
    async usersByName(_, { name }, ctx) {
      if (!ctx.user) {
        throw new Error('You are not authenticated!')
      }
      const isAuthorised = await authorization.can(ctx.user, 'read', {
        type: 'User',
      })
      if (!isAuthorised) {
        throw new Error('You are not authorized!')
      }
      const users = await UserManager.findListByName(name)
      users.forEach(dbUser => {
        userUtil.mapSqlToGraphql(dbUser)
      })
      return users
    },
    async usersByEmail(_, { email }, ctx) {
      if (!ctx.user) {
        throw new Error('You are not authenticated!')
      }
      const isAuthorised = await authorization.can(ctx.user, 'read', {
        type: 'User',
      })
      if (!isAuthorised) {
        throw new Error('You are not authorized!')
      }
      const users = await UserManager.findListByEmail(email)
      users.forEach(dbUser => {
        userUtil.mapSqlToGraphql(dbUser)
      })
      return users
    },
  },
  Mutation: {
    async epmc_signupUser(_, { input }, ctx) {
      const savedUser = await UserManager.signUp(input)

      const u = {
        email: input.email,
        id: savedUser.id,
      }
      logger.debug('user: ', u)

      return {
        user: u,
        token: authentication.token.create(u),
      }
    },

    // Authentication
    async epmc_signinUser(_, { input }) {
      let user
      try {
        user = await User.validateUser(input.email, input.password)
      } catch (error) {
        logger.error(error)
        throw new Error('You’ve entered an incorrect email address or password')
      }

      logger.debug('user: ', user)

      return {
        user,
        token: authentication.token.create(user),
      }
    },

    async epmc_resetPassword(_, { token, newPassword }) {
      const decoded = await authentication.token.verifySync(token)
      logger.debug('decoded: ', decoded)
      if (!decoded) {
        throw new Error('Invalid token, please reset password again')
      }
      const user = await UserManager.findById(decoded.id)
      if (!user) {
        throw new Error('User not found')
      }

      const identity = user.identities.find(
        identity =>
          identity.email.toLowerCase() === decoded.email.toLowerCase(),
      )
      if (!identity) {
        throw new Error('Invalid token, please reset password again')
      }
      if (identity.passwordResetToken !== token) {
        throw new Error('Invalid token, please reset password again')
      }

      Object.keys(identity).forEach(
        key => identity[key] == null && delete identity[key],
      )
      logger.debug('identity: ', identity)
      identity.updateProperties({
        passwordHash: await IdentityManager.model.hashPassword(newPassword),
        passwordResetToken: '',
      })
      const savedIdentity = await identity.save()
      logger.debug('savedIdentity: ', savedIdentity)
      return true
    },
    async epmc_updateCurrentUser(_, { input }, ctx) {
      return userUtil.mapSqlToGraphql(
        await UserManager.updateUser(ctx.user, input, ['identities'], ctx.user),
      )
    },
    async epmc_updateUser(_, { input }, ctx) {
      const isAuthorised = await authorization.can(ctx.user, 'read', {
        type: 'User',
      })
      if (!isAuthorised) {
        throw new Error('You are not authorized!')
      }
      return userUtil.mapSqlToGraphql(
        await UserManager.updateUser(
          input.id,
          input,
          ['identities', 'teams'],
          ctx.user,
        ),
      )
    },
    async mergeUser(_, { input }, ctx) {
      const isAuthorised = await authorization.can(ctx.user, 'update', {
        type: 'User',
      })
      if (!isAuthorised) {
        throw new Error('You are not authorized!')
      }
      await UserManager.mergeUser(input.from, input.to, ctx.user)
      return true
    },
  },

  Identity: {
    __resolveType(identity) {
      switch (identity.type) {
        case 'elife':
          return 'ElifeIdentity'
        default:
          return null
      }
    },
  },
}

module.exports = resolvers
