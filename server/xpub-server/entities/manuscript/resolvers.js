const logger = require('@pubsweet/logger')
const { subjCheck } = require('../util.js')
const rfr = require('rfr')

const { ManuscriptManager, OrganizationManager, JournalManager } = rfr(
  'server/xpub-model',
)
const { deleteManifest } = rfr('server/ebi-integration/removeEbi')

const resolvers = {
  UpdateResult: {
    __resolveType(obj, context, info) {
      if (obj.status) return 'Manuscript'
      return 'Error'
    },
  },
  Query: {
    async manuscript(_, { id }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }

      return ManuscriptManager.findById(id, user)
    },
    async manuscripts(_, vars, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }

      return ManuscriptManager.all(user)
    },
    async manuscriptActivityLog(_, { id }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }

      const activity = await ManuscriptManager.selectActivityById(id)
      return activity
    },
    async checkDuplicates(_, { id, articleIds, title }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }
      return ManuscriptManager.checkDuplicates(id, articleIds, title, user)
    },
    async countByStatus(_, vars, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }
      return ManuscriptManager.countByStatus(user)
    },
    async countSetByStatus(_, { preprint, set }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }
      const orgId = await OrganizationManager.getOrganizationID(preprint)
      return ManuscriptManager.countSetByStatus(orgId, subjCheck(set))
    },
    async preprintPriority(_, vars, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }
      const orgId = await OrganizationManager.getOrganizationID(true)
      return ManuscriptManager.countPreprintsByPriority(orgId)
    },
    async preprintPriorityList(_, { tier, page, pageSize }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }
      const orgId = await OrganizationManager.getOrganizationID(true)
      return ManuscriptManager.findByPriority(orgId, tier, page, pageSize)
    },
    async checkAge(_, { states, days, weekdays, preprint = false }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }
      const orgId = await OrganizationManager.getOrganizationID(preprint)
      return ManuscriptManager.checkAge(states, days, weekdays, orgId)
    },
    async adminManuscripts(
      _,
      { external, tagger, type, page, pageSize },
      { user },
    ) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }
      const statuses = (external && ['xml-qa']) ||
        (tagger && ['tagging']) || [
          'submitted',
          'file-error',
          'xml-triage',
          'xml-corrected',
          'xml-error',
          'repo-triage',
          'withdrawal-triage',
        ]
      let orgId = null
      if (type) {
        orgId = await OrganizationManager.getOrganizationID(
          type === 'preprints',
        )
      }
      return ManuscriptManager.findByStatus(
        statuses,
        page,
        pageSize,
        user,
        true,
        orgId,
      )
    },
    async citationManuscripts(_, { page, pageSize }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }
      return ManuscriptManager.findNeedsCitation(page, pageSize)
    },
    async findByStatus(_, { query, page, pageSize }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }
      return ManuscriptManager.findByStatus(
        query.split(','),
        page,
        pageSize,
        user,
      )
    },
    async searchManuscripts(_, { query, page, pageSize }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }

      return ManuscriptManager.search(query, page, pageSize, user)
    },
    async searchArticleIds(_, { id }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }

      return ManuscriptManager.findByArticleId(id, user)
    },
    async getManuscriptVersions(_, { query }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }

      return ManuscriptManager.getManuscriptVersions(query)
    },
  },

  Mutation: {
    async createManuscript(_, { data }, { user }) {
      if (!user) {
        throw new Error('Not logged in')
      }
      const organizationId = await OrganizationManager.getOrganizationID(
        data.preprint,
      )
      if (!data.preprint && data.journalId) {
        const preprintCheck = await JournalManager.preprintCheck(data.journalId)
        if (preprintCheck) {
          return {
            manuscript: null,
            errors: [preprintCheck],
          }
        }
      }
      delete data.preprint
      const dupeCheck = await ManuscriptManager.checkDuplicates(
        null,
        data.meta.articleIds && data.meta.articleIds.map(aid => aid.id),
        data.meta.title,
        user,
      )
      if (dupeCheck && dupeCheck.total > 0 && dupeCheck.manuscripts) {
        const { journalId, meta } = dupeCheck.manuscripts[0]
        const { unmatchedJournal } = meta
        if (
          (journalId && data.journalId && journalId === data.journalId) ||
          (unmatchedJournal &&
            data.meta.unmatchedJournal &&
            unmatchedJournal === data.meta.unmatchedJournal)
        ) {
          return {
            manuscript: dupeCheck.manuscripts[0],
            errors: [
              {
                type: 'warning',
                message:
                  'You already have a manuscript with this citation information.',
              },
            ],
          }
        }
      }
      const saved = await ManuscriptManager.create(data, user, organizationId)
      logger.debug('saved: ', saved)
      return { manuscript: saved }
    },
    async linkExisting(_, { id }, { user }) {
      if (!user) {
        throw new Error('Not logged in')
      }

      await ManuscriptManager.update({ id, status: 'link-existing' }, user)
      return ManuscriptManager.linkExistingEmail(id, user)
    },
    async deleteManuscript(_, { id }, { user }) {
      if (!user) {
        throw new Error('Not logged in')
      }

      return ManuscriptManager.delete(id, user)
    },
    async recoverManuscript(_, { id }, { user }) {
      if (!user) {
        throw new Error('Not logged in')
      }

      return ManuscriptManager.recover(id, user)
    },
    async replaceManuscript(_, { keepId, throwId }, { user }) {
      if (!user) {
        throw new Error('Not logged in')
      }

      return ManuscriptManager.replace(keepId, throwId, user)
    },
    async replaceManuscriptIds(_, { data, repoId, replace }, { user }) {
      if (!user) {
        throw new Error('Not logged in')
      }
      if (replace) {
        data.ebiState = 'Removing record with incorrect IDs from repository'
        await deleteManifest(data.id, repoId, user, replace)
      }
      await ManuscriptManager.update(data, user)
      return true
    },
    async updateManuscript(_, { data }, { user }) {
      if (!user) {
        throw new Error('Not logged in')
      }
      if (data.journalId) {
        const preprintCheck = await JournalManager.preprintCheck(data.journalId)
        if (preprintCheck) {
          return preprintCheck
        }
      }
      const updatedMan = await ManuscriptManager.update(data, user)
      if (data.status === 'repo-ready' && updatedMan.status === 'repo-ready') {
        return ManuscriptManager.update(
          { id: data.id, pdfDepositState: 'ADDING_CITATION' },
          user,
        )
      }
      if (data.status === 'being-withdrawn') {
        const pmcid =
          updatedMan.meta.articleIds &&
          updatedMan.meta.articleIds.find(a => a.pubIdType === 'pmcid')
        const pprid =
          updatedMan.meta.articleIds &&
          updatedMan.meta.articleIds.find(a => a.pubIdType === 'pprid')
        const deletedError = await deleteManifest(
          data.id,
          pprid ? pprid.id : pmcid.id,
          user,
        )
        if (deletedError) {
          return ManuscriptManager.update(deletedError, user)
        }
        return ManuscriptManager.update(
          { id: data.id, ncbiState: 'ebi-withdraw' },
          user,
        )
      }
      return updatedMan
    },
    async claimManuscript(_, { id }, { user }) {
      if (!user) {
        throw new Error('Not logged in')
      }

      const updatedMan = await ManuscriptManager.changeClaim(id, user)
      return updatedMan
    },
    async unclaimManuscript(_, { id }, { user }) {
      if (!user) {
        throw new Error('Not logged in')
      }

      const updatedMan = await ManuscriptManager.changeClaim(id, user, true)
      return updatedMan
    },

    async submitManuscript(_, { data }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }
      logger.info('Submitting manuscript: ', JSON.stringify(data))
      const submittedMan = await ManuscriptManager.submit(data, user)
      return submittedMan
    },

    async rejectManuscript(_, { data }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }
      const rejectedMan = await ManuscriptManager.reject(data, user)
      return rejectedMan
    },

    async setManuscriptReviewer(_, { id }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }
      const manuscript = await ManuscriptManager.setReviewer(id, user)
      return manuscript
    },

    async reviewManuscript(_, { data }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }
      const submittedMan = await ManuscriptManager.review(data, user)
      if (
        submittedMan.manuscript &&
        submittedMan.manuscript.status === 'repo-ready'
      ) {
        return ManuscriptManager.update(
          { id: data.id, pdfDepositState: 'ADDING_CITATION' },
          user,
        )
      }
      return submittedMan
    },

    async retagManuscript(_, { data }, { user }) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }
      const rejectedMan = await ManuscriptManager.retag(data, user)
      return rejectedMan
    },
  },
}

module.exports = resolvers
