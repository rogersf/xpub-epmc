const rfr = require('rfr')
const logger = require('@pubsweet/logger')

const authentication = rfr('server/utils/authentication')
const { userMessage, resetPassword, licenseEmail } = rfr('server/email')
const {
  ManuscriptManager,
  UserManager,
  NoteManager,
  IdentityManager,
  PropManager,
} = rfr('server/xpub-model')

const resolvers = {
  Mutation: {
    async epmc_email(
      _,
      { manuscriptId, manuscriptVersion, to, subject, message, cc, bcc },
      { user },
    ) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }
      const uniqueTo = [...new Set(to)]
      const uniqueCC = cc
        ? [...new Set(cc.split(',').map(s => s.trim()))]
        : null
      // Get funder emails
      const { value: funders } = await PropManager.find('planSFunders')
      const funderEmails = funders.reduce((obj, f) => {
        obj[f.funderName] = f.exceptionEmail
        return obj
      }, {})
      // Get listed email addresses
      const sendTo = await Promise.all(
        uniqueTo.map(async userId => {
          if (userId === 'helpdesk') {
            return 'helpdesk@europepmc.org'
          } else if (funderEmails[userId]) {
            return funderEmails[userId]
          }
          return UserManager.findEmail(userId)
        }),
      )
      const content = { to, uniqueCC, subject, message }
      const note = {
        manuscriptId,
        manuscriptVersion,
        notesType: 'userMessage',
        content: JSON.stringify(content),
      }
      const sender = await UserManager.findEmail(user)
      // Send email
      await userMessage(
        sendTo,
        `${manuscriptId}: ${subject}`,
        message,
        null,
        uniqueCC,
        bcc ? sender : null,
      )
      // Create a note
      await NoteManager.create(note, user)
      return true
    },
    async epmc_emailException(_, { manuscriptId, license: l }, { user }) {
      const manuscript = await ManuscriptManager.findById(manuscriptId, user)
      const { meta, teams, status, version } = manuscript
      const { notes, title } = meta
      const exceptionEmail =
        notes &&
        notes
          .slice()
          .reverse()
          .find(
            n =>
              n.notesType === 'userMessage' &&
              n.content.includes(
                '"subject":"License policy exception confirmation requested"',
              ),
          )
      const manInfo = { id: manuscriptId, title, status }
      if (exceptionEmail) {
        const license = l.toUpperCase().replace(/\//g, ' ')
        const emailSent = JSON.parse(exceptionEmail.content)
        const funder = emailSent.to.filter(t => t !== 'helpdesk')
        const from = exceptionEmail.updatedBy
        const person = teams.reduce((obj, t) => {
          const [p] = t.teamMembers
          const { user, alias } = p
          if (user.id === from) {
            return { id: user.id, name: alias.name }
          }
          return obj
        }, {})
        if (person) {
          person.email = await UserManager.findEmail(person.id)
          const { subject, html } = await licenseEmail(
            person,
            funder,
            license,
            manInfo,
          )
          const content = { to: [from], subject, message: html }
          const note = {
            manuscriptId,
            manuscriptVersion: version,
            notesType: 'userMessage',
            content: JSON.stringify(content),
          }
          await NoteManager.create(note, user)
          return true
        }
      }
      return true
    },
    async epmc_emailPasswordResetLink(_, { email }, ctx) {
      const user = await UserManager.findByEmail(email)
      if (!user) {
        throw new Error('Email not registered')
      }
      const token = authentication.token.create({
        email,
        id: user.id,
      })

      // Update password reset token
      const identity = user.identities.reduce((id, identity) => {
        if (identity.email.toLowerCase() === email.toLowerCase()) {
          return identity
        }
        return id
      }, undefined)

      if (!identity) {
        throw new Error(`No user is associated with email ${email}`)
      }

      let numUpdated = 0
      try {
        // identity.passwordResetToken = token
        // await IdentityManager.save(identity)
        numUpdated = await IdentityManager.model.updatePasswordResetToken(
          identity.id,
          token,
        )
      } catch (error) {
        logger.error('Error updating password reset token')
        throw error
      }
      if (numUpdated !== 1) {
        logger.error('Failed updating password reset token')
        return false
      }

      await resetPassword(email, user, token)
      return true
    },
  },
}

module.exports = resolvers
