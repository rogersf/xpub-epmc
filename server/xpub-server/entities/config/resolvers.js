const rfr = require('rfr')

const { JobManager, PropManager } = rfr('server/xpub-model')

const resolvers = {
  Query: {
    async getJobs(_, vars, { user }) {
      if (!user) {
        throw new Error('Not logged in')
      }
      return JobManager.selectAll()
    },
    async checkJob(_, { name }, { user }) {
      if (!user) {
        throw new Error('Not logged in')
      }
      return JobManager.find(name)
    },
    async getProps(_, vars, { user }) {
      if (!user) {
        throw new Error('Not logged in')
      }
      return PropManager.selectAll()
    },
    async findProp(_, { name }, { user }) {
      return PropManager.find(name)
    },
  },

  Mutation: {
    async updateJob(_, { data }, { user }) {
      if (!user) {
        throw new Error('Not logged in')
      }
      return JobManager.update(data, user)
    },
    async updateProp(_, { data }, { user }) {
      if (!user) {
        throw new Error('Not logged in')
      }
      return PropManager.update(data, user)
    },
  },
}

module.exports = resolvers
