const resolvers = {
  Identity: {
    __resolveType: () => 'Local',
  },
}

module.exports = resolvers
