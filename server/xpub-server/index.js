const { merge } = require('lodash')
const fs = require('fs')

const filterFileName = filename => entity =>
  fs.existsSync(`${__dirname}/entities/${entity}/${filename}`)

const entities = [
  'object',
  'file',
  'review',
  'manuscript',
  'journal',
  'note',
  'team',
  'role',
  'user',
  'identity',
  'grist',
  'email',
  'audit',
  'config',
]

// concatenate schemas
const xpubTypeDefs = fs.readFileSync(
  `${__dirname}/schema/xpub.graphqls`,
  'utf8',
)
const epmcTypeDefs = fs.readFileSync(
  `${__dirname}/schema/epmc.graphqls`,
  'utf8',
)

const entityTypeDefs = entities
  .filter(filterFileName('typeDefs.graphqls'))
  .map(name =>
    fs.readFileSync(`${__dirname}/entities/${name}/typeDefs.graphqls`, 'utf8'),
  )
const typeDefs = `
  ${xpubTypeDefs}
  ${epmcTypeDefs}
  ${entityTypeDefs.join('\n\n')}
`

// merge resolvers
const resolvers = merge(
  {},
  ...entities
    .filter(filterFileName('resolvers.js'))
    .map(name => require(`./entities/${name}/resolvers.js`)),
)

const registerRoutes = app => {
  entities
    .filter(filterFileName('routes.js'))
    .forEach(name => require(`./entities/${name}/routes.js`)(app))
}

module.exports = {
  backend: () => registerRoutes,
  typeDefs,
  resolvers,
  migrationsPath: `./schema/migrations`,
}
