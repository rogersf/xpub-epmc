const logger = require('@pubsweet/logger')
const superagent = require('superagent')
require('superagent-proxy')(superagent)
const { checkJobStatus, uncaughtError } = require('../job-runner')

const PrivacyNotice = require('../xpub-model/entities/privacyNotice/data-access')

if (!process.env.ENABLE_CRONJOB_PRIVACYNOTICECHECK) {
  logger.info(
    'ENABLE_CRONJOB_PRIVACYNOTICECHECK not defined. privacyNoticeCheck cronjob exits.',
  )
  process.exit(0)
}

process
  .on('uncaughtException', async (err, origin) => {
    logger.error(`Uncaught Exception thrown: ${err}`)
    logger.error('Exception thrown at: ', origin)
    await uncaughtError('privacy-notice', `Uncaught Exception thrown: ${err}`)
    PrivacyNotice.knex && (await PrivacyNotice.knex().destroy())
    process.exit(1)
  })
  .on('unhandledRejection', async (reason, promise) => {
    logger.error('Unhandled Rejection: ', reason)
    logger.error('Rejected at promise: ', promise)
    await uncaughtError('privacy-notice', `Unhandled Rejection: ${reason}`)
    PrivacyNotice.knex && (await PrivacyNotice.knex().destroy())
    process.exit(1)
  })

const privacyNoticeCheck = async () => {
  await checkJobStatus('privacy-notice', async () => {
    const privacyNotice = await PrivacyNotice.selectLastVersion()
    const data = await getLatestVersion()
    const privacyNotesData = data.nodes[0].node
    if (privacyNotesData['version Count'] !== privacyNotice.version) {
      const latestPrivacyNotice = {
        version: privacyNotesData['version Count'],
        effectiveDate: new Date(privacyNotesData.changed.split('-')[0].trim()),
      }
      await PrivacyNotice.insert(latestPrivacyNotice)
      logger.info('Updated privacy notice version number.')
    } else {
      logger.info('No update needed.')
    }
  })
  await PrivacyNotice.knex().destroy()
  process.exit()
}

function getLatestVersion() {
  return new Promise((resolve, reject) => {
    const superagentRequest = superagent(
      'GET',
      'https://www.ebi.ac.uk/data-protection/privacy-notice/json/5d182831-5e99-4072-816b-380cda5f58df',
    )
    if (process.env.superagent_http_proxy) {
      superagentRequest.proxy(process.env.superagent_http_proxy)
      logger.info('Proxy configured: ', process.env.superagent_http_proxy)
    }
    superagentRequest.end((err, response) => {
      if (err) {
        logger.error(err)
        reject(err)
      } else {
        resolve(response.body)
      }
    })
  })
}

;(() => {
  privacyNoticeCheck()
})()

module.export = privacyNoticeCheck
