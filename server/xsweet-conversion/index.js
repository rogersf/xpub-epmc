const fs = require('fs')
const superagent = require('superagent')
const logger = require('@pubsweet/logger')
const mime = require('mime-types')
const uuidv4 = require('uuid/v4')
const path = require('path')
const files = require('../utils/files.js')
const tar = require('../utils/unTar.js')
const File = require('../xpub-model/entities/file/')

module.exports.xsweetConvert = async function xsweetConvert(
  fileUrl,
  manuscriptId,
  manuscriptVersion,
  userId,
  configUrl,
  trx,
) {
  try {
    logger.info(`Posting file to XSweet`)
    const fileResponse = await superagent
      .get(`${configUrl}${fileUrl}`)
      .parse(superagent.parse.image)
      .buffer()
    const fileBuffer = fileResponse.body
    const request = superagent('POST', `${configUrl}/convertDocxToHTML`)
      .type('form')
      .attach('docx', fileBuffer, `${manuscriptId}.docx`)
    const response = await request
    const { text } = response

    const tmpPath = await tar.createTempDir()
    fs.writeFileSync(`${tmpPath}/${manuscriptId}.html`, text)
    const fileInfo = getFileInfo(
      `${tmpPath}/${manuscriptId}.html`,
      manuscriptId,
      manuscriptVersion,
      userId,
    )
    const uuid = uuidv4()

    // upload to gridFs
    logger.info(
      `GRIDFS uploading file to Xsweet for manuscript ${manuscriptId} version ${manuscriptVersion}`,
    )
    await files.fileUpload(
      manuscriptId,
      manuscriptVersion,
      `${uuid}${fileInfo.extension}`,
      fileInfo.filename,
      fileInfo.url,
      fileInfo.mimeType,
    )

    // update object details for database upsert
    fileInfo.url = `/download/${uuid}${fileInfo.extension}`
    delete fileInfo.extension

    const existingFile = await File.findByManIdAndType(
      manuscriptId,
      'source',
      trx,
    )
    if (existingFile && existingFile[0]) {
      fileInfo.id = existingFile[0].id
    }
    await File.save(fileInfo, userId, trx)

    logger.info('HTML converted file has been uploaded to Minio and to db')

    await files.tidyUp(tmpPath)
  } catch (err) {
    if (err.response) {
      const { body, status } = err.response
      logger.error(`Connection error: ${status}`)
      logger.error(body)
    }
    logger.error('Conversion failed:', err)
    throw err
  }
}

function getFileInfo(filepath, manuscriptId, manuscriptVersion, userId) {
  const filename = filepath.substring(filepath.lastIndexOf('/') + 1)
  const fileExt = path.extname(filepath)
  const fileSize = fs.statSync(filepath).size

  const fileInfo = {
    url: filepath,
    filename,
    type: 'source',
    size: fileSize,
    extension: fileExt,
    mimeType: `${mime.contentType(fileExt)}`,
    manuscriptId,
    manuscriptVersion,
    updatedBy: userId,
  }
  return fileInfo
}
