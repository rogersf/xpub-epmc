const File = require('../xpub-model/entities/file/data-access')
const xsweetConverter = require('../xsweet-conversion/index')
const tar = require('../utils/unTar')
const fileUtil = require('../utils/files.js')
const logger = require('@pubsweet/logger')
const fs = require('fs')
const config = require('config')
const http = require('http')
const userUtil = require('../utils/user.js')

const { internalBaseUrl } = config.get('pubsweet-server')
const beforeUpdate = Date.now()

xsweetMigratedData().then(() => {
  logger.info(
    `Xsweet conversion for the migrated manuscripts was done in: ${timeDiff(
      beforeUpdate,
    )} minutes`,
  )
})

const streamPromise = stream =>
  new Promise((resolve, reject) => {
    stream.on('finish', () => {
      resolve('finish')
    })
    stream.on('error', error => {
      reject(error)
    })
  })

/*
 * Issue when using foreach with await - Using 'Promise.all' will create too many tasks in parallel
 * which can be too heavy for the CPU or memory.
 * for..of giving 'no-restricted-syntax' with the linter
 * */
async function xsweetMigratedData() {
  try {
    const migratedManuscripts = await getMigratedData()
    const tmpPath = await tar.createTempDir()
    const adminUser = await userUtil.getAdminUser()
    const numFiles = migratedManuscripts.length

    for (let i = 0; i < numFiles; i += 1) {
      /* eslint-disable no-await-in-loop */
      const filename = await fileUtil.getFilename(migratedManuscripts[i].url)
      const wstreamTmp = fs.createWriteStream(`${tmpPath}/${filename}`)
      const fileUrl = `${internalBaseUrl}${migratedManuscripts[i].url}`
      http.get(fileUrl, response => response.pipe(wstreamTmp))
      await streamPromise(wstreamTmp)
      const fileSize = fs.statSync(`${tmpPath}/${filename}`).size

      if (fileSize > 0) {
        logger.info(
          `Starting Xsweet conversion of ${migratedManuscripts[i].url}`,
        )
        await xsweetConverter.xsweetConvert(
          `${tmpPath}/${filename}`,
          migratedManuscripts[i].manuscriptId,
          adminUser.id,
        )
        logger.info(
          `XSweet conversion of ${filename} for ${migratedManuscripts[i].manuscriptId} is done.`,
        )
      }
    }
    fileUtil.tidyUp(tmpPath)
    logger.info(`${numFiles} docx files have been converted.`)
  } catch (err) {
    return { err }
  }
}

function getMigratedData() {
  return new Promise(async resolve => {
    const migratedData = await File.selectMigratedUnpublished()

    // get only manuscriptId and url
    const data = migratedData.map(el => {
      const picked = (({ manuscriptId, url }) => ({ manuscriptId, url }))(el)
      return picked
    })
    await File.knex().destroy()
    resolve(data)
  })
}

function timeDiff(beforeUpdate) {
  return Math.floor((Date.now() - beforeUpdate) / 60000)
}
