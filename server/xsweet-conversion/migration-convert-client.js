const File = require('../xpub-model/entities/file/data-access')
const tar = require('../utils/unTar')
const fileUtil = require('../utils/files.js')
const logger = require('@pubsweet/logger')
const fs = require('fs')
const config = require('config')
const http = require('http')
const userUtil = require('../utils/user.js')
const kue = require('kue')

const { internalBaseUrl } = config.get('pubsweet-server')
const beforeUpdate = Date.now()
const queue = kue.createQueue()

let countJobs = 0
let countSuccessJobs = 0
let countFailedJobs = 0

xsweetMigratedData().then(() => {
  logger.info('Jobs have been launched.')
})

const streamPromise = stream =>
  new Promise((resolve, reject) => {
    stream.on('finish', () => {
      resolve('finish')
    })
    stream.on('error', error => {
      reject(error)
    })
  })

queue.on('job complete', (id, result) => {
  if (countSuccessJobs + countFailedJobs === countJobs) {
    logger.info('Xsweet conversion of migrated files have finished.')
    logger.info(
      `${countSuccessJobs} docx files have successfully been converted.`,
    )
    logger.info(`${countFailedJobs} docx files have failed the conversion.`)
    logger.info(
      `Xsweet conversion for the migrated manuscripts was done in: ${timeDiff(
        beforeUpdate,
      )} minutes`,
    )
  }

  kue.Job.get(id, (err, job) => {
    if (err) return
    job.remove(err => {
      if (err) throw err
    })
  })
})

async function xsweetMigratedData() {
  try {
    const migratedManuscripts = await getMigratedData()
    const tmpPath = await tar.createTempDir()
    const adminUser = await userUtil.getAdminUser()

    migratedManuscripts.forEach(async el => {
      /* eslint-disable no-await-in-loop */
      const filename = await fileUtil.getFilename(el.url)
      const wstreamTmp = fs.createWriteStream(`${tmpPath}/${filename}`)
      const fileUrl = `${internalBaseUrl}${el.url}`
      http.get(fileUrl, response => response.pipe(wstreamTmp))
      await streamPromise(wstreamTmp)
      const fileSize = fs.statSync(`${tmpPath}/${filename}`).size

      if (fileSize > 0) {
        logger.info(`Starting Xsweet conversion of ${el.url}`)

        const job = queue
          .create('xsweet conversion', {
            title: `converting docx manuscript for ${el.manuscriptId}`,
            filePath: `${tmpPath}/${filename}`,
            manuscriptId: el.manuscriptId,
            userId: adminUser.id,
          })
          .priority('high')
          .attempts(3)
          .save(err => {
            if (!err) {
              logger.info(`Starting Job ID: ${job.id}`)
              countJobs += 1
            }
          })

        job
          .on('complete', () => {
            logger.info(
              `Job ID ${job.id} for Manuscript ${job.data.manuscriptId} has completed.`,
            )
            countSuccessJobs += 1
            logger.info(
              `${countSuccessJobs} jobs have successfully completed so far.`,
            )
          })
          .on('failed attempt', (errorMessage, doneAttempts) => {
            logger.info(
              `Job ID ${job.id} for Manuscript ${job.data.manuscriptId} has failed on attempt ${doneAttempts}.`,
            )
          })
          .on('failed', errorMessage => {
            logger.info(
              `Job ID ${job.id} for Manuscript ${job.data.manuscriptId} has failed.`,
            )
            logger.info(errorMessage)
            countFailedJobs += 1
            logger.info(`${countFailedJobs} jobs has failed so far.`)
          })
      }
    })
    // fileUtil.tidyUp(tmpPath)
  } catch (err) {
    return { err }
  }
}

function getMigratedData() {
  return new Promise(async resolve => {
    const migratedData = await File.selectMigratedUnpublished()

    // get only manuscriptId and url
    const data = migratedData.map(el => {
      const picked = (({ manuscriptId, url }) => ({ manuscriptId, url }))(el)
      return picked
    })
    await File.knex().destroy()
    resolve(data)
  })
}

function timeDiff(beforeUpdate) {
  return Math.floor((Date.now() - beforeUpdate) / 60000)
}
