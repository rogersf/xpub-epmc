/* eslint-disable no-unused-vars */
const config = require('config')
const logger = require('@pubsweet/logger')
const libxml = require('libxmljs')
const File = require('../xpub-model/entities/file')
const Note = require('../xpub-model/entities/note')
const Organization = require('../xpub-model/entities/organization')
const Manuscript = require('../xpub-model/entities/manuscript/data-access')
const { parsed, sendMail } = require('../email')
const { createManifest } = require('../ebi-integration/toEbi')
const { xmlChange, getTransform } = require('../xml-validation')
const { checkJobStatus, uncaughtError } = require('../job-runner')
const getUser = require('../utils/user.js')
const fileUtils = require('../utils/files.js')
const { syncOrResendToRepo } = require('../ebi-integration/app')

let adminUser = {}
const { url, system } = config['epmc-email']
const { baseUrl } = config.get('pubsweet-server')

if (!process.env.ENABLE_CRONJOB_SPECIALCHECKS) {
  logger.info(
    'ENABLE_CRONJOB_SPECIALCHECKS not defined. special-db-checks cronjob exits.',
  )
  process.exit(0)
}

process
  .on('uncaughtException', async (err, origin) => {
    logger.error(`Uncaught Exception thrown: ${err}`)
    logger.error('Exception thrown at: ', origin)
    await uncaughtError(
      'special-db-checks',
      `Uncaught Exception thrown: ${err}`,
    )
    process.exit(1)
  })
  .on('unhandledRejection', async (reason, promise) => {
    logger.error('Unhandled Rejection: ', reason)
    logger.error('Rejected at promise: ', promise)
    await uncaughtError('special-db-checks', `Unhandled Rejection: ${reason}`)
    process.exit(1)
  })
;(async () => {
  await checkJobStatus('special-db-checks', async () => {
    const updateTime = Date.now()
    logger.info('Checking for special update needs')
    adminUser = await getUser.getAdminUser()
    await repoResendCheck()
    await xmlChangeCheck()
    // await preprintsEmailCheck()
    // await dbUpdateCheck()
    logger.info(`Checks were finished in ${Date.now() - updateTime} ms`)
  })
  Manuscript.knex().destroy()
  process.exit()
})()

async function repoResendCheck() {
  logger.info(
    `Running script to silently resend manuscripts to the EBI repository`,
  )
  const manuscripts = await Manuscript.query()
    .where('ebiState', 'like', 'Repo resend%')
    .whereNull('deleted')
  return manuscripts.reduce(async (promise, m) => {
    await promise
    if (m.ebiState === 'Repo resend') {
      return syncOrResendToRepo(m.id, adminUser.id)
    } else if (m.ebiState === 'Repo resend force') {
      return createManifest(m.id, adminUser.id)
    }
  }, Promise.resolve())
}

async function xmlChangeCheck() {
  logger.info(`Running script to edit XMLs en masse`)
  const queue = await Manuscript.query()
    .where('ebiState', '=', 'Mass edit XML')
    .whereNull('deleted')
  if (queue.length && queue.length > 0) {
    // Run transform on single id to cache XSL
    const start = queue.pop()
    await xmlChange(start.id, start.version)
    // Run cached XSL on remainder of list
    if (queue.length && queue.length > 0) {
      let cachedXSL = getTransform('changeXML')
      await queue.reduce(async (promise, m) => {
        await promise
        return xmlChange(m.id, m.version, cachedXSL)
      }, Promise.resolve())
      cachedXSL = null
      return true
    }
  }
}

function followUpTemplate(id, salutation, title, link, existing, fortnight) {
  return `
  <p style="margin:-32px 0 -12px; text-align: right; font-size: 12px;">${id}</p>
  <p>Dear ${salutation}</p>
  <p>We recently emailed asking you to check and approve the formatting of your preprint, <b>${title}</b>. Because your preprint has a Creative Commons license, the full text will automatically be displayed on the Europe PMC website for people to read on ${fortnight}.</p>
  <p>If you would rather check and approve the web format, it can be made available for reading on the Europe PMC website within 24 hours. To check the preprint before it goes live, please log into ${
    existing ? ' your' : ' or create a'
  } Europe PMC plus account by clicking the button below.</p>
  <p style="text-align:center"><a style="display: inline-block; font-size: 20px; padding: 14px 18px 12px; background-color:#20699C; color: #fff; font-weight: 300; border-radius: 1px; text-decoration: none" href="${link}">Review my preprint display</a></p>
  <p>If you have questions, please see the <a href="https://plus.europepmc.org/user-guide/preprintfaqs" style="color:#20699C">Preprint FAQs</a>.</p>
  <p>Kind regards,</p>
  <p>The Europe PMC Helpdesk</p>
`
}

async function followUpEmail(reviewer, manInfo, token) {
  const { id, title } = manInfo
  const { title: t, givenNames: g, surname: s } = reviewer.name
  const salutation = `${t ? `${t} ` : ''}${g} ${s}`
  let existing = true
  let link = `${url}submission/${id}/review`
  if (token && !reviewer.id) {
    existing = false
    link = `${url}dashboard?accept=${token}`
  }
  const fortnightDate = new Date(Date.now() + 12096e5)
  const fortnight = `${fortnightDate.getUTCDate()} ${fortnightDate.toLocaleString(
    'en-GB',
    { month: 'long' },
  )}`
  const html = followUpTemplate(
    id,
    salutation,
    parsed(title),
    link,
    existing,
    fortnight,
  )
  if (html) {
    const subject = `Display of your preprint in the Europe PMC archive.`
    return sendMail(reviewer.email, subject, html, null, null, system)
  }
}

async function sendFollowUp(manuscript) {
  const { id, teams, notes } = manuscript
  const title = manuscript['meta,title']
  const revTeam = teams.find(team => team.roleName === 'reviewer')
  const revUser = revTeam && revTeam.users && revTeam.users[0]
  try {
    let reviewer = null
    let token = null
    if (revUser && revUser.id) {
      const { title, givenNames, surname, identities } = revUser
      const { email, userId } = identities[0]
      reviewer = { id: userId, name: { title, givenNames, surname }, email }
    } else {
      const revNote = notes.find(n => n.notesType === 'selectedReviewer')
      reviewer = revNote && JSON.parse(revNote.content)
      token = revNote && revNote.id
    }
    logger.info(`Sending email and updating ${id}`)
    if (reviewer.email) {
      await followUpEmail(reviewer, { id, title }, token)
    } else {
      logger.error(`No reviewer email for ${id}`)
    }
    return Manuscript.query()
      .patch({ ebiState: 'Open access preprint clock reset' })
      .where('id', id)
  } catch (err) {
    logger.error(`Unable to update ${id}: ${err.message}`)
    return Promise.resolve()
  }
}

async function preprintsEmailCheck() {
  logger.info(`Running script to reset clocks for CC preprints`)
  const orgId = await Organization.getOrganizationID(true)
  const manuscripts = await Manuscript.query()
    .alias('m')
    .joinRelation('notes', { alias: 'n' })
    .where('n.notesType', 'openAccess')
    .whereNull('n.deleted')
    .where('m.status', 'xml-review')
    .where('m.organizationId', '=', orgId)
    .where('m.ebiState', '=', 'Send update email')
    .whereNull('m.deleted')
    .eager('[notes, teams.users.identities]')
    .modifyEager('notes', e => {
      e.whereNull('deleted')
    })
    .modifyEager('teams', e => {
      e.whereNull('deleted')
    })
  return manuscripts.reduce(async (promise, m) => {
    await promise
    return sendFollowUp(m)
  }, Promise.resolve())
}

async function dbUpdate(manId, manVersion) {
  try {
    const [pf] = await File.findByManIdAndType(manId, 'metadata')
    const pfString = await fileUtils.fetchFile(baseUrl + pf.url)
    const xmlDoc = libxml.parseXml(pfString)
    // Check metadata file for open access
    const xlink = 'http://www.w3.org/1999/xlink'
    const ali = 'http://www.niso.org/schemas/ali/1.0/'
    const license = xmlDoc.get(
      '//descendant-or-self::license[contains(@xlink:href, "creativecommons.org") or contains(ali:license_ref, "creativecommons.org")]/descendant-or-self::*[self::ali:license_ref or attribute::xlink:href]',
      { xlink, ali },
    )
    if (license) {
      const licenseLink = license.get('@xlink:href', { xlink })
        ? license.get('@xlink:href', { xlink }).value()
        : license.text()
      const isOpen =
        licenseLink.search(
          /\/(publicdomain|by|by-nc|by-nd|by-sa|by-nc-sa|by-nc-nd)\//i,
        ) >= 0
      if (isOpen) {
        await Note.create(
          {
            notesType: 'openAccess',
            content: licenseLink,
            manuscriptId: manId,
            manuscriptVersion: manVersion,
          },
          adminUser.id,
        )
      }
    }
  } catch (e) {
    logger.error(`Could not update ${manId}. Error:`, e)
  }
  return true
}

async function dbUpdateCheck() {
  logger.info(
    `Running script to update database records based on file contents`,
  )
  const manuscripts = await Manuscript.query()
    .alias('m')
    .select('m.id', 'm.version')
    .joinRelation('files', { alias: 'f' })
    .where('f.type', 'metadata')
    .joinRelation('organization', { alias: 'o' })
    .where('o.name', 'Europe PMC Preprints')
    .whereNotExists(
      Manuscript.relatedQuery('notes').where('notes.notesType', 'openAccess'),
    )
    .whereNull('m.deleted')
  return manuscripts.reduce(async (promise, m) => {
    await promise
    return dbUpdate(m.id, m.version)
  }, Promise.resolve())
}
