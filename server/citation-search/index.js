const { hydraSearch } = require('./requests/hydraSearch')

module.exports = {
  backend: () => require('./app'),
  hydraSearch,
}
