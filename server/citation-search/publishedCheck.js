const logger = require('@pubsweet/logger')
const config = require('config')
const fetch = require('node-fetch')
const { transaction } = require('objection')
const { checkJobStatus, uncaughtError } = require('../job-runner')
const { ManuscriptManager } = require('../xpub-model/')
const FileAccess = require('../xpub-model/entities/file/data-access')
const NoteAccess = require('../xpub-model/entities/note/data-access')
const { completedEmail, completedPreprintEmail } = require('../email')
const { getAdminUser } = require('../utils/user')
const { token } = require('../utils/authentication')

const { baseUrl } = config.get('pubsweet-server')

if (!process.env.ENABLE_CRONJOB_PUBLISHEDCHECK) {
  logger.info(
    'ENABLE_CRONJOB_PUBLISHEDCHECK not defined. publishedCheck cronjob exits.',
  )
  process.exit(0)
}

;(async () => {
  await checkJobStatus(
    'published-check',
    async () => {
      const beforeUpdate = Date.now()
      await publishedCheck()
      logger.info(
        `Published check was finished in ${Date.now() - beforeUpdate} ms`,
      )
    },
    // FTP healthcheck
    true,
  )
  await NoteAccess.knex().destroy()
  process.exit(0)
})()

process
  .on('uncaughtException', async (err, origin) => {
    logger.error(`Uncaught Exception thrown: ${err}`)
    logger.error('Exception thrown at: ', origin)
    await uncaughtError('published-check', `Uncaught Exception thrown: ${err}`)
    NoteAccess.knex && (await NoteAccess.knex().destroy())
    process.exit(1)
  })
  .on('unhandledRejection', async (reason, promise) => {
    logger.error('Unhandled Rejection: ', reason)
    logger.error('Rejected at promise: ', promise)
    await uncaughtError('published-check', `Unhandled Rejection: ${reason}`)
    NoteAccess.knex && (await NoteAccess.knex().destroy())
    process.exit(1)
  })

// module.exports = async publishedCheck => {
async function publishedCheck() {
  const {
    manuscripts,
    preprints,
  } = await ManuscriptManager.findPublishCheckReady()
  const adminUser = await getAdminUser()
  const bearer = token.create(adminUser)
  await manuscripts.reduce(async (promise, m) => {
    await promise
    if (await checkEuropepmc(m.id, bearer)) {
      let claim = null
      const trx = await transaction.start(NoteAccess.knex())
      try {
        const notes = await NoteAccess.selectByManuscriptId(m.id, trx)
        const claimed = notes && notes.find(n => n.notesType === 'orcidClaim')
        if (claimed) claim = claimed.content
        await ManuscriptManager.update(
          {
            id: m.id,
            status: 'published',
            ncbiState: null,
          },
          adminUser.id,
          trx,
        )
        await trx.commit()
        logger.info(`${m.id} moved to 'published'`)
      } catch (err) {
        logger.error(err)
        if (trx) await trx.rollback()
        return Promise.resolve()
      }
      if (m.audits && m.audits.length > 0) {
        logger.info(`Previously 'published'; won't send email again`)
        return Promise.resolve()
      }
      const userEmails = getUserEmails(m.users)
      const manInfo = {
        id: m.id,
        pmcid:
          m['meta,articleIds'].find(id => id.pubIdType === 'pmcid') &&
          m['meta,articleIds'].find(id => id.pubIdType === 'pmcid').id,
        title: m['meta,title'],
        claim,
      }
      return completedEmail(userEmails, manInfo)
    }
    return Promise.resolve()
  }, Promise.resolve())
  await preprints.reduce(async (promise, p) => {
    await promise
    const pprid = p['meta,articleIds'].find(id => id.pubIdType === 'pprid')
    if (pprid && (await checkEuropepmc(pprid.id, bearer, true))) {
      let claim = null
      const trx = await transaction.start(NoteAccess.knex())
      try {
        const notes = await NoteAccess.selectByManuscriptId(p.id, trx)
        const claimed = notes && notes.find(n => n.notesType === 'orcidClaim')
        if (claimed) claim = claimed.content
        const files = await FileAccess.selectAllByManuscriptId(p.id, trx)
        const versions = [
          ...new Set(files.map(f => parseInt(f.manuscriptVersion, 10))),
        ].sort((a, b) => a - b)
        const highest = versions.slice(-1)
        await ManuscriptManager.update(
          {
            id: p.id,
            status: 'published',
            ebiState: null,
          },
          adminUser.id,
          trx,
        )
        await trx.commit()
        logger.info(`${p.id} version ${p.version} moved to 'published'`)
        if (p.version < highest) {
          await new Promise(resolve => setTimeout(resolve, 500))
          logger.info(`Submitting version ${highest} of ${p.id}`)
          await ManuscriptManager.submit(
            {
              id: p.id,
              status: 'submitted',
              version: Math.floor(highest),
            },
            adminUser.id,
          )
        }
      } catch (err) {
        logger.error(err)
        if (trx) await trx.rollback()
        return Promise.resolve()
      }
      if (
        p.audits &&
        p.audits.filter(e => e.manuscriptVersion === p.version).length > 0
      ) {
        logger.info(`Previously 'published'; won't send email again`)
        return Promise.resolve()
      }
      const userEmails = getUserEmails(p.users)
      const manInfo = {
        id: p.id,
        version: Math.floor(p.version),
        pprid: pprid.id,
        title: p['meta,title'],
        claim,
      }
      return completedPreprintEmail(userEmails, manInfo)
    }
    return Promise.resolve()
  }, Promise.resolve())
  return true
}

async function checkEuropepmc(id, bearer, preprint) {
  const query = preprint ? `EXT_ID:${id}%20AND%20SRC:PPR` : `AUTH_MAN_ID:${id}`
  const headers = new fetch.Headers({ Authorization: `Bearer ${bearer}` })
  const response = await fetch(`${baseUrl}/ebisearch?query=${query}`, {
    headers,
  })
  const results = await response.json()
  const { hitCount, resultList } = results
  if (hitCount === 1) {
    const [hit] = resultList.result
    return hit && hit.inEPMC === 'Y'
  }
  return false
}

const getUserEmails = users => {
  const uniqueUsers = users.reduce((array, curr) => {
    if (
      !array.some(u => u.id === curr.id) &&
      !curr.deleted &&
      curr.roles.some(
        role => role.name === 'reviewer' || role.name === 'submitter',
      )
    ) {
      array.push(curr)
    }
    return array
  }, [])
  const getEmails = uniqueUsers.reduce((array, curr) => {
    const emails = curr.identities.filter(i => !i.deleted).map(i => i.email)
    const newArray = array.concat(emails)
    return newArray
  }, [])
  return getEmails
}
