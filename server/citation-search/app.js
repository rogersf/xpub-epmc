const logger = require('@pubsweet/logger')
const superagent = require('superagent')
const config = require('config')
const libxml = require('libxmljs')
const { getCitation } = require('./getCitation')
require('superagent-proxy')(superagent)
const { callPmcIdConv } = require('./requests/idConv')

const eutilsApiKey = config.get('eutils-api-key') || null
const { dev } = config.get('epmc-email')

module.exports = app => {
  const authBearer = app.locals.passport.authenticate('bearer', {
    session: false,
  })

  app.post('/citation', authBearer, async (req, res) => {
    res.set({ 'Content-Type': 'application/json' })
    const citation = await getCitation(req.body)
    res.status(200).send(citation)
  })

  /**
   * Call PMC ID converter tool with multiple PMIDs or DOIs.
   *
   * https://www.ncbi.nlm.nih.gov/pmc/tools/id-converter-api/
   *
   */
  app.get('/idconv', authBearer, async (req, res) => {
    res.set({ 'Content-Type': 'application/json' })
    const { pmid, doi } = req.query
    const idType = pmid ? 'pmid' : 'doi'
    const ids = pmid || doi
    try {
      const converted = await callPmcIdConv(idType, ids)
      res.status(200).send({ records: converted })
    } catch (error) {
      logger.error(`IdConv error: ${error.message}`, error)
      res.status(400).send({ error })
    }
  })

  app.get('/hydra', authBearer, async (req, res) => {
    res.set({ 'Content-Type': 'application/json' })
    /* Available "search" field values:
    - pubmed_search_citation.1 (up to 15 results with score above 0.5)
    - pubmed_search_citation_top_20.1 (up to 20 results with any score)
    - citation (only the top result in case its score is > 0.5, otherwise no results)
    - pmc_citation.1 (up to 6 results with score above 0.5)
    - pmc_citation_top_6.1 (maximum of 6 results, any positive score) 
    _citation searches only search metadata (not abstract) */
    const { query, type } = req.query
    const url = `https://www.ncbi.nlm.nih.gov/projects/hydra/hydra_search.cgi?search=${type ||
      'pmc_citation.1'}&tool=europepmc&query=${encodeURIComponent(query)}`

    try {
      logger.info('Getting Hydra results')
      let superagentRequest = superagent('GET', url)
      if (process.env.superagent_http_proxy) {
        superagentRequest = superagentRequest.proxy(
          process.env.superagent_http_proxy,
        )
      }
      const response = await superagentRequest
      const xmlDoc = libxml.parseXml(response.text)
      const ids = xmlDoc.find('/HydraResponse/IdList/Id') || []
      res.status(200).send(ids.map(id => id.text()))
    } catch (error) {
      logger.error(`Hydra search error: ${error}`)
      res.status(400).send({ error })
    }
  })

  app.get('/ebisearch', authBearer, async (req, res) => {
    res.set({ 'Content-Type': 'application/json' })

    const { pmid, query } = req.query
    const search = query || `SRC:MED%20EXT_ID:${pmid}`
    const url = `https://www.ebi.ac.uk/europepmc/webservices/rest/search?query=${search}&resulttype=core&format=json`

    try {
      logger.info('Getting EBI MED result')
      let superagentRequest = superagent('GET', url)
      if (process.env.superagent_http_proxy) {
        superagentRequest = superagentRequest.proxy(
          process.env.superagent_http_proxy,
        )
      }
      const response = await superagentRequest
      res.status(200).send(response.body)
    } catch (error) {
      logger.error(`EBI search error: ${error}`)
      res.status(400).send({ error })
    }
  })

  app.get('/crossref', authBearer, async (req, res) => {
    res.set({ 'Content-Type': 'application/json' })

    const { doi } = req.query
    const search = doi
      ? `/${doi}`
      : `?${Object.keys(req.query)
          .map(key => `${key}=${req.query[key]}`)
          .join('&')}&mailto=${dev}`
    const url = `https://api.crossref.org/works${search}`

    try {
      logger.info('Getting CrossRef result')
      let superagentRequest = superagent('GET', url)
      if (process.env.superagent_http_proxy) {
        superagentRequest = superagentRequest.proxy(
          process.env.superagent_http_proxy,
        )
      }
      const response = await superagentRequest
      res.status(200).send(response.body)
    } catch (error) {
      logger.error(`CrossRef search error: ${error}`)
      res.status(400).send({ error })
    }
  })

  app.get('/eutils/esearch', authBearer, async (req, res) => {
    res.set({ 'Content-Type': 'application/json' })

    const { term, db, retstart, sort } = req.query
    const encodedTerm = encodeURIComponent(term)
    const url = `https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=${db}&term=${encodedTerm}${
      /^\d+$/.test(encodedTerm) ? '[uid]' : ''
    }+NOT+pubmed+books[filter]&sort=${sort}&retmode=json&retstart=${retstart}&retmax=25${
      eutilsApiKey ? `&api_key=${eutilsApiKey}` : ''
    }`

    try {
      logger.info('Querying ESearch')
      let superagentRequest = superagent('GET', url)
      if (process.env.superagent_http_proxy) {
        superagentRequest = superagentRequest.proxy(
          process.env.superagent_http_proxy,
        )
      }
      const response = await superagentRequest
      res.status(200).send(response.body)
    } catch (error) {
      logger.error(`EUtils error: ${error}`)
      res.status(400).send({ error })
    }
  })
  app.get('/eutils/efetch', authBearer, async (req, res) => {
    res.set({ 'Content-Type': 'application/xml' })

    const { id, db } = req.query
    const url = `https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=${db}&id=${id}&retmode=xml${
      eutilsApiKey ? `&api_key=${eutilsApiKey}` : ''
    }`

    try {
      logger.info('Querying EFetch')
      let superagentRequest = superagent('GET', url)
      if (process.env.superagent_http_proxy) {
        superagentRequest = superagentRequest.proxy(
          process.env.superagent_http_proxy,
        )
      }
      const response = await superagentRequest
      res.status(200).send(response.text.toString())
    } catch (error) {
      logger.error(`EUtils error: ${error}`)
      res.status(400).send(JSON.stringify({ error }))
    }
  })
  app.get('/eutils/esummary', authBearer, async (req, res) => {
    res.set({ 'Content-Type': 'application/json' })

    const { id, db } = req.query
    const url = `https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=${db}&id=${id}&version=2.0&retmode=json${
      eutilsApiKey ? `&api_key=${eutilsApiKey}` : ''
    }`

    try {
      logger.info('Getting ESummary')
      let superagentRequest = superagent('GET', url)
      if (process.env.superagent_http_proxy) {
        superagentRequest = superagentRequest.proxy(
          process.env.superagent_http_proxy,
        )
      }
      const response = await superagentRequest
      res.status(200).send(response.body)
    } catch (error) {
      logger.error(`EUtils error: ${error}`)
      res.status(400).send({ error })
    }
  })
}
