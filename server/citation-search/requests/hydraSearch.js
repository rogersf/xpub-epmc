const { default: axios } = require('axios')
const axiosRetry = require('axios-retry')
const config = require('config')
const logger = require('@pubsweet/logger')

const { baseUrl } = config.get('pubsweet-server')

const instance = axios.create({
  baseURL: baseUrl,
  timeout: 10000,
})

axiosRetry(instance, {
  retries: 3,
  retryDelay: axiosRetry.exponentialDelay,
  retryCondition: _error => true, // Retry on all failures
  onRetry: (retryCount, error, requestConfig) => {
    const {
      manuscriptConfig: { manuscriptId },
    } = requestConfig
    logger.error(`[${manuscriptId}] hydra search error:`, error)
    logger.info(`[${manuscriptId}] hydra search retry attempt: ${retryCount}`)
  },
  shouldResetTimeout: true,
})

/**
 * Issues a request to the xpub-epmc citation-search api
 */
const hydraSearch = async (manuscriptId, searchString, bearer) => {
  const url = `${baseUrl}/hydra?query=${encodeURIComponent(searchString)}`
  logger.info(`[${manuscriptId}] Request url: ${url}`)
  const resposne = await instance({
    method: 'get',
    url,
    headers: {
      Authorization: `Bearer ${bearer}`,
    },
    manuscriptConfig: {
      manuscriptId,
    },
  })
  if (resposne.status !== 200) {
    throw new Error('hydra search failed')
  }
  return resposne.data
}

module.exports = {
  hydraSearch,
}
