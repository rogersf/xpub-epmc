const moment = require('moment')
const JournalManager = require('../xpub-model/entities/journal/')

const isDate = d => {
  const obj = {
    jatsDate: {},
  }
  let date = moment.utc(d, 'YYYY MMM DD')
  if (date.isValid()) {
    obj.jatsDate.day = moment(date).format('DD')
  } else {
    date = moment.utc(d, 'YYYY MMM')
    if (!date.isValid) {
      return false
    }
  }
  obj.jatsDate.year = moment(date).format('YYYY')
  obj.jatsDate.month = moment(date).format('MM')
  obj.date = date.format()
  return obj
}

module.exports.getCitation = async function getCitation(result) {
  const publicationDates = []
  const articleIds = [
    {
      pubIdType: 'pmid',
      id: result.uid,
    },
  ]
  if (result.articleids) {
    const doi = result.articleids.find(i => i.idtype === 'doi')
    const pmc = result.articleids.find(i => i.idtype === 'pmc')
    doi &&
      articleIds.push({
        pubIdType: 'doi',
        id: doi.value,
      })
    pmc &&
      articleIds.push({
        pubIdType: 'pmcid',
        id: pmc.value,
      })
  }
  if (result.pubdate) {
    const ppub = { type: 'ppub' }
    const pIsDate = isDate(result.pubdate)
    if (pIsDate) {
      ppub.date = pIsDate.date
      ppub.jatsDate = pIsDate.jatsDate
    } else {
      ppub.date = moment.utc(result.pubdate.substring(0, 4), 'YYYY')
      ppub.jatsDate = {
        year: result.pubdate.substring(0, 4),
        season: result.pubdate.substring(5),
      }
    }
    publicationDates.push(ppub)
  }
  if (result.epubdate) {
    const epub = { type: 'epub' }
    const eIsDate = isDate(result.epubdate)
    if (eIsDate) {
      epub.date = eIsDate.date
      epub.jatsDate = eIsDate.jatsDate
    } else {
      epub.date = moment.utc(result.epubdate.substring(0, 4), 'YYYY')
      epub.jatsDate = {
        year: result.epubdate.substring(0, 4),
        season: result.epubdate.substring(5),
      }
    }
    publicationDates.push(epub)
  }
  const pages = result.pages && result.pages.split('-')
  const elocations = result.elocationid && result.elocationid.split(' ')
  const citationData = {
    meta: {
      title: result.title,
      volume: result.volume,
      issue: result.issue,
      location: {
        fpage: pages ? pages[0] : null,
        lpage: pages ? pages.pop() : null,
        elocationId: elocations
          ? elocations.reduce((id, curr) => {
              if (
                curr === 'doi:' ||
                /^10.\d{4,9}\/[-._;()/:A-Z0-9]+$/i.test(curr)
              )
                return id
              if (id === 'pii:') return curr.replace(/\.$/, '')
              return curr
            }, '')
          : null,
      },
      publicationDates,
      articleIds,
    },
  }
  if (result.nlmuniqueid) {
    const journal = await JournalManager.selectWithNLM(result.nlmuniqueid)
    citationData.journalId = journal.id
    citationData.meta.unmatchedJournal = null
  }
  return citationData
}
