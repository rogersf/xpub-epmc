const Client = require('ftp')
const config = require('config')
const fs = require('fs')
const os = require('os')
const Dom = require('xmldom').DOMParser
const logger = require('@pubsweet/logger')
const getUser = require('../utils/user.js')
const { ManuscriptManager } = require('../xpub-model')
const { errorDevEmail } = require('../email')
const { checkJobStatus, uncaughtError } = require('../job-runner')

if (!process.env.ENABLE_CRONJOB_FROMNCBI) {
  logger.info('ENABLE_CRONJOB_FROMNCBI not defined. fromNcbi cronjob exits.')
  process.exit(0)
}

const NCBI_RESPONSE_EXT = new RegExp(/\S+\.(l|w)d\.response\.xml$/i)
const ftp = new Client()

;(async () => {
  await checkJobStatus('from-ncbi', async () => {
    logger.info('running from NCBI check')
    const beforeUpdate = Date.now()
    try {
      await fromNcbi()
    } catch (err) {
      try {
        await errorDevEmail('receiving from NCBI FTP', err)
        logger.info('Error email from NCBI process sent succesfully ')
      } catch (errEmail) {
        logger.error(
          `Error during Error email from NCBI process sent ${errEmail}`,
        )
      }
      close(ftp)
      throw new Error(err)
    }
    close(ftp)
    logger.info(
      `from NCBI check was finished in ${Date.now() - beforeUpdate} ms`,
    )
  })
  process.exit(0)
})()

function close(ftp) {
  ftp.end()
  ftp = null
  logger.info('connection terminated')
}

process
  .on('uncaughtException', async (err, origin) => {
    logger.error(`Uncaught Exception thrown: ${err}`)
    logger.error('Exception thrown at: ', origin)
    close(ftp)
    await uncaughtError('from-ncbi', `Uncaught Exception thrown: ${err}`)
    process.exit(1)
  })
  .on('unhandledRejection', async (reason, promise) => {
    logger.error('Unhandled Rejection: ', reason)
    logger.error('Rejected at promise: ', promise)
    close(ftp)
    await uncaughtError('from-ncbi', `Unhandled Rejection: ${reason}`)
    process.exit(1)
  })

async function fromNcbi() {
  return new Promise((resolve, reject) => {
    const updatedManuscripts = []
    ftp.on('ready', () => {
      ftp.list(
        `/${config.get('ncbi-ftp')['receive-folder']}`,
        false,
        async (err, list) => {
          if (err) {
            close(ftp)
            reject(err)
          }
          const responseFiles = list.filter(file =>
            NCBI_RESPONSE_EXT.test(file.name),
          )
          if (!responseFiles.length) {
            close(ftp)
            logger.info('No files to load')
            // reject(new Error('No files to load'))
            resolve(updatedManuscripts)
          }
          let counter = 0
          responseFiles.forEach((file, index, array) => {
            const resType =
              (file.name.match(/\.ld\./) && 'loading') ||
              (file.name.match(/\.wd\./) && 'withdrawal')
            const remoteFilePath = `/${
              config.get('ncbi-ftp')['receive-folder']
            }/${file.name}`
            ftp.get(remoteFilePath, (err, stream) => {
              if (err) {
                logger.error(remoteFilePath)
                logger.error(err.message)
                counter += 1
                if (counter === array.length) {
                  resolve(updatedManuscripts)
                }
              } else if (!stream) {
                logger.error(remoteFilePath)
                logger.error(`No FTP get stream`)
                counter += 1
                if (counter === array.length) {
                  resolve(updatedManuscripts)
                }
              } else {
                const path = `${os.tmpdir()}/${file.name}`
                const writeStream = stream.pipe(fs.createWriteStream(path))
                writeStream.on('finish', () => {
                  processFile(path).then(response => {
                    updateManuscriptNcbiStatus(
                      file.name,
                      response,
                      resType,
                    ).then(updatedManuscript => {
                      updatedManuscripts.push(updatedManuscript)
                      if (!updatedManuscript) {
                        counter += 1
                        if (counter === array.length) {
                          resolve(updatedManuscripts)
                        }
                      } else {
                        ftp.rename(
                          remoteFilePath,
                          `${remoteFilePath}.processed`,
                          err => {
                            if (err) {
                              logger.error(err)
                            }
                            counter += 1
                            if (counter === array.length) {
                              resolve(updatedManuscripts)
                            }
                          },
                        )
                      }
                    })
                  })
                })
              }
            })
          })
        },
      )
    })
    ftp.on('error', () => reject(new Error('Unable to connect to NCBI FTP')))
    const { host, user, password } = config.get('ncbi-ftp')
    const keepalive = 10000
    const connTimeout = 60000
    const pasvTimeout = 60000
    ftp.connect({ host, user, password, keepalive, connTimeout, pasvTimeout })
  })
}

function processFile(path) {
  return new Promise((resolve, reject) => {
    fs.readFile(path, 'utf8', (err, data) => {
      if (err) {
        reject(err)
      }
      const doc = new Dom().parseFromString(data)
      if (doc) {
        const receipt = doc.getElementsByTagName('receipt')[0]
        resolve({
          status: parseInt(receipt.getAttribute('status'), 10),
          pmcid: receipt.getAttribute('pmcid'),
          formState: data.match(
            /==== BEGIN: STDERR ====(.*)==== END: STDERR ====/s,
          )
            ? data.match(/==== BEGIN: STDERR ====(.*)==== END: STDERR ====/s)[0]
            : data,
        })
      } else {
        reject(new Error('xml parse error'))
      }
    })
  })
}

async function updateManuscriptNcbiStatus(fileName, response, resType) {
  // filename example: ems76395.2019_01_01-08_30_06.ld.response.xml
  const manId = fileName.split('.')[0].toUpperCase()
  const adminUser = await getUser.getAdminUser()
  const manuscript = await ManuscriptManager.findById(manId, adminUser.id)
  if (!manuscript) {
    logger.info(`manuscript ${manId} not found in db`)
    return null
  }
  const { articleIds, fundingGroup } = manuscript.meta
  const manInput = { id: manId }
  if (response.status) {
    manInput.ncbiState = 'failure'
    manInput.formState = `NCBI ${resType} failed\n\n${response.formState}`
    manInput.status =
      resType === 'loading' ? 'repo-triage' : 'withdrawal-triage'
  } else {
    const newIds = articleIds.filter(
      aid => !['pre-pmc', 'pmcid'].includes(aid.pubIdType),
    )
    manInput.ncbiState = resType === 'withdrawal' ? 'withdrawn' : 'success'
    if (response.pmcid) {
      const newPMCID = response.pmcid.startsWith('PMC')
        ? response.pmcid
        : `PMC${response.pmcid}`
      newIds.push({ pubIdType: 'pmcid', id: newPMCID })
      manInput.meta = {
        articleIds: newIds,
      }
      if (
        resType !== 'withdrawal' &&
        manuscript.fundingState !== 'Grants linked'
      ) {
        manInput.fundingState = 'Linking grants'
      }
    }
  }
  if (resType === 'withdrawal' && !response.status) {
    if (fundingGroup.length > 0) {
      manInput.status = 'link-existing'
      const linkMan = await ManuscriptManager.update(manInput, adminUser.id)
      logger.info(`manuscript ${manId} successfully withdrawn`)
      return linkMan
    }
    await ManuscriptManager.update(manInput, adminUser.id)
    const deletedMan = await ManuscriptManager.delete(manId, adminUser.id)
    logger.info(`manuscript ${manId} successfully withdrawn`)
    return deletedMan
  }
  const newManuscript = await ManuscriptManager.update(manInput, adminUser.id)
  logger.info(`manuscript ${manId} updated`)
  return newManuscript
}
