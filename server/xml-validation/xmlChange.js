const libxml = require('libxmljs')
const fs = require('fs')
const uuidv4 = require('uuid/v4')
const logger = require('@pubsweet/logger')
const config = require('config')
const { transaction } = require('objection')
const BaseModel = require('@pubsweet/base-model')
const tar = require('../utils/unTar.js')
const fileUtils = require('../utils/files.js')
const getUser = require('../utils/user.js')
const Manuscript = require('../xpub-model/entities/manuscript')
const File = require('../xpub-model/entities/file')
const { transform } = require('./transform')

const { baseUrl } = config.get('pubsweet-server')
const knex = BaseModel.knex()

const xmlChange = async function xmlChange(manId, version, xslt = 'changeXML') {
  const adminUser = await getUser.getAdminUser()
  const userId = adminUser.id
  const trx = await transaction.start(knex)
  try {
    const [xml] = await File.findByManIdAndType(manId, 'PMC', trx)
    if (xml) {
      const xmlString = await fileUtils.fetchFile(baseUrl + xml.url)
      const xmlIsWellformed = libxml.parseXml(xmlString)
      const { externalId, systemId } = await xmlIsWellformed.getDtd()
      const params = { externalId, systemId }
      const newXml = await transform(xmlString, xslt, trx, params)
      logger.info(`${manId} XML changed`)
      const tmpPath = await tar.createTempDir()
      const tmpFilePath = `${tmpPath}/${manId}.xml`
      fs.writeFileSync(tmpFilePath, newXml)
      const uuid = uuidv4()
      const mimeType = 'application/xml'
      const newPath = `/download/${uuid}.xml`
      logger.info(
        `GRIDFS Uploading file XML ${manId} for manuscipt ${manId} version ${version}`,
      )

      const fileSizeXML = fs.statSync(tmpFilePath).size
      await fileUtils.fileUpload(
        manId,
        version,
        `${uuid}.xml`,
        `${manId}.xml`,
        tmpFilePath,
        mimeType,
      )

      const fileInfo = {
        id: xml.id,
        url: newPath,
        filename: `${manId}.xml`,
        type: 'PMC',
        label: '',
        size: fileSizeXML,
        mimeType,
        manuscriptId: manId,
      }
      await File.save(fileInfo, userId, trx)
      logger.info('Changed XML uploaded')
      await trx.commit()
      await Manuscript.update(
        { id: manId, pdfDepositState: 'CONVERTING_XML' },
        userId,
      )
    } else {
      throw new Error('No XML file found')
    }
  } catch (err) {
    if (trx) {
      await trx.rollback()
    }
    await Manuscript.update(
      {
        id: manId,
        formState: `Error changing XML: ${err.message}`,
        pdfDepositState: null,
      },
      userId,
    )
    logger.error(err)
  }
}

module.exports = xmlChange
