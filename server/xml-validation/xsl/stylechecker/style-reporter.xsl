<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes"/>
  <xsl:param name="filename"/>
  <xsl:param name="showWarnings" select="'no'"/>

  <xsl:template match="ERR">
    <xsl:call-template name="reportmeat"/>
  </xsl:template>

  <xsl:template name="reportmeat">
    <div class="report-details">
      <xsl:if test="$filename">
        <p><strong>Filename: </strong><xsl:value-of select="$filename"/></p>
      </xsl:if>
      <xsl:if test="$showWarnings = 'yes' and //warning">
        <p><strong>Total warning count: </strong><xsl:value-of select="count(//warning)"/></p>
      </xsl:if>
      <p><strong>Total error count: </strong>
        <xsl:value-of select="count(//error)"/>
        <xsl:choose>
          <xsl:when test="count(//error) > 0"><span id="failNotify"/><br/><strong>Unique errors are listed below.</strong></xsl:when>
          <xsl:otherwise>
            <span id="passNotify"/>
          </xsl:otherwise>
        </xsl:choose>
      </p>
      <ol>        
        <xsl:apply-templates select="descendant::*[self::error or ($showWarnings = 'yes' and self::warning)]"/>
      </ol>
      <hr/>
      <ol>
        <pre style="white-space: pre-wrap;">
          <xsl:apply-templates mode="copy"/>
         </pre>
      </ol>
    </div>
  </xsl:template>

  <xsl:template match="error | warning">
    <xsl:variable name="nodepath">
      <xsl:call-template name="nodePath"/>
    </xsl:variable>
    <xsl:variable name="preceding-nodepath">
      <xsl:for-each select="preceding::*[self::error or self::warning][1]">
        <xsl:call-template name="nodePath"/>
      </xsl:for-each>
    </xsl:variable>
    <xsl:if test="$nodepath != $preceding-nodepath and string(node()) != string(preceding::*[self::error or self::warning][1])">
      <p>
        <a><xsl:attribute name="href"><xsl:text>#</xsl:text><xsl:call-template name="generate-id"/></xsl:attribute><xsl:value-of select="substring-before(substring-after($nodepath, '/ERR'), '/error')"/>:</a>
        <xsl:text> </xsl:text>

        <!--<xsl:if test="current()[self::warning]">
            <xsl:text> [warning] </xsl:text>
         </xsl:if>  -->

        <span>
          <xsl:attribute name="class">
            <xsl:choose>
              <xsl:when test="name() = 'warning'">
                <xsl:value-of select="'warning'"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="'error'"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:text>text</xsl:text>
          </xsl:attribute>
          <xsl:text>[</xsl:text>
          <xsl:value-of select="name()"/>
          <xsl:text>]</xsl:text>
        </span>
        <xsl:text> </xsl:text>
        <xsl:apply-templates/>
      </p>
    </xsl:if>

    <xsl:if test="$nodepath = $preceding-nodepath and string(node()) != string(preceding::*[self::error or self::warning][1])">
      <p>
        <a><xsl:attribute name="href"><xsl:text>#</xsl:text><xsl:call-template name="generate-id"/></xsl:attribute><xsl:value-of select="substring-before(substring-after($nodepath, '/ERR'), '/error')"/>:</a>
        <xsl:text> </xsl:text>

        <!--   <xsl:if test="current()[self::warning]">
            <xsl:text> [warning] </xsl:text>
         </xsl:if> -->

        <span>
          <xsl:attribute name="class">
            <xsl:choose>
              <xsl:when test="name() = 'warning'">
                <xsl:value-of select="'warning'"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="'error'"/>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:text>text</xsl:text>
          </xsl:attribute>
          <xsl:text>[</xsl:text>
          <xsl:value-of select="name()"/>
          <xsl:text>]</xsl:text>
        </span>
        <xsl:text> </xsl:text>
        <xsl:apply-templates/>
      </p>

    </xsl:if>
  </xsl:template>

  <xsl:template match="*" mode="copy">
    <xsl:text>&lt;</xsl:text>
    <xsl:value-of select="name()"/>
    <xsl:apply-templates select="@*" mode="copy"/>
    <xsl:text>&gt;</xsl:text>
    <xsl:apply-templates mode="copy"/>
    <xsl:text>&lt;/</xsl:text>
    <xsl:value-of select="name()"/>
    <xsl:text>&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="processing-instruction()" mode="copy">
    <xsl:choose>
      <xsl:when test="name() = 'SC-DETAILS' or name() = 'TITLE'"/>
      <xsl:otherwise>
        <xsl:text>&lt;?</xsl:text>
        <xsl:value-of select="name()"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="."/>
        <xsl:text>?&gt;</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="@*" mode="copy">
    <xsl:text> </xsl:text>
    <xsl:value-of select="name()"/>
    <xsl:text>="</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>"</xsl:text>
  </xsl:template>


  <xsl:template match="error" mode="copy">
    <span class="errortext">
      <xsl:attribute name="id">
        <xsl:call-template name="generate-id"/>
      </xsl:attribute>
      <xsl:text>[ERROR: </xsl:text>
      <xsl:apply-templates/>
      <xsl:text>]</xsl:text>
    </span>
  </xsl:template>

  <xsl:template match="warning" mode="copy">
    <span class="warningtext">
      <xsl:attribute name="id">
        <xsl:call-template name="generate-id"/>
      </xsl:attribute>
      <xsl:text>[WARNING: </xsl:text>
      <xsl:apply-templates/>
      <xsl:text>]</xsl:text>
    </span>
  </xsl:template>

  <xsl:template name="nodePath">
    <xsl:for-each select="ancestor-or-self::*">
      <xsl:choose>
        <xsl:when test="name() = 'warning'">
          <xsl:text>/error</xsl:text>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="concat('/', name())"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="tlink">
    <a href="{@target}" target="_new">
      <xsl:apply-templates/>
    </a>
  </xsl:template>

  <xsl:template name="generate-id">
    <!-- isolated so we can override it during testing -->
    <xsl:value-of select="generate-id()"/>
  </xsl:template>

</xsl:stylesheet>
