<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:mml="http://www.w3.org/1998/Math/MathML">

<!-- Test comment should appear in CVS version 1.9 -->

<!--	<xsl:output method="xml" omit-xml-declaration="yes" indent="no" encoding="UTF-8"
		doctype-public="-//NLM//DTD JATS (Z39.96) Journal Archiving and Interchange DTD v1.1 20151215//EN"
		doctype-system="JATS-journalpublishing1.dtd" />  -->

<xsl:output method="xml" omit-xml-declaration="yes" indent="no" encoding="UTF-8"
	doctype-public="-//NLM//DTD JATS (Z39.96) Journal Archiving and Interchange DTD v1.2 20190208//EN"
	doctype-system="JATS-archivearticle1.dtd" /> 


	<xsl:strip-space elements="*"/>
	<xsl:preserve-space elements="preformat title tex-math td p xref corresp note mixed-citation "/>

	<xsl:param name="jour" select="/article/front/journal-meta/journal-id[@journal-id-type='nlm-ta']"/>


	<!-- =================== -->
	<!-- Initialize the output files -->
	<xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>

	<xsl:template match="article" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xmlns:ali="http://www.niso.org/schemas/ali/1.0/">
		<article>
			<xsl:apply-templates select="@*"/>
			<!-- MS-3888: Want PI in all manuscripts, regardless -->
					<xsl:processing-instruction name="all-math-mml">
					<xsl:value-of select="'yes'"/>
					</xsl:processing-instruction>
					<xsl:processing-instruction name="use-mml"/>		
				
			<!--<xsl:if test="processing-instruction('all-math-mml')/text()='yes' or //processing-instruction('use-mml') or //processing-instruction('all-math-mml')">
				<xsl:processing-instruction name="all-math-mml">
					<xsl:value-of select="'yes'"/>
					</xsl:processing-instruction>
				<xsl:processing-instruction name="use-mml"/>
				</xsl:if>-->
		  
		  <xsl:apply-templates select="processing-instruction('properties')"/>
		  <xsl:apply-templates select="processing-instruction('origin')"/>
		  <xsl:apply-templates select="processing-instruction('move-footnotes')"/>

			<xsl:call-template name="write-response-subarticle-pis"/>

				
			<xsl:apply-templates select="front | body | back"/>
			<xsl:if test="not(back) and $jour='J Vis Exp'">
				<back>
					<fn-group>
						<xsl:call-template name="write-jove-footnote"/>
					</fn-group>
				</back>
				</xsl:if>
			<xsl:apply-templates select="floats-group | sub-article | response"/>
			<!-- process everything - then, if there is a sec[@sec-type="display-objects"] create a floats-group at the end of the doc -->
			<xsl:if test="//sec[@sec-type='display-objects']">
				<floats-group>
					<xsl:apply-templates select="//sec[@sec-type='display-objects']/fig">
						<xsl:sort
							select="translate(@id,'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYz','')"
							data-type="number"/>
					</xsl:apply-templates>
					<xsl:apply-templates select="//sec[@sec-type='display-objects']/table-wrap">
						<xsl:sort
							select="translate(@id,'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYz','')"
							data-type="number"/>
					</xsl:apply-templates>
				</floats-group>
			</xsl:if>
		</article>
	</xsl:template>

	<xsl:template match="processing-instruction('use-mml')"/>

	<xsl:template match="processing-instruction('all-math-mml')"/>

	<!-- ==============Templates from basic.xsl ===================-->
	<!-- ==================================================================== -->
	<!-- KEYS   -->
	<!-- ==================================================================== -->
	<!-- *** Retrieves all alternate-form-of attributes *** -->
	<xsl:key name="alternate-forms" match="@alternate-form-of" use="."/>

	<!-- ==================================================================== -->
	<!-- TEMPLATE: *|text() 
   
        Default action is to copy all elements and text
     -->

	<!-- ==================================================================== -->
	<xsl:template match="*|text()">
		<xsl:copy>
			<xsl:apply-templates select="*|@*|text()|processing-instruction()|comment()"/>
		</xsl:copy>
	</xsl:template>

	<!-- ==================================================================== -->
	<!-- TEMPLATE: @* 
   
        Default action is to copy all attributes
     -->
	<!-- ==================================================================== -->
	<xsl:template match="@*">
		<xsl:copy>
			<xsl:apply-templates select="*|@*|text()"/>
		</xsl:copy>
	</xsl:template>

	<!-- ==================================================================== -->
	<!-- TEMPLATE: *[self::publisher-loc or self::fpage
                 or self::lpage or self::caption] 
   
        Make sure following elements actually have content before copying them
        to output; if empty, the element will be suppressed. NOTE: Only include
        elements here when it is "ok" to suppress them. Some elements should 
        be output even when empty so that the data error will be found through
        style checking and corrected.
     -->
	<!-- ==================================================================== -->
	<xsl:template
		match="*[self::publisher-loc or 
                          self::fpage or 
                          self::lpage or
                          self::caption][not(parent::article-meta)]">
		<xsl:choose>
			<!-- Must have content somewhere in its descendants (child element
              is not enough since that child might be empty)-->
			<xsl:when test="self::caption">
				<xsl:if test="normalize-space() != ''">
					<xsl:copy>
						<xsl:apply-templates select="*|@*|text()"/>
					</xsl:copy>
				</xsl:if>
			</xsl:when>

			<!-- Check if the element has child elements or non whitespace text -->
			<xsl:otherwise>
				<!-- Has content: either elements or non-whitespace text nodes -->
				<xsl:if test="* or text()[normalize-space()]">
					<xsl:copy>
						<xsl:apply-templates select="*|@*|text()"/>
					</xsl:copy>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- ==================================================================== -->
	<!-- TEMPLATE: @dtd-version | @xml:lang[self::node()='EN']
                | @xml:lang[self::node()='en'] 
                | @xlink:type | @name-style[self::node()='western'] 
                | journal-id[@journal-id-type='pmc']
   
         These elements are ignored in the dump
     -->
	<!-- ==================================================================== -->
	<xsl:template
		match="@dtd-version
                      | @xml:lang[self::node()='EN' and parent::article]
                      | @xml:lang[self::node()='en' and parent::article] 
                      | @xlink:type
                      | @name-style[self::node()='western'] 
                      |journal-id[@journal-id-type='pmc']"/>
		
	<!-- ==================================================================== -->
	<!-- TEMPLATE: journal-title (2.3 to 3.0 )
     -->
	<!-- ==================================================================== -->
	
	<xsl:template match="journal-meta/journal-title">
		<journal-title-group>
			<xsl:apply-templates select="." mode="copy"/>
			<xsl:apply-templates select="../journal-subtitle" mode="copy"/>			
			<xsl:if test="../trans-title">
				<journal-title-group>
					<xsl:apply-templates select="../trans-title/@xml:lang"/>
					<xsl:apply-templates select="../trans-title | ../trans-subtitle" mode="copy"/>					
				</journal-title-group>
			</xsl:if>
			<xsl:apply-templates select="../abbrev-journal-title" mode="copy"/>			
		</journal-title-group>
	</xsl:template>
	
	<xsl:template match="
			  journal-meta/journal-subtitle 
			| journal-meta/trans-title 
			| journal-meta/trans-subtitle 
			| journal-meta/abbrev-journal-title
		"/>
	
	<xsl:template match="*" mode="copy">
		<xsl:element name="{name()}">
			<xsl:apply-templates select="@* | node()"/>
		</xsl:element>
	</xsl:template>

	<!-- ==================================================================== -->
	<!-- TEMPLATE: abstract
        
        Convert an abstract into a trans-abstract if the xml:lang attribute does
        not equal the xml:lang attribute of article
     -->
	<!-- ==================================================================== -->
	<xsl:template match="abstract">
		<xsl:variable name="lower" select="'abcdefghijklmnopqrstuvwxyz'"/>
		<xsl:variable name="upper" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
		<xsl:choose>
			<!-- Turn into a trans-abstract: note need to normalize the case -->
			<xsl:when
				test="@xml:lang and
               normalize-space(translate(@xml:lang,$lower,$upper)) 
            != normalize-space(translate(ancestor::front/parent::*[
                self::article or self::sub-article or self::response]/@xml:lang,
                $lower,$upper))">
				<trans-abstract>
					<xsl:copy-of select="@*"/>
					<xsl:apply-templates/>
				</trans-abstract>
			</xsl:when>
			<xsl:when test="@abstract-type='summary' and not(title)">
				<abstract>
					<title>Summary</title>
					<xsl:apply-templates/>
				</abstract>
			</xsl:when>
			<!-- Just copy out the abstract -->
			<xsl:otherwise>
				<xsl:copy>
					<xsl:apply-templates select="@*|node()"/>
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- ==================================================================== -->
	<!-- TEMPLATE: contrib
     -->
	<!-- ==================================================================== -->
	<xsl:template match="contrib">
		<xsl:copy>
			<!-- Copy out all the attributes except ones that need processing -->
			<xsl:copy-of
				select="@*[not(local-name() = 'corresp')
                             and not(local-name() = 'deceased')
                             and not(local-name() = 'equal-contrib')
                             and not(local-name() = 'rid')
                             and not(local-name() = 'type') 
                             and not(local-name() = 'contrib-type') ]"/>

			<!-- Now determine if these special attributes should be output -->
			<!-- @contrib-type if source is not author or editor -->
			<xsl:attribute name="contrib-type">
				<xsl:choose>
					<xsl:when test="@contrib-type='editor'">
						<xsl:value-of select="@contrib-type"/>
					</xsl:when>
					<xsl:when test="@contrib-type='reviewer'">
						<xsl:value-of select="@contrib-type"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>author</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>

			<!-- @corresp -->
			<xsl:choose>
				<xsl:when test="self::contrib//xref[@ref-type = 'corresp']">
					<!-- Don't copy @corresp if have an xref point to it -->
				</xsl:when>

				<xsl:when test="id(self::contrib//xref/@rid)[self::corresp]">
					<!-- Don't copy if have an xref that points to a corresp -->
				</xsl:when>

				<xsl:when test="id(self::contrib//xref/@rid)/self::fn[@fn-type = 'corresp']">
					<!-- Don't output the attribute -->
				</xsl:when>

				<xsl:otherwise>
					<xsl:copy-of select="@corresp"/>
				</xsl:otherwise>
			</xsl:choose>

			<!-- @equal-contrib -->
			<xsl:choose>
				<xsl:when test="id(self::contrib//xref/@rid)/self::fn[@fn-type = 'equal']">
					<!-- Don't output the attribute -->
				</xsl:when>

				<xsl:otherwise>
					<xsl:copy-of select="@equal-contrib"/>
				</xsl:otherwise>
			</xsl:choose>

			<!-- @deceased -->
			<xsl:choose>
				<xsl:when test="id(self::contrib//xref/@rid)/self::fn[@fn-type = 'deceased']">
					<!-- Don't output the attribute -->
				</xsl:when>

				<xsl:otherwise>
					<xsl:copy-of select="@deceased"/>
				</xsl:otherwise>
			</xsl:choose>

			<!-- @rid -->
			<xsl:choose>
				<xsl:when test="id(self::contrib/@rid)/self::fn[@fn-type = 'corresp']">
					<!-- Don't output the attribute -->
				</xsl:when>

				<xsl:otherwise>
					<xsl:copy-of select="@rid"/>
				</xsl:otherwise>
			</xsl:choose>

			<!-- Process all content -->
			<xsl:apply-templates/>
		</xsl:copy>
	</xsl:template>

	<!-- ==================================================================== -->
	<!-- TEMPLATE: ext-link  
     -->
	<!-- ==================================================================== -->
	<xsl:template match="ext-link">
		<!-- <xsl:message><xsl:value-of select="$document-uri"/>: In ext-ref.</xsl:message>
        -->

		<!-- Extract and clean up a URI if we can. -->
		<xsl:variable name="theHref">
			<xsl:choose>
				<xsl:when test="@xlink:href != ''">
					<xsl:call-template name="href-cleanup">
						<xsl:with-param name="href" select="@xlink:href"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when
					test="contains(.,'www.') or 
                         starts-with(., 'http://') or
                         starts-with(., 'ftp://') or
                         starts-with(., 'https://')">
					<xsl:call-template name="href-cleanup">
						<xsl:with-param name="href" select="string(.)"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text/>
					<!-- No href evident -->
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<!-- Calculate the appropriate href scheme prefix -->
		<xsl:variable name="xltype">
			<xsl:choose>
				<xsl:when test="starts-with($theHref, 'ftp://')">
					<xsl:text>ftp</xsl:text>
				</xsl:when>
				<xsl:when test="starts-with($theHref,'http') or starts-with($theHref,'www')">
					<xsl:text>uri</xsl:text>
				</xsl:when>
				<xsl:when test="@ext-link-type = 'url'">
					<xsl:text>uri</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@ext-link-type"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<!-- Style-checker reports a hard error for no xlink:href attribute, so
           don't even generate the ext-link in such cases. derose 2006-02-13.
        -->
		<xsl:choose>
			<!-- fail on placeholder links that may go somewhere unfortunate MS-3229. Does not 
			write it as a link-->
			<xsl:when test="contains($theHref,'xxx')">
				<xsl:value-of select="$theHref"/>
				</xsl:when>
			<xsl:when test="contains(.,'xxx')">
				<xsl:value-of select="."/>
				</xsl:when>
			<xsl:when test="normalize-space($theHref) != ''">
				<ext-link ext-link-type="{$xltype}" xlink:href="{$theHref}">
					<!-- Process other attributes and content -->
					<xsl:apply-templates
						select="@*[not(local-name() = 'ext-link-type')
                      and not(name() = 'xlink:href')]"/>
					<xsl:apply-templates select="*|text()"/>
				</ext-link>
			</xsl:when>

			<!-- Even if it's not a link, the content might be important. -->
			<xsl:otherwise>
				<xsl:apply-templates select="*|text()"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>


	<!-- ==================================================================== -->
	<!-- TEMPLATE: fpage[parent::article-meta]
        
        Process fpage inside of article-meta: must add in an lpage
        if not provided
     -->
	<!-- ==================================================================== -->
	<xsl:template match="fpage[parent::article-meta]">

		<!-- Copy out the fpage -->
		<xsl:copy-of select="."/>

		<!-- Now decide if we need to add a lpage -->
		<xsl:choose>
			<xsl:when test="following-sibling::lpage">
				<!-- Do nothing if already have an lpage-->
			</xsl:when>

			<!-- Mine the page-range for the lpage -->
			<xsl:when test="following-sibling::page-range">
				<lpage>
					<xsl:call-template name="get-lpage">
						<xsl:with-param name="content"
							select="normalize-space(string(following-sibling::page-range))"/>
					</xsl:call-template>
				</lpage>
			</xsl:when>

			<xsl:otherwise>
				<lpage>
					<xsl:value-of select="."/>
				</lpage>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- ==================================================================== -->
	<!-- TEMPLATE: graphic[ancestor::fig] | graphic[ancestor::table-wrap]
   
        Inside figs and tables, strip the extension from @xlink:href
     -->
	<!-- ==================================================================== -->
	<xsl:template match="graphic[ancestor::fig] | graphic[ancestor::table-wrap]">

		<xsl:variable name="file" select="@xlink:href"/>

		<graphic>
			<!-- Decide whether to output an alt-version attribute -->
			<xsl:if test="@alt-version = 'yes'">
				<xsl:choose>
					<!-- If this does not have an id, then it cannot be
                    referenced by an alternate-form-of attribute -->
					<xsl:when test="not(@id)">
						<!-- Do nothing -->
					</xsl:when>

					<!-- There is an alternate-form-of pointing to the graphic, so 
                    need to output this attribute -->
					<xsl:when test="key('alternate-forms', @id)">
						<xsl:copy-of select="@alt-version"/>
					</xsl:when>
				</xsl:choose>
			</xsl:if>

			<!-- Copy out any ids -->
			<xsl:copy-of select="@id"/>

			<xsl:attribute name="xlink:href">
				<xsl:choose>
					<xsl:when test="contains($file,'.gif')">
						<xsl:value-of select="substring-before($file,'.gif')"/>
					</xsl:when>
					<xsl:when test="contains($file,'.GIF')">
						<xsl:value-of select="substring-before($file,'.GIF')"/>
					</xsl:when>
					<xsl:when test="contains($file,'.tiff')">
						<xsl:value-of select="substring-before($file,'.tiff')"/>
					</xsl:when>
					<xsl:when test="contains($file,'.TIFF')">
						<xsl:value-of select="substring-before($file,'.TIFF')"/>
					</xsl:when>
					<xsl:when test="contains($file,'.tif')">
						<xsl:value-of select="substring-before($file,'.tif')"/>
					</xsl:when>
					<xsl:when test="contains($file,'.TIF')">
						<xsl:value-of select="substring-before($file,'.TIF')"/>
					</xsl:when>
					<xsl:when test="contains($file,'.eps')">
						<xsl:value-of select="substring-before($file,'.eps')"/>
					</xsl:when>
					<xsl:when test="contains($file,'.EPS')">
						<xsl:value-of select="substring-before($file,'.EPS')"/>
					</xsl:when>
					<xsl:when test="contains($file,'.jpg')">
						<xsl:value-of select="substring-before($file,'.jpg')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$file"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>

			<xsl:if test="@alternate-form-of">
				<xsl:attribute name="alternate-form-of">
					<xsl:value-of select="@alternate-form-of"/>
				</xsl:attribute>
			</xsl:if>

			<xsl:apply-templates/>
		</graphic>
	</xsl:template>

	<!-- ==================================================================== -->
	<!-- TEMPLATE: inline-graphic | graphic  -->
	<!-- ==================================================================== -->
	<xsl:template match="inline-graphic | graphic">

		<xsl:copy>
			<!-- Give special handling to any xlink:href attributes -->
			<xsl:if test="@xlink:href">
				<xsl:attribute name="xlink:href">
					<xsl:call-template name="normalize-xlink-href">
						<xsl:with-param name="value" select="@xlink:href"/>
					</xsl:call-template>
				</xsl:attribute>
			</xsl:if>

			<!-- Decide whether to output an alt-version attribute -->
			<xsl:if test="@alt-version = 'yes'">
				<xsl:choose>
					<!-- If this does not have an id, then it cannot be
                    referenced by an alternare-form-of attribute -->
					<xsl:when test="not(@id)">
						<!-- Do nothing -->
					</xsl:when>

					<!-- There is an alternate-form-of pointing to the graphic, so 
                    need to output this attribute -->
					<xsl:when test="key('alternate-forms', @id)">
						<xsl:copy-of select="@alt-version"/>
					</xsl:when>
				</xsl:choose>
			</xsl:if>

			<!-- Copy out everything else - but not the xlink:href, since this required 
               special handling to normalize; also do not copy out the
               alt-version attribute because need to check if there
               is truly an alternate-form attribute specified somewhere in
               the document. Would be better to simply output what is 
               in the source, but much legacy data for NAR has alt-version='yes'
               even though there is no alternate-form. Rather than have to pmcmod
               all these files, we build the test into the converter.
               -->
			<xsl:apply-templates select="*|@*[name()!='xlink:href' and name() != 'alt-version']|text()"/>
		</xsl:copy>
	</xsl:template>



	<!-- ==================================================================== -->
	<!-- TEMPLATE: element-citation
        
        nlm-citation must become citation because it is not defined in the archive article
        DTD. Get PMID information for all citations. -->

	<!-- changes from 2.3 
		1. Only look for element-citation - don't think we should get mixed citation for NIHMS
		2. Change @citation-type to @publication-type-->
	<!-- ==================================================================== -->
	<xsl:template match="element-citation">
		<element-citation>
			<xsl:attribute name="publication-type">

				<!-- Make sure there is a citation type and also normalize
              any existing value -->
				<xsl:choose>
					<xsl:when test="@publication-type">
						<xsl:choose>
							<xsl:when test="conf-name or conf-loc or @publication-type='confproc'"
								>confproc</xsl:when>
							<!-- added test for @citation-type="confproc" because some genuine "confproc" citations, without conf-name or conf-loc (MS-588) were being turned into books via the test for 'publisher-loc' below -->
							<xsl:when test="patent">patent</xsl:when>
							<xsl:when test="publisher or publisher-name or publisher-loc">book</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="@publication-type"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="conf-name">confproc</xsl:when>
							<xsl:when test="patent">patent</xsl:when>
							<xsl:when test="publisher or publisher-name or publisher-loc">book</xsl:when>
							<xsl:when test="volume">journal</xsl:when>
							<xsl:otherwise>other</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>

			<!-- Citations in a group or citations not inside a ref, must have
              an id attribute. Note that the id is apparently dropped if it was
              supplied but this is inside a ref and doesn't have any citation
              siblings. -->
			<!-- for 3.0 no nlm-citation or citation -->
			<xsl:if
				test="not(parent::ref) or preceding-sibling::element-citation 
            or preceding-sibling::mixed-citation 
            or following-sibling::element-citation 
            or following-sibling::mixed-citation">
				<xsl:call-template name="id-or-generate-id"/>
			</xsl:if>

			<!-- Special handling for citations inside manuscripts -->
			<xsl:choose>
				<xsl:when
					test="contains(//processing-instruction('properties'),'manuscript')
              and name(child::node()[1])= '' 
              and not(name(child::node()[position()!=1])='')
              and child::node()
              and string-length(child::node()[1]) &lt; 5">

					<!-- for citation in manuscripts where there is only one
                     text node and it is the first child, set it as label. -->
					<label>
						<xsl:call-template name="nopunct">
							<xsl:with-param name="str" select="normalize-space(child::node()[1])"/>
						</xsl:call-template>
					</label>

					<xsl:apply-templates select="*|text()[not(position()=1)]"/>
				</xsl:when>

				<xsl:otherwise>
					<xsl:apply-templates select="*|text()"/>
				</xsl:otherwise>
			</xsl:choose>

			<!-- Look up PMID if explicitly id'd as a journal article, or there is
              no publisher name and location (since books generally have these) -->
			<xsl:if
				test="@publication-type='journal'
                     or (not(publisher-name and publisher-loc)
                    and not(@publication-type='book'))">
				<xsl:choose>
					<xsl:when
						test="pub-id[@pub-id-type='pmid']/@xlink:href
                     or pub-id[@pub-id-type='pmid']/text()
                     or @publication-type='book' or @publication-type='other'"/>

					<xsl:otherwise>
						<!--NEED CONVERSION HELP HERE
			 
			         <xsl:call-template name="pmid-lookup-refs">
                     <xsl:with-param name="citation">
                        <xsl:value-of select="source"/>
                        <xsl:text>|</xsl:text>
                        <xsl:call-template name="nopunct">
                           <xsl:with-param name="str" select="year"/>
                        </xsl:call-template>
                        <xsl:text>|</xsl:text>
                        <xsl:value-of select="volume"/>
                        <xsl:text>|</xsl:text>
                        <xsl:value-of select="fpage"/>
                        <xsl:text>|</xsl:text>
                        <xsl:value-of select="current()//surname[1]"/>
                        <xsl:text>|</xsl:text>
                        <xsl:choose>
                           <xsl:when test="preceding-sibling::citation
                              or preceding-sibling::nlm-citation
                              or following-sibling::citation
                              or following-sibling::nlm-citation">
                              <xsl:value-of select="generate-id()"/>
                           </xsl:when>
                           
                           <xsl:when test="parent::ref/@id">
                              <xsl:value-of select="parent::ref/@id"/>
                           </xsl:when>
                           
                           <xsl:otherwise>
                              <xsl:value-of select="generate-id()"/>
                           </xsl:otherwise>
                        </xsl:choose>
                        
                        <xsl:text>|</xsl:text>
                     </xsl:with-param>
                  </xsl:call-template>  -->
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</element-citation>
	</xsl:template>
	
	<!-- ==================================================================== -->
	<!-- TEMPLATE: citation
        
	-->	
	<!-- ==================================================================== -->
	
	<xsl:template match="citation">
		<xsl:choose>
			<xsl:when test="
				  text()[normalize-space()] | bold | italic | monospace | overline
				| sans-serif | sc | strike | underline | inline-graphic | private-char
				| inline-formula | label | abbrev | milestone-end | milestone-start
				| named-content | email | ext-link | uri | elocation-id | etal | gov
				| isbn | institution | issn | issue-id | issue-title | name | object-id
				| page-range | role | std | volume-id				
			">
				<mixed-citation>
					<xsl:apply-templates select="@* | node()"/>
				</mixed-citation>
			</xsl:when>
			<xsl:otherwise>
				<nlm-citation>
					<xsl:apply-templates select="@*"/>
					<xsl:apply-templates select="person-group | collab"/>
					<xsl:apply-templates select="article-title | trans-title"/>
					<xsl:apply-templates select="source"/>
					<xsl:apply-templates select="patent"/>
					<xsl:apply-templates select="trans-source"/>
					<xsl:apply-templates select="year"/>
					<xsl:choose>
						<xsl:when test="month | day | timestamp">
							<xsl:apply-templates select="month"/>
							<xsl:apply-templates select="day"/>
							<xsl:apply-templates select="time-stamp"/>
						</xsl:when>
						<xsl:otherwise>							
							<xsl:apply-templates select="season"/>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:apply-templates select="access-date"/>
					<xsl:apply-templates select="volume"/>
					<xsl:apply-templates select="edition"/>
					<xsl:apply-templates select="conf-name"/>
					<xsl:apply-templates select="conf-date"/>
					<xsl:apply-templates select="conf-loc"/>
					<xsl:apply-templates select="issue | supplement"/>
					<xsl:apply-templates select="publisher-loc"/>
					<xsl:apply-templates select="publisher-name"/>
					<xsl:choose>
						<xsl:when test="fpage">							
							<xsl:for-each select="fpage">
								<xsl:apply-templates select="."/>
								<xsl:variable name="pos" select="position()"/>
								<xsl:for-each select="../lpage">
									<xsl:if test="position() = $pos">
										<xsl:apply-templates select="."/>
									</xsl:if>
								</xsl:for-each>
							</xsl:for-each>
						</xsl:when>
						<xsl:otherwise>
							<xsl:apply-templates select="lpage"/>
						</xsl:otherwise>
					</xsl:choose>					
					<xsl:apply-templates select="page-count"/>
					<xsl:apply-templates select="series"/>
					<xsl:apply-templates select="comment"/>
					<xsl:apply-templates select="pub-id"/>
					<xsl:apply-templates select="annotation"/>
				</nlm-citation>
			</xsl:otherwise>
		</xsl:choose>	
	</xsl:template>
	
	<xsl:template match="@citation-type">	
		<xsl:attribute name="publication-type">
			<xsl:value-of select="."/>
		</xsl:attribute>
	</xsl:template>
	

	<!-- ==================================================================== -->
	<!-- TEMPLATE: processing-instruction() -->
	<!-- ==================================================================== -->
 	<xsl:template match="processing-instruction()">
		<xsl:copy>
			<xsl:apply-templates select="*|@*|text()|processing-instruction()"/>
		</xsl:copy>
	</xsl:template>

	<!-- if you find a landscape PI ignore it - will need to change the PI to @orientation="landscape" for table-wrap, and fig? -->
	<xsl:template match="processing-instruction('landscape')"/>

	<!-- ==================================================================== -->
	<!-- TEMPLATE: season  -->
	<!-- ==================================================================== -->
	<xsl:template match="season">
		<xsl:variable name="guts" select="."/>

		<xsl:choose>
			<xsl:when test="number($guts) &gt; 0">
				<month>
					<xsl:value-of select="$guts"/>
				</month>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="find-month-season">
					<xsl:with-param name="month" select="$guts"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- ==================================================================== -->
	<!-- TEMPLATE: uri|self-uri
     -->
	<!-- ==================================================================== -->
	<xsl:template match="uri|self-uri">
		<xsl:copy>
			<xsl:copy-of select="@*[not(name() = 'xlink:href')]"/>

			<!-- Cleanup the href values to properly handle tildes -->
			<xsl:if test="@xlink:href">
				<xsl:attribute name="xlink:href">
					<xsl:call-template name="href-cleanup">
						<xsl:with-param name="href" select="@xlink:href"/>
					</xsl:call-template>
				</xsl:attribute>
			</xsl:if>

			<xsl:apply-templates/>
		</xsl:copy>
	</xsl:template>



	<!-- ==================================================================== -->
	<!-- TEMPLATE: address
        NOTE:     Merely delete if empty (as in some prevchrond)
     -->
	<!-- ==================================================================== -->
	<xsl:template match="address">
		<xsl:if test="normalize-space(.)!=''">
			<address><xsl:apply-templates/></address>
		</xsl:if>
	</xsl:template>
	
	
	
	<!-- ==================================================================== -->
	<!-- TEMPLATE: gloss-group
        NOTE:     does not exist in v3.0
     -->
	<!-- ==================================================================== -->

	<xsl:template match="gloss-group">
		<glossary>
			<xsl:attribute name="content-type">
				<xsl:text>gloss-group</xsl:text>
				<xsl:if test="normalize-space(@content-type) != ''">
					<xsl:value-of select="concat('-', @content-type)"/>
				</xsl:if>
			</xsl:attribute>
			<xsl:apply-templates select="@*  | node()"/>
		</glossary>
	</xsl:template>
	
	<xsl:template match="gloss-group/@content-type"/>
	
	<!-- ==================================================================== -->
	<!-- TEMPLATE: boxed-text title
        NOTE:     needs to be wrapped in caption in v3.0
     -->
	<!-- ==================================================================== -->
	
	<xsl:template match="boxed-text/title">
		<caption>
			<xsl:apply-templates select="." mode="copy"/>
		</caption>
	</xsl:template>
	

	<!-- ########################### NAMED TEMPLATES ########################### -->



	<!-- ==================================================================== -->
	<!-- TEMPLATE: get-lpage  
        
        Helper template retrieves the content to be used for the lpage
     -->
	<!-- ==================================================================== -->
	<xsl:template name="get-lpage">
		<xsl:param name="content"/>

		<xsl:choose>
			<xsl:when test="not($content)">
				<!-- All done -->
			</xsl:when>

			<!-- Has delimiters-->
			<xsl:when
				test="contains($content, ' ') or contains($content, '-')
                      or contains($content, ',')">
				<xsl:call-template name="get-lpage">
					<xsl:with-param name="content" select="normalize-space(substring($content, 2))"/>
				</xsl:call-template>
			</xsl:when>

			<xsl:otherwise>
				<xsl:value-of select="$content"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- ==================================================================== -->
	<!-- TEMPLATE: make-sec-type  
   
        Helper template creates a sec-type attribute for a section. This must be called 
        just after the <sec> is opened and before any content is created so that the 
        attribute is not orphaned. 
        
        Param:
           title-content = title content, if any
     -->
	<!-- ==================================================================== -->
	<xsl:template name="make-sec-type">
		<xsl:param name="title-content" select="''"/>
		<xsl:param name="existing-sec-type" select="''"/>

		<xsl:variable name="suggested-value">
			<xsl:call-template name="make-sec-type-value">
				<xsl:with-param name="title-value" select="$title-content"/>
			</xsl:call-template>
		</xsl:variable>

		<xsl:choose>
			<!-- We prefer the suggested value to any value provided in the source -->
			<xsl:when test="string-length($suggested-value) &gt; 0">
				<xsl:attribute name="sec-type">
					<xsl:value-of select="$suggested-value"/>
				</xsl:attribute>
			</xsl:when>

			<!-- If there is no suggested value but there is a provided value, we use that -->
			<xsl:when test="string-length($existing-sec-type) &gt; 0">
				<xsl:attribute name="sec-type">
					<xsl:value-of select="$existing-sec-type"/>
				</xsl:attribute>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<!-- ==================================================================== -->
	<!-- TEMPLATE: normalize-xlink-href
   
        Normalizes the content of the xlink:href attribute in inline-graphic elements by 
        removing file extensions. Any tifs and gif will become jpegs. All other types will 
        be output unchanged. Hrefs that have no extension will have a jpg added extension added. -->
	<!-- ==================================================================== -->
	<xsl:template name="normalize-xlink-href">
		<xsl:param name="value"/>

		<xsl:variable name="jpgExt" select="'.jpg'"/>

		<xsl:choose>
			<xsl:when test="contains($value,'.tiff')">
				<xsl:value-of select="concat(substring-before($value,'.tiff'), $jpgExt)"/>
			</xsl:when>
			<xsl:when test="contains($value,'.TIFF')">
				<xsl:value-of select="concat(substring-before($value,'.TIFF'), $jpgExt)"/>
			</xsl:when>
			<xsl:when test="contains($value,'.tif')">
				<xsl:value-of select="concat(substring-before($value,'.tif'), $jpgExt)"/>
			</xsl:when>
			<xsl:when test="contains($value,'.TIF')">
				<xsl:value-of select="concat(substring-before($value,'.TIF'), $jpgExt)"/>
			</xsl:when>
			<xsl:when test="contains($value,'.gif')">
				<xsl:value-of select="concat(substring-before($value,'.gif'), $jpgExt)"/>
			</xsl:when>
			<xsl:when test="contains($value,'.GIF')">
				<xsl:value-of select="concat(substring-before($value,'.GIF'), $jpgExt)"/>
			</xsl:when>
			<xsl:when test="contains($value,'.eps')">
				<xsl:value-of select="concat(substring-before($value,'.eps'), $jpgExt)"/>
			</xsl:when>
			<xsl:when test="contains($value,'.EPS')">
				<xsl:value-of select="concat(substring-before($value,'.EPS'), $jpgExt)"/>
			</xsl:when>
			<xsl:when test="not(contains($value, '.'))">
				<xsl:value-of select="concat($value, $jpgExt)"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$value"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- ==================================================================== -->
	<!-- TEMPLATE: publisher-jid  -->
	<!-- ==================================================================== -->
	<xsl:template name="publisher-jid">
		<xsl:apply-templates
			select="journal-id[@journal-id-type='publisher']
                                 | journal-id[@journal-id-type='publisher-id']"
		/>
	</xsl:template>

	<!-- ==================================================================== -->
	<!-- TEMPLATE: publisher-publisher -->
	<!-- ==================================================================== -->
	<xsl:template name="publisher-publisher">
		<xsl:apply-templates select="/article/front/journal-meta/publisher"/>
	</xsl:template>

	<!-- ==================================================================== -->
	<!-- TEMPLATE: display-object-test
   
        Helper template returns true if the element passed in as a parameter
        will be moved to the display-objects section; false, otherwise. -->
	<!-- ==================================================================== -->
	<xsl:template name="display-object-test">
		<xsl:param name="object"/>

		<!-- As rules diverge for the different elements, add new 'when' cases -->
		<xsl:choose>
			<xsl:when test="$object[self::media]">
				<!-- Test whether this element is going to be promoted to a fig:
            if it is only a media element, then it won't be moved at all -->
				<xsl:variable name="media-test">
					<xsl:call-template name="determine-media-category">
						<xsl:with-param name="element" select="$object"/>
					</xsl:call-template>
				</xsl:variable>

				<xsl:choose>
					<!-- If a media element or an anchored fig, don't move it -->
					<xsl:when
						test="$media-test = 'media'
                            or $media-test = 'anchored-fig'">
						<xsl:text>false</xsl:text>
					</xsl:when>

					<!--## In following cases, media element becomes a floating fig
                    and so we must decide whether to move it or not ##-->

					<!-- Don't move media in these contexts -->
					<xsl:when
						test="$object[
                  ancestor::table-wrap 
                  or ancestor::boxed-text 
                  or ancestor::app
                  or ancestor::supplementary-material]">
						<xsl:text>false</xsl:text>
					</xsl:when>

					<!-- Objects in sub-arts and responses are left in place (stop gap)-->
					<xsl:when
						test="$object[ancestor::sub-article
                                    or ancestor::response]">
						<xsl:text>false</xsl:text>
					</xsl:when>

					<!-- Move all other media (turned into floating fig) elements -->
					<xsl:otherwise>
						<xsl:text>true</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>

			<xsl:when
				test="$object[self::fig or self::fig-group
                              or self::table-wrap or self::table-wrap-group]">
				<xsl:choose>
					<!-- Table inside a fig should not be moved -->
					<xsl:when test="$object[self::table-wrap]/ancestor::fig">
						<xsl:text>false</xsl:text>
					</xsl:when>

					<!-- Object needs to follow its parent -->
					<xsl:when
						test="$object[parent::fig-group
                                    or parent::table-wrap-group]">
						<xsl:call-template name="display-object-test">
							<xsl:with-param name="object" select="$object/parent::*"/>
						</xsl:call-template>
					</xsl:when>

					<!-- Anchored objects are always anchored in place -->
					<xsl:when
						test="$object/@position='anchor'
                            or $object/@position='margin'">
						<xsl:text>false</xsl:text>
					</xsl:when>

					<!-- Leave objects inside boxed text in place -->
					<xsl:when test="$object[ancestor::boxed-text]">
						<xsl:text>false</xsl:text>
					</xsl:when>

					<!-- Objects in a sec inside back should be moved to a dedicated
                    display objects section if the sec they are in only consists
                    of diplay objects. -->
					<xsl:when test="$object/parent::sec/parent::back">
						<!-- Check if the sec contains any non-display objects -->
						<xsl:variable name="non-display-objects"
							select="$object/preceding-sibling::*[not(self::title)
                                  and not(self::fig) and not(self::table-wrap)
                                  and not(self::fig-group)
                                  and not(self::table-wrap-group)]
                           | $object/following-sibling::*[not(self::fig)
                                  and not(self::table-wrap)
                                  and not(self::fig-group)
                                  and not(self::table-wrap-group)]"/>
						<xsl:choose>
							<xsl:when test="$non-display-objects">
								<xsl:text>false</xsl:text>
							</xsl:when>

							<xsl:otherwise>
								<xsl:text>true</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>

					<!-- Other objects in back matter should be left in place
                      (otherwise might display strangely) -->
					<xsl:when test="$object/ancestor::back">
						<xsl:text>false</xsl:text>
					</xsl:when>

					<!-- Special processing (interim only): objects in response
                    and sub-articles are left in place -->
					<xsl:when
						test="$object/ancestor::response
                            or $object/ancestor::sub-article">
						<xsl:text>false</xsl:text>
					</xsl:when>

					<!-- Objects marked as floating are moved (this is
                    the default value from the DTD) -->
					<xsl:when test="$object/@position='float'">
						<xsl:text>true</xsl:text>
					</xsl:when>

					<!-- Default: when in doubt, leave in place -->
					<xsl:otherwise>
						<xsl:text>false</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>

			<!-- Everything else is not displayed in the special section -->
			<xsl:otherwise>
				<xsl:text>false</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>


	<xsl:template match="publisher-name">
		<xsl:if test="* or text()">
			<publisher-name>
				<xsl:apply-templates/>
			</publisher-name>
		</xsl:if>
	</xsl:template>


	<xsl:template match="italic">
		<xsl:choose>
			<xsl:when
				test="preceding-sibling::node() or
				preceding-sibling::text() or
				following-sibling::node() or
				following-sibling::text() or
				ancestor::def">
				<italic>
					<xsl:apply-templates/>
				</italic>
			</xsl:when>
			<xsl:when
				test="parent::article-title or
				parent::label or
				parent::named-content or 
				parent::p or
				parent::title">
				<xsl:apply-templates/>
			</xsl:when>
			<xsl:otherwise>
				<italic>
					<xsl:apply-templates/>
				</italic>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="bold">
		<xsl:choose>
			<xsl:when
				test="preceding-sibling::node() or
				preceding-sibling::text() or
				following-sibling::node() or
				following-sibling::text()">
				<bold>
					<xsl:apply-templates/>
				</bold>
			</xsl:when>
			<xsl:when
				test="parent::article-title or
				parent::kwd or 
				parent::label or
				parent::named-content or 
				parent::p[not(parent::fn)] or
				parent::term or 
				parent::title">
				<xsl:apply-templates/>
			</xsl:when>
			<xsl:otherwise>
				<bold>
					<xsl:apply-templates/>
				</bold>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- ==========================================================-->

	<xsl:template match="journal-meta">
		<journal-meta>
			<xsl:apply-templates
				select="//processing-instruction('jr-id') | //processing-instruction('nlm-journal-id')"
				mode="make-jid"/>
			<xsl:apply-templates/>
		</journal-meta>
	</xsl:template>


	<xsl:template match="processing-instruction('jr-id')" mode="make-jid">
		<journal-id journal-id-type="pubmed-jr-id">
			<xsl:value-of select="."/>
		</journal-id>
	</xsl:template>

	<xsl:template match="processing-instruction('nlm-journal-id')" mode="make-jid">
		<journal-id journal-id-type="nlm-journal-id">
			<xsl:value-of select="."/>
		</journal-id>
	</xsl:template>




	<xsl:template match="journal-title">
		<journal-title>
			<xsl:call-template name="nofinaldot">
				<xsl:with-param name="str" select="."/>
			</xsl:call-template>
		</journal-title>
	</xsl:template>



	<xsl:template match="article-meta">
		<article-meta>
			<xsl:call-template name="get-article-ids"/>
			<xsl:apply-templates
				select="article-id | article-version-alternatives | article-version | 
			                             article-categories |
												  title-group |
												  contrib-group |
												  aff |
												  author-notes |
												  pub-date"/>
			<xsl:call-template name="get-citation-info"/>
			<xsl:if test="permissions">
				<xsl:apply-templates select="permissions"/>
			</xsl:if>
			<xsl:call-template name="get-self-uri"/>
			<xsl:call-template name="get-relarts"/>
			<xsl:apply-templates
				select="abstract | trans-abstract |
			                             kwd-group"/>
		<!--  Commented this out of pnihms2pmc because there are no PIs to process within the NIHMS system 
			<xsl:call-template name="get-grant-nums">
				<xsl:with-param name="nodes" select="//processing-instruction('grant')"/>
			</xsl:call-template>
			<xsl:call-template name="get-grantors">
				<xsl:with-param name="nodes" select="//processing-instruction('grant')"/>
			</xsl:call-template> -->
			<xsl:apply-templates select="counts"/>


		</article-meta>
	</xsl:template>

	<xsl:template match="article-id[@pub-id-type='other']">
		<article-id pub-id-type="manuscript">
			<xsl:choose>
				<xsl:when test="//processing-instruction('wellcome') and contains(.,'nihms')">
					<xsl:text>ukpmcpa</xsl:text>
					<xsl:value-of select="substring-after(.,'nihms')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates/>
				</xsl:otherwise>
			</xsl:choose>
		</article-id>
	</xsl:template>


	<xsl:template name="get-article-ids">
		<!-- only write an article id if that type does not already exist in the article -->
		<xsl:if test="//processing-instruction('pmid') and not(article-id[@pub-id-type='pmid'])">
			<article-id pub-id-type="pmid">
				<xsl:value-of select="normalize-space(//processing-instruction('pmid'))"/>
			</article-id>
		</xsl:if>
		<xsl:if
			test="//processing-instruction('doi') and not(article-id[@pub-id-type='doi']) and string-length(normalize-space(//processing-instruction('doi'))) > 0">
			<article-id pub-id-type="doi">
				<xsl:value-of select="normalize-space(//processing-instruction('doi'))"/>
			</article-id>
		</xsl:if>
	</xsl:template>


	<xsl:template name="get-citation-info">
		<!-- only write citation info that does not already exist in the article -->
		<xsl:if test="//processing-instruction('pubdate') and not(pub-date[@pub-type='ppub'])">
			<!-- this is processing of the old PI that changes <?pubdate ?> to ppub -->
			<pub-date pub-type="ppub">
				<xsl:call-template name="get-date-from-string">
					<xsl:with-param name="date">
						<xsl:value-of select="normalize-space(//processing-instruction('pubdate'))"/>
					</xsl:with-param>
					<xsl:with-param name="order" select="'mdy'"/>
				</xsl:call-template>
			</pub-date>
		</xsl:if>
		<xsl:if test="//processing-instruction('epubdate') and not(pub-date[@pub-type='epub'])">
			<!-- <?epubdate ?> converts to epub -->
			<pub-date pub-type="epub">
				<xsl:call-template name="get-date-from-string">
					<xsl:with-param name="date">
						<xsl:value-of select="normalize-space(//processing-instruction('epubdate'))"/>
					</xsl:with-param>
					<xsl:with-param name="order" select="'mdy'"/>
				</xsl:call-template>
			</pub-date>
		</xsl:if>
		<xsl:if test="//processing-instruction('ppubdate') and not(pub-date[@pub-type='ppub'])">
			<!-- <?ppubdate ?> converts to pub and collection dates -->
			<pub-date pub-type="ppub">
				<xsl:choose>
					<xsl:when
						test="normalize-space(//processing-instruction('ppubseason')) and contains(normalize-space(//processing-instruction('ppubdate')),'--')">
						<season>
							<xsl:value-of select="normalize-space(//processing-instruction('ppubseason'))"
								disable-output-escaping="yes"/>
						</season>
						<year>
							<xsl:value-of
								select="substring-after(normalize-space(//processing-instruction('ppubdate')),'--')"
							/>
						</year>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="get-date-from-string">
							<xsl:with-param name="date">
								<xsl:value-of select="normalize-space(//processing-instruction('ppubdate'))"/>
							</xsl:with-param>
							<xsl:with-param name="order" select="'mdy'"/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</pub-date>
		</xsl:if>
		<xsl:if test="//processing-instruction('cpubdate') and not(pub-date[@pub-type='ppub'])">
			<!-- <?cpubdate ?> converts to pub and collection dates -->
			<pub-date pub-type="ppub">
				<xsl:call-template name="get-date-from-string">
					<xsl:with-param name="date">
						<xsl:value-of select="normalize-space(//processing-instruction('cpubdate'))"/>
					</xsl:with-param>
					<xsl:with-param name="order" select="'mdy'"/>
				</xsl:call-template>
			</pub-date>
			<pub-date pub-type="collection">
				<xsl:call-template name="get-date-from-string">
					<xsl:with-param name="date">
						<xsl:value-of select="normalize-space(//processing-instruction('cpubdate'))"/>
					</xsl:with-param>
					<xsl:with-param name="order" select="'mdy'"/>
				</xsl:call-template>
			</pub-date>
		</xsl:if>
		<xsl:if test="//processing-instruction('release-date')">
			<!-- <?ppubdate ?> converts to pub and collection dates -->
			<pub-date pub-type="pmc-release">
				<xsl:call-template name="get-date-from-string">
					<xsl:with-param name="date">
						<xsl:value-of select="normalize-space(//processing-instruction('release-date'))"/>
					</xsl:with-param>
					<xsl:with-param name="order" select="'mdy'"/>
				</xsl:call-template>
			</pub-date>
		</xsl:if>
		<!--
		<xsl:if test="normalize-space(//processing-instruction('vol'))!='' and not(volume)">
			<volume>
				<xsl:value-of select="normalize-space(//processing-instruction('vol'))"/>
			</volume>
		</xsl:if>
		<xsl:if test="normalize-space(//processing-instruction('iss'))!='' and not(issue)">
			<issue>
				<xsl:value-of select="normalize-space(//processing-instruction('iss'))"/>
			</issue>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="//processing-instruction('elocation-id')!=''">
				<elocation-id>
					<xsl:value-of select="normalize-space(//processing-instruction('elocation-id'))"/>
				</elocation-id>
			</xsl:when>
			<xsl:when test="//processing-instruction('fpage')!=''">
				<fpage>
					<xsl:value-of select="normalize-space(//processing-instruction('fpage'))"/>
				</fpage>
				<xsl:if
					test="//processing-instruction('lpage')
							and normalize-space(//processing-instruction('fpage'))!=''
							and not(lpage) and not(contains(//processing-instruction('lpage'),'nihms'))">
					<lpage>
						<xsl:value-of select="normalize-space(//processing-instruction('lpage'))"/>
					</lpage>
				</xsl:if>

			</xsl:when>
		</xsl:choose>-->
		<xsl:apply-templates select="volume|issue|elocation-id|fpage|lpage"/>
	</xsl:template>


	<xsl:template name="get-relarts">
		<xsl:for-each select="//processing-instruction('related-article')">
			<xsl:variable name="atts" select="normalize-space(.)"/>
			<related-article journal-id-type="nlm-ta"
				journal-id="{/article/front/journal-meta/journal-id[@journal-id-type='nlm-ta']}">
				<xsl:if test="$atts">
					<xsl:call-template name="get-relart-atts">
						<xsl:with-param name="nodes" select="$atts"/>
					</xsl:call-template>
				</xsl:if>
			</related-article>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="get-self-uri">
		<xsl:for-each select="//processing-instruction('fulltext-url')">
			<xsl:variable name="content" select="normalize-space(.)"/>
			<xsl:if test="$content!=''">
				<self-uri>
					<xsl:attribute name="xlink:href">
						<xsl:choose>
							<xsl:when test="contains($content,'http://')">
								<xsl:value-of select="$content"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="concat('http://',$content)"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
				</self-uri>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>


	<xsl:template name="get-relart-atts">
		<xsl:param name="nodes"/>
		<xsl:if test="$nodes">
			<xsl:choose>
				<xsl:when test="contains($nodes,' ')">
					<xsl:call-template name="make-att">
						<xsl:with-param name="attguts" select="normalize-space(substring-before($nodes,' '))"/>
					</xsl:call-template>
					<!-- and recurse -->
					<xsl:call-template name="get-relart-atts">
						<xsl:with-param name="nodes" select="normalize-space(substring-after($nodes,' '))"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="make-att">
						<xsl:with-param name="attguts" select="$nodes"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>

	<xsl:template name="make-att">
		<xsl:param name="attguts"/>
		<xsl:attribute name="{substring-before($attguts,'=')}">
			<xsl:value-of select="substring-after($attguts,'=')"/>
		</xsl:attribute>

	</xsl:template>

	<!-- template to toss out used PIs -->
	<xsl:template
		match="processing-instruction('pmid') |
	                     processing-instruction('doi') |
								processing-instruction('vol') |
								processing-instruction('iss') |
								processing-instruction('related-article') |
								processing-instruction('fpage') |
								processing-instruction('lpage') |
								processing-instruction('pubdate') |
								processing-instruction('ppubdate') |
								processing-instruction('epubdate') |
								processing-instruction('release-date') |
								processing-instruction('fulltext-url') |
								processing-instruction('cpubdate') |
								processing-instruction('jr-id') | 
								processing-instruction('submitter-name') |
								processing-instruction('nlm-journal-id')"/>

	<xsl:template match="article-meta/elocation-id"/>

	<!-- commenting these out of pnihms2pmc because there are no PIs to process within NIHMS 
	<xsl:template name="get-grant-nums">
		<xsl:param name="nodes"/>
		<xsl:if test="$nodes">
			<contract-num>
				<xsl:attribute name="rid">
					<xsl:choose>
						<xsl:when test="contains($nodes[1],'NIH')">
							<xsl:value-of select="'CS1'"/>
						</xsl:when>
						<xsl:when test="contains($nodes[1],'WT')">
							<xsl:value-of select="'CS2'"/>
						</xsl:when>
					</xsl:choose>
				</xsl:attribute>
				<xsl:value-of select="substring-after($nodes[1],'|')"/>
			</contract-num>
			<xsl:call-template name="get-grant-nums">
				<xsl:with-param name="nodes" select="$nodes[position() != 1]"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

	<xsl:template name="get-grantors">
		<xsl:param name="nodes"/>
		<xsl:param name="done"/>
		<xsl:if test="$nodes">
			<xsl:choose>
				<xsl:when test="contains($nodes[1],'NIH') and not(contains($done,'NIH'))">
					<contract-sponsor id="CS1">National Institutes of Health</contract-sponsor>
					<xsl:call-template name="get-grantors">
						<xsl:with-param name="nodes" select="$nodes[position() != 1]"/>
						<xsl:with-param name="done" select="concat($done,'NIH')"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="contains($nodes[1],'WT') and not(contains($done,'WT'))">
					<contract-sponsor id="CS2">Wellcome Trust</contract-sponsor>
					<xsl:call-template name="get-grantors">
						<xsl:with-param name="nodes" select="$nodes[position() != 1]"/>
						<xsl:with-param name="done" select="concat($done,'WT')"/>
					</xsl:call-template>
				</xsl:when>
			</xsl:choose>

		</xsl:if>
	</xsl:template> -->

	<!-- HERE ARE SOME CITATION CLEANUP TEMPLATES -->
	<xsl:template match="year[ancestor::ref]">
		<year>
			<xsl:apply-templates/>
		</year>
	</xsl:template>

	<xsl:template match="issue[ancestor::ref]">
		<issue>
			<xsl:apply-templates/>
		</issue>
	</xsl:template>

	<xsl:template match="source[ancestor::ref]">
		<source>
			<xsl:choose>
				<xsl:when test="*">
					<xsl:apply-templates/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="nofinaldot">
						<xsl:with-param name="str" select="."/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</source>
	</xsl:template>

	<xsl:template match="given-names[ancestor::ref]">
		<xsl:variable name="TEST">
			<xsl:call-template name="capitalize">
				<xsl:with-param name="str" select="."/>
			</xsl:call-template>
		</xsl:variable>
		<given-names>
			<xsl:choose>
				<xsl:when test="$TEST = .">
					<xsl:call-template name="nodot-nospace">
						<xsl:with-param name="str" select="."/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates/>
				</xsl:otherwise>
			</xsl:choose>
		</given-names>
	</xsl:template>


	<!-- 	<xsl:template match="sec[@sec-type='display-objects']">
		<sec id="{@id}" sec-type="display-objects">
			<xsl:apply-templates select="title"/>
			<xsl:apply-templates select="fig">
				<xsl:sort select="translate(@id,'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYz','')" data-type="number"/>
				</xsl:apply-templates>
			<xsl:apply-templates select="table-wrap">
				<xsl:sort select="translate(@id,'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYz','')" data-type="number"/>
				</xsl:apply-templates>
		</sec>
		</xsl:template> -->

	<!-- the content of this will be pulled in to a <floats-group> at the end of the document -->
	<xsl:template match="sec[@sec-type='display-objects']"/>

	<!-- new template to intercept <?landscape?> and turn it into @orientation for 3.0
		had to split the apply-templates up to match attributes and nodes separately because the DTD contains 
		a default value for @orientation -->
	<xsl:template match="table-wrap">
		<table-wrap>
			<xsl:apply-templates select="@*"/>
			<xsl:if test="processing-instruction('landscape')">
				<xsl:attribute name="orientation">landscape</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="node()"/>
		</table-wrap>
	</xsl:template>



	<!-- Stop stripping bold from citation to preserve meaning. Manuscript 3125. 3/2/2007 -->
	<!-- strip bold from citation -->
	<!--	<xsl:template match="bold[parent::citation]">
		<xsl:apply-templates/>
	</xsl:template> -->

	<xsl:template match="contract-num | contract-sponsor"/>

	<!-- send subject to title-case-string template -->
	<xsl:template match="subject">
		<subject>
			<xsl:call-template name="title-case">
				<xsl:with-param name="str" select="* | text()"/>
			</xsl:call-template>
		</subject>
	</xsl:template>


	<xsl:template match="comment()[contains(.,'private-char')]">
		<xsl:copy-of select="."/>
	</xsl:template>

	<xsl:template match="subject/sc">
		<xsl:apply-templates/>
	</xsl:template>

	<xsl:template match="processing-instruction('nihms')">
		<xsl:choose>
			<xsl:when test="//processing-instruction('wellcome')"/>
			<xsl:otherwise>
				<xsl:processing-instruction name="properties">
					<xsl:text>manuscript</xsl:text>
				</xsl:processing-instruction>
				<xsl:processing-instruction name="origin">
					<xsl:text>nihpa</xsl:text>
				</xsl:processing-instruction>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="processing-instruction('wellcome')">
		<xsl:processing-instruction name="properties">
			<xsl:text>manuscript</xsl:text>
		</xsl:processing-instruction>
		<xsl:processing-instruction name="origin">
			<xsl:text>ukpmcpa</xsl:text>
		</xsl:processing-instruction>
	</xsl:template>


	<!--=====================Templates from math.xsl ======================-->

	<!-- ==================================================================================== -->
	<!-- TEMPLATE: mml:annotation 
        
        Suppress annotations -->
	<!-- ==================================================================================== -->
	<xsl:template match="mml:annotation"/>

	<!-- ==================================================================================== -->
	<!-- TEMPLATE: mml:math  -->
	<!-- ==================================================================================== -->
	<xsl:template match="mml:math">

		<xsl:choose>
			<xsl:when test="//processing-instruction('nihms')">
				<xsl:copy>
					<xsl:apply-templates select="*|@*|text()|processing-instruction()"/>
				</xsl:copy>
			</xsl:when>

			<!-- Wrap block type equations in disp-formula to force them to render in a block. -->
			<xsl:when
				test="@display='block' and not(ancestor::disp-formula) and not(ancestor::inline-formula) ">
				<disp-formula>
					<xsl:call-template name="generate-id"/>
					

					<xsl:copy>
						<xsl:attribute name="id">
							<xsl:value-of select="@id"/>
						</xsl:attribute>

						<xsl:apply-templates/>
					</xsl:copy>
				</disp-formula>
			</xsl:when>

			<!-- If not block type, then will appear inline. -->
			<xsl:otherwise>
				<xsl:copy>
					<xsl:attribute name="id">
							<xsl:value-of select="@id"/>
					</xsl:attribute>

					<xsl:apply-templates/>
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- http://jira/browse/PMC-7967 -->
	<!-- All Depricated math attributes are replaced by their MathML 2.0 counterparts -->
	<xsl:template
		match="@*[local-name()='fontsize'
      or local-name()='fontweight'
      or local-name()='fontstyle'
      or local-name()='fontfamily'
      or local-name()='color'
      or local-name()='background'][parent::*[ancestor::*[local-name()='math']]]">
		<!--		<xsl:message>
         <xsl:text>&#xA;M2######DEBUG######DEBUG######DEBUG</xsl:text>
         <xsl:text>&#xA;</xsl:text>
         <xsl:value-of select="name()"/>
         <xsl:text>&#xA;</xsl:text>
         <xsl:value-of select="."/>
         <xsl:text>&#xA;######DEBUG######DEBUG######DEBUG</xsl:text>
         </xsl:message>-->
		<xsl:variable name="new-att-name">
			<xsl:choose>
				<xsl:when test="local-name(.)='fontsize'">
					<xsl:text>mathsize</xsl:text>
				</xsl:when>
				<xsl:when test="local-name(.)='fontweight'">
					<xsl:text>mathvariant</xsl:text>
				</xsl:when>
				<xsl:when test="local-name(.)='fontstyle'">
					<xsl:text>mathvariant</xsl:text>
				</xsl:when>
				<xsl:when test="local-name(.)='fontfamily'">
					<xsl:text>mathvariant</xsl:text>
				</xsl:when>
				<xsl:when test="local-name(.)='color'">
					<xsl:text>mathcolor</xsl:text>
				</xsl:when>
				<xsl:when test="local-name(.)='background'">
					<xsl:text>mathbackground</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>mathvariant</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="new-att-value">
			<xsl:choose>
				<xsl:when test="local-name(.)='fontsize'">
					<!-- CSS2: xx-small|x-small|small|smaller|medium|large|x-large|xx-large|larger|length|%|inherit -->
					<xsl:choose>
						<xsl:when
							test="contains('xx-small', string(.))
                     or contains('x-small', string(.))
                     or contains('small', string(.))
                     or contains('smaller', string(.))">
							<xsl:text>small</xsl:text>
						</xsl:when>
						<xsl:when test=". = 'medium'">
							<xsl:text>normal</xsl:text>
						</xsl:when>
						<xsl:when
							test="contains('large', string(.))
                     or contains('x-large', string(.))
                     or contains('xx-large', string(.))
                     or contains('larger', string(.))">
							<xsl:text>big</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="."/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:when test="local-name(.)='fontweight'">
					<!-- CSS2: normal|bold|bolder|lighter|100-900 (400=normal, 700=bold) -->
					<xsl:choose>
						<xsl:when test="(number(.) &gt; 699) or . = 'bold' or . = 'bolder'">
							<xsl:text>bold</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>normal</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:when test="local-name(.)='fontstyle'">
					<!-- CSS2: normal|italic|oblique|inherit -->
					<xsl:choose>
						<xsl:when test=". = 'italic'">
							<xsl:text>italic</xsl:text>
						</xsl:when>
						<xsl:when test=". = 'oblique'">
							<xsl:text>script</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>normal</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:when test="local-name(.)='fontfamily'">
					<!-- CSS2: family-name generic-family|inherit -->
					<!-- Serif|Sans-serif|Monospace -->
					<xsl:variable name="upper-str">
						<xsl:call-template name="capitalize">
							<xsl:with-param name="str" select="."/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:choose>
						<xsl:when test="contains($upper-str, 'SANS-SERIF') or contains($upper-str, 'ARIAL')">
							<xsl:text>sans-serif</xsl:text>
						</xsl:when>
						<xsl:when test="contains($upper-str, 'MONOSPACE')">
							<xsl:text>monospace</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>normal</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:when test="local-name(.)='color' or local-name(.)='background'">
					<!-- CSS2: family-name color_name|hex_number|rgb_number|inherit -->
					<!-- Color conversion Handled in the Math renderer -->
					<xsl:value-of select="."/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'normal'"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:attribute name="{$new-att-name}">
			<xsl:value-of select="$new-att-value"/>
		</xsl:attribute>
	</xsl:template>


	<!-- ==================================================================================== -->
	<!-- TEMPLATE: mml:semantics  
   
        Unwrap mml:semantics -->
	<!-- ==================================================================================== -->
	<xsl:template match="mml:semantics">
		<!-- MS-3482 -->
		<mml:mrow>
			<xsl:apply-templates/>
		</mml:mrow>		
	</xsl:template>

	<!-- ==================================================================================== -->
	<!-- TEMPLATE: tex-math  -->
	<!-- ==================================================================================== -->
	<xsl:template match="tex-math">
		<xsl:choose>
			<xsl:when test="contains(.,'\begin{document')">
				<xsl:copy>
					<xsl:attribute name="id">
						<xsl:value-of select="@id"/>
					</xsl:attribute>

					<xsl:apply-templates/>
				</xsl:copy>
			</xsl:when>

			<!-- in these next 4 tests, we need to replace eqnarray and equation with eqnarray* and equation* so that 
			     a (1) does not get generated by the Tex. 
				  
				  Also, in the two that test for a '%', the label has been stuck into the Tex inconsistently. We toss
				  this on conversion and rely on the label built with the @id.
				  
				  The only problem with this is multiple formula tagged in one disp-formula will get only one <label>
		   -->
			<xsl:when test="contains(.,'\begin{eqnarray}%')">
				<xsl:copy>
					<xsl:attribute name="id">
						<xsl:value-of select="@id"/>
					</xsl:attribute>

					<xsl:call-template name="insert-header"/>

					<xsl:text>\begin{eqnarray*} </xsl:text>
					<xsl:value-of
						select="substring-after(substring-after(substring-before(normalize-space(.),'\end{eqnarray}'),'\begin{eqnarray}'),' ')"/>
					<xsl:text>\end{eqnarray*}</xsl:text>
					<xsl:call-template name="fontend"/>
					<xsl:text>\end{document}</xsl:text>
				</xsl:copy>
			</xsl:when>

			<xsl:when test="contains(.,'\begin{eqnarray}')">
				<xsl:copy>
					<xsl:attribute name="id">
						<xsl:value-of select="@id"/>
					</xsl:attribute>

					<xsl:call-template name="insert-header"/>

					<xsl:text>\begin{eqnarray*}</xsl:text>
					<xsl:value-of
						select="substring-after(substring-before(normalize-space(.),'\end{eqnarray}'),'\begin{eqnarray}')"/>
					<xsl:text>\end{eqnarray*}</xsl:text>
					<xsl:call-template name="fontend"/>
					<xsl:text>\end{document}</xsl:text>
				</xsl:copy>
			</xsl:when>

			<xsl:when test="contains(.,'\begin{equation}%')">
				<xsl:copy>
					<xsl:attribute name="id">
						<xsl:value-of select="@id"/>
					</xsl:attribute>

					<xsl:call-template name="insert-header"/>

					<xsl:text>\begin{equation*} </xsl:text>
					<xsl:value-of
						select="substring-after(substring-after(substring-before(normalize-space(.),'\end{equation}'),'\begin{equation}'),' ')"/>
					<xsl:text>\end{equation*}</xsl:text>
					<xsl:call-template name="fontend"/>
					<xsl:text>\end{document}</xsl:text>
				</xsl:copy>
			</xsl:when>

			<xsl:when test="contains(.,'\begin{equation}')">
				<xsl:copy>
					<xsl:attribute name="id">
						<xsl:value-of select="@id"/>
					</xsl:attribute>

					<xsl:call-template name="insert-header"/>

					<xsl:text>\begin{equation*}</xsl:text>
					<xsl:value-of
						select="substring-after(substring-before(normalize-space(.),'\end{equation}'),'\begin{equation}')"/>
					<xsl:text>\end{equation*}</xsl:text>
					<xsl:call-template name="fontend"/>
					<xsl:text>\end{document}</xsl:text>
				</xsl:copy>
			</xsl:when>

			<xsl:otherwise>
				<xsl:copy>
					<xsl:attribute name="id">
						<xsl:value-of select="@id"/>
					</xsl:attribute>

					<xsl:call-template name="insert-header"/>

					<xsl:apply-templates/>
					<xsl:call-template name="fontend"/>
					<xsl:text>\end{document}</xsl:text>
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- ################################## NAMED TEMPLATES ################################## -->

	<!-- ==================================================================================== -->
	<!-- TEMPLATE: insert-header 
        
        Leave the indenting as-is! TeX is sensitive to whitespace. -->
	<!-- ==================================================================================== -->
	<xsl:template name="insert-header">
		<xsl:text>\documentclass[12pt]{minimal}
\usepackage{amsmath}
\usepackage{wasysym} 
\usepackage{amsfonts} 
\usepackage{amssymb} 
\usepackage{amsbsy}
\usepackage{mathrsfs}
\usepackage{upgreek}
\setlength{\oddsidemargin}{-69pt}
\begin{document}
</xsl:text>
		<xsl:if test="contains(.,'fontsize')">
			<xsl:text>{\fontsize{</xsl:text>
			<xsl:value-of select="substring-before(substring-after(.,'fontsize{'),'}')"/>
		</xsl:if>
		<xsl:text>}{</xsl:text>
		<xsl:value-of
			select="substring-before(substring-after(substring-after(.,'fontsize{'),'fontsize{'),'}')"/>
		<xsl:text>}</xsl:text>
	</xsl:template>


	<xsl:template name="fontend">
		<xsl:if test="contains(.,'fontsize')">
			<xsl:text>}</xsl:text>
		</xsl:if>
	</xsl:template>

	<!--===================================================================-->


	<!--======================Templates from mediaobject.xsl===============-->
	<!-- ==================================================================================== -->
	<!-- TEMPLATE: media 
   
     Template matches during regular processing. If media is supposed to be
     moved to a display-objects-section, then it is supressed; otherwise, it is output 
     in the current context.-->
	<!-- ==================================================================================== -->
	<xsl:template match="media">
		<!-- Call template to decide if this is being output here or not -->
		<xsl:variable name="move-to-display-section">
			<xsl:call-template name="display-object-test">
				<xsl:with-param name="object" select="."/>
			</xsl:call-template>
		</xsl:variable>

		<xsl:if test="$move-to-display-section = 'false'">
			<xsl:apply-templates select="." mode="output-media">
				<xsl:with-param name="position-value" select="'anchor'"/>
			</xsl:apply-templates>
		</xsl:if>
	</xsl:template>

	<!-- ==================================================================================== -->
	<!-- TEMPLATE: media 
        MODE: display-object-section
   
     Template matches during processing of display object section. If media is supposed to be
     moved to a display-objects-section, then it is output. -->
	<!-- ==================================================================================== -->
	<xsl:template match="media" mode="display-object-section">
		<!-- Call template to decide if this is being output here or not -->
		<xsl:variable name="move-to-display-section">
			<xsl:call-template name="display-object-test">
				<xsl:with-param name="object" select="."/>
			</xsl:call-template>
		</xsl:variable>

		<xsl:if test="$move-to-display-section = 'true'">
			<xsl:apply-templates select="." mode="output-media">
				<xsl:with-param name="position-value" select="'float'"/>
			</xsl:apply-templates>
		</xsl:if>
	</xsl:template>

	<!-- ==================================================================================== -->
	<!-- TEMPLATE: media
        MODE: output-media
        
        Outputs the media object when needed. Template takes a parameter specifying value of 
        the position attribute (will be determined by calling template).  -->
	<!-- ==================================================================================== -->
	<xsl:template match="media" mode="output-media">

		<xsl:param name="position-value" select="''"/>

		<xsl:variable name="media-test">
			<xsl:call-template name="determine-media-category">
				<xsl:with-param name="element" select="."/>
			</xsl:call-template>
		</xsl:variable>

		<xsl:choose>
			<!-- Becomes a fig -->
			<xsl:when test="$media-test = 'floating-fig' or $media-test= 'anchored-fig'">
				<fig>
					<xsl:call-template name="id-or-generate-id"/>


					<xsl:if test="string-length($position-value) &gt; 0">
						<xsl:attribute name="position">
							<xsl:value-of select="$position-value"/>
						</xsl:attribute>
					</xsl:if>

					<xsl:apply-templates select="label|caption"/>

					<media xlink:href="{@xlink:href}">
						<xsl:call-template name="get_mimetype">
							<xsl:with-param name="source" select="@xlink:href"/>
						</xsl:call-template>
					</media>

					<!-- Don't create alternate graphic for pdfs 
               <xsl:if test="not(contains(@xlink:href, '.pdf'))">
                  <graphic alternate-form-of="{generate-id()}">
                     <xsl:attribute name="xlink:href">
                        <xsl:call-template name="substring-before-last-dot">
                           <xsl:with-param name="str" select="@xlink:href"/>
                        </xsl:call-template>
                     </xsl:attribute>
                  </graphic>
               </xsl:if>-->
				</fig>

			</xsl:when>

			<!-- Copy out the media element -->
			<xsl:otherwise>
				<xsl:copy>
					<!-- Copy out all atttributes except position (will use the supplied value) -->
					<xsl:for-each select="@*[not( local-name() = 'position')]">
						<xsl:copy/>
					</xsl:for-each>

					<!-- Generate an id if one is not present -->
					<xsl:if test="not(@id)">
						<xsl:call-template name="generate-id"/>
					</xsl:if>

					<!-- Add the position attribute -->
					<xsl:if test="$position-value">
						<xsl:attribute name="position">
							<xsl:value-of select="$position-value"/>
						</xsl:attribute>
					</xsl:if>

					<!-- Now output the content -->
					<xsl:apply-templates/>
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- ######################### NAMED TEMPLATES ############################################# -->

	<!-- ==================================================================================== -->
	<!-- TEMPLATE: determine-media-category
        
        Helper determines what a media element in the source should become in the target:
        another media object, an anchored figure or a floating figure. Values returned will
        be: "media", "anchored-fig", "floating-fig".
        
        The rule per instructions by Beck is that an element that is floating OR that has 
        a caption OR that has a label should become a fig. Otherwise, it stays a media element.
        But wait, it's not so simple. media elements inside a fig should obviously not be
        promoted to fig and nor should media inside supplementary-material. 
        
        Note that this template will also need to be called by the display-object-test
        template because only floating figures should be moved to the back disp objects
        section. In other words, media elements that remain media elements won't be moved. -->
	<!-- ==================================================================================== -->
	<xsl:template name="determine-media-category">
		<xsl:param name="element"/>

		<xsl:choose>
			<!-- Inside a table-wrap: don't promote -->
			<!-- MS-3331 -->
			<xsl:when test="ancestor::table-wrap">
				<xsl:text>media</xsl:text>
			</xsl:when>
			
			<!-- Inside a supplementary material section: don't promote -->
			<xsl:when test="$element/parent::supplementary-material">
				<xsl:text>media</xsl:text>
			</xsl:when>

			<!-- Inside a fig: don't promote -->
			<xsl:when test="$element/ancestor::fig">
				<xsl:text>media</xsl:text>
			</xsl:when>

			<!-- Refers to a PDF: promote but anchor it -->
			<xsl:when test="contains($element/@xlink:href, '.pdf')">
				<xsl:text>anchored-fig</xsl:text>
			</xsl:when>

			<!-- Has label or caption: promote these (must decide whether is anchored or floating -->
			<xsl:when test="$element[label or caption]">
				<xsl:choose>
					<xsl:when test="$element[@position = 'float']">
						<xsl:text>floating-fig</xsl:text>
					</xsl:when>

					<xsl:otherwise>
						<xsl:text>anchored-fig</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>

			<!-- Floating media elements become figs as well -->
			<xsl:when test="$element/@position = 'float'">
				<xsl:text>floating-fig</xsl:text>
			</xsl:when>

			<!-- Anything else should stay a media object -->
			<xsl:otherwise>
				<xsl:text>media</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!--===================================================================-->



	<!--==================templates from suppdatata.xsl====================-->
	<!-- ==================================================================================== -->
	<!-- TEMPLATE: supplementary-material  -->
	<!-- ==================================================================================== -->
	<xsl:template match="supplementary-material">
		<supplementary-material>
			<xsl:attribute name="content-type">
				<xsl:choose>
					<xsl:when test="contains(@xlink:href,'http')">
						<xsl:value-of select="'external-data'"/>
					</xsl:when>

					<xsl:otherwise>
						<xsl:value-of select="'local-data'"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>

			<xsl:if test="@id">
				<xsl:attribute name="id">
					<xsl:value-of select="@id"/>
				</xsl:attribute>
			</xsl:if>

			<xsl:apply-templates select="label | caption"/>

			<xsl:choose>
				<xsl:when test="@mimetype">
					<xsl:call-template name="get-media"/>
				</xsl:when>

				<xsl:when test="*[not(self::label) and not(self::caption)]">
					<xsl:apply-templates select="*[not(self::label) and not(self::caption)]"/>
				</xsl:when>

				<xsl:otherwise>
					<xsl:text>NO MIMETYPE SUPPLIED - GET CONVERSION HELP</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</supplementary-material>
	</xsl:template>

	<!-- ################################## NAMED TEMPLATES ################################## -->

	<!-- ==================================================================================== -->
	<!-- TEMPLATE: get-media  -->
	<!-- ==================================================================================== -->
	<xsl:template name="get-media">
		<media xlink:href="{@xlink:href}">
			<xsl:call-template name="get_mimetype">
				<xsl:with-param name="source" select="@xlink:href"/>
			</xsl:call-template>

			<caption>
				<p>Click here for additional data file.</p>
			</caption>
		</media>
	</xsl:template>
	<!--===================================================================-->
	<!--==================templates from common area ======================-->
	<!-- ============================================= -->
	<!-- Template: make-sec-type-value
      Param: title-value: section title 
      
      Template returns the expected value of the
      sec-type attribute based on the provided
      section title.
      
      If section title is not "mapped", then the
      template returns nothing.
      
      Update this template if there are other section titles 
      that should be mapped universally across all
      journals. If, however, you have a title that applies
      only to a specific journal, then you should
      implement a local template that handles that
      case and then call this template 
      or the make-sec-type-attribute template as
      a default.-->
	<!-- ============================================= -->
	<xsl:template name="make-sec-type-value">
		<!-- Section title determines type of section -->
		<xsl:param name="title-value" select="''"/>

		<!-- Normalize to upper case for comparisons -->
		<xsl:variable name="normalized-value"
			select="translate(normalize-space($title-value), 'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>

		<xsl:choose>
			<!-- methods -->
			<xsl:when
				test="
            $normalized-value = 'CASES &amp; METHODS'
            or $normalized-value = 'CASES AND METHODS'
            or $normalized-value = 'EXPERIMENTAL PROCEDURE'
            or $normalized-value = 'EXPERIMENTAL PROCEDURES'
            or $normalized-value = 'METHOD'
            or $normalized-value = 'METHODOLOGY'
            or $normalized-value = 'METHODS'
            or $normalized-value = 'METHODS &amp; RESULTS'
            or $normalized-value = 'METHODS AND RESULTS'
            or $normalized-value = 'PATIENTS &amp; METHODS'
            or $normalized-value = 'PATIENTS AND METHODS'
            or $normalized-value = 'PROCEDURES'">
				<xsl:text>methods</xsl:text>
			</xsl:when>

			<!-- materials -->
			<xsl:when
				test="
            $normalized-value = 'MATERIAL &amp; PARTICIPANTS'
            or $normalized-value = 'MATERIAL AND PARTICIPANTS'
            or $normalized-value = 'MATERIALS'
            or $normalized-value = 'MATERIALS &amp; PARTICIPANTS'
            or $normalized-value = 'MATERIALS AND PARTICIPANTS'
            or $normalized-value = 'PARTICIPANTS &amp; MATERIALS'
            or $normalized-value = 'PARTICIPANTS AND MATERIALS'
            or $normalized-value = 'PATIENTS &amp; MATERIALS'
            or $normalized-value = 'PATIENTS AND MATERIALS'
            or $normalized-value = 'SUBJECTS &amp; MATERIALS'
            or $normalized-value = 'SUBJECTS AND MATERIALS'">
				<xsl:text>materials</xsl:text>
			</xsl:when>

			<!-- materials|methods -->
			<xsl:when
				test="
            $normalized-value = 'MATERIALS &amp; METHODS'
            or $normalized-value = 'MATERIALS _ENTITYSTART_#38_ENTITYEND_ METHODS'
            or $normalized-value = 'MATERIAL AND METHODS'
            or $normalized-value = 'MATERIALS AND METHODS'
            or $normalized-value = 'METHODS &amp; MATERIALS'
            or $normalized-value = 'METHODS AND MATERIALS'">
				<xsl:text>materials|methods</xsl:text>
			</xsl:when>

			<!-- signature -->
			<xsl:when
				test="
            $normalized-value = 'SIGNATURE'
            or $normalized-value = 'SIGNATURES'">
				<xsl:text>signature</xsl:text>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<!-- ======================================================== -->
	<!-- Replaces the ENTITYSTART string for a tilde with a 
      regular tilde keyboard character.
      
      This should be used when outputting xlink:href values 
      when the type is "uri" because escaped tildes do not
      operate correctly as an href link. -->
	<!-- ======================================================== -->
	<xsl:template name="href-cleanup">
		<xsl:param name="href" select="''"/>
		<!-- href value to clean -->

		<!-- Escaped tilde: this needs to be replaces with just a tilde -->
		<xsl:variable name="tilde" select="'_ENTITYSTART_#8764_ENTITYEND_'"/>

		<xsl:choose>
			<xsl:when test="string-length($href) = 0">
				<!-- All done -->
			</xsl:when>

			<!-- Has tilde to replace-->
			<xsl:when test="contains($href, $tilde)">
				<xsl:value-of select="substring-before($href, $tilde)"/>
				<xsl:text>~</xsl:text>

				<!-- Recurse on remainder of string -->
				<xsl:call-template name="href-cleanup">
					<xsl:with-param name="href" select="substring-after($href, $tilde)"/>
				</xsl:call-template>
			</xsl:when>

			<!-- Nothing left: return the string with spaces removed (MS-4004) -->
			<xsl:otherwise>
				<xsl:value-of select="translate(normalize-space($href),' ','')"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="title-case">
		<xsl:param name="str"/>
		<xsl:if test="$str">
			<xsl:choose>
				<xsl:when test="name($str[1])=''">
					<xsl:choose>
						<xsl:when test="$str[2]">
							<xsl:call-template name="title-case-string">
								<xsl:with-param name="str" select="$str[1]"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:choose>
								<xsl:when test="substring($str[1],  string-length($str[1]) )= '.'">
									<xsl:call-template name="title-case-string">
										<xsl:with-param name="str"
											select="substring($str[1], 1, string-length($str[1])-1)"/>
									</xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="title-case-string">
										<xsl:with-param name="str" select="$str[1]"/>
									</xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="$str[1]"/>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:call-template name="title-case">
				<xsl:with-param name="str" select="$str[position() != 1]"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

	<!-- Converts a phrase to title case -->
	<xsl:template name="title-case-string">
		<xsl:param name="str"/>
		<xsl:param name="mode"/>
		<xsl:param name="first" select="1"/>
		<xsl:if test="$str">
			<xsl:choose>
				<!-- Case 1: String has an emdash -->
				<xsl:when test="contains($str, '&#x02014;')">
					<!-- Title case the string before the emdash -->
					<xsl:call-template name="title-case-string">
						<xsl:with-param name="str" select="substring-before($str, '&#x02014;')"/>
						<xsl:with-param name="first" select="$first"/>
						<xsl:with-param name="mode" select="$mode"/>
					</xsl:call-template>

					<!-- Output the emdash -->
					<xsl:text>&#x02014;</xsl:text>

					<!-- Recurse on the remainder of string after endash -->
					<xsl:call-template name="title-case-string">
						<xsl:with-param name="str" select="substring-after($str,'&#x02014;')"/>
						<xsl:with-param name="mode" select="$mode"/>
						<!-- Note: no value passed for 'first' parameter; treat remainder 
						of string as if it were a new title -->
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="contains($str, '_ENTITYSTART_#8212_ENTITYEND_')">
					<!-- Title case the string before the emdash -->
					<xsl:call-template name="title-case-string">
						<xsl:with-param name="str"
							select="substring-before($str, '_ENTITYSTART_#8212_ENTITYEND_')"/>
						<xsl:with-param name="first" select="$first"/>
						<xsl:with-param name="mode" select="$mode"/>
					</xsl:call-template>

					<!-- Output the emdash -->
					<xsl:text>&#x02014;</xsl:text>

					<!-- Recurse on the remainder of string after endash -->
					<xsl:call-template name="title-case-string">
						<xsl:with-param name="str"
							select="substring-after($str,'_ENTITYSTART_#8212_ENTITYEND_')"/>
						<xsl:with-param name="mode" select="$mode"/>
						<!-- Note: no value passed for 'first' parameter; treat remainder 
						of string as if it were a new title -->
					</xsl:call-template>
				</xsl:when>

				<xsl:when test="contains($str, '&#x02013;')">
					<!-- Title case the string before the endash -->
					<xsl:call-template name="title-case-string">
						<xsl:with-param name="str" select="substring-before($str, '&#x02013;')"/>
						<xsl:with-param name="first" select="$first"/>
						<xsl:with-param name="mode" select="$mode"/>
					</xsl:call-template>

					<!-- Output the endash -->
					<xsl:text>&#x02013;</xsl:text>

					<!-- Recurse on the remainder of string after endash -->
					<xsl:call-template name="title-case-string">
						<xsl:with-param name="str" select="substring-after($str,'&#x02013;')"/>
						<xsl:with-param name="mode" select="$mode"/>
						<!-- Note: no value passed for 'first' parameter; treat remainder 
						of string as if it were a new title -->
					</xsl:call-template>
				</xsl:when>

				<!-- Case 1: String has a colon -->
				<xsl:when test="contains($str, ': ')">
					<!-- Title case the word before the colon -->
					<xsl:call-template name="title-case-string">
						<xsl:with-param name="str" select="substring-before($str, ': ')"/>
						<xsl:with-param name="first" select="$first"/>
						<xsl:with-param name="mode" select="$mode"/>
					</xsl:call-template>

					<!-- Output the colon space -->
					<xsl:text>: </xsl:text>

					<!-- Recurse on the remainder of string after colon -->
					<xsl:call-template name="title-case-string">
						<xsl:with-param name="str" select="substring-after($str,': ')"/>
						<xsl:with-param name="first" select="$first"/>
						<xsl:with-param name="mode" select="$mode"/>
					</xsl:call-template>
				</xsl:when>

				<!-- Case 3: String has space(s) -->
				<xsl:when test="contains($str,' ')">
					<xsl:call-template name="title-case-word">
						<xsl:with-param name="str" select="substring-before($str,' ')"/>
						<xsl:with-param name="first" select="$first"/>
						<xsl:with-param name="mode" select="$mode"/>
					</xsl:call-template>
					<xsl:text> </xsl:text>

					<xsl:call-template name="title-case-string">
						<xsl:with-param name="str" select="substring-after($str,' ')"/>
						<xsl:with-param name="first" select="0"/>
						<xsl:with-param name="mode" select="$mode"/>
					</xsl:call-template>
				</xsl:when>

				<!-- Case 4: No spaces, mdashes or colons, but has an underscore. Treat
                 this as an unchangeable string and end recursion. -->
				<xsl:when test="contains($str, '_')">
					<xsl:value-of select="$str"/>
				</xsl:when>

				<!-- Base case: no more spaces or emdashes, so is a single word to titlecase -->
				<xsl:otherwise>
					<xsl:call-template name="title-case-word">
						<xsl:with-param name="str" select="$str"/>
						<xsl:with-param name="mode" select="$mode"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>

	<!-- Converts a word to title case -->
	<xsl:template name="title-case-word">
		<xsl:param name="str"/>
		<xsl:param name="mode"/>
		<xsl:param name="first"/>
		<xsl:variable name="STR">
			<xsl:text>|</xsl:text>
			<xsl:call-template name="capitalize">
				<xsl:with-param name="str" select="$str"/>
			</xsl:call-template>
			<xsl:text>|</xsl:text>
		</xsl:variable>
		<xsl:variable name="punctuation">
			<xsl:text>({['"/:</xsl:text>
		</xsl:variable>
		<xsl:variable name="apos">
			<xsl:text>'</xsl:text>
		</xsl:variable>
		<xsl:variable name="nocap-words">
			<xsl:text>|A|AN|THE|AND|BUT|OR|NOR|FOR|ABOARD|ABOUT|ABOVE|ACROSS|AFTER|AGAINST|ALONG|AMID|AMONG|ANTI|AROUND|AS|AT|BEFORE|BEHIND|BELOW|BENEATH|BESIDE|BESIDES|BETWEEN|BEYOND|BOMBASTUS|BUT|BY|CONCERNING|CONSIDERING|DESPITE|DOWN|DURING|EXCEPT|EXCEPTING|EXCLUDING|FOLLOWING|FOR|FROM|IN|INTO|LIKE|MINUS|NEAR|OF|OFF|ON|ONTO|OPPOSITE|OUTSIDE|OVER|PER|PLUS|REGARDING|SAVE|SINCE|SO|THAN|THEOPHRASTUS|THROUGH|TO|TOWARD|TOWARDS|UNDER|UNDERNEATH|UNLIKE|UNTIL|UP|UPON|VERSUS|VIA|WITH|WITHIN|WITHOUT|A.M.|P.M.|bmj.com|</xsl:text>
		</xsl:variable>

		<xsl:variable name="cap-words">
			<xsl:text>|I.|II|II.|III|III.|IV|IV.|V|V.|VI|VI.|VII|VII.|VIII|VIII.|IX|IX.|X|X.|ACMG|A.G.M|AHRQ|AIDS|AMIA|ASA|ASHG|B.J.|BAEM|BJ|BMA|BMJ|BJ|BSG|CCDR|CCNP|CD|CD-ROM|CMA|CMAJ|CME|CRSN|CVMA|DNA|EMBO|ENSEMBL|FL|GSA|HIV|HSR|IAIMS|IQSY|ISA|ISCAIP|ISCB|JABA|JEAB|MLA|MP|MSSVD|N.A.S.|NAS|NCT|NIEHS|NLM|PNAS|PS|QUERI|RFLP|RNA|ROM|SAEM|SARS|SICS|STD|THI|TV|TWIB|U.K.|U.S.|UK|VA|WPA|</xsl:text>
			<!-- LK 2/22/06: POEM removed from cap-words list -->
		</xsl:variable>
		<xsl:variable name="jtitle-cap-words">
			<xsl:text>TAG.|</xsl:text>
		</xsl:variable>

		<xsl:variable name="noknockdown-words">
			<xsl:choose>
				<xsl:when test="$mode='jtitle'">
					<xsl:value-of select="concat($cap-words, $jtitle-cap-words)"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$cap-words"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>


		<xsl:variable name="keepasis-word">
			<xsl:call-template name="keepasis-words">
				<xsl:with-param name="str" select="$STR"/>
			</xsl:call-template>
		</xsl:variable>

		<xsl:if test="$str">
			<xsl:choose>
				<xsl:when test="contains($punctuation,substring($STR,2,1))">
					<xsl:value-of select="substring($STR,2,1)"/>
					<xsl:call-template name="title-case-word">
						<xsl:with-param name="str">
							<xsl:call-template name="strip-pipe">
								<xsl:with-param name="str" select="substring-after($STR,substring($STR,2,1))"/>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="mode" select="$mode"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="contains($STR,'-')">
					<xsl:call-template name="title-case-word">
						<xsl:with-param name="str">
							<xsl:call-template name="strip-pipe">
								<xsl:with-param name="str" select="substring-before($STR,'-')"/>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="mode" select="$mode"/>
					</xsl:call-template>
					<xsl:text>-</xsl:text>
					<xsl:call-template name="title-case-word">
						<xsl:with-param name="str">
							<xsl:call-template name="strip-pipe">
								<xsl:with-param name="str" select="substring-after($STR,'-')"/>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="mode" select="$mode"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="contains($STR,'/')">
					<xsl:call-template name="title-case-word">
						<xsl:with-param name="str">
							<xsl:call-template name="strip-pipe">
								<xsl:with-param name="str" select="substring-before($STR,'/')"/>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="mode" select="$mode"/>
					</xsl:call-template>
					<xsl:text>/</xsl:text>
					<xsl:call-template name="title-case-word">
						<xsl:with-param name="str">
							<xsl:call-template name="strip-pipe">
								<xsl:with-param name="str" select="substring-after($STR,'/')"/>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="mode" select="$mode"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="contains($STR,':')">
					<xsl:call-template name="title-case-word">
						<xsl:with-param name="str">
							<xsl:call-template name="strip-pipe">
								<xsl:with-param name="str" select="substring-before($STR,':')"/>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="mode" select="$mode"/>
					</xsl:call-template>
					<xsl:text>:</xsl:text>
					<xsl:call-template name="title-case-word">
						<xsl:with-param name="str">
							<xsl:call-template name="strip-pipe">
								<xsl:with-param name="str" select="substring-after($STR,':')"/>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="mode" select="$mode"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="contains($STR,$apos)">
					<xsl:call-template name="title-case-word">
						<xsl:with-param name="str">
							<xsl:call-template name="strip-pipe">
								<xsl:with-param name="str" select="substring-before($STR,$apos)"/>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="mode" select="$mode"/>
					</xsl:call-template>
					<xsl:text>'</xsl:text>
					<xsl:choose>
						<xsl:when
							test="string-length(substring-before(substring-after($STR,$apos),'|')) &lt;=1">
							<xsl:call-template name="knockdown">
								<xsl:with-param name="str"
									select="substring-before(substring-after($STR,$apos),'|')"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="title-case-word">
								<xsl:with-param name="str">
									<xsl:call-template name="strip-pipe">
										<xsl:with-param name="str" select="substring-after($STR,$apos)"/>
									</xsl:call-template>
								</xsl:with-param>
								<xsl:with-param name="mode" select="$mode"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:when test="$keepasis-word!=''">
					<xsl:value-of select="$keepasis-word"/>
				</xsl:when>
				<xsl:when test="contains($noknockdown-words,$STR)">
					<xsl:call-template name="strip-pipe">
						<xsl:with-param name="str" select="$STR"/>
					</xsl:call-template>
					<!--	<xsl:value-of select="substring-before(substring-after($STR;,'|'),'|')"/> -->
				</xsl:when>
				<xsl:when test="$first=1">
					<xsl:call-template name="title-case-word-guts">
						<xsl:with-param name="str" select="$str"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="contains($nocap-words,$STR) and $mode != 'name'">
					<xsl:call-template name="knockdown">
						<xsl:with-param name="str" select="$str"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="title-case-word-guts">
						<xsl:with-param name="str" select="$str"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>

	<xsl:template name="keepasis-words">
		<xsl:param name="str"/>
		<xsl:choose>
			<xsl:when test="$str='|ACADEMYHEALTH|'">AcademyHealth</xsl:when>
			<xsl:when test="$str='|ATPASES|'">ATPases</xsl:when>
			<xsl:when test="$str='|EJIAS|'">eJIAS</xsl:when>
			<xsl:when test="$str='|IRBS|'">IRBs</xsl:when>
			<xsl:when test="$str='|MEDGENMED|'">MedGenMed</xsl:when>
			<xsl:when test="$str='|MRNA|'">mRNA</xsl:when>
			<xsl:when test="$str='|PLOS|'">PLoS</xsl:when>
			<xsl:when test="$str='|STDS|'">STDs</xsl:when>
			<xsl:otherwise/>
		</xsl:choose>
	</xsl:template>


	<xsl:template name="title-case-word-guts">
		<xsl:param name="str"/>
		<xsl:call-template name="capitalize">
			<xsl:with-param name="str" select="substring($str,1,1)"/>
		</xsl:call-template>
		<xsl:call-template name="knockdown">
			<xsl:with-param name="str" select="substring($str,2,50)"/>
		</xsl:call-template>
	</xsl:template>

	<!-- Capitalize a string -->
	<xsl:template name="capitalize">
		<xsl:param name="str"/>
		<xsl:value-of
			select="translate($str, 
             'abcdefghjiklmnopqrstuvwxyz&#x00E0;&#x00E1;&#x00E2;&#x00E3;&#x00E4;&#x00E5;&#x00E6;&#x00E7;&#x00E8;&#x00E9;&#x00EA;&#x00EB;&#x00EC;&#x00ED;&#x00EE;&#x00EF;&#x00F0;&#x00F1;&#x00F2;&#x00F3;&#x00F4;&#x00F5;&#x00F6;&#x00F8;&#x00F9;&#x00FA;&#x00FB;&#x00FC;&#x00FD;&#x00FE;&#x0151;&#x010D;',
        'ABCDEFGHJIKLMNOPQRSTUVWXYZ&#x00C0;&#x00C1;&#x00C2;&#x00C3;&#x00C4;&#x00C5;&#x00C6;&#x00C7;&#x00C8;&#x00C9;&#x00CA;&#x00CB;&#x00CC;&#x00CD;&#x00CE;&#x00CF;&#x00D0;&#x00D1;&#x00D2;&#x00D3;&#x00D4;&#x00D5;&#x00D6;&#x00D8;&#x00D9;&#x00DA;&#x00DB;&#x00DC;&#x00DD;&#x00DE;&#x0150;&#x010C;')"
		/>
	</xsl:template>

	<!-- LowerCase a string -->
	<xsl:template name="knockdown">
		<xsl:param name="str"/>
		<xsl:value-of
			select="translate($str, 
             'ABCDEFGHJIKLMNOPQRSTUVWXYZ&#x00C0;&#x00C1;&#x00C2;&#x00C3;&#x00C4;&#x00C5;&#x00C6;&#x00C7;&#x00C8;&#x00C9;&#x00CA;&#x00CB;&#x00CC;&#x00CD;&#x00CE;&#x00CF;&#x00D0;&#x00D1;&#x00D2;&#x00D3;&#x00D4;&#x00D5;&#x00D6;&#x00D8;&#x00D9;&#x00DA;&#x00DB;&#x00DC;&#x00DD;&#x00DE;&#x0150;&#x010C;',
             'abcdefghjiklmnopqrstuvwxyz&#x00E0;&#x00E1;&#x00E2;&#x00E3;&#x00E4;&#x00E5;&#x00E6;&#x00E7;&#x00E8;&#x00E9;&#x00EA;&#x00EB;&#x00EC;&#x00ED;&#x00EE;&#x00EF;&#x00F0;&#x00F1;&#x00F2;&#x00F3;&#x00F4;&#x00F5;&#x00F6;&#x00F8;&#x00F9;&#x00FA;&#x00FB;&#x00FC;&#x00FD;&#x00FE;&#x0151;&#x010D;')"
		/>
	</xsl:template>

	<!--Removes letters from a string -->
	<xsl:template name="remlett">
		<xsl:param name="str"/>
		<xsl:value-of
			select="translate($str, 
             'ABCDEFGHJIKLMNOPQRSTUVWXYZabcdefghjiklmnopqrstuvwxyz','')"
		/>
	</xsl:template>

	<!--Removes letters from a string -->
	<xsl:template name="remnum">
		<xsl:param name="str"/>
		<xsl:value-of select="translate($str, 
             '1234567890','')"/>
	</xsl:template>

	<!--Removes periods from a string -->
	<xsl:template name="nodot">
		<xsl:param name="str"/>
		<xsl:value-of select="translate($str,'.','')"/>
	</xsl:template>

	<!--Removes period from the end of a string -->
	<xsl:template name="nofinaldot">
		<xsl:param name="str"/>
		<xsl:variable name="length" select="string-length($str)"/>
		<xsl:variable name="lastchar"
			select="substring-after($str, substring($str,1,string-length($str) - 1))"/>
		<xsl:choose>
			<xsl:when test="$lastchar='.'">
				<xsl:value-of select="substring($str,1,string-length($str) - 1)"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$str"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>


	<!--Removes periods and spaces from a string -->
	<xsl:template name="nodot-nospace">
		<xsl:param name="str"/>
		<xsl:value-of select="translate($str,'. ','')"/>
	</xsl:template>

	<!--Removes periods from a string -->
	<xsl:template name="nocomma">
		<xsl:param name="str"/>
		<xsl:value-of select="translate($str,',','')"/>
	</xsl:template>

	<!--Removes periods and commas from a string -->
	<xsl:template name="nocomma-nodot">
		<xsl:param name="str"/>
		<xsl:value-of select="normalize-space(translate($str,',.','  '))"/>
	</xsl:template>

	<!--Removes punctuation from a string -->
	<xsl:template name="nopunct">
		<xsl:param name="str"/>
		<xsl:value-of select="translate($str,'-,.()[]{};:','')"/>
	</xsl:template>

	<!--Removes punctuation from a string, keeps "-" -->
	<xsl:template name="voliss-nopunct">
		<xsl:param name="str"/>
		<xsl:value-of select="translate($str,',.()[]{};:','')"/>
	</xsl:template>


	<!--Removes | from a string -->
	<xsl:template name="strip-pipe">
		<xsl:param name="str"/>
		<xsl:value-of select="translate($str,'|','')"/>
	</xsl:template>

	<!-- ****************************END DATE CHECKING TEMPLATES ********************************-->
	<!--
        get-date-from-string   parses written-out date in the following forms to NLM  format
		             for use in <date> or <pub-date>
						 
-->

	<!-- parameters:  date - a string that has been run through normalize-space() 
	                         and had all punctuation removed
		  					order - (optional) a string that defines the order of the parts sent
									dmy = day month year
									mdy = month day year
									ymd
		The template will work for these types of dates (DD is day, MM is month, YYYY is year):
		
			Month DD, YYYY
			DD Month YYYY
			YYYY Month DD
			Season YYYY
			YYYY Season
			Month YYYY
			YYYY Month
			Month-Month YYYY
         Month/Month YYYY
			Season-Season YYYY
			DD-MM-YYYY (with order parameter)						
			DD/MM/YYYY (with order parameter)						
			DD.MM.YYYY (with order parameter)						
			MM-DD-YYYY (with order parameter)						
			MM/DD/YYYY (with order parameter)						
			MM.DD.YYYY (with order parameter)						
		  
		  
		  -->
	<xsl:template name="get-date-from-string">
		<xsl:param name="date"/>
		<xsl:param name="order"/>

		<!-- Remove all thin-space entities from date -->
		<xsl:variable name="no-thin-space">
			<xsl:call-template name="replace-thin-space-entity">
				<xsl:with-param name="str" select="$date"/>
			</xsl:call-template>
		</xsl:variable>

		<!-- Remove commas and spaces from date -->
		<xsl:variable name="cleandate">
			<xsl:call-template name="nocomma-nodot">
				<xsl:with-param name="str" select="normalize-space($no-thin-space)"/>
			</xsl:call-template>
		</xsl:variable>

		<xsl:variable name="space-count">
			<xsl:call-template name="count-space">
				<xsl:with-param name="date" select="$cleandate"/>
			</xsl:call-template>
		</xsl:variable>

		<xsl:choose>
			<xsl:when test="$space-count=0">
				<xsl:call-template name="date-nospace">
					<xsl:with-param name="date" select="$cleandate"/>
					<xsl:with-param name="order" select="$order"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when
				test="contains($date,'-')
				               or contains($date,'ENTITYSTART_hyphen')
				               or contains($date,'ENTITYSTART_ndash')
                           or contains($date, '&#x2013;')">
				<!-- need to add charent testing -->
				<xsl:call-template name="date-range">
					<xsl:with-param name="date" select="$cleandate"/>
					<xsl:with-param name="order" select="$order"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$space-count=1">
				<xsl:call-template name="date-1space">
					<xsl:with-param name="date" select="$cleandate"/>
					<xsl:with-param name="order" select="$order"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$space-count=2">
				<xsl:call-template name="date-2space">
					<xsl:with-param name="date" select="$cleandate"/>
					<xsl:with-param name="order" select="$order"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$space-count > 3">
				<xsl:choose>
					<xsl:when test="$order='citation'">
						<xsl:value-of select="$date"/>
					</xsl:when>
					<xsl:otherwise>
						<string-date>
							<xsl:value-of select="$date"/>
						</string-date>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$order='citation'">
				<xsl:value-of select="$date"/>
			</xsl:when>
			<xsl:otherwise>
				<string-date>
					<xsl:value-of select="$date"/>
				</string-date>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Replaces _ENTITYSTART_#8201_ENTITYEND_ with a simple space: 
	     entity for thin space is sometimes used to separate parts of date -->
	<xsl:template name="replace-thin-space-entity">
		<xsl:param name="str"/>

		<xsl:choose>
			<!-- Base Case: no more entities to remove -->
			<xsl:when test="not(contains($str, '_ENTITYSTART_#8201_ENTITYEND_'))">
				<xsl:value-of select="$str"/>
			</xsl:when>

			<!-- Case 1: Entity is in the string -->
			<xsl:otherwise>
				<xsl:value-of select="substring-before($str, '_ENTITYSTART_#8201_ENTITYEND_')"/>
				<xsl:text> </xsl:text>
				<xsl:call-template name="replace-thin-space-entity">
					<xsl:with-param name="str" select="substring-after($str, '_ENTITYSTART_#8201_ENTITYEND_')"
					/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="count-space">
		<xsl:param name="date"/>
		<xsl:choose>
			<xsl:when test="contains($date,' ')">
				<xsl:choose>
					<xsl:when test="contains(substring-after($date,' '),' ')">
						<xsl:choose>
							<xsl:when test="contains(substring-after(substring-after($date,' '),' '),' ')">
								<xsl:choose>
									<xsl:when
										test="contains(substring-after(substring-after(substring-after($date,' '),' '),' '),' ')"
										>4</xsl:when>
									<xsl:otherwise>3</xsl:otherwise>
								</xsl:choose>
							</xsl:when>
							<xsl:otherwise>2</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="contains($date,' ')">1</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>0</xsl:otherwise>
		</xsl:choose>
	</xsl:template>


	<xsl:template name="date-range">
		<xsl:param name="date"/>
		<xsl:variable name="stringdate-mod">
			<xsl:call-template name="fix-range">
				<xsl:with-param name="date" select="$date"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="space-count">
			<xsl:call-template name="count-space">
				<xsl:with-param name="date" select="$stringdate-mod"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$space-count=1">
				<xsl:call-template name="date-1space">
					<xsl:with-param name="date" select="$stringdate-mod"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$space-count=2">
				<xsl:call-template name="date-2space">
					<xsl:with-param name="date" select="$stringdate-mod"/>
					<xsl:with-param name="order" select="'range'"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$space-count > 2">
				<string-date>
					<xsl:value-of select="$stringdate-mod"/>
				</string-date>
			</xsl:when>
		</xsl:choose>
	</xsl:template>


	<xsl:template name="fix-range">
		<xsl:param name="date"/>
		<xsl:choose>
			<xsl:when test="contains($date, ' - ')">
				<xsl:value-of select="substring-before($date,' -')"
					/>_ENTITYSTART_#8211_ENTITYEND_<xsl:value-of select="substring-after($date,'- ')"/>
			</xsl:when>
			<xsl:when test="contains($date, '-')">
				<xsl:value-of select="substring-before($date,'-')"
					/>_ENTITYSTART_#8211_ENTITYEND_<xsl:value-of select="substring-after($date,'-')"/>
			</xsl:when>
			<xsl:when test="contains($date, '&#8211;')">
				<xsl:value-of select="normalize-space(substring-before($date,'&#8211;'))"
					/>_ENTITYSTART_#8211_ENTITYEND_<xsl:value-of
					select="normalize-space(substring-after($date,'&#8211;'))"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>




	<xsl:template name="date-nospace">
		<xsl:param name="date"/>
		<xsl:param name="order"/>
		<xsl:variable name="stringdate-mod" select="normalize-space(translate($date,'-./','   '))"/>
		<xsl:variable name="space-count">
			<xsl:call-template name="count-space">
				<xsl:with-param name="date" select="normalize-space($stringdate-mod)"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$space-count=0">
				<!-- just a year -->
				<year>
					<xsl:value-of select="$stringdate-mod"/>
				</year>
			</xsl:when>
			<xsl:when test="$space-count=1">
				<xsl:call-template name="date-1space">
					<xsl:with-param name="date" select="$stringdate-mod"/>
					<xsl:with-param name="order" select="$order"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$space-count=2">
				<xsl:call-template name="date-2space">
					<xsl:with-param name="date" select="$stringdate-mod"/>
					<xsl:with-param name="order" select="$order"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$order='citation'">
				<xsl:value-of select="$date"/>
			</xsl:when>
			<xsl:when test="$space-count > 2">
				<string-date>
					<xsl:value-of select="$stringdate-mod"/>
				</string-date>
			</xsl:when>
			<xsl:otherwise>
				<string-date>
					<xsl:value-of select="$date"/>
				</string-date>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="date-1space">
		<xsl:param name="date"/>
		<xsl:param name="order"/>
		<xsl:variable name="x1" select="substring-before($date,' ')"/>
		<xsl:variable name="x2" select="substring-after($date,' ')"/>
		<xsl:variable name="yeardiff" select="string-length($x1) - string-length($x2)"/>
		<xsl:choose>
			<!--  <xsl:when test="$x1 > 1000">
                <xsl:choose>
                    <xsl:when test="$yeardiff > 0">
                        <year><xsl:value-of select="$x1"/>&#x2013;<xsl:value-of select="substring($x1,1,$yeardiff)"/><xsl:value-of select="$x2"/></year>
                        </xsl:when>
                    <xsl:otherwise>
                        <year><xsl:value-of select="$x1"/>&#x2013;<xsl:value-of select="$x2"/></year>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>  -->

			<xsl:when test="number($x1) > 1000">
				<xsl:call-template name="find-month-season">
					<xsl:with-param name="month" select="$x2"/>
				</xsl:call-template>
				<year>
					<xsl:value-of select="$x1"/>
				</year>
			</xsl:when>
			<xsl:when test="number($x2) > 1000">
				<xsl:call-template name="find-month-season">
					<xsl:with-param name="month" select="$x1"/>
				</xsl:call-template>
				<year>
					<xsl:value-of select="$x2"/>
				</year>
			</xsl:when>
			<xsl:when test="string(number($x1)) != 'NaN' or string(number($x2)) != 'NaN'">
				<!-- when one is a number, the other is probably a month - since neither is a year -->
				<xsl:value-of select="$date"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$order='citation'">
						<xsl:value-of select="$date"/>
					</xsl:when>
					<xsl:otherwise>
						<string-date>
							<xsl:value-of select="$date"/>
						</string-date>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="date-2space">
		<!-- date has two spaces -->
		<xsl:param name="date"/>
		<xsl:param name="order"/>
		<xsl:variable name="x1" select="substring-before($date,' ')"/>
		<xsl:variable name="x2" select="substring-before(substring-after($date,' '),' ')"/>
		<xsl:variable name="x3" select="substring-after(substring-after($date,' '),' ')"/>
		<xsl:variable name="quartercheck">
			<xsl:call-template name="capitalize">
				<xsl:with-param name="str" select="$date"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="x2-is-valid-month">
			<!-- Added in case need to check if the second part of date is a month value -->
			<xsl:call-template name="valid-month">
				<xsl:with-param name="value" select="$x2"/>
			</xsl:call-template>
		</xsl:variable>

		<xsl:choose>
			<xsl:when
				test="contains($quartercheck,'QUARTER') or contains($quartercheck,'QTR') or contains($quartercheck,'Q')">
				<xsl:choose>
					<xsl:when test="number($x1) > 1000">
						<season>
							<xsl:value-of select="$x2"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="$x3"/>
						</season>
						<year>
							<xsl:value-of select="$x1"/>
						</year>
					</xsl:when>
					<xsl:when test="number($x3) > 1000">
						<season>
							<xsl:value-of select="$x1"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="$x2"/>
						</season>
						<year>
							<xsl:value-of select="$x3"/>
						</year>
					</xsl:when>
				</xsl:choose>
			</xsl:when>

			<xsl:when test="$order='range'">
				<xsl:choose>
					<!-- when we have a "year month day-day" don't process as a range -->
					<xsl:when
						test="number($x1) > 1000  and $x2-is-valid-month='true' and (contains($x3,'-') or contains($x3,'_ENTITYSTART'))">
						<day>
							<!-- when the date has a day range, take the first day -->
							<xsl:choose>
								<xsl:when test="contains($x3,'-')">
									<xsl:value-of select="substring-before($x3,'-')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="substring-before($x3,'_ENTITYSTART')"/>
								</xsl:otherwise>
							</xsl:choose>
						</day>
						<xsl:call-template name="find-month-season">
							<xsl:with-param name="month" select="$x2"/>
						</xsl:call-template>
						<year>
							<xsl:value-of select="$x1"/>
						</year>
					</xsl:when>
					<xsl:otherwise>
						<string-date>
							<xsl:value-of select="$date"/>
						</string-date>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="number($x1) > 1000 or $order='ymd'">
				<!-- YYYY Month DD -->
				<xsl:choose>
					<xsl:when test="number($x3) >= 0">
						<day>
							<xsl:value-of select="number($x3)"/>
						</day>
					</xsl:when>
					<xsl:otherwise>
						<day>
							<xsl:value-of select="$x3"/>
						</day>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:call-template name="find-month-season">
					<xsl:with-param name="month" select="$x2"/>
				</xsl:call-template>
				<year>
					<xsl:value-of select="$x1"/>
				</year>
			</xsl:when>
			<xsl:when test="$order='mdy'">
				<!-- Month DD YYYY  -->
				<day>
					<xsl:call-template name="remlett">
						<xsl:with-param name="str" select="number($x2)"/>
					</xsl:call-template>
				</day>
				<xsl:call-template name="find-month-season">
					<xsl:with-param name="month" select="$x1"/>
				</xsl:call-template>
				<year>
					<xsl:value-of select="$x3"/>
				</year>
			</xsl:when>
			<!-- 
			Two explicit tests for DD-MM-YYYY to handle cases where $order is not set.
			Note that if $order is not set, date will be treated as DD-MM-YYYY if $x2 is a number
			between 1 and 12, inclusive.
			 -->
			<xsl:when test="$order='dmy'">
				<!-- DD Month YYYY -->
				<day>
					<xsl:call-template name="remlett">
						<xsl:with-param name="str" select="number($x1)"/>
					</xsl:call-template>
				</day>
				<xsl:call-template name="find-month-season">
					<xsl:with-param name="month" select="$x2"/>
				</xsl:call-template>
				<year>
					<xsl:value-of select="$x3"/>
				</year>
			</xsl:when>
			<xsl:when test="number($x1) > 0 and $x2-is-valid-month='true'">
				<!-- DD Month YYYY -->
				<day>
					<xsl:call-template name="remlett">
						<xsl:with-param name="str" select="number($x1)"/>
					</xsl:call-template>
				</day>
				<xsl:call-template name="find-month-season">
					<xsl:with-param name="month" select="$x2"/>
				</xsl:call-template>
				<year>
					<xsl:value-of select="$x3"/>
				</year>
			</xsl:when>

			<xsl:otherwise>
				<!-- Month DD YYYY  -->
				<day>
					<xsl:choose>
						<xsl:when test="number($x2)">
							<xsl:value-of select="number($x2)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$x2"/>
						</xsl:otherwise>
					</xsl:choose>
				</day>
				<xsl:call-template name="find-month-season">
					<xsl:with-param name="month" select="$x1"/>
				</xsl:call-template>
				<year>
					<xsl:value-of select="$x3"/>
				</year>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="find-month-season">
		<xsl:param name="month"/>
		<xsl:choose>
			<xsl:when test="$month='January' or $month='Jan'">
				<month>1</month>
			</xsl:when>
			<xsl:when test="$month='February' or $month='Feb' or $month='Febraury'">
				<month>2</month>
			</xsl:when>
			<xsl:when test="$month='March' or $month='Mar'">
				<month>3</month>
			</xsl:when>
			<xsl:when test="$month='April' or $month='Apr'">
				<month>4</month>
			</xsl:when>
			<xsl:when test="$month='May'">
				<month>5</month>
			</xsl:when>
			<xsl:when test="$month='June' or $month='Jun'">
				<month>6</month>
			</xsl:when>
			<xsl:when test="$month='July' or $month='Jul'">
				<month>7</month>
			</xsl:when>
			<xsl:when test="$month='August' or $month='Aug'">
				<month>8</month>
			</xsl:when>
			<xsl:when test="$month='September' or $month='Sep' or $month='Sept'">
				<month>9</month>
			</xsl:when>
			<xsl:when test="$month='October' or $month='Oct'">
				<month>10</month>
			</xsl:when>
			<xsl:when test="$month='November' or $month='Nov'">
				<month>11</month>
			</xsl:when>
			<xsl:when test="$month='December' or $month='december' or $month='Dec'">
				<month>12</month>
			</xsl:when>
			<xsl:when test="1 &lt;= number($month) and number($month)&lt;= 12">
				<month>
					<xsl:value-of select="number($month)"/>
				</month>
			</xsl:when>
			<xsl:otherwise>
				<season>
					<xsl:call-template name="clean-season">
						<xsl:with-param name="season" select="$month"/>
					</xsl:call-template>
					<!--  <xsl:value-of select="$month"/> -->
				</season>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>


	<!-- Cleans month and season ranges, feeds values to define-season to build pmc-style-compliant season -->
	<xsl:template name="clean-season">
		<xsl:param name="season"/>
		<xsl:choose>
			<xsl:when test="contains($season,'/')">
				<xsl:variable name="x1">
					<xsl:call-template name="find-season">
						<xsl:with-param name="str">
							<xsl:call-template name="capitalize">
								<xsl:with-param name="str" select="substring-before($season,'/')"/>
							</xsl:call-template>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="x2">
					<xsl:call-template name="find-season">
						<xsl:with-param name="str">
							<xsl:call-template name="capitalize">
								<xsl:with-param name="str" select="substring-after($season,'/')"/>
							</xsl:call-template>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:variable>
				<xsl:value-of select="$x1"/>
				<xsl:text>&#x2013;</xsl:text>
				<xsl:value-of select="$x2"/>
			</xsl:when>
			<xsl:when test="contains($season,'-')">
				<xsl:variable name="x1">
					<xsl:call-template name="find-season">
						<xsl:with-param name="str">
							<xsl:call-template name="capitalize">
								<xsl:with-param name="str" select="substring-before($season,'-')"/>
							</xsl:call-template>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="x2">
					<xsl:call-template name="find-season">
						<xsl:with-param name="str">
							<xsl:call-template name="capitalize">
								<xsl:with-param name="str" select="substring-after($season,'-')"/>
							</xsl:call-template>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:variable>
				<xsl:value-of select="$x1"/>
				<xsl:text>&#x2013;</xsl:text>
				<xsl:value-of select="$x2"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$season"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Identifies string and writes out PMC style abbreviation.
         If no match, write out value in title case -->
	<xsl:template name="find-season">
		<xsl:param name="str"/>
		<xsl:choose>
			<xsl:when test="contains($str,'JAN')">Jan</xsl:when>
			<xsl:when test="contains($str,'FEB')">Feb</xsl:when>
			<xsl:when test="contains($str,'MAR')">Mar</xsl:when>
			<xsl:when test="contains($str,'APR')">Apr</xsl:when>
			<xsl:when test="contains($str,'MAY')">May</xsl:when>
			<xsl:when test="contains($str,'JUN')">Jun</xsl:when>
			<xsl:when test="contains($str,'JUL')">Jul</xsl:when>
			<xsl:when test="contains($str,'AUG')">Aug</xsl:when>
			<xsl:when test="contains($str,'SEP')">Sep</xsl:when>
			<xsl:when test="contains($str,'OCT')">Oct</xsl:when>
			<xsl:when test="contains($str,'NOV')">Nov</xsl:when>
			<xsl:when test="contains($str,'DEC')">Dec</xsl:when>
			<xsl:when test="$str='1'">Jan</xsl:when>
			<xsl:when test="$str='2'">Feb</xsl:when>
			<xsl:when test="$str='3'">Mar</xsl:when>
			<xsl:when test="$str='4'">Apr</xsl:when>
			<xsl:when test="$str='5'">May</xsl:when>
			<xsl:when test="$str='6'">Jun</xsl:when>
			<xsl:when test="$str='7'">Jul</xsl:when>
			<xsl:when test="$str='8'">Aug</xsl:when>
			<xsl:when test="$str='9'">Sep</xsl:when>
			<xsl:when test="$str='10'">Oct</xsl:when>
			<xsl:when test="$str='11'">Nov</xsl:when>
			<xsl:when test="$str='12'">Dec</xsl:when>
			<xsl:when test="contains($str,'WIN')">Winter</xsl:when>
			<xsl:when test="contains($str,'SPR')">Spring</xsl:when>
			<xsl:when test="contains($str,'SUM')">Summer</xsl:when>
			<xsl:when test="contains($str,'FAL')">Fall</xsl:when>
			<xsl:when test="contains($str,'AUT')">Autumn</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="title-case-string">
					<xsl:with-param name="str" select="$str"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>



	<!-- Returns 'true' when value passed in is a valid name for a month or numeric value from 1 to 12 -->
	<xsl:template name="valid-month">
		<xsl:param name="value"/>

		<xsl:choose>
			<xsl:when test="$value='January' or $value='Jan'">true</xsl:when>
			<xsl:when test="$value='February' or $value='Feb'">true</xsl:when>
			<xsl:when test="$value='March' or $value='Mar'">true</xsl:when>
			<xsl:when test="$value='April' or $value='Apr'">true</xsl:when>
			<xsl:when test="$value='May'">true</xsl:when>
			<xsl:when test="$value='June' or $value='Jun'">true</xsl:when>
			<xsl:when test="$value='July' or $value='Jul'">true</xsl:when>
			<xsl:when test="$value='August' or $value='Aug'">true</xsl:when>
			<xsl:when test="$value='September' or $value='Sep' or $value='Sept'">true</xsl:when>
			<xsl:when test="$value='October' or $value='Oct'">true</xsl:when>
			<xsl:when test="$value='November' or $value='Nov'">true</xsl:when>
			<xsl:when test="$value='December' or $value='december' or $value='Dec'">true</xsl:when>
			<xsl:when test="1 &lt;= number($value) and number($value)&lt;= 12">true</xsl:when>
			<xsl:otherwise>false</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- END DATE FROM STRING TEMPLATES -->

	<xsl:template name="get_mimetype">
		<xsl:param name="source"/>
		<xsl:choose>
			<xsl:when test="contains($source,'/')">
				<xsl:attribute name="mimetype">
					<xsl:value-of select="substring-before($source,'/')"/>
				</xsl:attribute>
				<xsl:attribute name="mime-subtype">
					<xsl:value-of select="substring-after($source,'/')"/>
				</xsl:attribute>
			</xsl:when>
			<!-- IMAGE -->
			<xsl:when
				test="contains($source,'bmp') or
							    contains($source,'gif') or
								 contains($source,'jpg') or
								 contains($source,'jpeg') or
								 contains($source,'tif') or
								 contains($source,'tiff')">
				<xsl:attribute name="mimetype">
					<xsl:value-of select="'image'"/>
				</xsl:attribute>
				<xsl:call-template name="image-subtype">
					<xsl:with-param name="source" select="$source"/>
				</xsl:call-template>
			</xsl:when>
			<!-- APPLICATION -->
			<xsl:when
				test="contains($source,'bin') or 
							    contains($source,'dcr') or
							    contains($source,'dir') or
							    contains($source,'doc') or
							    contains($source,'dxr') or
							    contains($source,'exe') or
							    contains($source,'gz') or
							    contains($source,'hlp') or
							    contains($source,'hqz') or
							    contains($source,'jar') or
							    contains($source,'mdb') or
							    contains($source,'pdf') or
							    contains($source,'ppt') or
							    contains($source,'ps') or
							    contains($source,'rtf') or
							    contains($source,'swf') or
							    contains($source,'tar') or
							    contains($source,'wpd') or
							    contains($source,'xls') or
								 contains($source,'zip')">
				<xsl:attribute name="mimetype">
					<xsl:value-of select="'application'"/>
				</xsl:attribute>
				<xsl:call-template name="application-subtype">
					<xsl:with-param name="source" select="$source"/>
				</xsl:call-template>
			</xsl:when>
			<!-- AUDIO -->
			<xsl:when
				test="contains($source,'au') or
							    contains($source,'midi') or
								 contains($source,'mp3') or
								 contains($source,'ra') or
								 contains($source,'wav')">
				<xsl:attribute name="mimetype">
					<xsl:value-of select="'audio'"/>
				</xsl:attribute>
				<xsl:call-template name="audio-subtype">
					<xsl:with-param name="source" select="$source"/>
				</xsl:call-template>
			</xsl:when>
			<!-- VIDEO -->
			<xsl:when
				test="contains($source,'asf') or
							    contains($source,'avi') or
								 contains($source,'mov') or
								 contains($source,'mpeg') or
								 contains($source,'mpg') or
								 contains($source,'rm') or
								 contains($source,'rv')">
				<xsl:attribute name="mimetype">
					<xsl:value-of select="'video'"/>
				</xsl:attribute>
				<xsl:call-template name="video-subtype">
					<xsl:with-param name="source" select="$source"/>
				</xsl:call-template>
			</xsl:when>
			<!-- TEXT -->
			<xsl:otherwise>
				<xsl:attribute name="mimetype">
					<xsl:value-of select="'text'"/>
				</xsl:attribute>
				<xsl:call-template name="text-subtype">
					<xsl:with-param name="source" select="$source"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>


	<xsl:template name="image-subtype">
		<xsl:param name="source"/>
		<xsl:attribute name="mime-subtype">
			<xsl:choose>
				<xsl:when test="contains($source,'bmp')">bmp</xsl:when>
				<xsl:when test="contains($source,'gif')">gif</xsl:when>
				<xsl:when test="contains($source,'jpg')">jpeg</xsl:when>
				<xsl:when test="contains($source,'jpeg')">jpeg</xsl:when>
				<xsl:when test="contains($source,'tif')">tiff</xsl:when>
				<xsl:when test="contains($source,'tiff')">tiff</xsl:when>
			</xsl:choose>
		</xsl:attribute>
	</xsl:template>


	<xsl:template name="application-subtype">
		<xsl:param name="source"/>
		<xsl:attribute name="mime-subtype">
			<xsl:choose>
				<xsl:when test="contains($source,'bin')">octet-stream</xsl:when>
				<xsl:when test="contains($source,'doc')">msword</xsl:when>
				<xsl:when
					test="contains($source,'dir') or 
			                 contains($source,'dxr') or 
								  contains($source,'dcr')"
					>x-director</xsl:when>
				<xsl:when test="contains($source,'exe')">x-msdownload</xsl:when>
				<xsl:when test="contains($source,'gz')">x-zip-compressed</xsl:when>
				<xsl:when test="contains($source,'hlp')">winhlp32</xsl:when>
				<xsl:when test="contains($source,'hqz')">mac-binhex40</xsl:when>
				<xsl:when test="contains($source,'jar')">java-archive</xsl:when>
				<xsl:when test="contains($source,'mdb')">vnd.ms-access</xsl:when>
				<xsl:when test="contains($source,'pdf')">pdf</xsl:when>
				<xsl:when test="contains($source,'ppt')">vnd.ms-powerpoint</xsl:when>
				<xsl:when test="contains($source,'ps')">ps</xsl:when>
				<xsl:when test="contains($source,'rtf')">rtf</xsl:when>
				<xsl:when test="contains($source,'swf')">x-shockwave-flash</xsl:when>
				<xsl:when test="contains($source,'tar')">x-zip-compressed</xsl:when>
				<xsl:when test="contains($source,'wpd')">wordperfect5.1</xsl:when>
				<xsl:when test="contains($source,'xls')">vnd.ms-excel</xsl:when>
				<xsl:when test="contains($source,'zip')">x-zip-compressed</xsl:when>
			</xsl:choose>
		</xsl:attribute>
	</xsl:template>

	<xsl:template name="audio-subtype">
		<xsl:param name="source"/>
		<xsl:attribute name="mime-subtype">
			<xsl:choose>
				<xsl:when test="contains($source,'au')">basic</xsl:when>
				<xsl:when test="contains($source,'midi')">x-midi</xsl:when>
				<xsl:when test="contains($source,'mp3')">mpeg</xsl:when>
				<xsl:when test="contains($source,'ra')">x-realaudio</xsl:when>
				<xsl:when test="contains($source,'wav')">wav</xsl:when>
			</xsl:choose>
		</xsl:attribute>
	</xsl:template>


	<xsl:template name="video-subtype">
		<xsl:param name="source"/>
		<xsl:attribute name="mime-subtype">
			<xsl:choose>
				<xsl:when test="contains($source,'asf')">x-ms-asf</xsl:when>
				<xsl:when test="contains($source,'avi')">x-msvideo</xsl:when>
				<xsl:when test="contains($source,'mov')">quicktime</xsl:when>
				<xsl:when test="contains($source,'mpeg')">mpeg</xsl:when>
				<xsl:when test="contains($source,'mpg')">mpeg</xsl:when>
				<xsl:when test="contains($source,'rm')">vnd.rn-realmedia</xsl:when>
				<xsl:when test="contains($source,'rv')">vnd.rn-realvideo</xsl:when>
			</xsl:choose>
		</xsl:attribute>
	</xsl:template>


	<xsl:template name="text-subtype">
		<xsl:param name="source"/>
		<xsl:attribute name="mime-subtype">
			<xsl:choose>
				<xsl:when test="contains($source,'html')">html</xsl:when>
				<xsl:when test="contains($source,'sgml')">sgml</xsl:when>
				<xsl:when test="contains($source,'xml')">xml</xsl:when>
				<xsl:otherwise>plain</xsl:otherwise>
			</xsl:choose>
		</xsl:attribute>
	</xsl:template>




	<!--===================================================================-->

	<xsl:template match="back">
		<back>
			<xsl:apply-templates select="@* | *"/>
			<xsl:if test="$jour='J Vis Exp' and not(fn-group)">
				<fn-group>
					<xsl:call-template name="write-jove-footnote"/>
				</fn-group>
				</xsl:if>
		</back>
		</xsl:template>

	<xsl:template match="fn-group">
		<fn-group>
			<xsl:apply-templates select="@* | *"/>
			<xsl:if test="$jour='J Vis Exp'">
					<xsl:call-template name="write-jove-footnote"/>
				</xsl:if>
		</fn-group>
		</xsl:template>



	<!-- this template should run in manuscript conversions only -->
	<xsl:template name="write-jove-footnote">
		<xsl:variable name="link">
			<xsl:text>http://dx.doi.org/</xsl:text>
			<xsl:value-of select="/article/processing-instruction('doi')"/>
			</xsl:variable>
	
		<fn id="jove-bleh">
			<p>A complete version of this article that includes the video component is available at the publisher's site.</p>
			</fn>
		</xsl:template>

<!-- PMC-23888 Remove copyright data elements from author manuscripts -->
<xsl:template match="copyright-statement | copyright-holder | copyright-year"/>

<xsl:template match="permissions">
	<xsl:if test="child::node()[not(self::copyright-statement) and not(self::copyright-year) and not(self::copyright-holder)]">
		<permissions>
			<xsl:apply-templates select="@*"/>
			<xsl:apply-templates select="*[not(self::copyright-statement) and not(self::copyright-year) and not(self::copyright-holder)]"/>
		</permissions>
		</xsl:if>
	</xsl:template>			


	<xsl:template name="write-response-subarticle-pis">
		<xsl:if test="/article/response">
			<xsl:processing-instruction name="inline-response">
				<xsl:choose>
					<xsl:when test="/article/response[1]/@id">
						<xsl:value-of select="/article/response[1]/@id"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:for-each select="/article/response[1]">
							<xsl:call-template name="generate-id-value"/>
						</xsl:for-each>						
					</xsl:otherwise>
				</xsl:choose>
				</xsl:processing-instruction>
			</xsl:if>

			<xsl:if test="/article/sub-article">
				<xsl:processing-instruction name="subarticle">
					<xsl:choose>
						<xsl:when test="/article/sub-article[1]/@id">
							<xsl:value-of select="/article/sub-article[1]/@id"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:for-each select="/article/sub-article[1]">
								<xsl:call-template name="generate-id-value"/>
							</xsl:for-each>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:processing-instruction>
			</xsl:if>
		</xsl:template>

	<!-- ======================================= -->
	<!-- HELPER TEMPLATES/FUNCTIONS -->
	<!-- ======================================= -->
	
	<xsl:template name="id-or-generate-id">
		<xsl:choose>
			<xsl:when test="@id">
				<xsl:attribute name="id">
					<xsl:value-of select="@id"/>
				</xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="generate-id"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="generate-id">
		<xsl:attribute name="id">
			<xsl:value-of select="generate-id()"/>
		</xsl:attribute>
	</xsl:template>
	
	<xsl:template name="generate-id-value">	
		<xsl:value-of select="generate-id()"/>		
	</xsl:template>
	
</xsl:stylesheet>
