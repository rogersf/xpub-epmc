<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:xs="http://www.w3.org/2001/XMLSchema"
  exclude-result-prefixes="xsi xs">
  
  <xsl:output method="xml" indent="yes" encoding="UTF-8" omit-xml-declaration="no"/>
  <xsl:param name="externalId"/>
  <xsl:param name="systemId"/>
  
  <xsl:template match="/">
    <xsl:value-of disable-output-escaping="yes" select="concat('&lt;!DOCTYPE article PUBLIC &quot;', $externalId ,'&quot; &quot;', $systemId, '&quot;>')"/>
    <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="permissions">
    <xsl:if test="copyright-statement or license[@type='open-access' and @xlink-href]">
      <permissions>
        <xsl:apply-templates/>
      </permissions>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="license[@type='open-access' and not(@xlink-href)]"/>
  
  <xsl:template match="*">
    <xsl:copy>
      <xsl:apply-templates select="@*"/>
      <xsl:apply-templates select="node() | processing-instruction() | comment()" />
    </xsl:copy>
  </xsl:template>
  <xsl:template match="@* | text() | comment() | processing-instruction()">
    <xsl:copy/>
  </xsl:template>
  
</xsl:stylesheet>