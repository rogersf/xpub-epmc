const logger = require('@pubsweet/logger')
const config = require('config')
const BaseModel = require('@pubsweet/base-model')
const { transaction } = require('objection')
const { ManuscriptManager, NoteManager } = require('../xpub-model')
const { checkJobStatus, uncaughtError } = require('../job-runner')
const getUser = require('../utils/user.js')
const fileUtils = require('../utils/files.js')
const { token } = require('../utils/authentication')
const {
  processXML,
  testStep,
  nxmlStep,
  htmlStep,
  updateStep,
} = require('./xmlProcess')
const { addCitation } = require('./xmlFinalize')
const { schValidate } = require('./xmlValidate')
const { getTransform } = require('./transform')

const { baseUrl } = config.get('pubsweet-server')

if (!process.env.ENABLE_CRONJOB_XMLCONVERTER) {
  logger.info(
    'ENABLE_CRONJOB_XMLCONVERTER not defined. xml-converter cronjob exits.',
  )
  process.exit(0)
}

const knex = BaseModel.knex()
let user = null

;(async () => {
  await checkJobStatus('xml-converter', async () => {
    const beforeUpdate = Date.now()
    user = await getUser.getAdminUser()
    const queue = await ManuscriptManager.findByDepositStates([
      'ADDING_CITATION',
      'CONVERTING_XML',
      'VALIDATING_XML',
    ])
    if (queue && queue.length > 0) {
      await finalizeQueue(
        queue.filter(q => q.pdfDepositState === 'ADDING_CITATION'),
      )
      await pushQueue(queue.filter(q => q.pdfDepositState === 'CONVERTING_XML'))
      await validateQueue(
        queue.filter(q => q.pdfDepositState === 'VALIDATING_XML'),
      )
    }
    logger.info(
      `XML conversion process finished in ${Date.now() - beforeUpdate} ms`,
    )
  })
  process.exit()
})()

process
  .on('uncaughtException', async (err, origin) => {
    logger.error(`Uncaught Exception thrown: ${err}`)
    logger.error('Exception thrown at: ', origin)
    await uncaughtError('xml-converter', `Uncaught Exception thrown: ${err}`)
    process.exit(1)
  })
  .on('unhandledRejection', async (reason, promise) => {
    logger.error('Unhandled Rejection: ', reason)
    logger.error('Rejected at promise: ', promise)
    await uncaughtError('xml-converter', `Unhandled Rejection: ${reason}`)
    process.exit(1)
  })

// Check for cached XSL and use for entire queue to increase speed
async function useTransform(name) {
  const xslt = await getTransform(name)
  xslt.name = name
  if (!xslt.update) return xslt
  return undefined
}

// Queue for finalizing XML for sending to repo
async function finalizeQueue(queue) {
  if (queue && queue.length > 0) {
    logger.info(`Adding citation info to XML (queue length ${queue.length}):`)
    let xslt = await useTransform('addCitation')
    const bearer = token.create(user)
    await queue.reduce(async (promise, m) => {
      await promise
      await addCitation(m, user.id, bearer, xslt)
      return true
    }, Promise.resolve())
    xslt = null
    logger.info(`All citation info added to XML. ${queue.length} processed.\n`)
  }
}

// Break pushQueue into smaller sets to complete within timeout
async function pushQueue(queue) {
  if (queue && queue.length > 0) {
    const total = queue.length
    logger.info(`Converting XML (queue length ${total}):`)
    while (queue.length > 20) {
      /* eslint-disable no-await-in-loop */
      const set = queue.splice(0, 20)
      await processQueue(set)
    }
    await processQueue(queue)
    logger.info(`All XML conversions complete. ${total} processed.\n`)
  }
}
// Queue for creating new NXML and HTML after tagging or finalizing
async function processQueue(queue) {
  if (queue && queue.length > 0) {
    const adminUser = user
    const total = queue.length
    logger.info(`Converting XML batch (${total}):`)
    // Validate and check style
    logger.info(`Converting XML step 1: Checking style. Queue length: ${total}`)
    let styleXSL = await useTransform('styleChecker')
    let styleResult = []
    await queue.reduce(async (promise, m) => {
      await promise
      const file =
        m.status !== 'tagging' &&
        m.files.some(f => !f.deleted && f.type === 'citationXML')
          ? m.files.find(f => !f.deleted && f.type === 'citationXML')
          : m.files.find(f => !f.deleted && f.type === 'PMC')
      const xml = await fileUtils.fetchFile(baseUrl + file.url)
      m.xmlFile = xml
      const processObj = {
        xml,
        xsl: styleXSL,
        manuscript: m,
        step: testStep,
        adminUser,
      }
      const tbc = await processXML(processObj)
      if (tbc) styleResult.push(m)
      return true
    }, Promise.resolve())
    styleXSL = null
    queue = null
    logger.info(
      `Converting XML step 2: Create NXML. Queue length: ${styleResult.length})`,
    )
    let nxmlXSL = await useTransform('createNXML')
    let nxmlResult = []
    await styleResult.reduce(async (promise, m) => {
      await promise
      const xml = m.xmlFile
      const processObj = {
        xml,
        xsl: nxmlXSL,
        manuscript: m,
        step: nxmlStep,
        adminUser,
      }
      const nxml = await processXML(processObj)
      if (nxml) {
        m.nxmlFile = nxml
        nxmlResult.push(m)
      }
      return true
    }, Promise.resolve())
    nxmlXSL = null
    styleResult = null
    logger.info(`Converting XML step 3 (queue length ${nxmlResult.length}):`)
    let htmlXSL = await useTransform('createHTML')
    const htmlResult = []
    await nxmlResult.reduce(async (promise, m) => {
      await promise
      const xml = m.nxmlFile
      const processObj = {
        xml,
        xsl: htmlXSL,
        manuscript: m,
        step: htmlStep,
        adminUser,
      }
      const tbc = await processXML(processObj)
      if (tbc) htmlResult.push(m)
      return true
    }, Promise.resolve())
    htmlXSL = null
    nxmlResult = null
    logger.info(`Converting XML step 4. Queue length: ${htmlResult.length})`)
    await htmlResult.reduce(async (promise, m) => {
      await promise
      const xml = m.xmlFile
      const processObj = { xml, manuscript: m, step: updateStep, adminUser }
      return processXML(processObj)
    }, Promise.resolve())
    logger.info(`XML conversion batch complete. ${total} processed.\n`)
  }
}

// Queue for validation after tagging
async function validateQueue(queue) {
  if (queue && queue.length > 0) {
    logger.info(`Validating XML (queue length ${queue.length})`)
    const errs = []
    let xslt = await useTransform('schematron')
    await queue.reduce(async (promise, m) => {
      await promise
      const trx = await transaction.start(knex)
      let warnings = []
      try {
        const file = m.files.find(file => !file.deleted && file.type === 'PMC')
        logger.info(` Validating ${file.filename} for ${m.id}`)
        warnings = await schValidate(file.url, xslt)
      } catch (err) {
        errs.push(m.id)
        const subject = 'Fix tagging errors [schematron]'
        // Send to file-error status to mark validation is complete
        await ManuscriptManager.update(
          { id: m.id, pdfDepositState: '', status: 'file-error' },
          user.id,
          trx,
        )
        // Send back for tagging correction
        await ManuscriptManager.retag(
          {
            manuscriptId: m.id,
            manuscriptVersion: m.version,
            notesType: 'userMessage',
            content: JSON.stringify({
              to: ['tagger'],
              subject,
              message: err.message,
            }),
          },
          user.id,
          trx,
        )
        return trx.commit()
      }
      const note = {
        notesType: 'schematron',
        content: JSON.stringify(warnings),
        manuscriptId: m.id,
        manuscriptVersion: m.version,
      }
      const [exists] = await NoteManager.findByManIdAndType(
        m.id,
        'schematron',
        trx,
      )
      if (exists) {
        note.id = exists.idtype
        await NoteManager.update(note, user.id, trx)
      } else {
        await NoteManager.create(note, user.id, trx)
      }
      await ManuscriptManager.update(
        { id: m.id, pdfDepositState: 'CONVERTING_XML' },
        user.id,
        trx,
      )
      return trx.commit()
    }, Promise.resolve())
    xslt = null
    logger.info(
      `All XML validations complete. ${queue.length} processed, ${errs.length} invalid.\n`,
    )
  }
}
