const { pushXML } = require('./xmlProcess')
const xmlChange = require('./xmlChange')
const { styleValidate } = require('./xmlValidate')
const { getTransform } = require('./transform')

module.exports = { pushXML, xmlChange, getTransform, styleValidate }
