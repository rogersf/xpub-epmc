const libxml = require('libxmljs')
const fs = require('fs')
const uuidv4 = require('uuid/v4')
const logger = require('@pubsweet/logger')
const config = require('config')
const saxon = require('saxon-js')
const fetch = require('node-fetch')
const { transaction } = require('objection')
const BaseModel = require('@pubsweet/base-model')
const tar = require('../utils/unTar.js')
const fileUtils = require('../utils/files.js')
const Manuscript = require('../xpub-model/entities/manuscript')
const File = require('../xpub-model/entities/file')
const Note = require('../xpub-model/entities/note')
const { transform } = require('./transform')

const { baseUrl } = config.get('pubsweet-server')
const knex = BaseModel.knex()
const { hydraSearch } = require('../citation-search')

const addCitation = async function addCitation(
  manuscript,
  userId,
  bearer,
  xslt = 'addCitation',
) {
  logger.info(`[${manuscript.id}] Adding citation`)
  const trx = await transaction.start(knex)
  const headers = new fetch.Headers({ Authorization: `Bearer ${bearer}` })
  try {
    const xml = manuscript.files.find(file => file.type === 'PMC')
    if (xml) {
      const { id: manId, meta, version, journal } = manuscript
      const { articleIds, notes, location, publicationDates, subjects } = meta
      const { journalTitle } = journal
      const { issn, nlmta } = journal.meta
      const { fpage, lpage, elocationId } = location || {}
      const license = notes && notes.find(n => n.notesType === 'openAccess')
      const licenseLink = license && license.content
      const openAccess =
        licenseLink && licenseLink.search(/\/(by-nd|by-nc-nd)\//i) < 0
      const addLicense = await Note.getLatestValue(manId, 'addLicense', trx)
      const subjGroup =
        subjects &&
        subjects.length > 0 &&
        (await saxon.getResource({
          text: `<subj-group subj-group-type="europepmc-category">${subjects
            .map(s => `<subject>${s}</subject>`)
            .join('')}</subj-group>`,
          type: 'xml',
        }))
      const xmlString = await fileUtils.fetchFile(baseUrl + xml.url)
      const xmlDoc = libxml.parseXml(xmlString)
      logger.debug('XML to transform read from DB')
      const { externalId, systemId } = await xmlDoc.getDtd()
      const refs = xmlDoc.find(
        '//*[self::element-citation or self::mixed-citation]',
      )
      logger.info(`Adding citations to ${manId} XML`)
      const refIDs = []
      if (refs && refs.length > 0) {
        logger.info(`[${manId}] Getting pub-ids for ${refs.length} refs...`)
        await refs.reduce(async (promise, ref) => {
          await promise
          if (!ref.get('@publication-type')) {
            throw new Error(
              `Missing attribute publication-type in ${ref.name()}`,
            )
          }
          const type = ref.get('@publication-type').value()
          if (['journal', 'book'].includes(type)) {
            const id = ref
              .parent()
              .get('@id')
              .value()
            const pos = ref.path().match(/\[([0-9]+)\]$/)
            const position = (pos && pos[1]) || 1
            const reftitle =
              ref.get(`*[contains(name(.), '-title')]`) &&
              ref.get(`*[contains(name(.), '-title')]`).text()
            const firstauthor =
              ref.get('.//*[self::collab or self::surname][1]') &&
              ref.get('.//*[self::collab or self::surname][1]').text()
            const source = ref.get('source') && ref.get('source').text()
            const year = ref.get('year') && ref.get('year').text()
            const volume = ref.get('volume') && ref.get('volume').text()
            const issue = ref.get('issue') && ref.get('issue').text()
            const page = ref.get('fpage')
              ? ref.get('fpage').text()
              : ref.get('elocation-id') && ref.get('elocation-id').text()
            const end = ref.get('lpage') && ref.get('lpage').text()

            const search = `${firstauthor ? `${firstauthor}. ` : ''}${
              source ? `${source}. ` : ''
            }${year ? `${year}` : ''}${volume ? `;${volume}` : ''}${
              issue ? `(${issue})` : ''
            }${page ? `:${page}${end ? `${end}` : ''}` : ''}${
              reftitle ? `|${reftitle}` : ''
            }`
            if (search) {
              const res = await hydraSearch(manId, search, bearer)
              const [pmid] = res
              if (pmid) {
                refIDs.push({ id, position, pmid })
              }
            } else {
              logger.info(
                `[${manId}] Ignore ref ID ${id} for hydra search. No valid search terms.`,
              )
            }
            return true
          }
        }, Promise.resolve())
      }

      let matched
      if (refIDs.length > 0) {
        const pmidList = refIDs.map(l => l.pmid).join()
        logger.info(`[${manId}] Reference IDs to be converted ${pmidList}`)
        const idconv = await fetch(`${baseUrl}/idconv?pmid=${pmidList}`, {
          headers,
        })
        const { records } = await idconv.json()
        logger.info(`[${manId}] IDs converted: ${JSON.stringify(records)}`)

        logger.info(`[${manId}] Adding pub-ids...`)
        const resultXML = buildCitationFragment(refIDs, records)

        logger.info(`[${manId}] citations to add:`)
        logger.info(resultXML)
        matched = await saxon.getResource({
          text: resultXML,
          type: 'xml',
        })
      } else {
        // matched will be left undefined
        logger.info(
          `[${manId}] No refs with valid PMID found for any references`,
        )
      }

      logger.debug(`XML checks to add references done for ${manId}`)
      const params = {
        externalId,
        systemId,
        journalTitle: journalTitle || '',
        nlmta: nlmta || '',
        volume: meta.volume || '',
        issue: meta.issue || '',
        fpage: fpage || '',
        lpage: lpage || '',
        elocationId: elocationId || '',
        permissions: '',
        subjects: subjGroup || '',
        addLicense: (!openAccess && addLicense) || '',
        version: (version > 0 && Math.round(version)) || '',
        matched,
      }
      const pf = manuscript.files.find(file => file.type === 'metadata')
      if (pf) {
        saxon
          .getResource({ location: baseUrl + pf.url, type: 'xml' })
          .then(p => {
            logger.debug('Permissions file retrieved')
            params.permissions = p
          })
          .catch(e => logger.debug('Permissions file invalid'))
      }
      issn.forEach(n => {
        const t = n.type.toLowerCase().charAt(0)
        params[`${t}issn`] = n.id
      })
      articleIds.forEach(aid => {
        if (aid.pubIdType !== 'pre-pmc') {
          params[aid.pubIdType] = aid.id
        }
      })
      publicationDates.forEach(pd => {
        params[pd.type] = pd.jatsDate
          ? Object.keys(pd.jatsDate)
              .filter(el => pd.jatsDate[el])
              .sort()
              .map(el => `${el}=${pd.jatsDate[el]}|`)
              .join('')
          : ''
      })
      logger.debug('XML generated. Trying to transform')
      const newXml = await transform(xmlString, xslt, trx, params)
      logger.debug('XML transformation done')
      const tmpPath = await tar.createTempDir()
      const tmpFilePath = `${tmpPath}/${manId}.xml`
      fs.writeFileSync(tmpFilePath, newXml)
      const uuid = uuidv4()
      const mimeType = 'application/xml'
      const newPath = `/download/${uuid}.xml`
      logger.debug(
        `GridFS uploading citation added XML for manuscript ${manId} version ${version}`,
      )
      const fileSize = fs.statSync(tmpFilePath).size
      await fileUtils.fileUpload(
        manId,
        version,
        `${uuid}.xml`,
        `${manId}.xml`,
        tmpFilePath,
        mimeType,
      )

      const fileInfo = {
        url: newPath,
        filename: `${manId}.xml`,
        type: 'citationXML',
        label: '',
        size: fileSize,
        mimeType,
        manuscriptId: manId,
        manuscriptVersion: version,
      }
      const existing = manuscript.files.find(
        file => file.type === 'citationXML',
      )
      if (existing) fileInfo.id = existing.id
      await File.save(fileInfo, userId, trx)
      logger.debug('XML uploaded')
      await Manuscript.update(
        { id: manId, pdfDepositState: 'CONVERTING_XML' },
        userId,
        trx,
      )
      await trx.commit()
      logger.info(`[${manId}] XML transformation process to add citation done`)
    } else {
      throw new Error('No XML file found')
    }
  } catch (err) {
    if (trx) {
      await trx.rollback()
    }
    logger.error(`[${manuscript.id}] Adding citation failed`)
    await Manuscript.update(
      {
        id: manuscript.id,
        formState: `Error adding citation to XML: ${err.message}`,
        status: 'repo-triage',
        pdfDepositState: null,
      },
      userId,
    )
    logger.error(err)
  }
}

const buildCitationFragment = (refIDs, records) => {
  // Build XML fragment
  if (records && records.length > 0) {
    const doc = libxml.Document()
    const citations = doc.node('citations')
    records.forEach(ids => {
      const r = refIDs.find(i => i.pmid === ids.pmid)
      const { id, position } = r
      const citation = citations.node('citation').attr({ refid: id, position })
      Object.keys(ids)
        .filter(t => ['pmid', 'pmcid', 'doi'].includes(t))
        .forEach(t =>
          citation.node('pub-id', ids[t]).attr({ 'pub-id-type': t }),
        )
    })
    return citations.toString()
  }
}

module.exports = {
  addCitation,
  buildCitationFragment,
}
