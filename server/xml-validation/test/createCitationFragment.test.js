const logger = require('@pubsweet/logger')
const { buildCitationFragment } = require('../xmlFinalize')

const refIDs = [{ id: 'R1', position: '1', pmid: 'pmid123' }]
const records = [
  {
    pmid: 'pmid123',
    doi: 'test doi with special chars <>',
    pmcid: 'pmcid321',
  },
]

test('create citations xml fragment', async () => {
  const resultXML = buildCitationFragment(refIDs, records)
  expect(resultXML).toEqual(expect.anything())
  logger.info(resultXML)
})
