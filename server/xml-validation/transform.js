const path = require('path')
const saxon = require('saxon-js')
const logger = require('@pubsweet/logger')
const { stringify, parse } = require('flatted')
const { XSLManager } = require('../xpub-model')
const fileUtils = require('../utils/files.js')

// Transformation file locations
const transforms = {
  // Frequent changes to addCitation, use XSL file
  addCitation: {
    sefPath: null,
    xslPath: path.resolve(__dirname, 'xsl/addcitation.xsl'),
  },
  // Infrequent changes, .sef.json included
  styleChecker: {
    sefPath: path.resolve(
      __dirname,
      'xsl/stylechecker/nlm-stylechecker.sef.json',
    ),
    xslPath: path.resolve(__dirname, 'xsl/stylechecker/nlm-stylechecker.xsl'),
  },
  styleReporter: {
    sefPath: path.resolve(
      __dirname,
      'xsl/stylechecker/style-reporter.sef.json',
    ),
    xslPath: path.resolve(__dirname, 'xsl/stylechecker/style-reporter.xsl'),
  },
  createNXML: {
    sefPath: path.resolve(__dirname, 'xsl/pnihms2pmc3.sef.json'),
    xslPath: path.resolve(__dirname, 'xsl/pnihms2pmc3.xsl'),
  },
  // Frequent changes and shared external location
  createHTML: {
    sefPath: null,
    xslPath:
      'https://gitlab.ebi.ac.uk/literature-services/public-projects/full-text-xsl/-/raw/master/jats2html.xsl',
  },
  schematron: {
    sefPath: null,
    xslPath:
      'https://gitlab.ebi.ac.uk/literature-services/public-projects/full-text-xsl/-/raw/master/schematron/epmc-schematron.xsl',
  },
  // Only used when the XSL has been changed, use XSL file
  changeXML: {
    sefPath: null,
    xslPath: path.resolve(__dirname, 'xsl/changexml.xsl'),
  },
}

// Saxon creates a fast cached version of the XSL.
// This is stored in the database config.xslt table.
// Set the row flag to true when this cache should be cleared.
// Flag is set automatically during seeding/redeployment.
const getTransform = async function getTransform(name, trx = null) {
  const { flag, object } = await XSLManager.find(name)
  if (!object || flag) {
    // If no cached XSLT in DB, or update is requested
    const { sefPath, xslPath } = transforms[name]
    if (sefPath) {
      // Update from local sef.json file
      return { update: true, type: 'stylesheetLocation', xsl: sefPath }
    }
    try {
      // Update from compiled XSLT
      const env = saxon.getPlatform()
      const xslString = xslPath.startsWith('http')
        ? await fileUtils.fetchFile(xslPath)
        : env.readFile(xslPath)
      const xsl = env.parseXmlFromString(xslString)
      xsl._saxonBaseUri = 'file:///'
      const sef = saxon.compile(xsl)
      return { update: true, type: 'stylesheetInternal', xsl: sef }
    } catch (err) {
      logger.err(`Error getting transformation for ${name}`, err)
      throw new Error(`${name} conversion XSL file not accessible`)
    }
  }
  return { update: false, type: 'stylesheetInternal', xsl: parse(object) }
}

// Store XSL cache in database
const updateTransform = async function updateTransform(
  name,
  object,
  trx = null,
) {
  logger.info(`Saving transform ${name} to database`)
  object = stringify(object)
  return XSLManager.save({ name, object, flag: false }, trx)
}

// Saxon async version is more performant but does not create cache
function saxonTransform(config) {
  return new Promise((resolve, reject) => {
    saxon
      .transform(config, 'async')
      .then(output => {
        resolve(output)
      })
      .catch(err => reject(err))
  })
}

// Export script
const transform = async function transform(
  xml,
  xslt,
  trx = null,
  params,
  callback,
) {
  const name = xslt.name ? xslt.name : xslt
  logger.info(`Performing transform ${name}`)
  // Create config settings
  const config = {
    sourceText: xml,
    destination: 'serialized',
    outputProperties: {
      indent: false,
    },
  }
  const { update, type, xsl } = xslt.name ? xslt : await getTransform(name, trx)
  config[type] = xsl
  if (params) {
    config.stylesheetParams = params
  }
  if (callback) {
    config.deliverMessage = callback
  }
  try {
    // Get cache for DB and result
    const { stylesheetInternal, principalResult } = update
      ? await saxon.transform(config)
      : await saxonTransform(config) // Async transform, will return cached style sheet

    // Store cache in DB if called to do so
    if (update) await updateTransform(name, stylesheetInternal, trx)
    // Return result
    return principalResult
  } catch (err) {
    logger.error('Error performing XSLT transformation', err)
    throw new Error(err)
  }
}

module.exports = { transform, getTransform }
