const path = require('path')
const libxml = require('libxmljs')
const config = require('config')
const serializer = require('xmlserializer')
const logger = require('@pubsweet/logger')
const fileUtils = require('../utils/files.js')
const { transform } = require('./transform')

const { baseUrl } = config.get('pubsweet-server')

const schValidate = async function schematron(xmlUrl, xslt = 'schematron') {
  // Run schematron XSL against the XML
  const xml = await fileUtils.fetchFile(baseUrl + xmlUrl)
  const messages = []
  await transform(xml, xslt, null, null, x => {
    const string = serializer.serializeToString(x)
    messages.push(string)
  })
  // Check for errors, not just warnings
  if (messages.some(err => err.startsWith('Error:'))) {
    const errorText = `<b>Schematron errors present:</b><br/>
<ul>${messages.map(e => `<li>${e}</li>`).join('')}</ul>
Check against the schematron before delivery:<br/><a href="https://gitlab.ebi.ac.uk/literature-services/public-projects/full-text-xsl/-/blob/master/schematron">https://gitlab.ebi.ac.uk/literature-services/public-projects/full-text-xsl/-/blob/master/schematron</a>`
    throw new Error(errorText)
  }
  return messages
}

const styleValidate = async function checkStyle(xmlString, filename) {
  logger.debug(`Validating unsaved XML`)
  const params = { showWarnings: 'no', filename }
  try {
    const styleChecked = await transform(xmlString, 'styleChecker')
    const reported = await transform(
      styleChecked,
      'styleReporter',
      null,
      params,
    )
    return reported
  } catch (err) {
    return '<p>Unable to check style! Please try again later.</p>'
  }
}

const xsdValidate = async function validate(xmlString, flavor, type) {
  logger.debug(`Validating ${type}`)
  try {
    // Check well formed
    const xml = libxml.parseXml(xmlString)
    // Validate
    const { systemId } = await xml.getDtd()
    const [sysId] = systemId.split('.')
    const xsdPath = `xsl/xsd/${flavor}`
    const baseUrl = `${path.resolve(__dirname, `${xsdPath}/`)}/`
    const xsdString = await fileUtils.readData(
      path.resolve(__dirname, `${xsdPath}/${sysId}.xsd`),
    )
    const xsd = libxml.parseXml(xsdString, { baseUrl })
    const xmlIsValid = xml.validate(xsd)
    const errors = xml.validationErrors
    if (!xmlIsValid) {
      let errString = `Invalid ${type}: \n`
      errors.forEach((err, i) => {
        errString += `${err}\nLine: ${err.line}.`
        if (err.str1) {
          errString += ` ID: ${err.str1}.`
        }
        if (i !== errors.length - 1) {
          errString += `\n\n`
        }
      })
      throw new Error(errString)
    }
  } catch (err) {
    throw new Error(err)
  }
}

module.exports = { xsdValidate, schValidate, styleValidate }
