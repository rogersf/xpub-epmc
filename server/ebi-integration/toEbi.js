const logger = require('@pubsweet/logger')
const config = require('config')
const moment = require('moment')
const superagent = require('superagent')
require('superagent-proxy')(superagent)
const {
  ManuscriptManager,
  IdentityManager,
  NoteManager,
} = require('../xpub-model')
const fileUtils = require('../utils/files')

const { baseUrl } = config.get('pubsweet-server')
const { submitUrl, testUrl, plusAuth, preprintAuth } = config.get(
  'ebi-repository',
)

const checkEmbargo = async (pubDates, monthDelay = 0, submitterId) => {
  const [submitter] = await IdentityManager.findByUserId(submitterId)
  const identity = submitter && submitter.meta && submitter.meta.publisher
  const epubPublisher = identity && identity === 'NPG'
  const releaseDelay = parseInt(monthDelay, 10)
  const epubDate =
    pubDates.find(date => date.type === 'epub') &&
    pubDates.find(date => date.type === 'epub').date
  const ppubDate =
    pubDates.find(date => date.type === 'ppub') &&
    pubDates.find(date => date.type === 'ppub').date
  const startDate =
    (epubPublisher && epubDate && epubDate) ||
    (ppubDate && ppubDate) ||
    (epubDate && epubDate)
  const publishDate = moment(startDate)
    .add(releaseDelay, 'M')
    .format('YYYY-MM-DD')
  return moment().isBefore(publishDate, 'day')
}

module.exports.createManifest = async function createManifest(
  manuscriptId,
  userId,
) {
  try {
    const manuscript = await ManuscriptManager.findById(manuscriptId, userId)
    const preprint = manuscript.organization.name === 'Europe PMC Preprints'
    logger.info(
      `[${manuscriptId}] Create manifest for ${
        preprint ? 'preprint' : 'author manuscript'
      }`,
    )
    const { teams, files: mFiles } = manuscript
    const fileTypes = [
      'supplement',
      'supplement_tag',
      'PMCfinal',
      'IMGview',
      'pdf4load',
      'pdf4print',
    ]
    const newTypes = ['supplement', 'supplement', 'xml', 'image', 'pdf', 'pdf']
    const {
      articleIds,
      notes,
      releaseDelay,
      publicationDates,
    } = manuscript.meta
    const underEmbargo =
      !preprint &&
      (await checkEmbargo(
        publicationDates,
        releaseDelay,
        teams.find(t => t.role === 'submitter') &&
          teams.find(t => t.role === 'submitter').teamMembers[0].user.id,
      ))
    const resend =
      manuscript.ebiState && manuscript.ebiState.startsWith('Repo resend')
    const indexOnly =
      (resend &&
        !['repo-processing', 'published'].includes(manuscript.status)) ||
      manuscript.ebiState === 'index only' ||
      underEmbargo
    const token = Date.parse(mFiles.find(f => f.type === 'PMCfinal').updated)
    const openAccess = notes && notes.some(n => n.notesType === 'openAccess')
    const addLicense = await NoteManager.getLatestValue(
      manuscriptId,
      'addLicense',
    )
    const files = await Promise.all(
      mFiles
        .filter(file => fileTypes.includes(file.type))
        .map(async f => {
          if (['supplement', 'pdf4load', 'pdf4print'].includes(f.type)) {
            f.filename = fileUtils.normalizeFilename(f)
          }
          const { filename, mimeType, type, url } = f
          return {
            filename,
            mimeType,
            type: newTypes[fileTypes.indexOf(type)],
            url: `${testUrl || baseUrl}${url}`,
          }
        }),
    )
    const metadata = {
      availabilityStatus: openAccess || addLicense === 'approved' ? 'O' : 'F',
      fullTextType: 'XML',
      hasPDF: files.some(f => f.type === 'pdf'),
      hasSupplements: files.some(f => f.type === 'supplement'),
      indexOnly,
    }

    // Set funderInitiativeId for preprint
    if (preprint && manuscript.meta && manuscript.meta.subjects) {
      const { subjects } = manuscript.meta
      if (subjects.includes('COVID-19')) {
        metadata.funderInitiative = 'COVID-19'
      } else {
        metadata.funderInitiative = 'EPMCfunders'
      }
    }

    const sendObj = {
      source: preprint ? 'ppr' : 'pmc',
      version: manuscript.version,
      clientId: manuscriptId,
      callback: `${testUrl || baseUrl}/api/${
        !resend && preprint ? 'ppr' : ''
      }id${resend ? 'update' : ''}/?token=${token}${
        indexOnly ? '&indexOnly=true' : ''
      }`,
      files,
      metadata,
    }
    if (articleIds.find(a => a.pubIdType === 'pmcid')) {
      sendObj.id = articleIds.find(a => a.pubIdType === 'pmcid').id
    } else if (articleIds.find(a => a.pubIdType === 'pprid')) {
      sendObj.id = articleIds.find(a => a.pubIdType === 'pprid').id
    }
    if (articleIds.find(a => a.pubIdType === 'pmid')) {
      sendObj.pmid = articleIds.find(a => a.pubIdType === 'pmid').id
    }
    try {
      logger.info(`Sending ${manuscriptId} to EBI`)
      logger.info(`URL: ${submitUrl}`)
      logger.info(`Request body: ${JSON.stringify([sendObj])}`)
      let superagentRequest = superagent('POST', submitUrl)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Authorization', preprint ? preprintAuth : plusAuth)
        .send(JSON.stringify([sendObj]))
      if (process.env.superagent_http_proxy) {
        superagentRequest = superagentRequest.proxy(
          process.env.superagent_http_proxy,
        )
      }

      const response = await superagentRequest
      logger.info(
        `Response status: ${response.status}, body: ${JSON.stringify(
          response.body,
        )}`,
      )
    } catch (error) {
      const { body, status } = error.response
      logger.error(`[${manuscript.id}] Exception caught sending to EBI repo`)
      throw new Error(
        `${status} ${error.message}\n ${
          body[0] && body[0].errorMessage
            ? body[0].errorMessage
            : JSON.stringify(body)
        }`,
      )
    }
    await ManuscriptManager.update(
      {
        id: manuscriptId,
        ebiState: `sent to EBI${indexOnly ? ' (index only)' : ''}`,
      },
      userId,
    )
    logger.info(`${manuscriptId} sent to EBI`)
  } catch (err) {
    await ManuscriptManager.update(
      {
        id: manuscriptId,
        formState: `Error sending to EBI.\n ${err}`,
        status: 'repo-triage',
        pdfDepositState: null,
      },
      userId,
    )
    logger.error(err)
  }
}
