/**
 * Validate the token of a specific manuscript
 */

const { FileManager } = require('../xpub-model')

const isValidTimestampToken = async ({ manuscriptId, timestampToken }) => {
  if (!manuscriptId) throw new Error('Invalid argument: manuscript id')
  if (!timestampToken) throw new Error('Invalid argument: timestamp')

  const [xml] = await FileManager.findByManIdAndType(manuscriptId, 'PMCfinal')
  if (!xml) return false

  const xmlUpdateTime = Date.parse(xml.updated)
  if (!xmlUpdateTime) throw new Error('XML has no update time')

  switch (typeof timestampToken) {
    case 'number':
      return timestampToken === xmlUpdateTime
    case 'string':
      return parseInt(timestampToken, 10) === xmlUpdateTime
    default:
      throw new Error(
        `Invalid argument type: timestamp. Expected number of string, actual is ${typeof timestampToekn}`,
      )
  }
}

module.exports = {
  isValidTimestampToken,
}
