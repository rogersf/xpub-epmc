const logger = require('@pubsweet/logger')
const express = require('express')
const { ManuscriptManager, FileManager } = require('../xpub-model')
const getUser = require('../utils/user.js')
const config = require('config')
const { createManifest } = require('./toEbi')
const { isValidTimestampToken } = require('./tokenValidation')
const { getMintedFtId } = require('./requests/getMintedFtId')

const { states } = config

const configureApp = async app => {
  const { id: aId } = await getUser.getAdminUser()
  app.use(express.json())
  // wait to prevent posting and receiving callback in same second
  app.post('/api/id/', async (req, res) => {
    // accept author manuscript callback
    const body = await req.body
    const { clientId } = body
    if (clientId) {
      logger.info(`${clientId} [/api/id] Request body`)
      logger.info(JSON.stringify(req.body))
      logger.info(`${clientId} [/api/id] Request query paramets`)
      logger.info(JSON.stringify(req.query))
      const man = await ManuscriptManager.findById(clientId, aId, null, false)
      if (man && man.status === 'repo-ready') {
        res.status(200).send('Callback response recieved')
        await new Promise(resolve => setTimeout(resolve, 1000))
        processManuscriptCompletionCallback(body, parseInt(req.query.token, 10))
      } else {
        res.status(503).send('ERROR: No submission in correct state')
      }
    } else {
      res.status(400).send('ERROR: No clientId included in request body')
    }
  })

  app.post('/api/pprid/', async (req, res) => {
    // accept preprint callback
    const body = await req.body
    const { clientId } = body
    if (clientId) {
      logger.info(`${clientId} [/api/pprid] Request body`)
      logger.info(JSON.stringify(req.body))
      logger.info(`${clientId} [/api/pprid] Request query paramets`)
      logger.info(JSON.stringify(req.query))
      const man = await ManuscriptManager.findById(clientId, aId, null, false)
      if (man && man.status === 'repo-ready') {
        const { token, indexOnly } = req.query
        res.status(200).send('Callback response recieved')
        await new Promise(resolve => setTimeout(resolve, 1000))
        processPreprintCompletionCallback(
          body,
          parseInt(token, 10),
          !!indexOnly,
        )
      } else {
        res.status(503).send('ERROR: No submission in correct state')
      }
    } else {
      res.status(400).send('ERROR: No clientId included in request body')
    }
  })

  app.post('/api/idupdate/', async (req, res) => {
    // accept callback without changing manuscript status
    const body = await req.body
    const { clientId } = body
    if (clientId) {
      logger.info(`${clientId} [/api/idupdate] Request body`)
      logger.info(JSON.stringify(req.body))
      logger.info(`${clientId} [/api/idupdate] Request query paramets`)
      logger.info(JSON.stringify(req.query))

      if (ManuscriptManager.findById(clientId, aId, null, false)) {
        const { token, indexOnly } = req.query
        res.status(200).send('Callback response recieved')
        await new Promise(resolve => setTimeout(resolve, 1000))
        processResendCompletionCallback(body, token, !!indexOnly)
      } else {
        res.status(503).send('ERROR: Database unavailable')
      }
    } else {
      res.status(400).send('ERROR: No clientId included in request body')
    }
  })

  app.post('/api/delete/', async (req, res) => {
    // accept deletion callback
    const body = await req.body
    const { clientId } = body
    if (clientId) {
      if (ManuscriptManager.findById(clientId, aId, null, false)) {
        const { token, replace } = req.query
        res.status(200).send('Callback response recieved')
        await new Promise(resolve => setTimeout(resolve, 1000))
        withdrawCitation(body, token, !!replace)
      } else {
        res.status(503).send('ERROR: Database unavailable')
      }
    } else {
      res.status(400).send('ERROR: No clientId included in request body')
    }
  })

  app.post('/api/resend-to-repo/:manuscriptId', async (req, res) => {
    const { manuscriptId } = req.params
    logger.info(`[${manuscriptId}] Resend to repo api`)
    try {
      const message = await syncOrResendToRepo(manuscriptId, aId)
      res.status(200).send({
        success: true,
        message,
      })
    } catch (e) {
      logger.error(e)
      res.status(500).send({
        result: 'failed',
        message: `[${manuscriptId}] Exception occurred when processing of the reqeust, check server log for details`,
      })
    }
  })
}

// Return a message indication of the operations performed
const syncOrResendToRepo = async (manuscriptId, userId) => {
  const manuscript = await ManuscriptManager.findById(
    manuscriptId,
    userId,
    null,
    true,
  )
  const isPreprint = !!(
    manuscript &&
    manuscript.organization &&
    manuscript.organization.name === 'Europe PMC Preprints'
  )

  logger.info(
    `[${manuscriptId}] Sync or resend. Type: ${
      isPreprint ? 'preprint' : 'manuscript'
    }`,
  )

  const { resultType, id } = await getMintedFtId(manuscript.id, isPreprint)

  switch (resultType) {
    case 0: {
      // fulltext ID assigned, update manuscript status
      logger.info(
        `[${manuscript.id}] Full text id present, completing the ebi repo submission process with full text id = ${id}`,
      )

      if (isPreprint) {
        const indexOnly =
          manuscript.ebiState && manuscript.ebiState.includes('index only')
        await completePreprint({
          manuscript,
          indexOnly,
        })
      } else {
        await completeManuscriptCitation({ manuscript, fullTextId: id })
      }
      return `[${manuscript.id}] Full text id retrieved, manscript status updated`
    }
    case 1: // No full text id assigned
    case 2: {
      // No submission found
      // resend to ebi repo
      logger.info(
        `[${manuscript.id}] Full text ID has not been assigned, resending to repo`,
      )

      // await ManuscriptManager.update({
      //   id: manuscriptId,
      //   status: 'Repo resend',
      // })
      await createManifest(manuscript.id, userId)

      return `[${manuscript.id}] Full text ID has not been assigned,resent to repo`
    }
    default:
      throw new Error(`[${manuscript.id}] Error occured`)
  }
}

async function processManuscriptCompletionCallback(data, token) {
  const { clientId: manuscriptId, id, message, success } = data
  logger.info(`[${manuscriptId}] Processing EBI response for author manuscript`)

  const adminUser = await getUser.getAdminUser()

  try {
    if (success && id) {
      const manuscript = await ManuscriptManager.findById(
        manuscriptId,
        adminUser.id,
      )
      if (
        await isValidTimestampToken({
          manuscriptId,
          timestampToken: token,
        })
      ) {
        await completeManuscriptCitation({ manuscript, fullTextId: id })
      } else {
        throw new Error('Bad or out of date XML file token, please resend')
      }
    } else {
      if (!id) {
        throw new Error(`No PMCID generated. ${message}`)
      }
      logger.error(`[${manuscriptId}] EBI repo returned failure`)
      throw new Error(message)
    }
  } catch (err) {
    logger.error(err)
    await ManuscriptManager.update(
      {
        id: manuscriptId,
        formState: `Error processing EBI response: ${err.message}`,
        status: 'repo-triage',
        pdfDepositState: null,
      },
      adminUser.id,
    )
  }
}

async function completeManuscriptCitation({ manuscript, fullTextId }) {
  const adminUser = await getUser.getAdminUser()
  const { articleIds } = manuscript.meta
  const hasId = articleIds.some(aid => aid.pubIdType === 'pmcid')
  const meta = {
    articleIds: articleIds.filter(aid => aid.pubIdType !== 'pmcid'),
  }
  meta.articleIds.push({
    pubIdType: 'pmcid',
    id: fullTextId,
  })
  if (!hasId) {
    meta.articleIds.push({
      pubIdType: 'pre-pmc',
      id: fullTextId,
    })
    const { manuscript: dupeMan } = await ManuscriptManager.findByArticleId(
      fullTextId,
      adminUser.id,
    )
    if (dupeMan) {
      throw new Error(
        `EBI tried to assign ${fullTextId}. ${fullTextId} already assigned to ${dupeMan.id}. Please contact EBI fulltext repository.`,
      )
    }
  }
  logger.info(`Success. ${manuscript.id} received by EBI`)
  await ManuscriptManager.update(
    {
      formState: null,
      id: manuscript.id,
      meta,
      decision: 'accepted',
      ebiState: hasId ? 'EBI received' : 'adding PMCID to EBI package',
      status: hasId ? 'repo-processing' : manuscript.status,
    },
    adminUser.id,
  )
  if (!hasId) {
    await new Promise(resolve => setTimeout(resolve, 1000))
    await ManuscriptManager.update(
      {
        id: manuscript.id,
        pdfDepositState: 'ADDING_CITATION',
      },
      adminUser.id,
    )
  }
}

async function processPreprintCompletionCallback(data, token, indexOnly) {
  const { clientId: manuscriptId, message, success } = data
  logger.info(`[${manuscriptId}] Processing EBI response for preprint`)
  const adminUser = await getUser.getAdminUser()
  const manuscript = await ManuscriptManager.findById(
    manuscriptId,
    adminUser.id,
    null,
    false,
  )
  try {
    if (success) {
      logger.info(`[${manuscriptId}] EBI repo success result`)
      if (
        await isValidTimestampToken({
          manuscriptId,
          timestampToken: token,
        })
      ) {
        await completePreprint({
          manuscript,
          indexOnly,
        })
      } else {
        throw new Error(
          `[${manuscriptId}] Bad or out of date XML file token, please resend`,
        )
      }
    } else {
      logger.error(
        `[${manuscriptId}] Failure returned from EBI repo: ${message}`,
      )
      throw new Error(message)
    }
  } catch (err) {
    logger.error(
      `[${manuscriptId}] Error found when processoing EBI callback`,
      err,
    )
    await ManuscriptManager.update(
      {
        id: manuscriptId,
        formState: `Error from EBI: ${err.message}`,
        status: 'repo-triage',
        ebiState: indexOnly ? 'index only' : null,
        pdfDepositState: null,
      },
      adminUser.id,
    )
  }
}

async function completePreprint({ manuscript, indexOnly }) {
  logger.info(
    `[${manuscript.id}] Preprint full text successfully registered at EBI, update preprint status`,
  )
  const adminUser = await getUser.getAdminUser()
  const updateObj = {
    formState: null,
    id: manuscript.id,
    decision: 'accepted',
    status: indexOnly ? 'xml-qa' : 'repo-processing',
    ebiState: indexOnly ? 'EBI received index only copy' : 'success',
  }
  await ManuscriptManager.update(updateObj, adminUser.id)
}

async function processResendCompletionCallback(data, token, indexOnly) {
  const { clientId: manuscriptId, id, message, success } = data
  logger.info(`[${manuscriptId}] Processing EBI response for resend`)
  const adminUser = await getUser.getAdminUser()
  try {
    if (success && id) {
      // check token validity
      if (
        !isValidTimestampToken({
          manuscriptId,
          timestampToken: token,
        })
      ) {
        throw new Error('Bad or out of date XML file token, please resend')
      }
      logger.info(
        `[${manuscriptId}] Full text successfully registered at EBI by resend`,
      )
      const updateObj = {
        id: manuscriptId,
        decision: 'accepted',
        ebiState:
          (id.startsWith('PMC') && 'EBI received') ||
          (indexOnly && 'EBI received index only copy') ||
          'success',
      }
      await ManuscriptManager.update(updateObj, adminUser.id)
    } else {
      throw new Error(message)
    }
  } catch (err) {
    await ManuscriptManager.update(
      {
        id: manuscriptId,
        formState: `Error from EBI: ${err.message}`,
        status: 'repo-triage',
        ebiState: indexOnly ? 'index only' : null,
        pdfDepositState: null,
      },
      adminUser.id,
    )
    logger.error(
      `[${manuscriptId}] Error processing EBI response for resend`,
      err,
    )
  }
}

async function withdrawCitation(data, token, replace) {
  const { clientId, id, message, success } = data
  logger.info(`Processing EBI response for ${clientId}`)
  const adminUser = await getUser.getAdminUser()
  try {
    if (success) {
      // check token validity
      const timestampToken =
        typeof token === 'string' ? parseInt(token, 10) : token
      const [xml] = await FileManager.findByManIdAndType(clientId, 'PMC')
      if (!xml || Date.parse(xml.updated) !== timestampToken) {
        throw new Error(
          'Bad or out of date XML file token, please attempt withdrawal again',
        )
      }
      if (replace) {
        const upMan = await ManuscriptManager.update(
          {
            id: clientId,
            ebiState: 'Record removed',
            decision: null,
          },
          adminUser.id,
        )
        if (states.indexOf(upMan.status) >= states.indexOf('repo-ready')) {
          await ManuscriptManager.update(
            {
              id: clientId,
              status: 'repo-ready',
              pdfDepositState: 'ADDING_CITATION',
            },
            adminUser.id,
          )
        }
      } else if (id.startsWith('PMC')) {
        await ManuscriptManager.update(
          {
            id: clientId,
            ebiState: 'Record removed',
            ncbiState: 'ncbi-withdraw',
          },
          adminUser.id,
        )
      } else if (id.startsWith('PPR')) {
        await ManuscriptManager.delete(clientId, adminUser.id)
      } else {
        throw new Error('Unknown ID type')
      }
    } else {
      throw new Error(message)
    }
  } catch (err) {
    await ManuscriptManager.update(
      {
        id: clientId,
        formState: `Withdrawal error from EBI: ${err.message}`,
        status: 'withdrawal-triage',
        pdfDepositState: null,
      },
      adminUser.id,
    )
    logger.error(err)
  }
}

module.exports = {
  configureApp,
  syncOrResendToRepo,
}
