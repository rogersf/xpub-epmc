const config = require('config')
const Email = require('@pubsweet/component-send-email')
const logger = require('@pubsweet/logger')
const lodash = require('lodash')
const {
  htmlEmailBase,
  setReviewerTemplate,
  newReviewerTemplate,
  newReviewFinalTemplate,
  setReviewFinalTemplate,
  checkReviewerTemplate,
  rejectSubmissionTemplate,
  removeDupeTemplate,
  alreadyCompleteTemplate,
  newPackageForTaggingTemplate,
  processedTaggingFilesTemplate,
  bulkUploadTemplate,
  finalReviewTemplate,
  correctedReviewTemplate,
  preprintReviewTemplate,
  preprintReviewTemplateCovid,
  preprintVersionReviewTemplate,
  completedTemplate,
  completedPreprintTemplate,
  resetPasswordTemplate,
  grantsAddedTemplate,
  licenseAddedTemplate,
  incompleteTemplate,
  stalledTemplate,
  stalledPreprintTemplate,
  stalledErrorTemplate,
  deletedTemplate,
  deletedTaggingTemplate,
} = require('./templates')
const superagent = require('superagent')

require('superagent-proxy')(superagent)

const { europepmcUrl } = config
const tagger = config.ftp_tagger.email
const { sender, url, testAddress, system, dev, listName } = config['epmc-email']

const parsed = string => lodash.unescape(string)

const sendMail = async (
  to,
  subject,
  message,
  from = null,
  cc = null,
  bcc = null,
) => {
  const mailData = {
    from: from || sender,
    to: testAddress || to,
    subject: `[${listName}] ${subject}`,
    html: htmlEmailBase(message, url),
  }
  if (cc) {
    mailData.cc = testAddress || cc
  }
  if (bcc) {
    mailData.bcc = testAddress || bcc
  }

  const { transport } = require(config.get('mailer.path'))
  logger.info(
    `Sending email. Mailer host: ${transport.host}, port: ${transport.port}`,
  )
  logger.info(
    `Recipients: ${to}${cc ? `, CC: ${cc}` : ''}${bcc ? `, BCC: ${bcc}` : ''}`,
  )
  if (testAddress) {
    logger.info('Testing! Mail recipients overwritten to', testAddress)
  }

  return Email.send(mailData)
}

const userMessage = async (
  email,
  subject,
  message,
  from = null,
  cc = null,
  bcc = null,
) => {
  const regex = /\b((?:[a-z][\w-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()[\]{};:'".,<>?«»“”‘’]))/gi
  const linkAdded = message.replace(
    regex,
    '<a href="$&" style="color:#20699C">$&</a>',
  )
  const systemBcc = (bcc && [system].concat(bcc)) || system
  const messageDiv = `<div style="white-space: pre-wrap;overflow-wrap: break-word;">${linkAdded}</div>`
  return sendMail(email, subject, messageDiv, from, cc, systemBcc)
}

const taggerErrorEmail = async (subject, message) => {
  const html = `<p>This manuscript has been returned to you. Please correct the following tagging errors.</p>${message}`
  return sendMail(tagger, subject, html, null, null, system)
}

const taggerEmail = async (email, manId, title, link) => {
  const html = newPackageForTaggingTemplate(manId, parsed(title), link)
  const subject = 'New Package Available for Tagging'
  return sendMail(email, subject, html, null, null, system)
}

const processedTaggerEmail = async (email, manId, title, packageName) => {
  const html = processedTaggingFilesTemplate(manId, parsed(title), packageName)
  const subject = 'Tagging files have been uploaded'
  return sendMail(email, subject, html, null, null, system)
}

const errorDevEmail = async (process, err) => {
  const html = `<p>Something went wrong with ${process}<p>
  <p>Error: <b>${err}</b>
  <p>The Europe PMC plus system</p>`
  const subject = `Something went wrong with ${process}`
  return sendMail(dev, subject, html, null, null, system)
}

const bulkUploaderEmail = async (email, message, filename = '') => {
  const html = bulkUploadTemplate(message, filename)
  const subject = `Error while processing package ${filename}`
  return sendMail(email, subject, html, null, null, [system, dev])
}

const resetPassword = async (email, user, token) => {
  const link = `${url}password-reset/${token}`
  const html = resetPasswordTemplate(user, link)
  const subject = `Password reset requested`
  return sendMail(email, subject, html, null, null, system)
}

const completedEmail = async (email, manInfo) => {
  const { title, pmcid, claim } = manInfo
  const link = `${europepmcUrl}/article/PMC/${pmcid}`
  const html = completedTemplate(parsed(title), link, pmcid, claim)
  const subject = `${manInfo.id}: Manuscript available in Europe PMC`
  return sendMail(email, subject, html, null, null, system)
}

const completedPreprintEmail = async (email, manInfo) => {
  const { pprid, title, version, claim } = manInfo
  const link = `${europepmcUrl}/article/PPR/${pprid}`
  const html = completedPreprintTemplate(parsed(title), version, link, claim)
  const subject = `${manInfo.id}: Preprint displayed in Europe PMC`
  return sendMail(email, subject, html, null, null, system)
}

const deletedEmail = async (email, manInfo) => {
  const html = deletedTemplate(parsed(manInfo.title))
  const subject = `${manInfo.id}: Submission cancelled`
  return sendMail(email, subject, html, null, null, system)
}

const deletedTaggerEmail = async manId => {
  const html = deletedTaggingTemplate(manId)
  const subject = `${manId}: Cancel tagging`
  return sendMail(tagger, subject, html, null, null, system)
}

const incompleteEmail = async (manInfo, submitter) => {
  const { title, givenNames, surname } = submitter
  const { email } = submitter.identities[0]
  const salutation = `${title ? `${title} ` : ''}${givenNames} ${surname}`
  const link = `${url}submission/${manInfo.id}/${
    manInfo.status === 'INITIAL' ? 'create' : 'submit'
  }`
  const html = incompleteTemplate(salutation, parsed(manInfo.title), link)
  const subject = `${manInfo.id}: Your submission is incomplete`
  return sendMail(email, subject, html, null, null, system)
}

const stalledEmail = async (manInfo, reviewer, preprint) => {
  const { title, givenNames, surname, email, token } = reviewer
  const salutation = `${title ? `${title} ` : ''}${givenNames} ${surname}`
  const link = token
    ? `${url}dashboard?accept=${token}`
    : `${url}submission/${manInfo.id}/${
        manInfo.status === 'xml-review' ? 'review' : 'submit'
      }`
  const args = [salutation, parsed(manInfo.title), link]
  const html = preprint
    ? stalledPreprintTemplate(...args)
    : stalledTemplate(...args)
  const subject = `${manInfo.id}: Please review your ${
    preprint ? 'preprint' : 'submission'
  }`
  return sendMail(email, subject, html, null, null, system)
}

const stalledErrorEmail = async (manInfo, user) => {
  const { title, givenNames, surname } = user
  const { email } = user.identities[0]
  const salutation = `${title ? `${title} ` : ''}${givenNames} ${surname}`
  const link = `${url}submission/${manInfo.id}/submit`
  const html = stalledErrorTemplate(salutation, parsed(manInfo.title), link)
  const subject = `${manInfo.id}: Your submission still needs corrections`
  return sendMail(email, subject, html, null, null, system)
}

const grantEmail = async (pi, grants, manInfo, reviewer) => {
  const html = grantsAddedTemplate(pi, grants, manInfo, reviewer)
  const subject = `Publication linked to your grants`
  return sendMail(pi.email, subject, html, null, null, system)
}

const licenseEmail = async (person, funder, license, manInfo) => {
  const { title: t, givenNames, surname } = person.name
  const salutation = `${t ? `${t} ` : ''}${givenNames} ${surname}`
  const { id, title, status } = manInfo
  const loc = status === 'INITIAL' ? 'create' : 'submit'
  const link = `${url}submission/${id}/${loc}`
  const html = licenseAddedTemplate(salutation, funder, title, license, link)
  const subject = `${id}: License exception confirmed`
  await sendMail(person.email, subject, html, null, null, system)
  return { subject, html }
}

const reviewerEmail = async ({ reviewer, manInfo, submitter, token }) => {
  const { title, givenNames, surname } = submitter
  const submitterName = `${title ? `${title} ` : ''}${givenNames} ${surname}`
  const { title: t, givenNames: g, surname: s } = reviewer.name
  const salutation = `${t ? `${t} ` : ''}${g} ${s}`
  const link = `${url}submission/${manInfo.id}/submit`
  const args = [salutation, parsed(manInfo.title), submitterName, link]
  let html = ''
  if (token === 'correction') {
    html = checkReviewerTemplate(...args)
  } else if (reviewer.id) {
    html = setReviewerTemplate(...args)
  } else {
    args[3] = `${url}dashboard?accept=${token}`
    html = newReviewerTemplate(...args)
  }
  const subject = `Approve submission of ${manInfo.id}`
  return sendMail(reviewer.email, subject, html, null, null, system)
}

const finalReviewerEmail = async ({
  reviewer,
  manInfo,
  submitter = null,
  token,
}) => {
  const { title, givenNames, surname } = submitter || ''
  const submitterName =
    submitter && `${title ? `${title} ` : ''}${givenNames} ${surname}`
  const { title: t, givenNames: g, surname: s } = reviewer.name
  const salutation = `${t ? `${t} ` : ''}${g} ${s}`
  const finalReview = manInfo.status === 'xml-review'
  const link = `${url}submission/${manInfo.id}/review`
  const args = [
    salutation,
    parsed(manInfo.title),
    submitterName,
    link,
    finalReview,
  ]
  let html = ''
  if (reviewer.id) {
    html = setReviewFinalTemplate(...args)
  } else if (token) {
    args[3] = `${url}dashboard?accept=${token}`
    html = newReviewFinalTemplate(...args)
  }
  if (html) {
    const subject = finalReview
      ? `Please review manuscript ${manInfo.id}`
      : `You've been assigned to review ${manInfo.id}`
    return sendMail(reviewer.email, subject, html, null, null, system)
  }
}

const submitterRejectEmail = async ({
  reviewer,
  manInfo,
  submitter,
  message,
}) => {
  const { title, givenNames, surname } = submitter
  const { email } = submitter.identities[0]
  const salutation = `${title ? `${title} ` : ''}${givenNames} ${surname}`
  const { title: t, givenNames: g, surname: s } = reviewer
  const reviewerName = `${t ? `${t} ` : ''}${g} ${s}`
  const link = `${url}submission/${manInfo.id}/submit`
  const html = rejectSubmissionTemplate(
    salutation,
    parsed(manInfo.title),
    reviewerName,
    message,
    link,
  )
  const subject = `${manInfo.id}: Submission rejected`
  return sendMail(email, subject, html, null, null, system)
}

const removeDuplicateEmail = async (user, badInfo, goodInfo) => {
  const { title, givenNames, surname } = user
  const { email } = user.identities[0]
  const salutation = `${title ? `${title} ` : ''}${givenNames} ${surname}`
  const pmcid =
    goodInfo['meta,articleIds'] &&
    goodInfo['meta,articleIds'].find(id => id.pubIdType === 'pmcid')
      ? goodInfo['meta,articleIds'].find(id => id.pubIdType === 'pmcid').id
      : null
  const html = removeDupeTemplate(
    salutation,
    badInfo.id,
    parsed(badInfo['meta,title']),
    goodInfo.id,
    pmcid,
  )
  const subject = `${badInfo.id}: Duplicate submission`
  return sendMail(email, subject, html, null, null, system)
}

const alreadyCompleteEmail = async (user, manInfo) => {
  const { title, givenNames, surname } = user
  const { email } = user.identities[0]
  const salutation = `${title ? `${title} ` : ''}${givenNames} ${surname}`
  const pmcid =
    manInfo['meta,articleIds'] &&
    manInfo['meta,articleIds'].find(id => id.pubIdType === 'pmcid') &&
    manInfo['meta,articleIds'].find(id => id.pubIdType === 'pmcid').id
  const epmcURL = `https://www.ebi.ac.uk/europepmc/webservices/rest/search?query=PMCID:${pmcid}&resulttype=lite&format=json`
  let superagentRequest = superagent.get(epmcURL)
  if (process.env.superagent_http_proxy) {
    superagentRequest = superagentRequest.proxy(
      process.env.superagent_http_proxy,
    )
  }
  const response = await superagentRequest
  const { body } = response
  const epmc =
    body.resultList &&
    body.resultList.result[0] &&
    body.resultList.result[0].inEPMC
  const inEPMC = epmc === 'Y'
  const html = alreadyCompleteTemplate(
    salutation,
    manInfo.id,
    manInfo['meta,title'],
    pmcid,
    inEPMC,
    europepmcUrl,
  )
  const subject = `${manInfo.id}: Article already in Europe PMC`
  return sendMail(email, subject, html, null, null, system)
}

const finalReviewEmail = async (reviewer, manInfo) => {
  const { title, givenNames, surname } = reviewer
  const { email } = reviewer.identities[0]
  const salutation = `${title ? `${title} ` : ''}${givenNames} ${surname}`
  const link = `${url}submission/${manInfo.id}/review`
  const releaseDelay =
    manInfo.releaseDelay === '0'
      ? 'immediately'
      : `${manInfo.releaseDelay} month${
          manInfo.releaseDelay === '1' ? '' : 's'
        }`
  const html = finalReviewTemplate(
    salutation,
    parsed(manInfo.title),
    link,
    releaseDelay,
  )
  const subject = `${manInfo.id}: Ready for final review`
  return sendMail(email, subject, html, null, null, system)
}

const correctedReviewEmail = async (reviewer, manInfo) => {
  const { id, title, preprint, releaseDelay } = manInfo
  const { title: t, givenNames: g, surname: s } = reviewer
  const { email } = reviewer.identities[0]
  const salutation = `${t ? `${t} ` : ''}${g} ${s}`
  const link = `${url}submission/${id}/review`
  const html = correctedReviewTemplate(
    salutation,
    parsed(title),
    link,
    preprint,
    releaseDelay,
  )
  const subject = `${id}: Review your corrected ${
    preprint ? 'preprint' : 'manuscript'
  }`
  return sendMail(email, subject, html, null, null, system)
}

const preprintReviewEmail = async (reviewer, manInfo, token) => {
  const { id, title, server, openAccess, version, subjects } = manInfo
  const { title: t, givenNames: g, surname: s } = reviewer.name
  const salutation = `${t ? `${t} ` : ''}${g} ${s}`
  const unrestricted =
    openAccess && openAccess.search(/\/(by-nd|by-nc-nd)\//i) < 0
  let existing = true
  let link = `${url}submission/${id}/review`
  if (token && !reviewer.id) {
    existing = false
    link = `${url}dashboard?accept=${token}`
  }
  const fortnightDate = new Date(Date.now() + 12096e5)
  const fortnight = `${fortnightDate.getUTCDate()} ${fortnightDate.toLocaleString(
    'en-GB',
    { month: 'long' },
  )}`

  const templateFunc =
    subjects && subjects.includes('COVID-19')
      ? preprintReviewTemplateCovid
      : preprintReviewTemplate

  const html = templateFunc({
    id,
    salutation,
    title: parsed(title),
    link,
    version,
    existing,
    openAccess,
    unrestricted,
    fortnight,
  })
  if (html) {
    const subject = `Enabling search and display of your preprint${
      server ? ` from ${server}` : ''
    } in the Europe PMC archive.`
    return sendMail(reviewer.email, subject, html, null, null, system)
  }
}

const preprintVersionEmail = async (reviewer, manInfo) => {
  const { id, title, server } = manInfo
  const { title: t, givenNames: g, surname: s } = reviewer
  const { email } = reviewer.identities[0]
  const salutation = `${t ? `${t} ` : ''}${g} ${s}`
  const subject = 'Display new preprint version in Europe PMC'
  const link = `${url}submission/${id}/review`
  const html = preprintVersionReviewTemplate(
    id,
    salutation,
    parsed(title),
    server,
    link,
  )
  return sendMail(email, subject, html, null, null, system)
}

module.exports = {
  parsed,
  sendMail,
  userMessage,
  taggerEmail,
  processedTaggerEmail,
  errorDevEmail,
  taggerErrorEmail,
  bulkUploaderEmail,
  reviewerEmail,
  finalReviewerEmail,
  submitterRejectEmail,
  grantEmail,
  licenseEmail,
  removeDuplicateEmail,
  alreadyCompleteEmail,
  finalReviewEmail,
  correctedReviewEmail,
  preprintReviewEmail,
  preprintVersionEmail,
  completedEmail,
  completedPreprintEmail,
  deletedEmail,
  deletedTaggerEmail,
  resetPassword,
  incompleteEmail,
  stalledEmail,
  stalledErrorEmail,
}
