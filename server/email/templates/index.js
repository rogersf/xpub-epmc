const { resetPasswordTemplate } = require('./resetPassword')
const {
  newReviewerTemplate,
  setReviewerTemplate,
  checkReviewerTemplate,
  newReviewFinalTemplate,
  setReviewFinalTemplate,
} = require('./reviewerAdded')
const { rejectSubmissionTemplate } = require('./rejectSubmission')
const { removeDupeTemplate } = require('./removeDuplicate')
const {
  newPackageForTaggingTemplate,
  processedTaggingFilesTemplate,
  bulkUploadTemplate,
} = require('./ftp')
const {
  finalReviewTemplate,
  correctedReviewTemplate,
  preprintReviewTemplate,
  preprintReviewTemplateCovid,
  preprintVersionReviewTemplate,
} = require('./finalReview')
const {
  completedTemplate,
  completedPreprintTemplate,
  alreadyCompleteTemplate,
} = require('./completed')
const { deletedTemplate, deletedTaggingTemplate } = require('./deleted')
const {
  incompleteTemplate,
  stalledTemplate,
  stalledPreprintTemplate,
  stalledErrorTemplate,
} = require('./incomplete')
const { grantsAddedTemplate, licenseAddedTemplate } = require('./grantsAdded')

const htmlEmailBase = (
  content,
  urlBase,
) => `<body style="height: 100%;margin: 0;padding: 0;width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
<table id="bodyTable" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 0;width: 100%;background-color: #EEF6FC;" width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" align="center">
  <tbody>
  <tr>
    <td align="center" valign="top" style="padding-top: 30px;background-color:#EEF6FC">            
    </td>
  </tr>
  <tr>
    <td></td>
    <td align="center" style="width:775px; background-color:#EEF6FC;" valign="top">
      <table border="0" cellpadding="0" cellspacing="0" style="width:100%; background-color:#ffffff; font-family:'Open Sans',Helvetica, Arial, sans-serif;font-size:16px;color:#494949;">
        <tbody>
          <tr>
            <td align="left" valign="top" style="background-color:#ffffff; border:1px solid #CCCCCC; border-bottom: 1px solid #E96012; padding: 16px;">
                <img src="https://plus.europepmc.org/assets/static/media/epmcpluslogo.d3f2ea4a.png" style="width: 350px;max-width:100%"/>
            </td>
          </tr>
          <tr>
            <td align="center" style="background-color:#FBFBFB; border-right:1px solid #CCCCCC;border-left:1px solid #CCCCCC;" valign="top">
              <table align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%;font-family:'Open Sans',Helvetica, Arial, sans-serif;font-size:16px;line-height:24px;color:#494949">
                <tbody>
                  <tr>
                    <td align="left" style="padding: 32px" valign="top">
                      ${content}
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td align="center" style="background-color:#FFFFFF; border:1px solid #CCCCCC; font-family:'Open Sans',Helvetica, Arial, sans-serif;font-size:14px;color:#494949;" valign="top">
              <table align="center" border="0" cellpadding="16px" cellspacing="0" style="width:100%;font-family:'Open Sans',Helvetica, Arial, sans-serif;font-size:14px;color:#494949;padding:0;">
                <tbody>
                  <tr>
                    <td align="left" valign="top">
                      For assistance, consult the <a style="color:#20699C" href="${urlBase}user-guide">User Guide</a>, email <a style="color:#20699C" href="mailto:helpdesk@europepmc.org">helpdesk@europepmc.org</a> or call +44 1223 494118.
                    </td>
                    <td align="right" valign="top">
                      <a style="color:#20699C; white-space:nowrap;" href="https://www.ebi.ac.uk/data-protection/privacy-notice/europe-pmc-plus">Privacy Notice</a>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </td>
    <td></td>
  </tr>
  <tr>
    <td align="center" valign="top" style="padding-top: 40px;background-color:#EEF6FC">            
    </td>
  </tr>
</tbody></table>
</body>`

module.exports = {
  htmlEmailBase,
  resetPasswordTemplate,
  newReviewerTemplate,
  setReviewerTemplate,
  newReviewFinalTemplate,
  setReviewFinalTemplate,
  checkReviewerTemplate,
  rejectSubmissionTemplate,
  grantsAddedTemplate,
  licenseAddedTemplate,
  alreadyCompleteTemplate,
  removeDupeTemplate,
  finalReviewTemplate,
  correctedReviewTemplate,
  preprintReviewTemplateCovid,
  preprintReviewTemplate,
  preprintVersionReviewTemplate,
  newPackageForTaggingTemplate,
  processedTaggingFilesTemplate,
  bulkUploadTemplate,
  completedTemplate,
  completedPreprintTemplate,
  deletedTemplate,
  deletedTaggingTemplate,
  incompleteTemplate,
  stalledTemplate,
  stalledPreprintTemplate,
  stalledErrorTemplate,
}
