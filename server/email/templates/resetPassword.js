const resetPasswordTemplate = ({ title, givenNames, surname }, link) => `
  <h1 style="font-weight:600;font-size:30px;line-height:34px;">Reset your password</h1>
	<p>Dear ${title ? `${title} ` : ''}${givenNames} ${surname},</p>
	<p>Please click the link to <a style="color:#20699C" href="${link}">reset the password</a> of your Europe PMC plus account. This link will expire in 24 hours.</p>
  <p>If you did not request a password reset, please disregard this message.</p>
	<p>Kind regards,</p>
	<p>The Europe PMC Helpdesk</p>
`

module.exports = {
  resetPasswordTemplate,
}
