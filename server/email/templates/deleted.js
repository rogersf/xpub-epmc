const deletedTemplate = title => `
  <h1 style="font-weight:600;font-size:30px;line-height:34px;">Submisson cancelled</h1>
  <p>This manuscript submission, <b>${title}</b>, was cancelled by its submitter and removed from our system. No further action is required.</p>
  <p>If this submission was cancelled in error, please let us know by replying to this message, or use the contact information below.</p>
  <p>Kind regards,</p>
  <p>The Europe PMC Helpdesk</p>
`

const deletedTaggingTemplate = id => `
  <p>This manuscript submission was canceled after it was sent to you for tagging. Please <bold>do not</bold> tag the package for ${id}.</p>
  <p>Kind regards,</p>
  <p>The Europe PMC Helpdesk</p>
`

module.exports = { deletedTemplate, deletedTaggingTemplate }
