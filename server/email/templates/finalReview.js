const finalReviewTemplate = (salutation, title, link, releaseDelay) => `
  <h1 style="font-weight:600;font-size:30px;line-height:34px;">Review web version</h1>
  <p>Dear ${salutation},</p>
  <p>The web version of your Europe PMC plus submission, <b>${title}</b>, is now ready for your review. This is a necessary step to complete your submission to Europe PMC.</p>
  <p>Please proceed to <a style="color:#20699C" href="${link}">${link}</a> to approve or request corrections to the web version of your manuscript.</p>
  <p>Please note that only errors or omissions that impact the scientific accuracy of your article are eligible for correction.</p>
  <p>Once approved, your manuscript will be made available in Europe PMC ${releaseDelay} after the final publication of the article. You will be notified when your article is freely available in Europe PMC.</p>
  <p>Kind regards,</p>
  <p>The Europe PMC Helpdesk</p>
`

const correctedReviewTemplate = (
  salutation,
  title,
  link,
  preprint,
  releaseDelay,
) => `
  <h1 style="font-weight:600;font-size:30px;line-height:34px;">Review your corrected ${
    preprint ? 'preprint' : 'manuscript'
  }</h1>
  <p>Dear ${salutation},</p>
  <p>The errors you reported have been corrected. Please proceed to <a style="color:#20699C" href="${link}">${title}</a> to ${
  preprint
    ? 'check and approve your preprint. '
    : 'approve or request additional corrections to the web version of your manuscript. Please note that only errors or omissions that impact the scientific accuracy of your article are eligible for correction.</p><p>'
}Once approved, your ${
  preprint ? 'preprint' : 'manuscript'
} will be made available${preprint ? ' for reading' : ''} in Europe PMC ${
  preprint
    ? 'within 24 hours'
    : `${
        releaseDelay > 0 ? `${releaseDelay} months` : 'immediately'
      } after the final publication of the article. You will be notified when your article is freely available in Europe PMC`
}.</p>
  ${preprint ? '<p>Please let us know if you have any questions.</p>' : ''}
  <p>Kind regards,</p>
  <p>The Europe PMC Helpdesk</p>
`

const epmcDescription = `<h2 style="text-align: center"><img src="https://europepmc.org/img/Europe%20PMC%20Extended%20Logo%20CMYK.33decb46.png" alt="About Europe PMC" style="width: 250px;max-width:100%"/></h2>
  <p>Europe PMC is a worldwide, life sciences literature resource funded by <a style="color:#20699C" href="https://europepmc.org/Funders/">36 biomedical funders</a>, hosted at the <a style="color:#20699C" href="https://www.ebi.ac.uk">EMBL-EBI</a>, and <a style="color:#20699C" href="https://www.ncbi.nlm.nih.gov/pmc/about/pmci/">built in partnership with PMC</a> from the U.S. National Library of Medicine. Europe PMC has been including preprint abstracts since 2018 and now has over 450k biomedical preprint abstracts, from 24 preprint servers including bioRxiv, medRxiv and Research Square, freely available to search, alongside 7.9 million peer-reviewed full text articles and 40 million abstracts.</p>`

const preprintReviewTemplateCovid = ({
  id,
  salutation,
  title,
  link,
  version,
  existing,
  openAccess,
  unrestricted,
  fortnight,
}) => `
  <p style="margin:-32px 0 -12px; text-align: right; font-size: 12px;">${id}</p>
  <p>Dear ${salutation},</p>
  <p><a href="https://europepmc.org" style="color:#20699C">Europe PMC</a> has been funded by Wellcome to include the full text of COVID-19-related preprints. This means that the full text of your preprint, <b>${title}${
  version ? ` (version ${version})` : ''
}</b>, will be searchable alongside peer-reviewed articles in Europe PMC${
  unrestricted ? ' and freely available for programmatic text mining' : ''
}. If you have questions, please see the <a href="https://plus.europepmc.org/user-guide/preprintfaqs" style="color:#20699C">Preprint FAQs</a>.</p>
  <p>${
    openAccess
      ? `The full text will also be displayed on the Europe PMC website for people to read on ${fortnight}. If you review it, it can be made available within 24 hours.`
      : 'We would also like to display the full text of your preprint on the Europe PMC website for people to read.'
  } Please log into ${
  existing ? ' your' : ' or create a'
} Europe PMC plus account in order to check ${
  openAccess ? 'the web format of' : 'and approve'
} your preprint, by clicking the button below.</p>
  <p style="text-align:center"><a style="display: inline-block; font-size: 20px; padding: 14px 18px 12px; background-color:#20699C; color: #fff; font-weight: 300; border-radius: 1px; text-decoration: none" href="${link}">${
  openAccess ? 'Review my preprint display' : 'Approve display on Europe PMC'
}</a></p>
  ${
    !openAccess
      ? '<p>Once you have approved the display of the preprint, it will be made available for reading on the Europe PMC website within 24 hours. You will also be asked if you would like to add a license to the Europe PMC version of your preprint to make it available for text mining.</p><p>We very much hope you participate in this project, which may prove to be a pivotal moment in how the results of research are published more rapidly and openly, as well as increase accessibility of research results on COVID-19.</p>'
      : `${
          !unrestricted
            ? '<p>You will also be asked if you would like to add a license to the Europe PMC version of your preprint to make text mining results available for it.</p>'
            : ''
        }`
  }
  <p>Kind regards,</p>
  <p>The Europe PMC Helpdesk</p>
  <table align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 32px 0 0; font-family:'Open Sans',Helvetica, Arial, sans-serif;font-size:16px;line-height:24px;color:#494949">
    <tr>
      <td align="left" style="padding: 32px 48px; background-color: #fff; border: 1px solid #E96012;" valign="top">
        ${epmcDescription}
        <p>This initiative is supported by Wellcome, and was previously supported by a joint award from Wellcome, UK Medical Research Council, and Swiss National Science Foundation. It is endorsed by the Chief Scientist of the World Health Organisation (WHO).</p>
        <table align="center" border="0" cellpadding="0" cellspacing="0" style="font-family:'Open Sans',Helvetica, Arial, sans-serif;font-size:16px;line-height:24px;color:#494949; margin-top: 24px">
          <tr>
            <td style="font-size: 84px; line-height: 70px; font-family: Georgia; vertical-align: top; padding-right: 8px">&ldquo;</td>
            <td style="vertical-align: top">
              <p style="margin-top: 0; display: inline-block;font-size: 18px; line-height: 26px">As the Chief Scientist of WHO I welcome the huge increase in the use of pre-prints by researchers to rapidly share the emerging evidence from the many studies on Covid-19. However, these are published as .pdf documents and I recognise that the information they contain could be more rapidly searched and linkages made between the results and data they contain if they were converted to the standard publishing language XML. I therefore support this initiative by Europe PMC to take on this task."</p>
              <p>&mdash;Dr. Soumya Swaminathan, Chief Scientist, WHO; 4 May 2020.</p>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
`
const preprintReviewTemplate = ({
  id,
  salutation,
  title,
  link,
  version,
  unrestricted,
  fortnight,
}) => `
  <p style="margin:-32px 0 -12px; text-align: right; font-size: 12px;">${id}</p>
  <p>Dear ${salutation},</p>
  <p><a href="https://europepmc.org" style="color:#20699C">Europe PMC</a> is funded by 36 international life sciences funders. Because your article acknowledges a grant from at least one of these funders, the full text of your preprint: <b>${title}${
  version ? ` (version ${version})` : ''
}</b>, will be searchable alongside peer-reviewed articles in Europe PMC${
  unrestricted ? ', and freely available for programmatic text mining' : ''
}. If you have questions, please see the <a href="https://plus.europepmc.org/user-guide/preprintfaqs" style="color:#20699C">Preprint FAQs</a>.</p>
  <p>
    The full text will also be displayed on the Europe PMC website for people to read on ${fortnight}. If you’d like to review the formatting before the article goes live, please log into or create a Europe PMC plus account by clicking the button below. Reviewing the article will also make it available in Europe PMC earlier, usually within 24 hours.
  </p>
  <p style="text-align:center">
    <a style="display: inline-block; font-size: 20px; padding: 14px 18px 12px; background-color:#20699C; color: #fff; font-weight: 300; border-radius: 1px; text-decoration: none" href="${link}">
    Review my preprint display
    </a>
  </p>
  <p>Kind regards,</p>
  <p>The Europe PMC Helpdesk</p>
  <table align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 32px 0 0; font-family:'Open Sans',Helvetica, Arial, sans-serif;font-size:16px;line-height:24px;color:#494949">
    <tr>
      <td align="left" style="padding: 32px 48px; background-color: #fff; border: 1px solid #E96012;" valign="top">
        ${epmcDescription}
      </td>
    </tr>
  </table>
`

const preprintVersionReviewTemplate = (id, salutation, title, server, link) => `
  <p style="margin:-32px 0 -12px; text-align: right; font-size: 12px;">${id}</p>
  <p>Dear ${salutation},</p>
  <p>A new version of your preprint, <b>${title}</b>, has been submitted to ${server}. Please click the button below, and log into your Europe PMC plus account to check and approve the new version.</p>
  <p style="text-align:center"><a style="display: inline-block; font-size: 20px; padding: 14px 18px 12px; background-color:#20699C; color: #fff; font-weight: 300; border-radius: 1px; text-decoration: none" href="${link}">Approve my preprint on Europe PMC</a></p>
  <p>Kind regards,</p>
  <p>The Europe PMC Helpdesk</p>
`

module.exports = {
  finalReviewTemplate,
  correctedReviewTemplate,
  preprintReviewTemplate,
  preprintReviewTemplateCovid,
  preprintVersionReviewTemplate,
}
