const pm2Config = {
  apps: [
    {
      name: 'ftpBulkMonitor',
      script: './server/ftp-integration/api.js',
      exec_mode: 'cluster_mode',
      instances: 1,
      env: {
        APP_NAME: 'ftp-bulk-monitor',
      },
    },
    {
      name: 'ftpTaggerMonitor',
      script: './server/ftp-integration/taggersImport.js',
      exec_mode: 'cluster_mode',
      instances: 1,
      env: {
        APP_NAME: 'ftp-tagger-monitor',
      },
    },
  ],
}

module.exports = pm2Config
