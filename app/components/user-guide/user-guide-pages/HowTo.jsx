import React from 'react'
import styled from 'styled-components'
import { Link, H2, H3, H4 } from '@pubsweet/ui'
import { europepmcUrl } from 'config'
import { A } from '../../ui'
import MSSHorizontal from '../../../assets/mss_process_horizontal.svg'
import MSSVertical from '../../../assets/mss_process_vertical.svg'
// import BulkUpload from '../../../assets/nihms-bulk-sub.tar.gz'
import { Head, GuidePage, Menu, MenuLink, Content } from './UserGuideElements'
import { Video } from './VideoTutorials'

const Image = styled.img`
  width: 100%;
  min-height: 480px;
  max-width: 750px;
  &.vertical {
    display: none;
  }
  @media screen and (max-width: 740px) {
    &.horizontal {
      display: none;
    }
    &.vertical {
      display: block;
      max-width: 550px;
      min-height: 540px;
    }
  }
`
const altText = `Author or submitter: Upload the final version of a manuscript, modified and accepted for publication. Europe PMC plus: Transform the submission into archival and web versions for reading, indexing, and searching. Author or submitter: Review the final web versions of the manuscript. Europe PMC plus: Make the manuscript freely available on Europe PMC and PubMed Central.`

const fundersLink = (
  <A href={`${europepmcUrl}/Funders/`} target="_blank">
    Europe PMC funders
  </A>
)
const planSLink = (
  <A
    href="https://www.coalition-s.org/guidance-on-the-implementation-of-plan-s/"
    target="_blank"
  >
    Plan S Open Access policy
  </A>
)
const horizonLink = (
  <A href="https://ec.europa.eu/info/horizon-europe_en" target="_blank">
    Horizon Europe
  </A>
)

const sections = [
  {
    title: 'Criteria for submission',
    content: (
      <p>
        {`Submitted manuscripts must be peer-reviewed, original (primary) research papers that have been accepted for publication in a journal, and that have direct costs supported, in whole or in part, by one or more `}
        {fundersLink}
        {'.'}
      </p>
    ),
  },
  {
    title: 'Overview',
    content: (
      <div>
        <p>{`If a manuscript is submitted correctly, and promptly reviewed by the author, the process of transforming the manuscript into a web page can take as little as two weeks. After the submission is complete, and once any embargo period has passed, the manuscript will be released to Europe PMC and PubMed Central (PMC). Europe PMC will index and display the manuscripts in three forms: abstract, online full-text, and PDF.`}</p>
        <Image alt={altText} className="horizontal" src={MSSHorizontal} />
        <Image alt={altText} className="vertical" src={MSSVertical} />
      </div>
    ),
  },
  {
    title: 'Getting started',
    content: (
      <div>
        <p>
          {`To submit a manuscript, `}
          <Link to="/login">login to Europe PMC plus</Link>
          {`, or `}
          <Link to="/signup">create an account</Link>
          {`. The submission wizard will guide you through the process.`}
        </p>
        <H4>What you will need</H4>
        <ul>
          <li>The grant numbers</li>
          <li>The final manuscript file as a PDF or Microsoft Word file</li>
          <li>
            {`The figures and supplementary files in an `}
            <Link to="/user-guide/allowedfiles">accepted type</Link>
          </li>
          <li>
            {`If the citation isn’t found, you will need the name of the publishing journal`}
          </li>
          <li>The embargo period required by the publishing journal</li>
        </ul>
        <Video index={0} />
        <H4>Who should submit</H4>
        <p>{`Manuscript files may be submitted to Europe PMC Plus by the author, the publisher, or anyone given access to the author's files (administrative personnel, graduate students, librarians, etc.)`}</p>
        <H4>Which version to submit</H4>
        <p>
          {`The accepted, peer-reviewed version of the manuscript should be submitted. The author may submit the version of record for Creative-Commons-licensed articles; otherwise, if the author wishes to submit a PDF created by the publisher, the publisher must have granted permission for this use. Check your journal’s version policy at `}
          <A href="http://www.sherpa.ac.uk/romeo/index.php" target="_blank">
            SHERPA/RoMEO
          </A>
          {`. Supplementary material that has been submitted to the journal in support of the manuscript should also be included.`}
        </p>
      </div>
    ),
  },
  {
    title: 'Open Access policies',
    content: (
      <div>
        <p>
          {`Europe PMC funders expect research papers resulting from their funding to be made freely available in Europe PMC. The Europe PMC plus submission system enables direct deposit of research to assist grantees in meeting this funder mandate for Open Access. After deposit, the manuscript will be made available in Europe PMC and PubMed Central (PMC). For more information about the Open Access policies of individual funders, please see the `}
          {fundersLink}
          {' page.'}
        </p>
        <H4>Embargo period</H4>
        <p>{`Europe PMC strongly encourages authors to make their manuscripts available as soon as possible. If an embargo period, or delay in availability, is required by the publishing journal, it should be indicated during the submission, and the manuscript will be released to Europe PMC after the embargo period has passed.`}</p>
      </div>
    ),
  },
  {
    title: 'Licensing policies',
    content: (
      <div>
        <p>
          {`Funders who are members of `}
          <A href="https://www.coalition-s.org/" target="_blank">
            Coalition S
          </A>
          {` require researchers receiving their grants to comply with the `}
          {planSLink}
          {`. This policy requires that publications resulting from funded research be made openly available immediately, with a `}
          <A
            href="https://creativecommons.org/licenses/by/4.0/"
            target="_blank"
          >
            Creative Commons Attribution (CC BY)
          </A>
          {` license, unless an exception has been granted by the funder. Grantees can meet this requirement by publishing the article in an Open Access journal or platform; publishing in a subscription journal with an Open Access transformative arrangement; or self-archiving the article in an Open Access repository, such as Europe PMC.`}
        </p>
        <p>
          {`Similarly, publications resulting from research funded by a European Research Council (ERC) `}
          {horizonLink}
          {` grant must be made openly available immediately, with a CC BY license, or a license with equivalent rights.`}
        </p>
        <H4>Self archiving a manuscript</H4>
        <p>{`If a funder requires compliance with Plan S policy, and an article is published in a subscription journal without an Open Access transformative arrangement, then the researcher must self-archive either the final published version, or the author's accepted manuscript version, in an Open Access repository such as Europe PMC, where it will be made immediately available with a CC BY license, unless a license exception has been granted.`}</p>
        <H4>Does my funder have license and embargo requirements?</H4>
        <p>
          {`For information about the Open Access policies of individual funders, and for more details of the ERC Horizon Europe Model Grant Agreement, please see the `}
          {fundersLink}
          {` page. When a manuscript is submitted to the Europe PMC plus manuscript submission system, the submitter will be informed if its funding requires compliance with the `}
          {planSLink}
          {', or with ERC '}
          {horizonLink}
          {` grant requirements.`}
        </p>
      </div>
    ),
  },
  {
    title: 'Reviewing a submission',
    content: (
      <div>
        <p>{`If you are an author, and a manuscript was submitted to Europe PMC plus on your behalf, you will receive an email notification to review it. Click the link in the email, and you will be taken to Europe PMC plus where you should login or create an account.`}</p>
        <p>{`The submission that is ready for your review will be listed on the My manuscripts page. Once you click on the submission, you will see the web preview of the manuscript, which you will need to check and either Approve or Reject. On the right, you will see all of the files, funders, and citation information the submitter entered, which can be edited if necessary. To report any problems, use the Reject button.`}</p>
        <p>{`If you received an email to review the manuscript, but you are not an author of the manuscript, please reply to the email and let the helpdesk staff know.`}</p>
        <Video index={1} />
      </div>
    ),
  },
  {
    title: 'Checking the status',
    content: (
      <div>
        <p>{`After you have reviewed the manuscript, updates to the manuscript will be listed on the My Manuscripts page, which you can access by logging into Europe PMC plus. If you need to take action on a manuscript, it will appear at the top of the list.`}</p>
        <Video index={2} />
      </div>
    ),
  },
  {
    title: 'Requesting changes',
    content: (
      <div>
        <p>
          {`You can still request some changes to manuscript or submission details after the submission is complete. `}
          <Link to="/login">Login to Europe PMC plus</Link>
          {` and click on the manuscript, where you will see the details of the submission. If any changes are needed, click on the Helpdesk link at the top right, and describe any changes needed.`}
        </p>
        <Video index={3} />
      </div>
    ),
  },
]

export const HowTo = () => (
  <GuidePage>
    <Menu>
      <ul>
        {sections.map(({ title }) => (
          <li key={title}>
            <MenuLink id={title.split(' ').join('_')}>{title}</MenuLink>
          </li>
        ))}
      </ul>
    </Menu>
    <Content>
      <H2>How to</H2>
      {sections.map(({ title, content }) => (
        <React.Fragment key={title}>
          <Head h={H3} open>
            {title}
          </Head>
          <React.Fragment>{content}</React.Fragment>
        </React.Fragment>
      ))}
    </Content>
  </GuidePage>
)
