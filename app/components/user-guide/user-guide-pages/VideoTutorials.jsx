import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { H2, H3, Icon } from '@pubsweet/ui'
import { A, LoadingIcon } from '../../ui'
import { GuidePage, Menu, MenuLink, Content } from './UserGuideElements'

const Section = styled.div`
  h3 {
    margin: ${th('gridUnit')} auto;
  }
  p {
    margin-top: 0;
  }
  margin: calc(${th('gridUnit')} * 2) auto calc(${th('gridUnit')} * 4);
`
const IFrameWrapper = styled.div`
  position: relative;
  overflow: hidden;
  padding-top: 56.25%;
  span {
    position: absolute;
    top: 40%;
    right: 50%;
    z-index: 1;
  }
`
const IFrame = styled.iframe`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  border: 0;
  z-index: 2;
`
const Flex = styled.div`
  display: flex;
  align-items: center;
  iframe {
    width: 269px;
    margin-right: calc(${th('gridUnit')} * 2);
  }
  p {
    font-weight: 600;
    a {
      display: inline-flex;
      align-items: bottom;
    }
  }
  @media screen and (max-width: 900px) {
    flex-wrap: wrap;
  }
`
const videos = [
  {
    title: 'Creating a submission',
    link: 'NxRpCEjL9eQ',
    length: '3:02',
  },
  {
    title: 'Reviewing a submission',
    link: 'npSIRIfV6pU',
    length: '1:05',
  },
  {
    title: 'Checking the status',
    link: 'oKNB8EG9J5s',
    length: '0:57',
  },
  {
    title: 'Requesting changes',
    link: 'D39IktJg_vk',
    length: '0.50',
  },
]

export const Video = ({ index }) => {
  const { title, link, length } = videos[index]
  return (
    <Flex>
      <iframe
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
        frameBorder="0"
        src={`https://www.youtube.com/embed/${link}`}
        title={title}
      />
      <p>
        <A href={`https://www.youtube.com/watch?v=${link}`} target="_blank">
          {title} ({length} minutes)
          <Icon color="red" size={2.5} strokeWidth={2}>
            youtube
          </Icon>
        </A>
      </p>
    </Flex>
  )
}

export const VideoTutorials = () => (
  <GuidePage>
    <Menu>
      <ul>
        {videos.map(({ title, link }) => (
          <li key={link}>
            <MenuLink id={title.split(' ').join('_')}>{title}</MenuLink>
          </li>
        ))}
      </ul>
    </Menu>
    <Content>
      <H2>Video tutorials</H2>
      {videos.map(({ link, title, length }) => (
        <Section id={title.split(' ').join('_')} key={link}>
          <IFrameWrapper>
            <IFrame
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen
              frameBorder="0"
              src={`https://www.youtube.com/embed/${link}`}
              title={title}
            />
            <LoadingIcon size={5} />
          </IFrameWrapper>
          <H3>
            <A href={`https://www.youtube.com/watch?v=${link}`} target="_blank">
              {title}
            </A>
          </H3>
          <p>
            {length} minutes
            <br />
            Subtitles
          </p>
        </Section>
      ))}
    </Content>
  </GuidePage>
)
