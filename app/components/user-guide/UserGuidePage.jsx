import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { Link, H1 } from '@pubsweet/ui'
import { Page, Toggle } from '../ui'
import {
  AllowedFiles,
  VideoTutorials,
  PreprintFaqs,
  HowTo,
} from './user-guide-pages/'

const UserGuidePage = ({ children }) => {
  const subPage = window.location.pathname
  return (
    <Page style={{ maxWidth: '1100px' }}>
      <H1>User guide</H1>
      <Toggle>
        <Link
          className={
            !subPage.includes('allowedfiles') &&
            !subPage.includes('videotutorials') &&
            !subPage.includes('preprintfaqs')
              ? 'current'
              : ''
          }
          to="/user-guide"
        >
          How to
        </Link>
        <Link
          className={subPage.includes('allowedfiles') ? 'current' : ''}
          to="/user-guide/allowedfiles"
        >
          Accepted file types
        </Link>
        <Link
          className={subPage.includes('videotutorials') ? 'current' : ''}
          to="/user-guide/videotutorials"
        >
          Video tutorials
        </Link>
        <Link
          className={subPage.includes('preprintfaqs') ? 'current' : ''}
          to="/user-guide/preprintfaqs"
        >
          Preprint FAQs
        </Link>
      </Toggle>
      <Switch>
        <Route component={AllowedFiles} path="/user-guide/allowedfiles" />
        <Route component={PreprintFaqs} path="/user-guide/preprintfaqs" />
        <Route component={VideoTutorials} path="/user-guide/videotutorials" />
        <Route component={HowTo} path="/user-guide" />
      </Switch>
    </Page>
  )
}

export default UserGuidePage
