import React from 'react'
import { withRouter } from 'react-router'
import { Query } from 'react-apollo'
import { H3, H4 } from '@pubsweet/ui'
import {
  Loading,
  LoadingIcon,
  HTMLString,
  SectionContent as Content,
  SectionHeader as Header,
} from '../ui'
import ListErrors from './ListErrors'
import ReviewHistory, { TableStyle } from './ReviewHistory'
import { GET_REVIEWS } from './operations'

const ReviewFormDisplay = ({
  manuscript,
  previews,
  review,
  deleted,
  history,
  reportError = false,
}) => {
  const { annotations } = review || []
  const { status, teams, meta } = manuscript
  const { teamMembers } = teams.find(t => t.role === 'reviewer') || {}
  const [reviewer] = teamMembers || []
  deleted = deleted.filter(r => r.annotations.length > 0)
  const { notes } = meta
  const taggerNote = notes
    .slice()
    .reverse()
    .find(
      n =>
        n.content &&
        n.content.includes('"to":["tagger"],"subject":"Fix tagging errors"'),
    )
  const taggerEmail = taggerNote && JSON.parse(taggerNote.content)
  const taggerTable = taggerEmail && taggerEmail.message
  const taggerErrors =
    taggerTable && taggerTable.replace(/border:1px solid #CCCCCC;/g, '')
  if (status === 'tagging') {
    return (
      <React.Fragment>
        <Content>
          <H3>Tagging complete?</H3>
          <p>
            {`When all files have been uploaded and both the Web and PDF previews have been generated, please click the button below to mark tagging complete for this manuscript.`}
          </p>
          {taggerErrors && (
            <React.Fragment>
              <H4>Tagger error report</H4>
              <HTMLString element={TableStyle} string={taggerErrors} />
            </React.Fragment>
          )}
        </Content>
      </React.Fragment>
    )
  } else if (status === 'file-error') {
    return (
      <React.Fragment>
        <Header>
          <H3>Check file errors</H3>
        </Header>
        <Content>
          <div>
            <p>
              {`You can fix any errors and send for QA, or request a fix from the taggers by clicking 'Ask for correction'.`}
            </p>
            <p>
              {`If an error is caused by missing or incomplete files, click 'Ask for correction' and you will be given the opportunity to write a message explaining what is needed, and send the manuscript back to the submitter or reviewer to upload files.`}
            </p>
          </div>
        </Content>
      </React.Fragment>
    )
  } else if (['xml-triage', 'xml-corrected', 'xml-error'].includes(status)) {
    const user = review ? review.user : null
    const { givenNames, surname } = (user && user.identities[0].name) || ''
    return (
      <React.Fragment>
        <Header>
          <H3>
            Review
            {(review && ' correction request') ||
              (manuscript.formState && ' errors') ||
              ' and route'}
          </H3>
        </Header>
        <Content>
          <div>
            {review && (
              <p>
                Reported by {givenNames} {surname}
                {reviewer && reviewer.user.id === user.id && ' (Reviewer)'}
              </p>
            )}
            {annotations && <ListErrors annotations={annotations} />}
            {(deleted.length > 0 || taggerErrors) && (
              <ReviewHistory
                reviews={deleted}
                status={status}
                taggerErrors={taggerErrors}
              />
            )}
            <p>
              {`You can fix any errors and approve for review, or click 'Ask for correction' to ${
                review
                  ? 'send this list of issues to the taggers to fix. You will be given the change to edit the annotation text to provide more detail if necessary'
                  : 'request a fix from the taggers'
              }.`}
            </p>
            <p>
              {`If an error is caused by missing or incomplete files, click 'Ask for correction' and you will be given the opportunity to write a message explaining what is needed, and send the manuscript back to the submitter or reviewer to upload files.`}
            </p>
          </div>
        </Content>
      </React.Fragment>
    )
  } else if (status === 'xml-qa' || status === 'xml-review') {
    if (status === 'xml-review') {
      deleted = deleted.filter(r => reviewer && reviewer.user.id === r.user.id)
    }
    if (annotations || deleted.length > 0) {
      return (
        <React.Fragment>
          <Header>
            <H3>Error report</H3>
          </Header>
          <Content>
            <div>
              {annotations && <ListErrors annotations={annotations} />}
              {deleted.length > 0 && <ReviewHistory reviews={deleted} />}
            </div>
          </Content>
        </React.Fragment>
      )
    }
    return null
  } else if (status === 'repo-triage') {
    return null
  }
  history.push('/')
  return null
}

const ReviewFormRouter = withRouter(ReviewFormDisplay)

const ReviewForm = ({ manuscript, ...props }) => (
  <Query query={GET_REVIEWS} variables={{ manuscriptId: manuscript.id }}>
    {({ data: { deletedReviews }, loading }) => {
      if (loading || !deletedReviews) {
        return (
          <Loading>
            <LoadingIcon />
          </Loading>
        )
      }
      return (
        <ReviewFormRouter
          deleted={deletedReviews}
          manuscript={manuscript}
          {...props}
        />
      )
    }}
  </Query>
)

export default ReviewForm
