import React from 'react'
import parse from 'html-react-parser'
import CssModules from 'react-css-modules'
import { createGlobalStyle } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import MathJax from '../../react-mathjax-preview-local'
import styles from '../../assets/epmc_new_plus.scss'
import emsHeader from '../../assets/logo-wtpa2.gif'

const CodeBox = createGlobalStyle`
  #html-preview pre, #html-preview pre code {
    white-space: pre-wrap !important;
    overflow-wrap: break-word;
    border: 0 !important;
    margin-top: 0 !important;
    font-size: ${th('fontSizeBaseSmall')} !important;
    background-color: #fff !important;
  }
`

class HTMLPreview extends React.Component {
  state = { html: '' }
  async componentDidMount() {
    try {
      const response = await fetch(this.props.url)
      const html = await response.text()
      if (this.div) {
        await this.setState({ html })
      }
    } catch (error) {
      if (this.div) {
        this.setState({ html: `'No Web Preview generated. Error: ${error}` })
      }
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (this.props.loaded && this.div) {
      // Check if completely loaded — special check for math
      if (prevState.html === '' && this.state.html !== '') {
        const math = this.state.html.lastIndexOf('mathjax')
        if (math > 0) {
          const mathLoc = this.state.html.indexOf('id="', math)
          const idLoc = mathLoc + 4
          const lastMath = this.state.html.slice(
            idLoc,
            this.state.html.indexOf('"', idLoc),
          )
          this.div.lastMath = lastMath
        } else {
          this.props.loaded(true)
        }
      }
    }
  }
  mathComplete = id => {
    // Check if completely loaded — check if final math is displayed
    if (id === this.div.lastMath) {
      setTimeout(() => this.props.loaded(true), 350)
    }
  }
  setRef = div => {
    this.div = div
  }
  render() {
    const { mathComplete } = this
    let lines = []
    if (this.props.xml && this.state.html) {
      lines = this.state.html.split('\n')
    }
    return (
      <div id="html-preview" ref={this.setRef}>
        {this.props.xml ? (
          <React.Fragment>
            <CodeBox />
            <pre>
              <code className="language-xml">
                {lines.map((l, i) => {
                  /* eslint-disable react/no-array-index-key */
                  const rx = / id="([0-9A-Za-z]+)"/
                  const [, id] = l.match(rx) || []
                  return (
                    <React.Fragment key={l + i}>
                      <span id={id}>{l}</span>
                      {'\n'}
                    </React.Fragment>
                  )
                })}
              </code>
            </pre>
          </React.Fragment>
        ) : (
          parse(this.state.html, {
            replace: domNode => {
              if (domNode.type === 'tag') {
                if (
                  domNode.name === 'span' &&
                  domNode.attribs.class === 'f mathjax mml-math'
                ) {
                  return (
                    <MathJax
                      id={domNode.attribs.id}
                      key={domNode.attribs.id}
                      math={domNode.children[0].data}
                      onDisplay={() => mathComplete(domNode.attribs.id)}
                      style={domNode.attribs.style}
                      wrapperTag="span"
                    />
                  )
                }
                if (
                  domNode.name === 'img' &&
                  domNode.attribs.src ===
                    'https://europepmc.org/corehtml/pmc/pmcgifs/logo-wtpa2.gif'
                ) {
                  return (
                    <a href="//plus.europepmc.org">
                      <img
                        alt="Europe PMC Funders Group Author Manuscript"
                        src={emsHeader}
                      />
                    </a>
                  )
                }
              }
            },
          })
        )}
      </div>
    )
  }
}

export default CssModules(HTMLPreview, styles)
