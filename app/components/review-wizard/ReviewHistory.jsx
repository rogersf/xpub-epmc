import React from 'react'
import moment from 'moment'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Action, H5, Icon } from '@pubsweet/ui'
import { ZebraList, ZebraListItem, HTMLString } from '../ui'

const History = styled.div`
  padding: 0 0 calc(${th('gridUnit')} * 2);
`
const ListItem = styled(ZebraListItem)`
  font-size: ${th('fontSizeBaseSmall')};
`
const ErrorReport = styled.div`
  padding: ${th('gridUnit')} 0 calc(${th('gridUnit')} * 2);
  h5 {
    margin-top: 0;
  }
`
const Drop = styled(Action)`
  font-weight: 600;
  display: flex;
  align-items: flex-start;
`
export const TableStyle = styled.div`
  table,
  tbody,
  tr,
  td {
    display: block;
    border: 0;
  }
  tbody {
    border-top: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
    margin-bottom: calc(${th('gridUnit')} * 3);
  }
  tr {
    border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
    border-top: 0;
    background-color: ${th('colorTextReverse')};
    padding: ${th('gridUnit')};
    &:first-child {
      display: none;
    }
    &:nth-child(even) {
      background-color: ${th('colorBackgroundHue')};
    }
  }
  td:nth-child(1):before {
    content: 'Location: ';
    font-weight: 600;
  }
  td:nth-child(2):before {
    content: 'Selected text: ';
    font-weight: 600;
  }
  td:nth-child(3):before {
    content: 'Error description: ';
    font-weight: 600;
  }
`
const DeletedErrors = ({ annotations }) => {
  const list = { tempHTML: [], pdf4print: [], PMC: [] }
  annotations.forEach(a => list[a.file.type].push(a))
  return (
    <ErrorReport>
      {list.tempHTML.length > 0 && (
        <React.Fragment>
          <H5>Web preview errors</H5>
          <AList list={list.tempHTML} />
        </React.Fragment>
      )}
      {list.pdf4print.length > 0 && (
        <React.Fragment>
          <H5>PDF preview errors</H5>
          <AList list={list.pdf4print} />
        </React.Fragment>
      )}
      {list.PMC.length > 0 && (
        <React.Fragment>
          <H5>XML errors</H5>
          <AList list={list.PMC} />
        </React.Fragment>
      )}
    </ErrorReport>
  )
}

const AList = ({ list }) => (
  <ZebraList>
    {list.map(a => (
      <ListItem key={a.id}>
        <em>{a.quote}</em>
        <br />
        {a.text}
      </ListItem>
    ))}
  </ZebraList>
)

class ReviewHistory extends React.Component {
  state = {
    showList: false,
    show: this.props.reviews.map(r => false),
  }
  render() {
    const { reviews = [], status, taggerErrors } = this.props
    const { showList, show } = this.state
    const TagErrorList = () => (
      <React.Fragment>
        <H5>Most recent tagger errors</H5>
        <HTMLString element={TableStyle} string={taggerErrors} />
      </React.Fragment>
    )
    return (
      <History>
        {taggerErrors && status === 'xml-corrected' && <TagErrorList />}
        <Drop onClick={() => this.setState({ showList: !showList })}>
          Previous error reports
          <Icon color="currentColor" size={2.5}>
            {showList ? 'chevron-down' : 'chevron-right'}
          </Icon>
        </Drop>
        {showList && (
          <React.Fragment>
            {taggerErrors && status !== 'xml-corrected' && <TagErrorList />}
            {reviews.map((review, i) => {
              const { created, user, annotations } = review
              const { givenNames, surname } = user.identities[0].name
              return (
                <React.Fragment key={review.id}>
                  <Drop
                    onClick={() =>
                      this.setState({
                        show: show.map((s, n) => (n === i ? !s : s)),
                      })
                    }
                  >
                    {givenNames} {surname} {moment(created).format('D/M/YY')}
                    <Icon color="currentColor" size={2.5}>
                      {show[i] ? 'chevron-down' : 'chevron-right'}
                    </Icon>
                  </Drop>
                  {show[i] && (
                    <DeletedErrors annotations={annotations} key={review.id} />
                  )}
                </React.Fragment>
              )
            })}
          </React.Fragment>
        )}
      </History>
    )
  }
}

export default ReviewHistory
