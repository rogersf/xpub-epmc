import React from 'react'
import { withRouter } from 'react-router'
import { Mutation } from 'react-apollo'
import { Action, Button, H2, Icon } from '@pubsweet/ui'
import { PreviewFooter, Notification, Buttons, Portal, CloseModal } from '../ui'
import SubmissionCancel from '../SubmissionCancel'
import { REVIEW_MANUSCRIPT, GET_REVIEWS, CURRENT_REVIEW } from './operations'
import ReviewInstructions from './ReviewInstructions'
import ListErrors from './ListErrors'
import SubmissionError from './SubmissionError'
import PreprintReview from '../PreprintReview'

class ReviewFooter extends React.Component {
  state = {
    report: false,
    approve: false,
    preprintWizard: false,
    cancel: false,
  }
  render() {
    const {
      currentUser,
      manuscript,
      previews,
      review,
      history,
      showMessage,
    } = this.props
    const { report, approve, cancel, preprintWizard } = this.state
    const { annotations } = review || []
    const { meta, status, teams, organization, decision } = manuscript
    const submitter = teams.find(t => t.role === 'submitter').teamMembers[0]
    const preprint = organization.name === 'Europe PMC Preprints'
    const { articleType } = meta
    const xStates = [
      'xml-qa',
      'xml-triage',
      'xml-corrected',
      'xml-review',
      'xml-error',
    ]
    const xErrorStates = ['xml-triage', 'xml-corrected', 'xml-error']
    return (
      <PreviewFooter>
        <div>
          <Mutation
            mutation={REVIEW_MANUSCRIPT}
            refetchQueries={() => [
              {
                query: GET_REVIEWS,
                variables: { manuscriptId: manuscript.id },
              },
              {
                query: CURRENT_REVIEW,
                variables: { manuscriptId: manuscript.id },
              },
            ]}
          >
            {(reviewManuscript, { data }) => {
              const setStatus = async newStatus => {
                const data = { id: manuscript.id, status: newStatus }
                await showMessage('info', 'Changing status', true)
                const review = await reviewManuscript({
                  variables: { data },
                })
                const { errors } = review.data.reviewManuscript
                if (errors && errors.length > 0) {
                  await showMessage('error', errors[0].message)
                }
              }
              if (status === 'tagging') {
                return (
                  <Button
                    disabled={!previews}
                    onClick={async () => {
                      await setStatus('xml-qa')
                      history.push(`/submission/${manuscript.id}/activity`)
                    }}
                    primary
                  >
                    Mark tagging complete
                  </Button>
                )
              } else if (status === 'repo-triage') {
                return (
                  <Button
                    onClick={async () => {
                      await setStatus('xml-complete')
                      history.push(`/submission/${manuscript.id}/activity`)
                    }}
                    primary
                  >
                    Retry repository submission
                  </Button>
                )
              } else if (status === 'file-error') {
                return (
                  <div>
                    <Button
                      disabled={!previews}
                      onClick={async () => {
                        await setStatus('xml-qa')
                        history.push(`/submission/${manuscript.id}/activity`)
                      }}
                      primary
                    >
                      {articleType && articleType.startsWith('preprint-')
                        ? 'Release'
                        : 'Send for QA'}
                    </Button>
                    <Button onClick={() => this.setState({ report: true })}>
                      Ask for correction
                    </Button>
                    {report && (
                      <SubmissionError
                        annotations={annotations}
                        close={() => this.setState({ report: false })}
                        currentUser={currentUser}
                        manuscript={manuscript}
                      />
                    )}
                  </div>
                )
              } else if (xStates.includes(status)) {
                return (
                  <React.Fragment>
                    <div>
                      <Button
                        disabled={!previews}
                        onClick={() => this.setState({ approve: true })}
                        primary
                      >
                        Approve{xErrorStates.includes(status) && ' for review'}
                      </Button>
                      <Button onClick={() => this.setState({ report: true })}>
                        {xErrorStates.includes(status)
                          ? 'Ask for correction'
                          : 'Report errors'}
                      </Button>
                    </div>
                    {currentUser.id === submitter.user.id &&
                      decision !== 'accepted' && (
                        <Action
                          onClick={() => this.setState({ cancel: true })}
                          style={{
                            display: 'inline-flex',
                            alignItems: 'center',
                          }}
                        >
                          <Icon color="currentColor" size={2.5}>
                            trash-2
                          </Icon>
                          Cancel submission
                        </Action>
                      )}
                    {approve && (
                      <Portal
                        style={{ width: preprintWizard ? '740px' : '600px' }}
                        transparent
                      >
                        {preprintWizard ? (
                          <PreprintReview
                            complete={async () => {
                              this.setState({ approve: false })
                              await setStatus('xml-complete')
                            }}
                            manuscript={manuscript}
                          />
                        ) : (
                          <React.Fragment>
                            {preprint && status === 'xml-review' && (
                              <p>
                                {`After approval, the full text of your preprint will be displayed on Europe PMC within 24 hours.`}
                              </p>
                            )}
                            <p>
                              {xErrorStates.includes(status) &&
                                'Confirm files are correct and send for final review?'}
                              {status === 'xml-qa' &&
                                'Confirm QA has been completed, and approve the web versions of the manuscript?'}
                              {status === 'xml-review' &&
                                `Approve the ${
                                  preprint
                                    ? 'web versions of the preprint for display on'
                                    : 'final web versions of the manuscript for release to'
                                } Europe PMC now?`}
                            </p>
                            {!['xml-triage', 'xml-error'].includes(status) &&
                              annotations &&
                              annotations.length > 0 && (
                                <Notification type="warning">
                                  {`Errors have been highlighted! Are you sure you want to approve the submission, rather than submitting your error report?`}
                                </Notification>
                              )}
                            <Buttons right>
                              <Button
                                onClick={async () => {
                                  if (preprint && status === 'xml-review') {
                                    this.setState({ preprintWizard: true })
                                  } else {
                                    this.setState({ approve: false })
                                    await setStatus(
                                      status === 'xml-review'
                                        ? 'xml-complete'
                                        : 'xml-review',
                                    )
                                  }
                                }}
                                primary
                              >
                                Yes
                              </Button>
                              <Button
                                onClick={() =>
                                  this.setState({ approve: false })
                                }
                              >
                                No
                              </Button>
                            </Buttons>
                          </React.Fragment>
                        )}
                      </Portal>
                    )}
                    {report && (
                      <React.Fragment>
                        {xErrorStates.includes(status) ? (
                          <SubmissionError
                            annotations={annotations}
                            close={() => this.setState({ report: false })}
                            currentUser={currentUser}
                            manuscript={manuscript}
                          />
                        ) : (
                          <React.Fragment>
                            {annotations && annotations.length > 0 ? (
                              <Portal transparent>
                                <CloseModal
                                  onClick={() =>
                                    this.setState({ report: false })
                                  }
                                />
                                <H2>Send the following errors?</H2>
                                <ListErrors annotations={annotations} />
                                <Buttons right>
                                  <Button
                                    onClick={async () => {
                                      this.setState({ report: false })
                                      await setStatus(
                                        status === 'xml-qa'
                                          ? 'xml-triage'
                                          : 'xml-error',
                                      )
                                      history.push('/')
                                    }}
                                    primary
                                  >
                                    Send error report
                                  </Button>
                                  <Button
                                    onClick={() =>
                                      this.setState({ report: false })
                                    }
                                  >
                                    Cancel
                                  </Button>
                                </Buttons>
                              </Portal>
                            ) : (
                              <ReviewInstructions
                                close={() => this.setState({ report: false })}
                                preprint={preprint}
                              />
                            )}
                          </React.Fragment>
                        )}
                      </React.Fragment>
                    )}
                  </React.Fragment>
                )
              }
              history.push('/')
              return null
            }}
          </Mutation>
        </div>
        {cancel && (
          <SubmissionCancel
            callback={() => this.props.history.push('/')}
            close={() => this.setState({ cancel: false })}
            manuscriptId={manuscript.id}
          />
        )}
      </PreviewFooter>
    )
  }
}

export default withRouter(ReviewFooter)
