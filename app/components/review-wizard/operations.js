import gql from 'graphql-tag'
import { ManuscriptFragment } from '../operations'

export const CONVERT_XML = gql`
  mutation convertXML($id: ID!) {
    convertXML(id: $id)
  }
`

const ReviewFragment = gql`
  fragment ReviewFragment on Review {
    id
    created
    user {
      id
      identities {
        ... on Local {
          name {
            givenNames
            surname
          }
        }
      }
    }
    annotations {
      id
      updated
      quote
      text
      ranges {
        start
        startOffset
      }
      file {
        type
        url
      }
    }
  }
`

export const CURRENT_REVIEW = gql`
  query($manuscriptId: ID!) {
    currentReview(manuscriptId: $manuscriptId) {
      ...ReviewFragment
    }
  }
  ${ReviewFragment}
`

export const GET_REVIEWS = gql`
  query($manuscriptId: ID!) {
    deletedReviews(manuscriptId: $manuscriptId) {
      ...ReviewFragment
    }
  }
  ${ReviewFragment}
`

export const REVIEW_MANUSCRIPT = gql`
  mutation ReviewManuscript($data: ManuscriptInput!) {
    reviewManuscript(data: $data) {
      manuscript {
        ...ManuscriptFragment
      }
      errors {
        type
        message
      }
    }
  }
  ${ManuscriptFragment}
`

export const RETAG_MANUSCRIPT = gql`
  mutation RetagManuscript($data: NewNoteInput!) {
    retagManuscript(data: $data) {
      manuscript {
        ...ManuscriptFragment
      }
      errors {
        message
      }
    }
  }
  ${ManuscriptFragment}
`
