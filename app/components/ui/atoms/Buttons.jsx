import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

const Buttons = styled.div`
  display: flex;
  flex-direction: ${props => (props.left ? 'row' : 'row-reverse')};
  flex-wrap: wrap;
  align-items: center;
  justify-content: ${props =>
    props.right || props.left ? 'flex-start' : 'space-between'}};
  margin: ${th('gridUnit')} calc(${th('gridUnit')} * -1);
  button {
    margin: calc(${th('gridUnit')} / 2) calc(${th('gridUnit')} * 1.5);
  }
`
export { Buttons }
