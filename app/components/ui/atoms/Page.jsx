import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

const Page = styled.div`
  margin: 0 auto;
  max-width: 1000px;
  min-height: calc(
    100vh - (${th('gridUnit')} * ${props => (props.withHeader ? '28' : '20')})
  );
  padding: ${th('gridUnit')} calc(${th('gridUnit')} * 4)
    calc(${th('gridUnit')} * 4);
  box-sizing: border-box;
  @media screen and (max-width: 995px) {
    min-height: calc(
      100vh -
        (
          ${th('gridUnit')} *
            ${props => (props.withHeader ? '31.125' : '23.125')}
        )
    );
  }
`
const FlexBar = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: stretch;
  background-color: ${th('colorTextReverse')};
  height: calc(${th('gridUnit')} * 8);
  box-sizing: border-box;
  & > div {
    margin-bottom: 0;
    height: 100%;
    overflow: hidden;
    box-sizing: border-box;
    & > div {
      padding: calc(${th('gridUnit')} * 2.25) calc(${th('gridUnit')} * 2);
      box-sizing: border-box;
    }
  }
`
const LeftSide = styled.div`
  flex: 1 1 1000px;
  & > div {
    max-width: 1000px;
    margin: 0 0 0 auto;
  }
`
const RightSide = styled.div`
  flex: 1 1 500px;
  z-index: 1;
  & > div {
    max-width: 500px;
    margin: 0 auto 0 0;
    text-align: right;

    a,
    button {
      display: inline-flex;
      align-items: center;
      margin-left: calc(${th('gridUnit')} * 2);
    }
  }
`
export { Page, FlexBar, LeftSide, RightSide }
