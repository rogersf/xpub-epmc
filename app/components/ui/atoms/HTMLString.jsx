import React from 'react'
import parse from 'html-react-parser'

const HTMLString = ({ string, element: Element = 'span' }) => {
  const parsed = parse(string)
  return <Element>{parsed}</Element>
}

export default HTMLString
