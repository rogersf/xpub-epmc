import React from 'react'
import { th } from '@pubsweet/ui-toolkit'
import styled, { createGlobalStyle } from 'styled-components'

const Limit = createGlobalStyle`
  body,
  #root > div > div {
    overflow: hidden;
  }
`
const Screen = styled.div`
  position: fixed;
  right: 0;
  left: 0;
  top: 0;
  bottom: 0;
  width: 100%;
  height: 100%;
  background-color: ${props =>
    props.transparent ? 'rgba(255, 255, 255, 0.6)' : th('colorTextReverse')};
  z-index: 3;
  display: flex;
  justify-content: center;
  align-items: center;
`
const Scroll = styled.div`
  margin: auto;
  width: 100%;
  max-height: 100%;
  overflow: auto;
`
export const Cover = React.forwardRef(({ children, ...props }, ref) => (
  <Screen {...props}>
    <Limit />
    <Scroll ref={ref}>{children}</Scroll>
  </Screen>
))
