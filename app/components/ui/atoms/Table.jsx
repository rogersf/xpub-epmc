import styled from 'styled-components'
import { th, darken } from '@pubsweet/ui-toolkit'

export const Table = styled.table`
  max-width: 100%;
  padding: 0;
  margin: 0;
  border-spacing: 0;
  background-color: ${th('colorTextReverse')};
  border-collapse: collapse;
  td,
  th {
    text-align: left;
    padding: ${th('gridUnit')};
    border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  }
  th {
    background-color: ${darken('colorBackgroundHue', 7)};
    font-weight: 600;
  }
  tr:nth-child(odd) {
    background-color: ${th('colorBackgroundHue')};
  }
  @media screen and (max-width: 870px) {
    td {
      hyphens: auto;
    }
  }
`
