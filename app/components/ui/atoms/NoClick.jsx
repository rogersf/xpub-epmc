import { createGlobalStyle } from 'styled-components'

export const NoClick = createGlobalStyle`
  body {
    pointer-events: none;
  }
`
