A form input select control, which provides a menu of options.

```js
initialState = { 
  value: '',
  options: [
    'one',
    'two',
    'three'
  ]
}

;<Select
  options={state.options}
  onChange={event => setState({ value: event.target.value })}
/>
```

The select dropdown can have a label.

```js
initialState = { 
  value: '',
  options: [
    'one',
    'two',
    'three'
  ]
}

;<Select
  label="Select option"
  options={state.options}
  onChange={event => setState({ value: event.target.value })}
/>
```

You can provide labels for your options, so the option value and the option label text shown to the user can be different.

```js
initialState = { 
  value: '',
  options: [
    {
      label: 'One week',
      value: 1,
    },
    {
      label: 'Two weeks',
      value: 2,
    },
    {
      label: 'Three weeks',
      value: 3,
  ]
}

;<Select
  label="How many weeks?"
  options={state.options}
  onChange={event => setState({ value: event.target.value })}
/>
```

You can change the icon, either by changing the text inside the Icon element (`icon`), or by replacing the icon element entirely with a custom component (`dropIcon`. The custom component will receive the `icon` property.

```js
initialState = { 
  value: '',
  options: [
    'one',
    'two',
    'three'
  ]
}

;<Select
  label="Select option"
  icon="chevron-down"
  options={state.options}
  onChange={event => setState({ value: event.target.value })}
/>
```
