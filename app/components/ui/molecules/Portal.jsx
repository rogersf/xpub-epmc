import React from 'react'
import ReactDOM from 'react-dom'
import { th } from '@pubsweet/ui-toolkit'
import styled from 'styled-components'
import { Cover } from '../atoms'

const Modal = styled.div`
  pointer-events: auto;
  box-shadow: 0 0 8px ${th('colorBorder')};
  background-color: ${props =>
    props.accent ? th('colorBackgroundHue') : th('colorTextReverse')};
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  padding: calc(${th('gridUnit')} * 6);
  width: 800px;
  max-width: 90%;
  box-sizing: border-box;
  margin: calc(${th('gridUnit')} * 4) auto;
  h2:first-child {
    margin-top: 0;
  }
`

class Dialog extends React.Component {
  componentDidMount() {
    const el = document.getElementById('root').firstChild
    el.ariaHidden = true
    el.setAttribute('aria-hidden', 'true')
    document.body.addEventListener(
      'focus',
      event => {
        const dialog = document.getElementById('dialogBox')
        if (dialog && !dialog.contains(event.target)) {
          event.preventDefault()
          event.stopPropagation()
          dialog.querySelector('input, button, a').focus()
        }
      },
      { capture: true },
    )
  }
  componentWillUnmount() {
    const el = document.getElementById('root').firstChild
    el.ariaHidden = false
    el.removeAttribute('aria-hidden')
  }
  render() {
    const { children, ...props } = this.props
    return (
      <Modal id="dialogBox" {...props}>
        {children}
      </Modal>
    )
  }
}

export const Portal = React.forwardRef(
  ({ children, transparent, ...props }, ref) =>
    ReactDOM.createPortal(
      <Cover ref={ref} transparent={transparent}>
        <Dialog {...props}>{children}</Dialog>
      </Cover>,
      document.getElementById('root').firstChild,
    ),
)
