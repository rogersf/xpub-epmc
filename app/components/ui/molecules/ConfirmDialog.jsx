import React from 'react'
import { Button } from '@pubsweet/ui'
import { Buttons } from '../atoms'
import { Portal } from './Portal'

const ConfirmDialog = ({ children, type, message, isOpen, action }) => {
  if (!isOpen) {
    return null
  }
  return (
    <Portal style={{ width: '60%', maxWidth: '650px' }} transparent>
      {message}
      <Buttons right>
        <Button onClick={() => action(true)} primary>
          Yes
        </Button>
        <Button onClick={() => action(false)}>No</Button>
      </Buttons>
    </Portal>
  )
}
export { ConfirmDialog }
