import React from 'react'
import styled, { css } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { TextField, Icon, Button } from '@pubsweet/ui'

const iconButton = css`
  position: relative;
  input {
    padding-right: calc(${th('gridUnit')} * 3);
  }
  button {
    position: absolute;
    top: 0;
    right: 0;
    border: none;
    color: ${th('colorPrimary')}
    background-color: transparent;
    min-width: initial;
    padding: 0;
    line-height: initial;
    height: calc(100% - (${th('gridUnit')} * 3));
    &:hover,
    &:active,
    &:focus {
      color: ${th('colorPrimary')};
      background-color: transparent !important;
    }
    &:focus {
      border: none;
      box-shadow: none;
    }
    &:disabled {
      color: ${th('colorTextPlaceholder')} !important;
    }
  }
`
const iconButtonOffset = css`
  button {
    top: calc(${th('gridUnit')} * 3);
    height: calc(100% - (${th('gridUnit')} * 6));
  }
`
const sepButton = css`
  div:first-child {
    max-width: 100% !important;
    width: calc(100% - (${th('gridUnit')} * 12));
    margin-right: calc(${th('gridUnit')} * 3);
  }
`
const Form = styled.form`
  display: flex;
  align-items: center;
  justify-content: space-between;

  ${props => (props.noButton ? iconButton : sepButton)};
  ${props => props.noButton && props.hasLabel && iconButtonOffset};
`
const SubmitButton = styled(Button)`
  max-width: calc(${th('gridUnit')} * 12);
`
const Text = styled(TextField)`
  width: 100%;
`
const SearchForm = ({
  label,
  noButton,
  onSubmit,
  name,
  onChange,
  placeholder,
  value,
  disabled,
  buttonLabel,
  ...props
}) => (
  <Form hasLabel={label} noButton={noButton} onSubmit={onSubmit} {...props}>
    <Text
      aria-label="Search term"
      label={label}
      name={name}
      onChange={onChange}
      placeholder={placeholder}
      value={value}
    />
    <SubmitButton
      aria-label="Search"
      disabled={disabled}
      primary={buttonLabel}
      title="Search"
      type="submit"
    >
      {buttonLabel || (
        <Icon color="currentColor" size={3}>
          search
        </Icon>
      )}
    </SubmitButton>
  </Form>
)

export default SearchForm
