import React from 'react'
import styled, { css } from 'styled-components'
import { th, darken } from '@pubsweet/ui-toolkit'
import { Icon } from '@pubsweet/ui'

const fullscreen = css`
  margin: 0 auto;
  justify-content: center;
  & > span {
    padding-left: calc(${th('gridUnit')} * 2);
    flex: 0 0 30px;
  }
  & > div {
    flex: 0 1 1470px;
  }
  @media screen and (max-width: 1500px) {
    & > span {
      padding-left: ${th('gridUnit')};
      flex: 0 0 15px;
    }
    & > div {
      flex: 0 1 1485px;
    }
  }
`
const Container = styled.div`
  padding: ${th('gridUnit')};
  margin: ${th('gridUnit')} 0;
  width: 100%;
  display: flex;
  align-items: flex-start;
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  box-sizing: border-box;
  button,
  a:link,
  a:visited {
    text-decoration: underline;
  }
  &.hidden {
    display: none;
  }
  &.warning {
    background-color: ${th('colorWarning')};
  }
  &.info {
    background-color: ${darken('colorBackgroundHue', 7)};
  }
  &.warning,
  &.info {
    color: ${th('colorText')};
    button,
    a:link,
    a:visited {
      color: ${darken('colorPrimary', 20)};
    }
    button:hover,
    a:hover {
      color: ${darken('colorPrimary', 40)};
    }
  }
  &.error {
    background-color: ${th('colorError')};
  }
  &.success {
    background-color: ${th('colorSuccess')};
  }
  &.error,
  &.success {
    color: ${th('colorTextReverse')};
    button,
    a:link,
    a:visited {
      color: ${th('colorTextReverse')};
    }
    button:hover,
    a:hover {
      color: ${darken('colorBackgroundHue', 5)};
    }
  }
  & > div,
  & > div * {
    font-size: ${th('fontSizeBaseSmall')};
  }
  ${props => props.fullscreen && fullscreen};
`
const NotifIcon = {
  warning: 'alert-triangle',
  error: 'alert-circle',
  info: 'info',
  success: 'check-circle',
}

const Notification = ({ children, type, className, ...props }) => (
  <Container className={`${className || ''} ${type}`} {...props}>
    {NotifIcon[type] && (
      <Icon color="currentColor" size={2}>
        {NotifIcon[type]}
      </Icon>
    )}
    <div>{children}</div>
  </Container>
)
export { Notification }
