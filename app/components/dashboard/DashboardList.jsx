import React from 'react'
import styled, { withTheme } from 'styled-components'
import { graphql, compose } from 'react-apollo'
import { th, lighten } from '@pubsweet/ui-toolkit'
import { Action, Button, Icon, Link } from '@pubsweet/ui'
import {
  B,
  HTMLString,
  ZebraList,
  ZebraListItem,
  Notification,
  PreprintLabel,
} from '../ui'
import PreprintSelections from '../PreprintSelections'
import { States, timeSince } from './dashboard'
import { CLAIM_MANUSCRIPT, UNCLAIM_MANUSCRIPT } from '../operations'
import { ADMIN_MANUSCRIPTS } from './operations'

const ListItem = styled(ZebraListItem)`
  display: flex;
  align-items: top;
  justify-content: space-between;
  padding: ${th('gridUnit')} calc(${th('gridUnit')} * 2);
  @media screen and (max-width: 800px) {
    display: block;
  }
  a {
    font-weight: 600;
  }
`
const Title = styled.p`
  margin: 0 0 ${th('gridUnit')};
`
const ItemDetails = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: start;
  flex-wrap: wrap;
  font-size: ${th('fontSizeBaseSmall')};
  & > * {
    margin-right: calc(${th('gridUnit')} * 2);
  }
`
const Column = styled.div`
  font-size: ${th('fontSizeBaseSmall')};
  margin-left: calc(${th('gridUnit')} * 4);
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  @media screen and (max-width: 800px) {
    margin: 0;
    align-items: flex-start;
  }
  & > *:not(:first-child) {
    margin-top: calc(${th('gridUnit')} / 2);
  }
  a {
    display: inline-flex;
    align-items: center;
  }
  button {
    margin-top: 0;
    font-size: ${th('fontSizeBaseSmall')};
    font-weight: 600;
  }
  div {
    width: 100%;
    font-size: ${th('fontSizeBaseSmall')};
    display: flex;
    justify-content: space-between;
    align-items: baseline;
  }
`
const ItemStatus = styled.div`
  white-space: nowrap;
  padding: 0 ${th('gridUnit')};
  color: ${th('colorTextReverse')};
  font-size: ${th('fontSizeBaseSmall')};
  min-width: calc(${th('gridUnit')} * 27);
  text-align: center;
  &.warning {
    background-color: ${th('colorWarning')};
    color: ${th('colorText')};
  }
  &.error {
    background-color: ${th('colorError')};
  }
  &.normal {
    background-color: ${lighten('colorPrimary', 30)};
  }
  &.success {
    background-color: ${th('colorSuccess')};
  }
  &.removed {
    background-color: ${th('colorBorder')};
  }
  @media screen and (max-width: 800px) {
    min-width: 0;
    width: calc(${th('gridUnit')} * 27);
    white-space: normal;
    margin-top: ${th('gridUnit')};
  }
`
const ClaimAction = styled(Button)`
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('fontSize')};
  padding: 0 calc(${th('gridUnit')} * 1.5);
  margin-top: ${th('gridUnit')};
  min-width: 0;
`
const UnclaimAction = styled(Action)`
  display: flex;
  align-items: center;
  font-size: ${th('fontSizeBaseSmall')};
  &.disabled,
  &:disabled {
    opacity: 1;
    color: ${th('colorText')};
    cursor: default;
    &:hover {
      text-decoration: none;
      background-color: transparent;
    }
  }
  & > *:nth-child(even) {
    display: none;
  }
  &.active:hover {
    & > *:nth-child(even) {
      display: inline-flex;
    }
    & > *:nth-child(odd) {
      display: none;
    }
  }
`
const claimStates = [
  'submitted',
  'file-error',
  'xml-triage',
  'xml-corrected',
  'xml-error',
  'repo-triage',
  'withdrawal-triage',
]

const NoteIcon = withTheme(({ theme, indexed }) => (
  <Icon color={theme.colorSuccess} size={2}>
    message-square
  </Icon>
))

const DashboardList = ({
  currentUser,
  listData,
  sectionRole,
  sectionColor,
  claimManuscript,
  unclaimManuscript,
  errors,
}) => (
  <ZebraList>
    {listData.map(manuscript => {
      if (manuscript.teams) {
        if (manuscript.id && manuscript.status) {
          const {
            claiming,
            meta,
            journal,
            created,
            updated,
            deleted,
            status,
            teams,
            organization,
            version,
          } = manuscript
          const {
            articleIds,
            releaseDelay,
            unmatchedJournal,
            notes = [],
            subjects = [],
          } = meta
          const preprint = organization.name === 'Europe PMC Preprints'
          const { teamMembers: sTeam } =
            teams.find(t => t.role === 'submitter') || {}
          const [submitter] = sTeam || []
          const { title: st, givenNames: sg, surname: ss } = submitter
            ? submitter.alias.name
            : {}
          const { teamMembers: rTeam } =
            teams.find(t => t.role === 'reviewer') || {}
          const [reviewer] = rTeam || []
          const { title: rt, givenNames: rg, surname: rs } = reviewer
            ? reviewer.alias.name
            : {}
          const userRole =
            sectionRole ||
            (currentUser.admin && 'admin') ||
            (reviewer && reviewer.user.id === currentUser.id && 'reviewer') ||
            (submitter && submitter.user.id === currentUser.id && 'submitter')
          const state =
            (deleted && {
              color: 'removed',
              status: 'Deleted',
              url: 'activity',
            }) ||
            States[userRole][status]
          const { url } = state
          const adminNoteCheck =
            notes &&
            notes.filter(
              n =>
                n.notesType === 'userMessage' &&
                typeof JSON.parse(n.content) === 'string',
            ).length > 0
          const reviewerNote =
            notes &&
            notes.find(n => n.notesType === 'selectedReviewer') &&
            JSON.parse(
              notes.find(n => n.notesType === 'selectedReviewer').content,
            )
          const rnName = reviewerNote && reviewerNote.name
          const preprintCheck =
            notes &&
            notes.some(n =>
              ['addLicense', 'versionApproval', 'versionList'].includes(
                n.notesType,
              ),
            )
          return (
            <ListItem key={manuscript.id}>
              <div>
                <Title>
                  <Link to={`/submission/${manuscript.id}/${url}`}>
                    <HTMLString string={meta.title} />
                  </Link>
                </Title>
                <ItemDetails>
                  {reviewerNote ? (
                    <React.Fragment>
                      {!preprint && (
                        <span>
                          {`SUBMITTER:${st ? ` ${st} ` : ' '}${sg} ${ss}`}
                        </span>
                      )}
                      <span>{`INVITED REVIEWER: ${
                        rnName.givenNames ? `${rnName.givenNames} ` : ''
                      }${rnName.surname}`}</span>
                    </React.Fragment>
                  ) : (
                    <React.Fragment>
                      {reviewer &&
                      submitter &&
                      reviewer.user.id === submitter.user.id ? (
                        <span>
                          {`SUBMITTER/REVIEWER:${
                            st ? ` ${st} ` : ' '
                          }${sg} ${ss}`}
                        </span>
                      ) : (
                        <React.Fragment>
                          {!preprint && (
                            <span>
                              {`SUBMITTER:${st ? ` ${st} ` : ' '}${sg} ${ss}`}
                            </span>
                          )}
                          {reviewer && (
                            <span>
                              {`REVIEWER:${rt ? ` ${rt} ` : ' '}${rg} ${rs}`}
                            </span>
                          )}
                        </React.Fragment>
                      )}
                    </React.Fragment>
                  )}
                </ItemDetails>
                <ItemDetails>
                  {preprint && (
                    <PreprintLabel subjects={subjects} version={version} />
                  )}
                  <span>{manuscript.id}</span>
                  {articleIds &&
                    articleIds
                      .filter(
                        aid => !['doi', 'pre-pmc'].includes(aid.pubIdType),
                      )
                      .map(aid => (
                        <span key={aid.id}>
                          {aid.pubIdType.toUpperCase()}: {aid.id}
                        </span>
                      ))}
                  <span>
                    {preprint ? 'SOURCE' : 'JOURNAL'}
                    {': '}
                    {journal ? journal.meta.nlmta : unmatchedJournal}
                  </span>
                  {releaseDelay && !preprint && (
                    <span>
                      EMBARGO: {releaseDelay} month
                      {parseInt(releaseDelay, 10) !== 1 && 's'}
                    </span>
                  )}
                </ItemDetails>
              </div>
              <Column>
                <ItemStatus className={sectionColor || state.color}>
                  {state.status}
                </ItemStatus>
                <span>
                  <B>{timeSince(updated)}</B> since last update
                </span>
                {currentUser && currentUser.admin && (
                  <span>
                    <B>{timeSince(created)}</B> in process
                  </span>
                )}
                {preprintCheck && userRole === 'reviewer' && (
                  <PreprintSelections manuscript={manuscript} />
                )}
                <div>
                  {currentUser && currentUser.admin && (
                    <Link to={`/submission/${manuscript.id}/activity`}>
                      Activity
                      {adminNoteCheck && <NoteIcon />}
                    </Link>
                  )}
                  {((userRole === 'admin' && claimStates.includes(status)) ||
                    (currentUser.external && status === 'xml-qa') ||
                    (currentUser.tagger && status === 'tagging')) && (
                    <React.Fragment>
                      {claiming ? (
                        <UnclaimAction
                          className={claiming.id === currentUser.id && 'active'}
                          disabled={claiming.id !== currentUser.id}
                          onClick={async () => {
                            const newData = await unclaimManuscript({
                              variables: { id: manuscript.id },
                            })
                            errors(newData.data.unclaimManuscript.errors)
                            return newData
                          }}
                        >
                          <Icon color="currentColor" size={2}>
                            check
                          </Icon>
                          <Icon color="currentColor" size={2}>
                            x
                          </Icon>
                          <span>{claiming.givenNames}</span>
                          <span>Unclaim</span>
                        </UnclaimAction>
                      ) : (
                        <ClaimAction
                          onClick={async () => {
                            const newData = await claimManuscript({
                              variables: { id: manuscript.id },
                            })
                            errors(newData.data.claimManuscript.errors)
                            return newData
                          }}
                          primary
                        >
                          Claim
                        </ClaimAction>
                      )}
                    </React.Fragment>
                  )}
                </div>
              </Column>
            </ListItem>
          )
        }
        return (
          <ListItem key={manuscript.id}>
            <Notification type="warning">Invalid manuscript data</Notification>
          </ListItem>
        )
      }
      return null
    })}
  </ZebraList>
)

export default compose(
  graphql(CLAIM_MANUSCRIPT, {
    name: 'claimManuscript',
    options: props => ({
      refetchQueries: [
        { query: ADMIN_MANUSCRIPTS, variables: props.variables },
      ],
      awaitRefetchQueries: true,
    }),
  }),
  graphql(UNCLAIM_MANUSCRIPT, {
    name: 'unclaimManuscript',
    options: props => ({
      refetchQueries: [
        { query: ADMIN_MANUSCRIPTS, variables: props.variables },
      ],
      awaitRefetchQueries: true,
    }),
  }),
)(DashboardList)
