import gql from 'graphql-tag'

export const CURRENT_USER = gql`
  query($email: String) {
    epmc_currentUser(email: $email) {
      id
    }
  }
`
export const ADD_REVIEWER = gql`
  mutation AddReviewer($noteId: ID!) {
    addReviewer(noteId: $noteId) {
      success
      message
    }
  }
`
export const COUNT_MANUSCRIPTS = gql`
  query CountByStatus {
    countByStatus {
      type
      count
    }
  }
`
export const ALERT_DAYS = gql`
  query CheckAge(
    $states: [String]
    $days: Int
    $weekdays: Boolean
    $preprint: Boolean
  ) {
    checkAge(
      states: $states
      days: $days
      weekdays: $weekdays
      preprint: $preprint
    ) {
      alert
    }
  }
`
export const GET_MANUSCRIPT = gql`
  query SearchArticleIds($id: String!) {
    searchArticleIds(id: $id) {
      manuscript {
        id
        status
      }
      errors {
        message
      }
    }
  }
`

const DashboardFragment = gql`
  fragment DashboardManuscript on Manuscript {
    id
    version
    status
    created
    updated
    deleted
    formState
    count
    claiming {
      id
      givenNames
      surname
    }
    journal {
      id
      journalTitle
      meta {
        nlmta
      }
    }
    meta {
      title
      articleIds {
        pubIdType
        id
      }
      publicationDates {
        type
        date
      }
      fundingGroup {
        fundingSource
        awardId
        title
        pi {
          surname
          givenNames
          title
          email
        }
      }
      releaseDelay
      unmatchedJournal
      notes {
        id
        notesType
        content
      }
      subjects
    }
    organization {
      name
    }
    teams {
      role
      teamMembers {
        user {
          id
        }
        alias {
          name {
            title
            surname
            givenNames
          }
        }
      }
    }
  }
`

export const USER_MANUSCRIPTS = gql`
  query LoadManuscripts {
    manuscripts {
      ...DashboardManuscript
    }
  }
  ${DashboardFragment}
`

export const ADMIN_MANUSCRIPTS = gql`
  query LoadAdminManuscripts(
    $external: Boolean
    $tagger: Boolean
    $type: String
    $page: Int
    $pageSize: Int
  ) {
    adminManuscripts(
      external: $external
      tagger: $tagger
      type: $type
      page: $page
      pageSize: $pageSize
    ) {
      total
      manuscripts {
        ...DashboardManuscript
      }
    }
  }
  ${DashboardFragment}
`

export const CITATION_MANUSCRIPTS = gql`
  query CitationManuscripts($page: Int, $pageSize: Int) {
    citationManuscripts(page: $page, pageSize: $pageSize) {
      total
      manuscripts {
        ...DashboardManuscript
        journal {
          meta {
            pubmedStatus
          }
        }
      }
    }
  }
  ${DashboardFragment}
`

export const MANUSCRIPTS_BY_STATUS = gql`
  query FindManuscriptsByStatus($query: String!, $page: Int, $pageSize: Int) {
    findByStatus(query: $query, page: $page, pageSize: $pageSize) {
      total
      manuscripts {
        ...DashboardManuscript
      }
    }
  }
  ${DashboardFragment}
`

export const MANUSCRIPTS_BY_EMAIL_OR_LASTNAME = gql`
  query MANUSCRIPTS_BY_EMAIL_OR_LASTNAME(
    $query: String!
    $page: Int
    $pageSize: Int
  ) {
    searchManuscripts(query: $query, page: $page, pageSize: $pageSize) {
      total
      manuscripts {
        ...DashboardManuscript
      }
    }
  }
  ${DashboardFragment}
`
