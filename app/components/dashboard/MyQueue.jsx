import React from 'react'
import { Query } from 'react-apollo'
import { withRouter } from 'react-router'
import styled from 'styled-components'
import URLSearchParams from 'url-search-params'
import { th } from '@pubsweet/ui-toolkit'
import { Action, H2 as HTwo } from '@pubsweet/ui'
import { pageSize } from 'config'
import { Loading, LoadingIcon, Notification, Pagination, Toggle } from '../ui'
import { ADMIN_MANUSCRIPTS } from './operations'
import DashboardList from './DashboardList'

const H2 = styled(HTwo)`
  font-size: ${th('fontSizeHeading4')};
  line-height: ${th('lineHeightHeading4')};
`
const PaginationPane = styled.div`
  align-items: center;
  display: flex;
  &.top {
    justify-content: space-between;
    margin-bottom: calc(${th('gridUnit')} * 3);
  }
  &.bottom {
    justify-content: flex-end;
    margin-top: calc(${th('gridUnit')} * 3);
  }
`
class MyQueue extends React.Component {
  state = { type: null, currentPage: 1 }
  componentDidMount() {
    const { search } = this.props.location
    if (search) {
      const params = new URLSearchParams(search)
      const type = params.get('type')
      const page = params.get('page') ? parseInt(params.get('page'), 10) : 1
      this.setState({ type, currentPage: page })
    }
  }
  render() {
    const { currentUser, history, errors, setErrors } = this.props
    const { type, currentPage } = this.state
    const page = currentPage ? currentPage - 1 : 0
    const onPageChange = p => {
      this.setState({ currentPage: p })
      history.replace(`/dashboard?${type ? `type=${type}&` : ''}page=${p}`)
    }
    const onTypeChange = t => {
      this.setState({ currentPage: 1, type: t })
      history.replace(`/dashboard?${t ? `type=${t}` : ''}`)
    }
    const variables = {
      external: !!currentUser.external,
      tagger: !!currentUser.tagger,
      type,
      page,
      pageSize,
    }
    return (
      <Query
        fetchPolicy="network-only"
        query={ADMIN_MANUSCRIPTS}
        variables={variables}
      >
        {({ data, loading }) => {
          if (loading) {
            return (
              <Loading>
                <LoadingIcon />
              </Loading>
            )
          }
          if (!data) {
            history.go()
            return null
          }
          const mine = []
          const claimed = []
          const unclaimed = []
          const { manuscripts, total } = data.adminManuscripts
          const PageNavigation = () => (
            <Pagination
              currentPage={currentPage}
              onPageEntered={onPageChange}
              pageSize={pageSize}
              totalSize={total}
            />
          )
          if (manuscripts.length === 0) {
            if (currentUser.admin) {
              history.push('/admin-dashboard')
            } else if (currentUser.external) {
              return (
                <Notification type="info">
                  No submissions currently need QA.
                </Notification>
              )
            } else if (currentUser.tagger) {
              return (
                <Notification type="info">
                  No submissions currently need tagging.
                </Notification>
              )
            }
          }
          manuscripts.forEach(m => {
            if (m.claiming) {
              if (m.claiming.id === currentUser.id) {
                mine.push(m)
              } else {
                claimed.push(m)
              }
            } else {
              unclaimed.push(m)
            }
          })
          return (
            <React.Fragment>
              {errors.map(e => (
                <Notification key="message" type="warning">
                  {e.message}
                </Notification>
              ))}
              {manuscripts.length === 0 && (
                <Notification key="message" type="info">
                  No submissions of this type in the queue.
                </Notification>
              )}
              <PaginationPane className="top">
                <Toggle>
                  <Action
                    className={!type && 'current'}
                    onClick={() => onTypeChange(null)}
                  >
                    All
                    {!type && ` (${total})`}
                  </Action>
                  <Action
                    className={type === 'preprints' && 'current'}
                    onClick={() => onTypeChange('preprints')}
                  >
                    Preprints
                    {type === 'preprints' && ` (${total})`}
                  </Action>
                  <Action
                    className={type === 'manuscripts' && 'current'}
                    onClick={() => onTypeChange('manuscripts')}
                  >
                    Author Manuscripts
                    {type === 'manuscripts' && ` (${total})`}
                  </Action>
                </Toggle>
                {total > pageSize && <PageNavigation />}
              </PaginationPane>
              {mine.length > 0 && (
                <React.Fragment>
                  <H2>My claimed manuscripts ({mine[0].count})</H2>
                  <DashboardList
                    currentUser={currentUser}
                    errors={setErrors}
                    listData={mine}
                    sectionColor="error"
                    sectionRole="admin"
                    variables={variables}
                  />
                </React.Fragment>
              )}
              {unclaimed.length > 0 && (
                <React.Fragment>
                  <H2>Unclaimed ({unclaimed[0].count})</H2>
                  <DashboardList
                    currentUser={currentUser}
                    errors={setErrors}
                    listData={unclaimed}
                    sectionColor="error"
                    sectionRole="admin"
                    variables={variables}
                  />
                </React.Fragment>
              )}
              {claimed.length > 0 && (
                <React.Fragment>
                  <H2>{`In process with another user (${claimed[0].count})`}</H2>
                  <DashboardList
                    currentUser={currentUser}
                    errors={setErrors}
                    listData={claimed}
                    sectionColor="warning"
                    sectionRole="admin"
                    variables={variables}
                  />
                </React.Fragment>
              )}
              {total > pageSize && (
                <PaginationPane className="bottom">
                  <PageNavigation />
                </PaginationPane>
              )}
            </React.Fragment>
          )
        }}
      </Query>
    )
  }
}

export default withRouter(MyQueue)
