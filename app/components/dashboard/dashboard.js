const submitterState = {
  INITIAL: {
    status: 'Incomplete',
    color: 'error',
    url: 'create',
  },
  READY: {
    status: 'Incomplete',
    color: 'error',
    url: 'submit',
  },
  'submission-error': {
    status: 'Submission error',
    color: 'error',
    url: 'submit',
  },
  'in-review': {
    status: 'Initial review',
    color: 'warning',
    url: 'submit',
  },
  submitted: {
    status: 'Europe PMC QA',
    color: 'normal',
    url: 'submit',
  },
  tagging: {
    status: 'Processing',
    color: 'normal',
    url: 'submit',
  },
  'file-error': {
    status: 'Processing',
    color: 'normal',
    url: 'submit',
  },
  'xml-qa': {
    status: 'Processing',
    color: 'normal',
    url: 'submit',
  },
  'xml-review': {
    status: 'Final review',
    color: 'warning',
    url: 'submit',
  },
  'xml-triage': {
    status: 'Processing',
    color: 'normal',
    url: 'submit',
  },
  'xml-corrected': {
    status: 'Processing',
    color: 'normal',
    url: 'submit',
  },
  'xml-error': {
    status: 'Processing',
    color: 'normal',
    url: 'submit',
  },
  'xml-complete': {
    status: 'Approved for Europe PMC',
    color: 'success',
    url: 'submit',
  },
  'link-existing': {
    status: 'Grant links submitted',
    color: 'success',
    url: 'submit',
  },
  'repo-ready': {
    status: 'Approved for Europe PMC',
    color: 'success',
    url: 'submit',
  },
  'repo-processing': {
    status: 'Approved for Europe PMC',
    color: 'success',
    url: 'submit',
  },
  'repo-triage': {
    status: 'Approved for Europe PMC',
    color: 'success',
    url: 'submit',
  },
  published: {
    status: 'Available in Europe PMC',
    color: 'success',
    url: 'submit',
  },
  'being-withdrawn': {
    status: 'Removal request sent',
    color: 'removed',
    url: 'submit',
  },
  'withdrawal-triage': {
    status: 'Removal request sent',
    color: 'removed',
    url: 'submit',
  },
}

const reviewerState = {
  INITIAL: {
    status: 'Incomplete',
    color: 'warning',
    url: 'create',
  },
  READY: {
    status: 'Incomplete',
    color: 'warning',
    url: 'submit',
  },
  'submission-error': {
    status: 'Submission error',
    color: 'warning',
    url: 'submit',
  },
  'in-review': {
    status: 'Initial review',
    color: 'error',
    url: 'submit',
  },
  submitted: {
    status: 'Europe PMC QA',
    color: 'normal',
    url: 'submit',
  },
  tagging: {
    status: 'Processing',
    color: 'normal',
    url: 'submit',
  },
  'file-error': {
    status: 'Processing',
    color: 'normal',
    url: 'submit',
  },
  'xml-qa': {
    status: 'Processing',
    color: 'normal',
    url: 'submit',
  },
  'xml-review': {
    status: 'Final review',
    color: 'error',
    url: 'review',
  },
  'xml-triage': {
    status: 'Processing',
    color: 'normal',
    url: 'submit',
  },
  'xml-corrected': {
    status: 'Processing',
    color: 'normal',
    url: 'submit',
  },
  'xml-error': {
    status: 'Correcting',
    color: 'normal',
    url: 'submit',
  },
  'xml-complete': {
    status: 'Approved for Europe PMC',
    color: 'success',
    url: 'submit',
  },
  'link-existing': {
    status: 'Grant links submitted',
    color: 'success',
    url: 'submit',
  },
  'repo-ready': {
    status: 'Approved for Europe PMC',
    color: 'success',
    url: 'submit',
  },
  'repo-processing': {
    status: 'Approved for Europe PMC',
    color: 'success',
    url: 'submit',
  },
  'repo-triage': {
    status: 'Approved for Europe PMC',
    color: 'success',
    url: 'submit',
  },
  published: {
    status: 'Available in Europe PMC',
    color: 'success',
    url: 'submit',
  },
  'being-withdrawn': {
    status: 'Removal request sent',
    color: 'removed',
    url: 'submit',
  },
  'withdrawal-triage': {
    status: 'Removal request sent',
    color: 'removed',
    url: 'submit',
  },
}

const adminState = {
  INITIAL: {
    status: 'Incomplete',
    color: 'normal',
    url: 'create',
  },
  READY: {
    status: 'Incomplete',
    color: 'normal',
    url: 'submit',
  },
  'submission-error': {
    status: 'Submission error',
    color: 'normal',
    url: 'submit',
  },
  'in-review': {
    status: 'Initial review',
    color: 'normal',
    url: 'submit',
  },
  submitted: {
    status: 'Needs QA',
    color: 'error',
    url: 'submit',
  },
  tagging: {
    status: 'XML tagging',
    color: 'normal',
    url: 'review',
  },
  'file-error': {
    status: 'File loading error',
    color: 'error',
    url: 'review',
  },
  'xml-qa': {
    status: 'Needs XML QA',
    color: 'error',
    url: 'review',
  },
  'xml-review': {
    status: 'Final review',
    color: 'normal',
    url: 'review',
  },
  'xml-triage': {
    status: 'XML errors',
    color: 'error',
    url: 'review',
  },
  'xml-corrected': {
    status: 'XML corrected',
    color: 'error',
    url: 'review',
  },
  'xml-error': {
    status: 'XML reviewed',
    color: 'error',
    url: 'review',
  },
  'xml-complete': {
    status: 'Needs citation',
    color: 'warning',
    url: 'activity',
  },
  'link-existing': {
    status: 'Grant links submitted',
    color: 'success',
    url: 'activity',
  },
  'repo-ready': {
    status: 'Approved for Europe PMC',
    color: 'success',
    url: 'activity',
  },
  'repo-processing': {
    status: 'Sent to Europe PMC',
    color: 'success',
    url: 'activity',
  },
  'repo-triage': {
    status: 'Repository errors',
    color: 'error',
    url: 'review',
  },
  published: {
    status: 'Available in Europe PMC',
    color: 'success',
    url: 'activity',
  },
  'being-withdrawn': {
    status: 'Removal request sent',
    color: 'removed',
    url: 'activity',
  },
  'withdrawal-triage': {
    status: 'Removal request error',
    color: 'error',
    url: 'activity',
  },
}

export const States = {
  submitter: submitterState,
  reviewer: reviewerState,
  admin: adminState,
}

export const timeSince = date => {
  const seconds = Math.floor((new Date() - new Date(date)) / 1000)
  let interval = Math.floor(seconds / 31536000)
  if (interval >= 1) {
    return `${interval} year${interval !== 1 ? 's' : ''}`
  }
  interval = Math.floor(seconds / 2592000)
  if (interval >= 1) {
    return `${interval} month${interval !== 1 ? 's' : ''}`
  }
  interval = Math.floor(seconds / 86400)
  if (interval >= 1) {
    return `${interval} day${interval !== 1 ? 's' : ''}`
  }
  interval = Math.floor(seconds / 3600)
  if (interval >= 1) {
    return `${interval} hour${interval !== 1 ? 's' : ''}`
  }
  interval = Math.floor(seconds / 60)
  if (interval >= 1) {
    return `${interval} min${interval !== 1 ? 's' : ''}`
  }

  const time = Math.floor(seconds) < 0 ? 0 : Math.floor(seconds)

  return `${time} sec${interval !== 1 ? 's' : ''}`
}
