import React from 'react'
import { withRouter } from 'react-router'
import { ApolloConsumer } from 'react-apollo'
import styled from 'styled-components'
import URLSearchParams from 'url-search-params'
import { th } from '@pubsweet/ui-toolkit'
import { H2 } from '@pubsweet/ui'
import { Notification, SearchForm } from '../ui'
import { GET_MANUSCRIPT } from './operations'
import { States } from './dashboard'

export const QueryLabel = styled(H2)`
  float: left;
  max-width: 50%;
  margin: 0 auto calc(${th('gridUnit')} * 2);
  @media screen and (max-width: 880px) {
    float: none;
    max-width: 100%;
  }
`
export const PaginationPane = styled.div`
  display: flex;
  justify-content: flex-end;
`

const SearchArea = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  input {
    height: calc(${th('gridUnit')} * 5);
  }
  & > *:first-child {
    max-width: calc(${th('gridUnit')} * 15);
    margin-right: calc(${th('gridUnit')} * 3);
  }
  & > *:nth-child(2) {
    min-width: calc(${th('gridUnit')} * 38);
  }
  @media screen and (max-width: 880px) {
    justify-content: flex-start;
    margin-top: ${th('gridUnit')};
    flex-wrap: wrap;
  }
`

let errorTimer

class SearchBoxes extends React.Component {
  state = {
    id: '',
    search: '',
    errors: [],
  }

  componentDidMount() {
    const params = new URLSearchParams(this.props.history.location.search)
    const search = params.get('search')
    if (search) {
      this.setState({ search })
    }
  }
  componentDidUpdate() {
    const { errors } = this.state

    if (errorTimer) {
      clearTimeout(errorTimer)
    }

    if (errors && this.area) {
      errorTimer = setTimeout(() => {
        this.setState({ errors: [] })
      }, 10000)
    }
  }
  componentWillUnmount() {
    clearTimeout(errorTimer)
  }

  setRef = area => {
    this.area = area
  }
  onSearchValChanged = e => {
    this.setState({
      [e.target.name]: e.target.value,
      errors: [],
    })
  }
  onSearchValSubmitted = (where, e) => {
    e.preventDefault()
    const val = this.state[where]
    const route = `/search?${where}=${val}`
    this.props.history.push(route)
  }
  render() {
    const { id, search } = this.state
    return (
      <React.Fragment>
        <SearchArea ref={this.setRef}>
          <ApolloConsumer>
            {client => {
              const goToArticle = async e => {
                e.preventDefault()
                const { data } = await client.query({
                  query: GET_MANUSCRIPT,
                  variables: { id },
                })
                const { errors, manuscript } = data.searchArticleIds
                if (errors) {
                  this.setState({ errors: data.searchArticleIds.errors })
                } else if (manuscript) {
                  this.props.history.push(
                    `submission/${manuscript.id}/${
                      States[this.props.reviewer ? 'reviewer' : 'admin'][
                        manuscript.status
                      ].url
                    }`,
                  )
                }
              }
              return (
                <SearchForm
                  disabled={!id}
                  name="id"
                  noButton="true"
                  onChange={this.onSearchValChanged}
                  onSubmit={e => goToArticle(e)}
                  placeholder="ID"
                  value={id}
                />
              )
            }}
          </ApolloConsumer>
          <SearchForm
            disabled={!search}
            name="search"
            noButton="true"
            onChange={this.onSearchValChanged}
            onSubmit={e => this.onSearchValSubmitted('search', e)}
            placeholder="Title, or author last name or email"
            value={search}
          />
        </SearchArea>
        {this.state.errors.map(e => (
          <Notification key={e.message} type="error">
            {e.message}
          </Notification>
        ))}
      </React.Fragment>
    )
  }
}

export default withRouter(SearchBoxes)
