import { compose } from 'recompose'
import { withFormik } from 'formik'
import { graphql } from 'react-apollo'
import * as yup from 'yup' // for everything
import MyAccount from './MyAccount'
import { UPDATE_CURRENT_USER } from './operations'

const handleSubmit = (values, { props, setSubmitting, setValues, setErrors }) =>
  props
    .epmc_updateCurrentUser({
      variables: {
        input: {
          title: values.title,
          givenNames: values.givenNames,
          surname: values.surname,
          currentPassword: values.currentPassword,
          newPassword: values.newPassword,
          email: values.email,
        },
      },
    })
    .then(({ data, errors }) => {
      if (!errors) {
        setSubmitting(true)
        setValues(
          Object.assign(values, {
            success: 'Account details are updated successfully!',
            error: '',
          }),
        )
        setTimeout(() => {
          setValues(
            Object.assign(values, {
              success: '',
              error: '',
            }),
          )
        }, 1000)
      } else {
        throw new Error()
      }
    })
    .catch(e => {
      if (e.graphQLErrors) {
        setSubmitting(false)
        setErrors(e.graphQLErrors[0].message)
        setValues(
          Object.assign(values, {
            success: '',
            error: e.graphQLErrors[0].message,
          }),
        )
      }
    })

const enhancedFormik = withFormik({
  mapPropsToValues: props => {
    const [currentIdentity] = props.currentUser.identities.filter(
      identity => identity.type === 'local',
    )
    return {
      email: currentIdentity.email,
      currentPassword: '',
      newPassword: '',
      title: currentIdentity.name.title ? currentIdentity.name.title : '',
      givenNames: currentIdentity.name.givenNames,
      surname: currentIdentity.name.surname,
    }
  },
  validationSchema: yup.object().shape({
    email: yup
      .string()
      .email('Email not valid')
      .required('Email is required'),
    givenNames: yup.string().required('Given names is required'),
    surname: yup.string().required('Surname is required'),
    newPassword: yup
      .string()
      .min(8, 'Password requires minimum length of 8 characeter'),
    currentPassword: yup.string().when('newPassword', (newPassword, schema) => {
      if (newPassword) {
        return schema.required('Current password is required')
      }
      return schema
    }),
  }),
  displayName: 'login',
  handleSubmit,
})(MyAccount)

export default compose(
  graphql(UPDATE_CURRENT_USER, { name: 'epmc_updateCurrentUser' }),
)(enhancedFormik)
