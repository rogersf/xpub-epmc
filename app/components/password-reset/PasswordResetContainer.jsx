import { compose } from 'recompose'
import { withFormik } from 'formik'
import { graphql } from 'react-apollo'
import * as yup from 'yup' // for everything
import PasswordReset from './PasswordReset'
import { resetPassword } from './operations'

const handleSubmit = (
  values,
  { props, setSubmitting, setErrors, resetForm, setStatus },
) =>
  props
    .resetPassword({
      variables: { token: props.match.params.id, newPassword: values.password },
    })
    .then(({ data, errors }) => {
      resetForm()
      !errors && setStatus({ reset: true })
    })
    .catch(e => {
      if (e.graphQLErrors) {
        setSubmitting(false)
        setErrors(e.graphQLErrors[0].message)
      }
    })

const enhancedFormik = withFormik({
  initialValues: {
    password: '',
    confirmPassword: '',
  },
  mapPropsToValues: props => ({
    password: props.password,
    confirmPassword: props.confirmPassword,
  }),
  validationSchema: yup.object().shape({
    password: yup
      .string()
      .min(8, 'Password must be 8 characters or longer')
      .required('Password is required'),
    confirmPassword: yup
      .string()
      .oneOf(
        [yup.ref('password'), null],
        "Confirm password and password don't match",
      )
      .required('Confirm Password is required'),
  }),
  displayName: 'password-reset',
  handleSubmit,
})(PasswordReset)

export default compose(graphql(resetPassword, { name: 'resetPassword' }))(
  enhancedFormik,
)
