import { compose } from 'recompose'
import { withFormik } from 'formik'
import { graphql } from 'react-apollo'
import { createBrowserHistory } from 'history'
import * as yup from 'yup' // for everything
import { LOGIN_USER } from '../operations'
import { handleSSO } from '../login/LoginContainer'
import redirectPath from '../login/redirect'
import userHelper from '../helpers/userHelper'
import Signup from './Signup'
import { SIGNUP_USER } from './operations'

const handleSubmit = (
  values,
  { props, setSubmitting, setErrors, setValues },
) => {
  if (values.password.length < 8) {
    return
  }
  return props
    .epmc_signupUser({
      variables: {
        input: {
          title: values.title,
          givenNames: values.givenNames,
          surname: values.surname,
          password: values.password,
          email: values.email,
        },
      },
    })
    .then(({ data, errors }) => {
      if (!errors) {
        props
          .epmc_signinUser({
            variables: {
              input: { email: values.email, password: values.password },
            },
          })
          .then(({ data, errors }) => {
            if (!errors) {
              userHelper.setToken(data.epmc_signinUser.token)
              const { search } = props.location
              if (handleSSO(search, data.epmc_signinUser.token)) {
                return
              }
              let path = ''
              if (search && search.includes('?next=')) {
                path = search.split(/=(.+)/)[1]
                  ? search.split(/=(.+)/)[1]
                  : redirectPath({ location: props.location })
              }
              createBrowserHistory({ forceRefresh: true }).push(path)
              setSubmitting(true)
            }
          })
          .catch(e => {
            if (e.graphQLErrors) {
              setSubmitting(false)
              setErrors(e.graphQLErrors[0].message)
              setValues(
                Object.assign(values, {
                  error: e.graphQLErrors[0].message,
                }),
              )
            }
          })
        setSubmitting(true)
      }
    })
    .catch(e => {
      if (e.graphQLErrors) {
        setSubmitting(false)
        setErrors(e.graphQLErrors[0].message)
        setValues(
          Object.assign(values, {
            error: e.graphQLErrors[0].message,
          }),
        )
      }
    })
}

const enhancedFormik = withFormik({
  mapPropsToValues: props => ({
    email: props.email,
    password: props.password,
    title: props.title,
    givenNames: props.givenNames,
    surname: props.surname,
    confirmPassword: props.confirmPassword,
    signup: true,
    location: props.location,
  }),
  validationSchema: yup.object().shape({
    email: yup
      .string()
      .email('Email not valid')
      .required('Email is required'),
    givenNames: yup.string().required('Given names is required'),
    surname: yup.string().required('Surname is required'),
    password: yup.string().required('Password is required'),
  }),
  displayName: 'login',
  handleSubmit,
})(Signup)

export default compose(
  graphql(SIGNUP_USER, { name: 'epmc_signupUser' }),
  graphql(LOGIN_USER, { name: 'epmc_signinUser' }),
)(enhancedFormik)
