import React from 'react'
import { withRouter } from 'react-router-dom'
import config from 'config'
import { H2, H3, Action, Link } from '@pubsweet/ui'
import { Select, Notification, BoldCheck } from '../ui'

const gristUrl = config['grist-api'].url

class AccessSelect extends React.Component {
  state = {
    embargoDisabled: true,
    embargo: this.props.selectedEmbargo ? this.props.selectedEmbargo : '',
    embargoOptions: [
      {
        value: '',
        label: 'Select',
      },
    ],
  }
  async componentDidMount() {
    if (this.props.grants && !this.props.isPlanS) {
      const newState = { embargoDisabled: false }
      const fundersUrl = `${gristUrl}/rest/api/get/funders`
      const response = await fetch(fundersUrl)
      const json = await response.json()
      const { funderInfoList } = json
      const releaseDelays = this.props.grants.map(grant => {
        const { releaseDelay } = funderInfoList.find(
          record => record.name === grant.fundingSource,
        )
        return parseInt(releaseDelay, 10)
      })
      const minReleaseDelay = Math.min(...releaseDelays)
      newState.embargoOptions = [
        {
          value: '',
          label: 'Please select an embargo period',
        },
      ]
      for (let i = 0; i <= minReleaseDelay; i += 1) {
        newState.embargoOptions.push({
          value: i,
          label: `${i}  month${i !== 1 ? 's' : ''}`,
        })
      }
      this.setState(newState)
    }
  }
  selectEmbargo = event => {
    this.props.changedEmbargo(event.target.value)
    this.setState({ embargo: event.target.value })
  }

  updateEmbargoChkBox = event => {
    const value = event.target.checked ? '0' : ''
    this.props.changedEmbargo(value)
  }

  render() {
    const { embargo, embargoDisabled, embargoOptions } = this.state
    const { isPlanS, exempt, funderMailer } = this.props
    const checked = !!(
      this.props.isPlanS && parseInt(this.props.selectedEmbargo, 10) === 0
    )
    return (
      <React.Fragment>
        {isPlanS ? (
          <React.Fragment>
            <div>
              <H2>Access</H2>
              {exempt && (
                <Notification type="success">
                  Your license exception is confirmed
                </Notification>
              )}
              <H3>Licensing and embargo period</H3>
              <p>
                {`You've selected a grant that requires compliance with one or more funder `}
                <Link to="/user-guide#Licensing_policies">
                  licensing and embargo policies
                </Link>
                {`. This means that the manuscript must be made freely available in Europe PMC with no delay. To meet these requirements, the following will be applied:`}
              </p>
              <ul>
                <li>
                  {exempt
                    ? `In accordance with the exception confirmed by your funder, a CC ${exempt
                        .toUpperCase()
                        .replace(
                          /\//g,
                          ' ',
                        )} license will be applied to your manuscript.`
                    : `If the manuscript does not have a CC BY license already, one will be applied by Europe PMC.`}
                </li>
                <li>The manuscript will be given a 0-month embargo period.</li>
              </ul>
              <BoldCheck
                checked={checked}
                label="I accept these requirements."
                onChange={this.updateEmbargoChkBox}
              />
              <p>
                {`If you have questions, `}
                <Action onClick={() => this.props.openMailer(true)}>
                  contact the Helpdesk
                </Action>
                .
                {!exempt && (
                  <React.Fragment>
                    {` If you believe you have a license exception, `}
                    <Action onClick={() => this.props.openMailer(funderMailer)}>
                      contact your funder for confirmation
                    </Action>
                    .
                  </React.Fragment>
                )}
              </p>
            </div>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <div>
              <H2>Access</H2>
              <p>
                The embargo period delays free access to the full text of the
                manuscript, and is measured from the publication date. Europe
                PMC strongly encourages authors to make their manuscripts
                available as soon as possible.
              </p>
              <Select
                disabled={embargoDisabled}
                icon="chevron_down"
                label="Select the embargo period"
                onChange={this.selectEmbargo}
                options={embargoOptions}
                value={embargo}
                width="400px"
              />
            </div>
          </React.Fragment>
        )}
      </React.Fragment>
    )
  }
}

export default withRouter(AccessSelect)
