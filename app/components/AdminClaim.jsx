import React from 'react'
import styled from 'styled-components'
import { graphql, compose } from 'react-apollo'
import { th } from '@pubsweet/ui-toolkit'
import { Action, Icon } from '@pubsweet/ui'
import {
  CLAIM_MANUSCRIPT,
  UNCLAIM_MANUSCRIPT,
  GET_MANUSCRIPT,
} from './operations'

const UnclaimAction = styled(Action)`
  font-weight: 600;
  &.disabled,
  &:disabled {
    opacity: 1;
    color: ${th('colorText')};
    cursor: default;
    &:hover {
      text-decoration: none;
      background-color: transparent;
    }
  }
  & > *:nth-child(even) {
    display: none;
  }
  &.abled:hover {
    & > *:nth-child(even) {
      display: inline-flex;
    }
    & > *:nth-child(odd) {
      display: none;
    }
  }
`

const Claimer = ({
  currentUser,
  claiming,
  manId,
  claimManuscript,
  unclaimManuscript,
}) =>
  claiming ? (
    <UnclaimAction
      className={claiming.id === currentUser.id && 'abled'}
      disabled={claiming.id !== currentUser.id}
      onClick={async () =>
        unclaimManuscript({
          variables: { id: manId },
        })
      }
    >
      <Icon color="currentColor" size={2.5} strokeWidth={2.5}>
        check
      </Icon>
      <Icon color="currentColor" size={2.5} strokeWidth={2.5}>
        x
      </Icon>
      <span>{claiming.givenNames}</span>
      <span>Unclaim</span>
    </UnclaimAction>
  ) : (
    <Action
      onClick={async () =>
        claimManuscript({
          variables: { id: manId },
        })
      }
      style={{ fontWeight: 600 }}
    >
      <Icon color="currentColor" size={2.5} strokeWidth={2.5}>
        plus
      </Icon>
      Claim
    </Action>
  )

export default compose(
  graphql(CLAIM_MANUSCRIPT, {
    name: 'claimManuscript',
    options: props => ({
      refetchQueries: [
        { query: GET_MANUSCRIPT, variables: { id: props.manId } },
      ],
    }),
  }),
  graphql(UNCLAIM_MANUSCRIPT, {
    name: 'unclaimManuscript',
    options: props => ({
      refetchQueries: [
        { query: GET_MANUSCRIPT, variables: { id: props.manId } },
      ],
    }),
  }),
)(Claimer)
