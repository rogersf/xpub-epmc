import React from 'react'
import { H1, Link, Button, Checkbox } from '@pubsweet/ui'
import { withApollo } from 'react-apollo'
import gql from 'graphql-tag'

import { Page } from './ui'

const UPDATE_CURRENT_USER = gql`
  mutation($input: UpdateCurrentUserInput) {
    epmc_updateCurrentUser(input: $input) {
      id
      identities {
        type
        email
        name {
          title
          givenNames
          surname
        }
      }
      privacyNoticeReviewRequired
    }
  }
`
class PrivacyNoticeChange extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      accept: false,
      currentUser: props.currentUser,
    }
  }
  setButtonRef = button => {
    this.button = button
  }
  toggleAccept = () => {
    const { accept, currentUser } = this.state
    this.setState({
      accept: !accept,
      currentUser,
    })
  }

  onSubmitAccept = () => {
    const [currentIdentity] = this.state.currentUser.identities.filter(
      identity => identity.type === 'local',
    )
    const options = {
      mutation: UPDATE_CURRENT_USER,
      variables: {
        input: {
          currentPassword: '',
          email: currentIdentity.email,
          givenNames: currentIdentity.name.givenNames,
          newPassword: '',
          surname: currentIdentity.name.surname,
          title: currentIdentity.name.title ? currentIdentity.name.title : '',
          privacyNoticeReviewRequired: !this.state.currentUser
            .privacyNoticeReviewRequired,
        },
      },
    }
    this.props.client.mutate(options).then(response => response)
  }
  render() {
    return (
      <Page>
        <H1>Changes to our Privacy Notices</H1>
        <p>
          We have improved our privacy notices to be more transparent about what
          personal data we collect from you, and how we protect your data and
          privacy.
        </p>
        <p>
          {`To continue using your account, please read and accept our `}
          <Link
            target="_blank"
            to="//www.ebi.ac.uk/data-protection/privacy-notice/europe-pmc-plus"
          >
            privacy notice
          </Link>
          {` for Europe PMC’s Advanced User Services.`}
        </p>
        <p>
          <Checkbox
            checked={this.state.accept}
            label="I have read and accept the privacy notice."
            onChange={() => this.toggleAccept()}
          />
        </p>
        <p>
          <Button
            disabled={!this.state.accept}
            onClick={e => this.onSubmitAccept(e)}
            primary
            ref={this.setButtonRef}
          >
            Continue
          </Button>
        </p>
      </Page>
    )
  }
}

export default withApollo(PrivacyNoticeChange)
