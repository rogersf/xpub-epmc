import React from 'react'
import { Field } from 'formik'
import { ErrorText, TextField } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import styled from 'styled-components'

const Inputs = styled.div`
  display: flex;
  align-items: flex-start;
  flex-wrap: wrap;
  margin: 0 calc(${th('gridUnit')} * -1.5);
  & > * {
    box-sizing: border-box;
    margin: 0 calc(${th('gridUnit')} * 1.5);
    width: 408px;
    max-width: 100%;
    &:first-child {
      max-width: 104px;
      min-width: calc(7 * ${th('gridUnit')});
      & + * {
        width: 280px;
      }
    }
  }
`
const TitleInput = props => (
  <TextField label="Title (optional)" {...props.field} />
)

const GivenNamesInput = props => (
  <TextField
    invalidTest={props.invalidTest}
    label="Given name(s) or initials"
    {...props.field}
  />
)
const SurnameInput = props => (
  <TextField invalidTest={props.invalidTest} label="Surname" {...props.field} />
)
const EmailInput = props => (
  <TextField
    invalidTest={props.invalidTest}
    label="Email address"
    {...props.field}
  />
)

const UserCore = ({ values, errors, handleSubmit, touched, children }) => (
  <Inputs>
    <Field component={TitleInput} name="title" />
    <div>
      <Field
        component={GivenNamesInput}
        invalidTest={errors.givenNames && touched.givenNames}
        name="givenNames"
      />
      {errors.givenNames && touched.givenNames && (
        <ErrorText>{errors.givenNames}</ErrorText>
      )}
    </div>
    <div>
      <Field
        component={SurnameInput}
        invalidTest={errors.surname && touched.surname}
        name="surname"
      />
      {errors.surname && touched.surname && (
        <ErrorText>{errors.surname}</ErrorText>
      )}
    </div>
    <div>
      <Field
        component={EmailInput}
        invalidTest={errors.email && touched.email}
        name="email"
      />
      {errors.email && touched.email && <ErrorText>{errors.email}</ErrorText>}
    </div>
    {children}
  </Inputs>
)

export default UserCore
