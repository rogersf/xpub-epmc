import gql from 'graphql-tag'

export const UPDATE_USER = gql`
  mutation($input: UpdateUserInput) {
    epmc_updateUser(input: $input) {
      id
      identities {
        type
        email
        name {
          title
          givenNames
          surname
        }
        meta {
          publisher
          ftpUserName
        }
      }
      teams
    }
  }
`

export const MERGE_USER = gql`
  mutation($input: MergeRequest) {
    mergeUser(input: $input)
  }
`

export const GET_USER = gql`
  query($id: ID!) {
    epmc_user(id: $id) {
      id
      identities {
        type
        email
        name {
          title
          givenNames
          surname
        }
        meta {
          publisher
          ftpUserName
        }
      }
      teams
    }
  }
`
export const GET_ROLES_BY_TYPE = gql`
  query($organization: Boolean!) {
    rolesByType(organization: $organization)
  }
`
