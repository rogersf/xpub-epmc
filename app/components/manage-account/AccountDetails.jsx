import React from 'react'
import { H1, Link, Button, Icon, Checkbox, TextField } from '@pubsweet/ui'
import { Field } from 'formik'
import { th } from '@pubsweet/ui-toolkit'
import styled from 'styled-components'
import { Notification, ConfirmDialog, Select, A } from '../ui'
import UserCore from '../UserCore'

const NO_PRIVILEGE = 'No Privilege'

const Publisher = styled.div`
  display: flex;
  align-items: flex-start;
  & > label {
    height: calc(${th('gridUnit')} * 9);
    margin-right: calc(${th('gridUnit')} * 3) !important;
  }
  div div {
    margin-bottom: 0;
  }
  p {
    margin-top: 0;
    font-size: ${th('fontSizeBaseSmall')};
  }
`

const PublisherName = props => (
  <TextField
    label="Publisher username (required for publisher)"
    style={{ width: '400px' }}
    {...props.field}
  />
)
const FtpUserName = props => (
  <TextField
    label="FTP User name (for bulk uploading)"
    style={{ width: '400px' }}
    {...props.field}
  />
)

class AccountDetails extends React.Component {
  state = {
    isOpen: false,
    teamOptions: [],
    team: '',
    publisher: !!this.props.values.publisher,
  }

  toggleReset = () => {
    const { isOpen } = this.state
    this.setState({
      isOpen: !isOpen,
    })
  }

  togglePublisher = () => {
    const { publisher } = this.state
    if (publisher) {
      this.props.values.publisher = ''
    }
    this.setState({ publisher: !publisher })
  }

  render() {
    const { publisher } = this.state
    const { values, handleSubmit } = this.props
    // handling letter casing in admin user management
    if (values.manageAccount) {
      this.state.teamOptions = values.teamOptions
      this.state.team =
        values.team.charAt(0).toUpperCase() + values.team.slice(1)
    }
    const action = async input => {
      if (input) {
        values.success = await values.resetPassword(this.props)
      }
      this.toggleReset()
    }
    const selectTeam = e => {
      values.team =
        e.target.value !== NO_PRIVILEGE
          ? e.target.value.charAt(0).toLowerCase() + e.target.value.slice(1)
          : e.target.value
      this.setState({
        team: values.team,
      })
    }
    return (
      <React.Fragment>
        {values.signup && <H1>Create a Europe PMC plus account</H1>}
        {values.myAccount && <H1>My Account</H1>}

        {values.success && (
          <Notification type="success"> {values.success} </Notification>
        )}
        {values.error && (
          <Notification type="error">
            Error occured, please try again.
          </Notification>
        )}
        <form onSubmit={handleSubmit}>
          <UserCore {...this.props} />
          <Select
            icon="chevron_down"
            label="Privilege level"
            onChange={selectTeam}
            options={this.state.teamOptions}
            value={this.state.team}
            width="400px"
          />
          <Publisher>
            <Checkbox
              checked={publisher}
              label="Publisher"
              onChange={() => this.togglePublisher()}
            />
            {publisher && (
              <div>
                <Field component={PublisherName} name="publisher" />
                <p>
                  <A
                    href="https://www.ncbi.nlm.nih.gov/pmc/utils/nihms/db/report_publs.cgi?system=ukmss&auth=publisher"
                    target="_blank"
                  >
                    Check the list
                  </A>{' '}
                  for expected ukmss publisher usernames
                </p>
                <Field component={FtpUserName} name="ftpUserName" />
                <p>Be careful changing this; it could break FTP access!</p>
              </div>
            )}
          </Publisher>
          <Button primary type="submit">
            Confirm changes
          </Button>
        </form>
        <div className="reset-pass">
          <Icon color="currentColor" size={2}>
            rotate-ccw
          </Icon>
          <Link onClick={() => this.toggleReset()} to="#">
            Reset password
          </Link>
          <ConfirmDialog
            action={action}
            isOpen={this.state.isOpen}
            message="Are you sure you want to reset password?"
          />
        </div>
      </React.Fragment>
    )
  }
}

export { NO_PRIVILEGE, AccountDetails }
