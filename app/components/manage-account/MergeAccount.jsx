import React from 'react'
import { withApollo } from 'react-apollo'
import { ConfirmDialog, Notification } from '../ui/'
import UserIdSearch from '../UserIdSearch'
import { MERGE_USER } from './operations'

class MergeAccount extends React.Component {
  state = {
    isOpen: false,
    mergeId: null,
    success: null,
    error: null,
  }
  mergeAccount = input => {
    const { isOpen } = this.state
    if (!input) {
      this.setState({ isOpen: !isOpen })
      return
    }
    this.setState({
      success: '',
      error: '',
    })
    const options = {
      mutation: MERGE_USER,
      variables: {
        input: {
          from: this.state.mergeId,
          to: this.props.user.id,
        },
      },
    }
    this.props.client
      .mutate(options)
      .then(response => {
        if (response.data.mergeUser) {
          this.setState({
            success: 'Account has been merged successfully!',
            isOpen: !isOpen,
          })
        } else {
          this.setState({
            error: 'Error occured. please try again',
            isOpen: !isOpen,
          })
        }
      })
      .catch(e => {
        this.setState({
          error: 'Error occured. please try again',
          isOpen: !isOpen,
        })
      })
  }

  render() {
    const { isOpen, success, error } = this.state
    const merge = async input => {
      this.mergeAccount(input)
    }
    return (
      <div>
        {success && <Notification type="success">{success}</Notification>}
        {error && <Notification type="error">{error}</Notification>}
        <UserIdSearch
          success={v => this.setState({ mergeId: v.id, isOpen: !isOpen })}
          successLabel="Merge"
        />
        <ConfirmDialog
          action={merge}
          isOpen={isOpen}
          message="Once the content is merged, the other account will be deactivated. Are you sure you want to merge?"
        />
      </div>
    )
  }
}
export default withApollo(MergeAccount)
