import React from 'react'
import { Wax } from 'wax-prose-mirror'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import PDFViewer from '../component-pdf-viewer'
import { Notification, Loading, LoadingIcon, A } from './ui'

const Doc = styled.div`
  font-family: ${th('fontReading')};
  padding: calc(${th('gridUnit')} * 2);
`
class ManuscriptPreview extends React.Component {
  state = { hasError: false, html: '' }
  async componentDidMount() {
    if (this.props.file.type === 'source') {
      const response = await fetch(this.props.file.url)
      const html = await response.text()
      if (this.refdiv) {
        this.setState({ html })
      }
    }
  }
  componentDidUpdate() {
    const warning = `<span style="background-color: #fff2cb; font-size:.8em; padding: 2px 6px; margin-left: 4px;">Image file type cannot be displayed in web browser</span>`
    const emfs = document.querySelectorAll('img[src^="data:image/emf"]')
    Array.from(emfs).map(emf => emf.insertAdjacentHTML('afterend', warning))
  }
  componentDidCatch(error, info) {
    if (error) {
      this.setState({ hasError: true })
    }
  }
  setRef = refdiv => {
    this.refdiv = refdiv
  }
  render() {
    const { file, original } = this.props
    if (!file || this.state.hasError) {
      return (
        <Notification type="error">
          Error: Unable to generate manuscript preview.
        </Notification>
      )
    }
    if (file.type === 'source') {
      return (
        <React.Fragment>
          <Notification type="warning">
            {`The preview is intended for the review of content only. If it is not formatted correctly, you can check formatting by `}
            <A
              download={original.filename}
              href={original.url}
              title="Download the manuscript"
            >
              downloading the original source file
            </A>
            {'.'}
          </Notification>
          <Doc id={this.state.html && 'html-preview'} ref={this.setRef}>
            {this.state.html ? (
              <Wax
                readonly
                renderLayout={({ editor, ...props }) => <div>{editor}</div>}
                value={this.state.html}
              />
            ) : (
              <Loading>
                <LoadingIcon />
              </Loading>
            )}
          </Doc>
        </React.Fragment>
      )
    } else if (file.mimeType === 'application/pdf') {
      return (
        <PDFViewer
          maxPages={50}
          textContent={this.props.textContent}
          url={file.url}
        />
      )
    }
    return (
      <Notification type="warning">
        {`Unable to generate manuscript preview from ${file.filename}. `}
        {file.mimeType === 'application/pdf' ||
        file.mimeType ===
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ? (
          <React.Fragment>
            {`Please ensure your file is uncorrupted and check content and formatting by `}
          </React.Fragment>
        ) : (
          <React.Fragment>
            {`Previews can only be generated from files with the extensions `}
            <em>.docx</em>
            {` and `}
            <em>.pdf</em>
            {` Please ensure your file is uncorrupted and check content and formatting by `}
          </React.Fragment>
        )}
        <A
          download={file.filename}
          href={file.url}
          title="Download the manuscript"
        >
          downloading the original source file
        </A>
        {'.'}
      </Notification>
    )
  }
}

export default ManuscriptPreview
