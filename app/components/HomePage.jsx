import React from 'react'
import styled, { withTheme } from 'styled-components'
import { th, override } from '@pubsweet/ui-toolkit'
import { Link, H1, H2, H3, Icon } from '@pubsweet/ui'
import { europepmcUrl } from 'config'
import { Page, A, Callout } from './ui'

const List = styled.ul`
  list-style-type: none;
  padding-left: 0;

  li {
    margin: ${th('gridUnit')} auto;
    display: flex;
    align-items: center;
    font-size: ${th('fontSizeHeading4')};

    & > span {
      margin-right: ${th('gridUnit')};
    }
  }
`
const LinkButton = styled(Link)`
  display: inline-block;
  margin: ${th('gridUnit')} auto;
  &:link,
  &:visited,
  &:hover {
    color: ${th('colorTextReverse')};
    text-decoration: none;
  }
  border-radius: 2px;
  ${override('ui.Button')};
`

const Check = withTheme(({ theme }) => (
  <Icon color={theme.colorFurniture} size={4} strokeWidth={5}>
    check
  </Icon>
))

const HomePage = () => (
  <Page>
    <H1>Submit your manuscript</H1>
    <p>
      {`If your research is funded by a `}
      <A href={`${europepmcUrl}/Funders/`} target="_blank">
        Europe PMC funder
      </A>
      {`, you are expected to make it freely available in Europe PMC. The Europe PMC plus submission system assists with Europe PMC funder mandates by enabling direct deposit of research. After deposit, the manuscript will be made available in Europe PMC, and also in PubMed Central (PMC).`}
    </p>
    <p>
      You can also use Europe PMC plus to link funding to a publication already
      available in Europe PMC.
    </p>
    <H2>Criteria for submission</H2>
    <List>
      <li>
        <Check /> The manuscript has been accepted for publication by a journal.
      </li>
      <li>
        <Check />
        <span>
          The manuscript results from research funded by a{' '}
          <A href={`${europepmcUrl}/Funders/`} target="_blank">
            Europe PMC funder
          </A>
          .
        </span>
      </li>
    </List>
    <LinkButton primary="true" to="/login">
      Login to start a submission
    </LinkButton>
    <H3>Manuscripts that do not meet the submission criteria</H3>
    <p>
      {`Europe PMC is an archive of biomedical literature published or posted elsewhere. Its submission system is limited to manuscripts that meet the criteria above. If you have questions about whether your manuscript can be submitted, email `}
      <A href="mailto:helpdesk@europepmc.org">helpdesk@europepmc.org</A>.
    </p>
    <Callout>
      <H3>Approving preprints</H3>
      <p>
        {`Europe PMC seeks to make the full text, not just abstracts, of preprints supported by Europe PMC funders and COVID-19 related preprints searchable and available for deeper analysis. These preprints will be pulled directly from preprint servers. The full text for preprints which are not supported by Europe PMC funders, or COVID-related, is usually not displayed in Europe PMC.`}
      </p>
    </Callout>
  </Page>
)

export default HomePage
