import React from 'react'
import { withRouter } from 'react-router'
import { Query } from 'react-apollo'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { H1 } from '@pubsweet/ui'
import { Page, Loading, LoadingIcon } from '../ui'
import { QUERY_ACTIVITY_INFO } from './operations'
import SubmissionHeader from '../SubmissionHeader'
import ActivityDetails from './ActivityDetails'
import QuickView from './QuickView'
import VersionList from './VersionList'

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: baseline;
  margin-bottom: calc(${th('gridUnit')} * 2);
  h1 {
    margin-bottom: 0;
  }
`

const ActivityPage = props => {
  const { meta } = props.manuscript
  const { notes } = meta
  let emsids = null
  if (notes) {
    const versionNote = notes.find(n => n.notesType === 'versionList')
    const versions = versionNote && JSON.parse(versionNote.content)
    emsids = versions && versions.emsid
    if (emsids) emsids.push(props.manuscript.id)
  }
  return (
    <Page withHeader>
      <Header>
        <H1>Manuscript activity</H1>
        {emsids && (
          <VersionList current={props.manuscript.id} versions={emsids} />
        )}
      </Header>
      <QuickView manuscript={props.manuscript} />
      <ActivityDetails {...props} />
    </Page>
  )
}

const ActivityPageWithHeader = SubmissionHeader(ActivityPage)

const ActivityPageContainer = ({ currentUser, history, match, ...props }) => (
  <Query
    fetchPolicy="cache-and-network"
    query={QUERY_ACTIVITY_INFO}
    variables={{ id: match.params.id }}
  >
    {({ data, loading, refetch }) => {
      if (loading) {
        return (
          <Loading>
            <LoadingIcon />
          </Loading>
        )
      }
      if (!data) {
        history.go()
        return null
      }
      if (currentUser.admin) {
        return (
          <ActivityPageWithHeader
            currentUser={currentUser}
            manuscript={data.activities}
            refetchActivityInfo={refetch}
            saved={new Date()}
            {...props}
          />
        )
      }
      history.push('/')
      return null
    }}
  </Query>
)

export default withRouter(ActivityPageContainer)
