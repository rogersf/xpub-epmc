import React from 'react'
import { Field } from 'formik'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Button, TextField, Action, H3, H4, ErrorText } from '@pubsweet/ui'
import { Buttons, Toggle } from '../ui'

const Flex = styled.div`
  display: flex;
  align-items: flex-end;
  min-width: 0;
  .hidden {
    display: none;
  }
  & > div {
    flex: 1 1 50%;
    min-width: 0;
    margin-left: ${th('gridUnit')};
    margin-right: ${th('gridUnit')};
    &:first-child {
      margin-left: 0;
      & ~ * {
        margin-right: 0;
        & + * {
          margin-left: 0;
        }
      }
    }
  }
`
const SmallToggle = styled(Toggle)`
  button {
    font-size: ${th('fontSizeBaseSmall')};
  }
  margin-bottom: ${th('gridUnit')};
`

const Title = props => (
  <TextField invalidTest={props.invalidTest} label="Title" {...props.field} />
)
const Volume = props => <TextField label="Volume" {...props.field} />
const Issue = props => <TextField label="Issue" {...props.field} />
const FPage = props => (
  <TextField className={props.className} label="First page" {...props.field} />
)
const LPage = props => (
  <TextField className={props.className} label="Last page" {...props.field} />
)
const ELocation = props => (
  <TextField className={props.className} label="eLocation" {...props.field} />
)
const DOI = props => (
  <TextField invalidTest={props.invalidTest} label="DOI" {...props.field} />
)
const PYear = props => (
  <TextField invalidTest={props.invalidTest} label="Year" {...props.field} />
)
const PMonth = props => (
  <TextField invalidTest={props.invalidTest} label="Month" {...props.field} />
)
const PDay = props => (
  <TextField invalidTest={props.invalidTest} label="Day" {...props.field} />
)
const PSeason = props => (
  <TextField className={props.className} label="Season" {...props.field} />
)
const EYear = props => (
  <TextField invalidTest={props.invalidTest} label="Year" {...props.field} />
)
const EMonth = props => (
  <TextField invalidTest={props.invalidTest} label="Month" {...props.field} />
)
const EDay = props => (
  <TextField invalidTest={props.invalidTest} label="Day" {...props.field} />
)
const ESeason = props => (
  <TextField className={props.className} label="Season" {...props.field} />
)
const CiteUrl = props => (
  <TextField
    className={props.className}
    invalidTest={props.invalidTest}
    label="Citation URL (must enter to send manual citation to NCBI)"
    {...props.field}
  />
)
const FullUrl = props => (
  <TextField
    className={props.className}
    invalidTest={props.invalidTest}
    label="Full text URL (include if full text is open access)"
    {...props.field}
  />
)

const toggleSwitch = button => {
  if (!button.classList.contains('current')) {
    const buttons = Array.from(button.parentNode.children)
    buttons.forEach(element => {
      element.classList.toggle('current')
    })
    const things = Array.from(button.parentNode.nextElementSibling.children)
    things.forEach(element => {
      element.classList.toggle('hidden')
    })
  }
}

const CitationForm = ({ values, errors, touched, ...props }) => (
  <form onSubmit={props.handleSubmit}>
    <Flex>
      <div>
        <H3>Enter citation</H3>
        <Flex>
          <Field component={Volume} name="volume" />
          <Field component={Issue} name="issue" />
        </Flex>
      </div>
      <div>
        <SmallToggle>
          <Action
            className={(!values.elocationId || values.fpage) && 'current'}
            onClick={e => toggleSwitch(e.currentTarget)}
          >
            Pages
          </Action>
          <Action
            className={!values.fpage && values.elocationId && 'current'}
            onClick={e => toggleSwitch(e.currentTarget)}
          >
            eLocation
          </Action>
        </SmallToggle>
        <Flex>
          <Field
            className={
              !values.fpage && values.elocationId ? 'hidden' : undefined
            }
            component={FPage}
            name="fpage"
          />
          <Field
            className={
              !values.fpage && values.elocationId ? 'hidden' : undefined
            }
            component={LPage}
            name="lpage"
          />
          <Field
            className={
              !values.elocationId || values.fpage ? 'hidden' : undefined
            }
            component={ELocation}
            name="elocationId"
          />
        </Flex>
      </div>
    </Flex>
    <div>
      <Field
        component={Title}
        invalidTest={errors.title && touched.title}
        name="title"
      />
      {errors.title && touched.title && <ErrorText>{errors.title}</ErrorText>}
    </div>
    <div>
      <Field
        component={DOI}
        invalidTest={errors.doi && touched.doi}
        name="doi"
      />
      {errors.doi && touched.doi && <ErrorText>{errors.doi}</ErrorText>}
    </div>
    <Flex>
      <div>
        <H4>Print date (Issue date)</H4>
        <Field
          component={PYear}
          invalidTest={errors.printyear && touched.printyear}
          name="printyear"
        />
        {errors.printyear && touched.printyear && (
          <ErrorText>{errors.printyear}</ErrorText>
        )}
      </div>
      <div>
        <SmallToggle>
          <Action
            className={!values.printseason && 'current'}
            onClick={e => toggleSwitch(e.currentTarget)}
          >
            Month
          </Action>
          <Action
            className={values.printseason && 'current'}
            onClick={e => toggleSwitch(e.currentTarget)}
          >
            Season
          </Action>
        </SmallToggle>
        <Flex>
          <div className={values.printseason && 'hidden'}>
            <Field
              component={PMonth}
              invalidTest={errors.printmonth && touched.printmonth}
              name="printmonth"
            />
            {errors.printmonth && touched.printmonth && (
              <ErrorText>{errors.printmonth}</ErrorText>
            )}
          </div>
          <div className={values.printseason && 'hidden'}>
            <Field
              component={PDay}
              invalidTest={errors.printday && touched.printday}
              name="printday"
            />
            {errors.printday && touched.printday && (
              <ErrorText>{errors.printday}</ErrorText>
            )}
          </div>
          <Field
            className={!values.printseason ? 'hidden' : undefined}
            component={PSeason}
            name="printseason"
          />
        </Flex>
      </div>
    </Flex>
    <Flex>
      <div>
        <H4>Electronic date</H4>
        <Field
          component={EYear}
          invalidTest={errors.electronicyear && touched.electronicyear}
          name="electronicyear"
        />
        {errors.electronicyear && touched.electronicyear && (
          <ErrorText>{errors.electronicyear}</ErrorText>
        )}
      </div>
      <div>
        <SmallToggle>
          <Action
            className={!values.electronicseason && 'current'}
            onClick={e => toggleSwitch(e.currentTarget)}
          >
            Month
          </Action>
          <Action
            className={values.electronicseason && 'current'}
            onClick={e => toggleSwitch(e.currentTarget)}
          >
            Season
          </Action>
        </SmallToggle>
        <Flex>
          <div className={values.electronicseason && 'hidden'}>
            <Field
              component={EMonth}
              invalidTest={errors.electronicmonth && touched.electronicmonth}
              name="electronicmonth"
            />
            {errors.electronicmonth && touched.electronicmonth && (
              <ErrorText>{errors.electronicmonth}</ErrorText>
            )}
          </div>
          <div className={values.electronicseason && 'hidden'}>
            <Field
              component={EDay}
              invalidTest={errors.electronicday && touched.electronicday}
              name="electronicday"
            />
            {errors.electronicday && touched.electronicday && (
              <ErrorText>{errors.electronicday}</ErrorText>
            )}
          </div>
          <Field
            className={!values.electronicseason ? 'hidden' : undefined}
            component={ESeason}
            name="electronicseason"
          />
        </Flex>
      </div>
    </Flex>
    <H4>URLs</H4>
    <Field
      component={CiteUrl}
      invalidTest={errors.citeref && touched.citeref}
      name="citeref"
    />
    {errors.citeref && touched.citeref && (
      <ErrorText>{errors.citeref}</ErrorText>
    )}
    <Field
      component={FullUrl}
      invalidTest={errors.fulltext && touched.fulltext}
      name="fulltext"
    />
    {errors.fulltext && touched.fulltext && (
      <ErrorText>{errors.fulltext}</ErrorText>
    )}
    <Buttons right>
      <Button primary type="submit">
        Save
      </Button>
      <Button onClick={props.close}>Exit</Button>
    </Buttons>
  </form>
)

export default CitationForm
