import gql from 'graphql-tag'
import { ManuscriptFragment } from '../operations'

export const GET_USER = gql`
  query GetUser($id: ID!) {
    user(id: $id) {
      id
      identities {
        ... on Local {
          name {
            givenNames
            surname
          }
        }
      }
    }
  }
`

export const GET_JOURNAL = gql`
  query($id: ID!) {
    journal(id: $id) {
      id
      journalTitle
    }
  }
`

export const GET_VERSION = gql`
  query($query: [ID!]) {
    getManuscriptVersions(query: $query) {
      manuscripts {
        id
        version
      }
    }
  }
`

export const EDIT_IDENTIFIERS = gql`
  mutation EditIdentifiers(
    $data: ManuscriptInput!
    $repoId: String!
    $replace: Boolean!
  ) {
    replaceManuscriptIds(data: $data, repoId: $repoId, replace: $replace)
  }
`

export const LINK_EXISTING = gql`
  mutation LinkExisting($id: ID!) {
    linkExisting(id: $id) {
      success
      message
    }
  }
`

export const INVITE_REVIEWER = gql`
  mutation SetNewReviewer($id: ID!) {
    setManuscriptReviewer(id: $id)
  }
`

export const EXCEPTION_ALERT = gql`
  mutation EmailException($manuscriptId: ID!, $license: String) {
    epmc_emailException(manuscriptId: $manuscriptId, license: $license)
  }
`

export const QUERY_ACTIVITY_INFO = gql`
  query ManuscriptActivityLog($id: ID!) {
    activities: manuscriptActivityLog(id: $id) {
      ...ManuscriptFragment
      deleted
      ebiState
      fundingState
      meta {
        volume
        issue
        location {
          fpage
          lpage
          elocationId
        }
        citerefUrl
        fulltextUrl
      }
      audits {
        id
        created
        updated
        user {
          id
          title
          givenNames
          surname
          identities {
            email
          }
          deleted
        }
        action
        originalData
        changes
        objectType
        objectId
        manuscriptId
        manuscriptVersion
      }
    }
  }
  ${ManuscriptFragment}
`
