import React from 'react'
import { Button } from '@pubsweet/ui'
import { graphql, compose } from 'react-apollo'
import { states } from 'config'
import { Buttons } from '../ui'
import SelectReviewer from '../SelectReviewer'
import { INVITE_REVIEWER, QUERY_ACTIVITY_INFO } from './operations'

class ReviewerEdit extends React.Component {
  state = { newReviewer: null }
  render() {
    const { newReviewer } = this.state
    const {
      currentUser,
      manuscript,
      close,
      changeNote,
      deleteNote,
      newNote,
      inviteReviewer,
    } = this.props
    const { teams, meta, status } = manuscript
    const { fundingGroup, notes } = meta
    const tagged = states.indexOf(status) >= states.indexOf('tagging')
    const reviewerNote = notes
      ? notes.find(n => n.notesType === 'selectedReviewer')
      : null
    let reviewer = null
    if (teams && teams.find(team => team.role === 'reviewer')) {
      const [rev] = teams.find(team => team.role === 'reviewer').teamMembers
      reviewer = {
        id: rev.user.id,
        name: rev.alias.name,
      }
    }
    const { teamMembers } =
      (teams && teams.find(team => team.role === 'submitter')) || {}
    const [submitter = null] = teamMembers || []
    return (
      <React.Fragment>
        <SelectReviewer
          currentUser={currentUser}
          funding={fundingGroup || []}
          reviewer={reviewer}
          reviewerNote={reviewerNote}
          setReviewerNote={v => this.setState({ newReviewer: v })}
          submitter={submitter}
        />
        <Buttons right>
          <Button
            disabled={!newReviewer}
            onClick={async () => {
              let result = false
              if (reviewerNote && newReviewer) {
                if (newReviewer === 'delete') {
                  result = await deleteNote(reviewerNote.id)
                } else {
                  result = await changeNote({
                    id: reviewerNote.id,
                    notesType: 'selectedReviewer',
                    content: JSON.stringify(newReviewer),
                  })
                }
              } else {
                result = await newNote({
                  notesType: 'selectedReviewer',
                  content: JSON.stringify(newReviewer),
                })
              }
              if (result) {
                if (tagged && newReviewer !== 'delete') {
                  const success = await inviteReviewer({
                    variables: { id: manuscript.id },
                  })
                  if (success) {
                    close()
                  }
                } else {
                  close()
                }
              }
            }}
            primary
          >
            Save{tagged && newReviewer !== 'delete' && ' & send invitation'}
          </Button>
          <Button onClick={close}>Exit</Button>
        </Buttons>
      </React.Fragment>
    )
  }
}

export default compose(
  graphql(INVITE_REVIEWER, {
    name: 'inviteReviewer',
    options: props => ({
      refetchQueries: [
        { query: QUERY_ACTIVITY_INFO, variables: { id: props.manuscript.id } },
      ],
    }),
  }),
)(ReviewerEdit)
