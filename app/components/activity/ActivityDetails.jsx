import React from 'react'
import { Mutation } from 'react-apollo'
import { H2, H3, Button, Action, Link, Spinner } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import styled from 'styled-components'
import moment from 'moment'
import { Buttons, CloseModal, Table, Portal, TextArea, Toggle } from '../ui'
import Mailer from '../mailer'
import { CREATE_NOTE } from '../operations'
import EventDescription from './EventDescription'
import { QUERY_ACTIVITY_INFO } from './operations'

const DetailsTable = styled(Table)`
  width: 100%;
  @media screen and (max-width: 600px) {
    th {
      display: none;
    }
    tr {
      border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
    }
  }
`
const TD = styled.td`
  font-size: ${th('fontSizeBaseSmall')};
  word-break: break-word;
  @media screen and (max-width: 600px) {
    display: inline-block;
    width: 100%;
    border: 0 !important;
  }
`
const TdDate = styled(TD)`
  vertical-align: top;
  width: 1%;
  white-space: nowrap;
  @media screen and (max-width: 600px) {
    width: 50%;
    white-space: normal;
  }
`
const TdPerson = TdDate

const NewMsgBtn = styled(Button)`
  padding: calc(${th('gridUnit')} / 2) ${th('gridUnit')};
  margin-left: ${th('gridUnit')};
  font-size: ${th('fontSizeBaseSmall')};
`
const DetailsHeading = styled.div`
  display: flex;
  align-items: baseline;
  justify-content: space-between;
`
const DetailsToggle = styled.div`
  display: flex;
  align-items: baseline;
  div {
    margin-left: calc(${th('gridUnit')} * 8);
  }
  @media screen and (max-width: 680px) {
    flex: 2;
    display block;
    div {
      margin-left: 0;
      margin-bottom: ${th('gridUnit')};
    }
  }
`
const VersionToggle = styled.div`
  display: flex;
  align-items: baseline;
  div {
    margin-left: calc(${th('gridUnit')} * 4);
    button + button {
      margin-left: calc(${th('gridUnit')} * 2);
    }
  }
`
const VersionHeading = styled(H3)`
  margin-top: 0;
  font-size: ${th('fontSizeHeading4')};
`

const Loading = () => <Spinner size={2} />

const formatName = name =>
  `${name.title ? `${name.title} ` : ''}${name.givenNames} ${name.surname}`

const NewNote = ({
  close,
  manuscriptId,
  manuscriptVersion,
  message,
  onChange,
}) => (
  <Mutation
    mutation={CREATE_NOTE}
    refetchQueries={() => [
      {
        query: QUERY_ACTIVITY_INFO,
        variables: { id: manuscriptId },
      },
    ]}
  >
    {(createNote, { data }) => {
      const newNote = async () => {
        await createNote({
          variables: {
            data: {
              manuscriptId,
              manuscriptVersion,
              notesType: 'userMessage',
              content: JSON.stringify(message),
            },
          },
        })
        onChange('')
        close()
      }
      return (
        <Portal transparent>
          <CloseModal onClick={() => close()} />
          <H2>Add a note</H2>
          <TextArea
            label="Note (visible only to administrators)"
            onChange={e => onChange(e.target.value)}
            value={message}
          />
          <Buttons right>
            <Button disabled={!message} onClick={() => newNote()} primary>
              Save
            </Button>
            <Button onClick={() => close()}>Cancel</Button>
          </Buttons>
        </Portal>
      )
    }}
  </Mutation>
)

const ActivityList = ({ audits, manuscript }) =>
  audits.map(audit => (
    <tr key={audit.id}>
      <TdDate>{moment(audit.created).format('DD/MM/YYYY HH:mm')}</TdDate>
      <TdPerson>
        {audit.user && (
          <React.Fragment>
            {audit.user.deleted ? (
              `${audit.user.givenNames} ${audit.user.surname}`
            ) : (
              <Link to={`/manage-account/${audit.user.id}`}>
                {`${audit.user.givenNames} ${audit.user.surname}`}
              </Link>
            )}
          </React.Fragment>
        )}
      </TdPerson>
      <TD>
        <EventDescription audit={audit} manuscript={manuscript} />
      </TD>
    </tr>
  ))

class ActivityDetails extends React.Component {
  state = {
    sendingMail: false,
    newNote: false,
    message: '',
    audits: [],
    versions: [],
    showAudits: 'all',
    showVersion: 'all',
    resendToRepoLoading: false,
  }
  componentDidMount() {
    this.changeAudits('all', 'all')
  }
  componentDidUpdate(prevProps) {
    if (
      this.props.manuscript.audits.length > prevProps.manuscript.audits.length
    ) {
      this.changeAudits(this.state.showAudits, this.state.showVersion)
    }
    if (this.state.versions.length === 0) {
      this.getVersions()
    }
  }
  getVersions = () => {
    const { audits } = this.state
    const versions = [
      ...new Set(audits.map(audit => audit.manuscriptVersion)),
    ].sort((a, b) => a - b)
    this.setState({ versions })
  }
  changeAudits = (showAudits, showVersion) => {
    let audits = this.props.manuscript.audits.slice().reverse()
    if (showAudits !== 'all') {
      audits = audits.filter(a => a.objectType === showAudits)
    }
    if (showVersion !== 'all') {
      audits = audits.filter(a => a.manuscriptVersion === showVersion)
    }
    this.setState({ audits, showVersion, showAudits })
  }

  resendToRepo = () => {
    const that = this
    const { manuscript, currentUser, refetchActivityInfo } = this.props
    const resendUrl = `/api/resend-to-repo/${manuscript.id}?userId=${currentUser.id}`

    const headers = new Headers({
      Authorization: `Bearer ${window.localStorage.getItem('token')}`,
      'Content-Type': 'application/json',
    })

    this.setState({
      resendToRepoLoading: true,
    })

    fetch(resendUrl, { method: 'POST', headers })
      .then(refetchActivityInfo)
      .finally(() => {
        that.setState({
          resendToRepoLoading: false,
        })
      })
  }

  render() {
    const { currentUser, manuscript } = this.props
    const { audits, versions, showAudits, showVersion } = this.state
    const showResendToRepoButton =
      manuscript &&
      manuscript.status &&
      manuscript.status === 'repo-ready' &&
      manuscript.ebiState &&
      manuscript.ebiState.startsWith('sent to EBI') // 'sent to EBI' and 'sent to EBI (index only)' states

    return (
      <React.Fragment>
        <DetailsHeading>
          <DetailsToggle>
            <H2>Details</H2>
            <Toggle>
              <Action
                className={showAudits === 'all' && 'current'}
                onClick={() => this.changeAudits('all', showVersion)}
              >
                All
              </Action>
              <Action
                className={showAudits === 'file' && 'current'}
                onClick={() => this.changeAudits('file', showVersion)}
              >
                Files
              </Action>
              <Action
                className={showAudits === 'note' && 'current'}
                onClick={() => this.changeAudits('note', showVersion)}
              >
                Messages
              </Action>
            </Toggle>
          </DetailsToggle>
          <div>
            {showResendToRepoButton &&
              (this.state.resendToRepoLoading ? (
                <NewMsgBtn>
                  <Loading />
                </NewMsgBtn>
              ) : (
                <NewMsgBtn onClick={() => this.resendToRepo()}>
                  Resend to repo
                </NewMsgBtn>
              ))}
            <NewMsgBtn onClick={() => this.setState({ newNote: true })}>
              Add note
            </NewMsgBtn>
            <NewMsgBtn
              onClick={() => this.setState({ sendingMail: true })}
              primary
            >
              Send email
            </NewMsgBtn>
          </div>
        </DetailsHeading>
        {versions.length > 1 && (
          <VersionToggle>
            <VersionHeading>Versions</VersionHeading>
            <Toggle>
              <Action
                className={showVersion === 'all' && 'current'}
                onClick={() => this.changeAudits(showAudits, 'all')}
              >
                All
              </Action>
              {versions.map(version => (
                <Action
                  className={showVersion === version && 'current'}
                  key={`version${version}`}
                  onClick={() => this.changeAudits(showAudits, version)}
                >
                  {version}
                </Action>
              ))}
            </Toggle>
          </VersionToggle>
        )}
        <DetailsTable>
          <tbody>
            <tr>
              <th>Date</th>
              <th>Person</th>
              <th>Event</th>
            </tr>
            <ActivityList audits={audits} manuscript={manuscript} />
          </tbody>
        </DetailsTable>
        {this.state.sendingMail && (
          <Mailer
            close={() => this.setState({ sendingMail: false })}
            currentUser={currentUser}
            manuscript={manuscript}
            message={`\nKind regards,\n
${formatName(currentUser.identities[0].name)}
Europe PMC Helpdesk`}
            refetch={[
              {
                query: QUERY_ACTIVITY_INFO,
                variables: {
                  id: manuscript.id,
                },
              },
            ]}
          />
        )}
        {this.state.newNote && (
          <NewNote
            close={() => this.setState({ newNote: false })}
            manuscriptId={manuscript.id}
            manuscriptVersion={manuscript.version}
            message={this.state.message}
            onChange={message => this.setState({ message })}
          />
        )}
      </React.Fragment>
    )
  }
}

export default ActivityDetails
