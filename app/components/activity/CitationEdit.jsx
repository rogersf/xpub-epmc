import React from 'react'
import { omit } from 'lodash'
import styled, { withTheme } from 'styled-components'
import { Action, H2, H3, Icon } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import { B, Toggle, Notification } from '../ui'
import PubMedSearch, { JournalSearch, PreprintSearch } from '../citation-search'
import ManualCitation from './ManualCitation'
import IdentifierEdit from './IdentifierEdit'
import { Exit } from './Edit'

const JournalCheck = styled.div`
  display: flex;
  align-items: start;
  & > div:first-child {
    flex: 9;
    padding-right: ${th('gridUnit')};
    & + div {
      flex: 1;
      text-align: center;
    }
  }
`
const Title = styled.div`
  display: table;
  width: 100%;
  & > div {
    display: table-row;
    & > div,
    h2 {
      display: table-cell;
    }
    h2 {
      width: 70%;
    }
  }
  p {
    display: flex;
    align-items: center;
    margin: 0 auto;
    button {
      font-size: ${th('fontSizeBaseSmall')};
      margin-left: ${th('gridUnit')};
      display: inline-flex;
      align-items: center;
    }
  }
`
const CitationDiv = styled.div`
  max-width: 100%;
  box-sizing: border-box;
  display: flex;
  align-items: start;
  & > div:first-child {
    flex: 2;
    min-width: 0;
  }
`
const UserCitation = styled.div`
  flex: 1;
  min-width: 0;
  background-color: ${th('colorBackgroundHue')};
  margin-left: calc(${th('gridUnit')} * 2);
  padding: 0 ${th('gridUnit')} ${th('gridUnit')};
`
const Small = styled.span`
  display: block;
  font-size: ${th('fontSizeBaseSmall')}
  font-weight: 600;
  & + p {
    margin: ${th('gridUnit')} auto;
  }
`
const IndexedIcon = withTheme(({ theme, indexed }) => (
  <Icon color={indexed ? theme.colorSuccess : theme.colorError}>
    {indexed ? 'check' : 'x'}
  </Icon>
))

class CitationEdit extends React.Component {
  state = { show: 'search' }
  componentDidMount() {
    const { manuscript } = this.props
    const { journal, organization } = manuscript
    const preprint = organization.name === 'Europe PMC Preprints'
    if (journal) {
      const newStatus = {
        show:
          journal.meta.pubmedStatus || journal.meta.pmcStatus || preprint
            ? 'search'
            : 'enter',
      }
      this.setState(newStatus)
    }
  }
  componentDidUpdate(prevProps) {
    const oldStatus = prevProps.manuscript.journal
      ? prevProps.manuscript.journal.meta.pubmedStatus
      : !!prevProps.manuscript.meta.unmatchedJournal
    const newStatus = this.props.manuscript.journal
      ? this.props.manuscript.journal.meta.pubmedStatus
      : !!this.props.manuscript.meta.unmatchedJournal
    if (oldStatus !== newStatus) {
      ;(() => this.setState({ show: newStatus ? 'search' : 'enter' }))()
    }
  }
  render() {
    const { manuscript, change, close } = this.props
    const { journal, meta, organization } = manuscript
    const { notes, articleIds: aids, unmatchedJournal } = meta
    const indexed =
      journal && (journal.meta.pubmedStatus || journal.meta.pmcStatus)
    const articleIds = aids ? aids.map(aid => omit(aid, '__typename')) : []
    const note =
      (notes && notes.find(n => n.notesType === 'userCitation')) || null
    const preprint = organization.name === 'Europe PMC Preprints'
    const isPlanS = !!(notes && notes.find(n => n.notesType === 'planS'))
    const { show } = this.state
    return (
      <React.Fragment>
        <H2>Journal</H2>
        {unmatchedJournal && (
          <Notification type="error">
            {`'${unmatchedJournal}' is not in the NLM catalog`}
          </Notification>
        )}
        <JournalCheck>
          <div>
            <JournalSearch
              journal={journal || { journalTitle: unmatchedJournal }}
              setJournal={e => {
                if (e.id) {
                  change({ journalId: e.id, meta: { unmatchedJournal: null } })
                } else {
                  change({
                    journalId: null,
                    meta: { unmatchedJournal: e.journalTitle },
                  })
                }
              }}
            />
          </div>
          {journal && (
            <div>
              <Small>Indexed?</Small>
              <p>
                <IndexedIcon indexed={indexed} />
              </p>
            </div>
          )}
        </JournalCheck>
        <Title>
          <div>
            <H2>Edit citation</H2>
            <div>
              {articleIds.map(
                aid =>
                  ['pmcid', 'pprid', 'pmid'].includes(aid.pubIdType) && (
                    <p key={aid.id}>
                      <span>
                        <B>{aid.pubIdType.toUpperCase()}: </B>
                        {aid.id}
                      </span>
                    </p>
                  ),
              )}
            </div>
          </div>
          <Toggle>
            <div>
              <Action
                className={show === 'search' && 'current'}
                onClick={() => this.setState({ show: 'search' })}
              >
                Search
              </Action>
              <Action
                className={show === 'enter' && 'current'}
                disabled={unmatchedJournal}
                onClick={() => this.setState({ show: 'enter' })}
              >
                Enter manually
              </Action>
            </div>
            <div
              style={
                !articleIds.some(aid => aid.pubIdType !== 'doi')
                  ? { textAlign: 'right' }
                  : {}
              }
            >
              <Action
                className={show === 'ids' && 'current'}
                onClick={() => this.setState({ show: 'ids' })}
              >
                Edit IDs
              </Action>
            </div>
          </Toggle>
        </Title>
        {show === 'search' && (
          <React.Fragment>
            {preprint ? (
              <React.Fragment>
                <PreprintSearch
                  adminRemove
                  citationData={async (e, d) => {
                    await change(e, d)
                    close()
                  }}
                  search={meta.title}
                />
                <Exit close={close} />
              </React.Fragment>
            ) : (
              <React.Fragment>
                <PubMedSearch
                  adminRemove
                  changeInfo={() => false}
                  citationData={async (e, d) => {
                    await change(e, d)
                    close()
                  }}
                  lenientChecks={!isPlanS}
                  search={meta.title}
                />
                <Exit close={close} />
              </React.Fragment>
            )}
          </React.Fragment>
        )}
        {show === 'enter' && (
          <CitationDiv>
            <div>
              <ManualCitation
                citationData={change}
                close={close}
                id={manuscript.id}
                journal={journal}
                meta={meta}
              />
            </div>
            {note && (
              <UserCitation>
                <H3>User citation</H3>
                {note.content}
              </UserCitation>
            )}
          </CitationDiv>
        )}
        {show === 'ids' && (
          <IdentifierEdit
            articleIds={articleIds}
            close={close}
            decision={manuscript.decision}
            id={manuscript.id}
            preprint={preprint}
          />
        )}
      </React.Fragment>
    )
  }
}

export default CitationEdit
