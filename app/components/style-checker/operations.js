import gql from 'graphql-tag'

export const CHECK_STYLE = gql`
  mutation checkStyle($file: Upload!) {
    checkStyle(file: $file)
  }
`
