import React from 'react'
import styled, { createGlobalStyle } from 'styled-components'
import { Query } from 'react-apollo'
import { th } from '@pubsweet/ui-toolkit'
import { GlobalStyle, AppBar, Action, Icon, Link } from '@pubsweet/ui'
import { createBrowserHistory } from 'history'
import { Loading, LoadingIcon, ReaderText } from './ui'
import mainLogo from '../assets/epmcpluslogo.png'
import { CURRENT_USER, GET_USER_TEAMS } from './helpers/AuthorizeGraphQLQueries'
import userHelper from './helpers/userHelper'
import Footer from './Footer'

const Global = createGlobalStyle`
  html,
  body,
  #root,
  #root > div,
  #root > div > div { min-height: 100vh; }
  body {
    font-family: ${th('fontInterface')};
    background-color: ${th('colorBackground')};
  }
  div, p {
    font-size: ${th('fontSizeBase')};
    line-height: ${th('lineHeightBase')};
  }
  p {
    margin-top: ${th('gridUnit')};
    margin-bottom: calc(${th('gridUnit')} * 2);
  }
  .hidden {
    display: none;
  }
  *:focus {
    outline-offset: -2px;
    outline: 2px ${th('borderStyle')} rgba(32, 105, 156, .8);
  }
  input:focus {
    box-shadow: ${th('dropShadow')}
  }
  ::placeholder {
    color: ${th('colorTextPlaceholder')};
    opacity: .7;
  }
  :-ms-input-placeholder {
    color: ${th('colorTextPlaceholder')};
    opacity: .7;
  }
  ::-ms-input-placeholder { 
    color: ${th('colorTextPlaceholder')};
    opacity: .7;
  }
`
const NavSec = styled.div`
  height: calc(${th('gridUnit')} * 12);
  background-color: ${th('colorTextReverse')};
  border-bottom: ${th('borderWidth')} ${th('borderStyle')}
    ${th('colorFurniture')};
  @media screen and (max-width: 630px) {
    height: calc(${th('gridUnit')} * 9);
  }
`
const BrandingSection = styled.span`
  display: flex;
  flex-wrap: no-wrap;
  align-items: center;
  height: 100%;
  padding: 0 calc(${th('gridUnit')} * 2);
`
const MainTitle = styled.img`
  width: 300px;
  @media screen and (max-width: 1300px) {
    width: 240px;
  }
  @media screen and (max-width: 900px) {
    width: 220px;
  }
  @media screen and (max-width: 800px) {
    width: 200px;
  }
`
const Tail = styled.span`
  margin-top: -1px;
  span {
    display: inline-block;
    padding-left: ${th('gridUnit')};
    font-style: italic;
    color: ${th('colorText')};
  }
`
const Spacer = styled.span`
  display: inline-block;
  padding: 0 ${th('gridUnit')};
  margin-top: -2px;
  &:before {
    content: '|';
    display: inline-block;
  }}
  @media screen and (max-width: 1200px) {
    display: none;
  }
`
const Tag = styled.span`
  margin-top: -1px;
  display: inline-block;
  white-space: nowrap;
  @media screen and (max-width: 1200px) {
    display: none;
  }
`
const MobileButton = styled(Action)`
  display: none;
  @media screen and (max-width: 900px) {
    display: flex;
    padding: calc(${th('gridUnit')} * 2);
  }
`
const RightSection = styled.div`
  display: flex;
  flex-wrap: no-wrap;
  align-items: center;
  justify-content: flex-end;
  height: 100%;
  padding: 0 calc(${th('gridUnit')} * 2);
  @media screen and (min-width: 1532px) {
    padding-right: 0;
  }
  @media screen and (max-width: 1300px) {
    padding-left: 0;
  }
  @media screen and (min-width: 901px) {
    &.hidden {
      display: flex !important;
    }
  }
  @media screen and (max-width: 900px) {
    &.hidden {
      display: none !important;
    }
    flex-direction: column;
    width: 100%;
    height: auto;
    position: absolute;
    top: 96px;
    z-index: 10;
    padding: 0;
  }
  @media screen and (max-width: 630px) {
    top: 72px;
  }
`
const Section = styled.span`
  display: inline-flex;
  white-space: nowrap;
  align-items: center;
  height: 100%;
  padding: 0 calc(${th('gridUnit')} * 2);
  a,
  button {
    display: flex;
    align-items: center;
    white-space: nowrap;
    &:focus {
      box-shadow: none;
      text-decoration: underline;
    }
  }
  & > a,
  & > button {
    font-weight: 600;
  }
  &:last-of-type {
    padding-right: 0;
  }
  @media screen and (max-width: 1100px) {
    padding: 0 calc(${th('gridUnit')} * 1.5);
  }
  @media screen and (max-width: 900px) {
    display: block;
    height: auto;
    background-color: ${th('colorTextReverse')};
    width: 100%;
    padding: 0;
    box-shadow: 0 4px 4px -4px ${th('colorBorder')};
    a,
    button {
      display: block;
      width: 100%;
      padding: calc(${th('gridUnit')} * 2) calc(${th('gridUnit')} * 4);
      border-top: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
      &.current {
        border-bottom: 0;
      }
    }
    & > button {
      display: none;
    }
    .hidden {
      display: block;
    }
  }
`
const Dropdown = styled.div`
  position: absolute;
  top: 70%;
  right: ${th('gridUnit')};
  z-index: 5;
  width: 100%;
  min-width: calc(${th('gridUnit')} * 16);
  box-sizing: border-box;
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  border-top: 0;
  & > * {
    width: 100%;
    height: calc(${th('gridUnit')} * 6);
    padding: ${th('gridUnit')};
    background-color: ${th('colorTextReverse')};
    border-top: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
    &:hover {
      background-color: ${th('colorTextReverse')} !important;
      border-top: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
    }
  }
  @media screen and (max-width: 900px) {
    position: relative;
    top: auto;
    right: auto;
    border-right: 0;
    border-left: 0;
    & > * {
      height: auto;
      font-weight: 600;
    }
  }
`
const Branding = () => (
  <BrandingSection>
    <MainTitle alt="Europe PMC plus" src={mainLogo} />
    {!window.location.hostname.startsWith('plus') && (
      <Tail>
        <span>beta</span>
      </Tail>
    )}
    <Spacer />
    <Tag>manuscript submission system</Tag>
  </BrandingSection>
)

const toggleDropdown = button => {
  const expanded = button.getAttribute('aria-expanded')
  button.setAttribute('aria-expanded', expanded === 'true' ? 'false' : 'true')
  button.nextSibling.classList.toggle('hidden')
}

const logoutUser = () => {
  userHelper.clearCurrentUser()
  createBrowserHistory({ forceRefresh: true }).push('/login')
}

const RightComponent = ({ user, loginLink, onLogoutClick }) => (
  <React.Fragment>
    <MobileButton
      aria-expanded="false"
      onClick={e => toggleDropdown(e.currentTarget)}
    >
      <ReaderText>Site menu</ReaderText>
      <Icon color="currentColor" size={4} strokeWidth={2}>
        menu
      </Icon>
    </MobileButton>
    <RightSection
      className="hidden"
      onClick={e => toggleDropdown(e.currentTarget.previousSibling)}
    >
      {user && (
        <React.Fragment>
          {user.admin && (
            <Section>
              <Link
                className={
                  window.location.pathname === '/admin-dashboard' && 'current'
                }
                to="/admin-dashboard"
              >
                Dashboard
              </Link>
            </Section>
          )}
          <Section>
            <Link
              className={window.location.pathname === '/dashboard' && 'current'}
              to="/dashboard"
            >
              {user.admin || user.tagger || user.external
                ? 'My queue'
                : 'My manuscripts'}
            </Link>
          </Section>
        </React.Fragment>
      )}
      <Section>
        {(user && user.admin && (
          <Link
            className={
              window.location.pathname.startsWith('/manage') && 'current'
            }
            to="/manage"
          >
            Management
          </Link>
        )) ||
          (user && user.tagger && (
            <Link
              className={
                window.location.pathname === '/manage/priority' && 'current'
              }
              to="/manage/priority"
            >
              Preprint priority
            </Link>
          )) || (
            <Link
              className={
                window.location.pathname.startsWith('/user-guide') && 'current'
              }
              to="/user-guide"
            >
              User guide
            </Link>
          )}
      </Section>
      {user ? (
        <Section
          id="toggleButton"
          onClick={e => toggleDropdown(e.currentTarget.lastChild)}
          onMouseEnter={e =>
            e.currentTarget.lastChild.classList.remove('hidden')
          }
          onMouseLeave={e => e.currentTarget.lastChild.classList.add('hidden')}
          style={{ position: 'relative' }}
        >
          <Action>
            <Icon color="currentColor" size={3} strokeWidth={2}>
              user
            </Icon>
            <span>{user.identities[0].name.givenNames}</span>
            <Icon color="currentColor" size={3} strokeWidth={2}>
              chevron-down
            </Icon>
          </Action>
          <Dropdown className="hidden">
            <Link to="/my-account">My account</Link>
            <Action onClick={onLogoutClick}>Logout</Action>
          </Dropdown>
        </Section>
      ) : (
        <Section>
          <Action to={loginLink}>
            <Icon color="currentColor" size={3}>
              log-in
            </Icon>
            <span>Login</span>
          </Action>
        </Section>
      )}
    </RightSection>
  </React.Fragment>
)

export const UserContext = React.createContext({
  currentUser: {},
  setCurrentUser: () => {},
})

const App = ({ children, journal, history, match }) => (
  <Query fetchPolicy="cache-and-network" query={CURRENT_USER}>
    {({ data, loading }) => {
      if (loading) {
        return (
          <Loading>
            <LoadingIcon />
          </Loading>
        )
      }
      const currentUser = data ? data.epmc_currentUser : undefined
      const userId = currentUser ? currentUser.id : undefined
      return (
        <Query
          fetchPolicy="cache-and-network"
          query={GET_USER_TEAMS}
          skip={!userId}
          variables={{ userId }}
        >
          {({ data, loading }) => {
            if (loading) {
              return (
                <Loading>
                  <LoadingIcon />
                </Loading>
              )
            }

            const userTeams = data ? data.userTeams : []
            if (userTeams.some(t => t.role === 'admin')) {
              currentUser.admin = true
            } else if (userTeams.some(t => t.role === 'external-admin')) {
              currentUser.external = true
            } else if (userTeams.some(t => t.role === 'tagger')) {
              currentUser.tagger = true
            }

            return (
              <UserContext.Provider value={currentUser}>
                <React.Fragment>
                  <GlobalStyle />
                  <Global />
                  <NavSec>
                    <AppBar
                      brand={Branding()}
                      onLogoutClick={logoutUser}
                      rightComponent={RightComponent}
                      user={currentUser}
                    />
                  </NavSec>
                  {children}
                  <Footer />
                </React.Fragment>
              </UserContext.Provider>
            )
          }}
        </Query>
      )
    }}
  </Query>
)

export default App
