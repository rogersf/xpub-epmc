import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { H2, Link } from '@pubsweet/ui'
import { A } from './ui'

const Head = styled(H2)`
  font-size: ${th('fontSizeHeading4')};
  line-height: ${th('lineHeightHeading4')};
  margin-top: calc(${th('gridUnit')} * 3);
  margin-bottom: calc(${th('gridUnit')} * 1.5);
`

const SignInFooter = () => (
  <div>
    <Head>Need help depositing a manuscript?</Head>
    <ul>
      <li>
        See the Europe PMC plus <Link to="/user-guide">User Guide</Link> for
        more information.
      </li>
      <li>
        {`Contact the Europe PMC helpdesk at `}
        <A href="mailto:helpdesk@europepmc.org">helpdesk@europepmc.org</A>
        {` or +44 1223 494118.`}
      </li>
    </ul>
  </div>
)

export default SignInFooter
