import React from 'react'
import { Icon } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import styled from 'styled-components'
import moment from 'moment'
import { Notification } from '../ui'

const Christmas = styled(Notification)`
  border-color: #a80202;
  background-color: ${th('colorTextReverse')};
  div {
    display: flex;
    align-items: center;
    .text {
      margin-left: ${th('gridUnit')};
    }
    div {
      flex-direction: column;
    }
  }
`
const Holiday = ({ startDate, stopDate }) => {
  const start = moment(startDate).format('dddd, D MMMM')
  const end = moment(stopDate).format('dddd, D MMMM')
  return (
    <Christmas>
      <div>
        <Icon color="#a80202" size={2.5}>
          gift
        </Icon>
        <br />
        <Icon color="green" size={2.5}>
          gift
        </Icon>
      </div>
      <span className="text">
        Please note that the Helpdesk will be unavailable from {start} to {end}.
        Any calls or e-mails will be saved and responded to on our return. Happy
        Holidays from the Europe PMC team.{' '}
      </span>
      <div>
        <Icon color="green" size={2.5}>
          gift
        </Icon>
        <br />
        <Icon color="#a80202" size={2.5}>
          gift
        </Icon>
      </div>
    </Christmas>
  )
}

export default Holiday
