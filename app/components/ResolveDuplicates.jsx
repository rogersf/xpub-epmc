import React from 'react'
import { withRouter } from 'react-router'
import { Mutation } from 'react-apollo'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Action, H3, H4, Button, Icon, Link, ErrorText } from '@pubsweet/ui'
import { B, Buttons, CloseModal, Portal, Notification } from './ui'
import { States, timeSince } from './dashboard'
import Citation from './Citation'
import { REPLACE_MANUSCRIPT, CHECK_DUPES } from './operations'
import { NoteMutations } from './SubmissionMutations'

const Columns = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: stretch;
  justify-content: flex-end;
  h3 {
    margin-top: 0;
  }
  h4 {
    margin: 0;
    display: flex;
    align-items: center;
    justify-content: space-between;
  }
  button {
    display: flex;
    align-items: center;
  }
  & > * {
    width: 48%;
    box-sizing: border-box;
    margin-right: 1%;
    margin-left: 1%;
    opacity: 1;
    transition: opacity 0.5s linear;
    &.scaled {
      opacity: 0;
    }
  }
`
const Current = styled.div`
  margin-top: ${th('gridUnit')};
  display: flex;
  align-items: stretch;
  justify-content: space-between;
  flex-direction: column;
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  padding: calc(${th('gridUnit')} * 2);
  background-color: ${th('colorBackground')};
`
const Dupes = styled.div`
  margin-top: ${th('gridUnit')};
  display: flex;
  align-items: stretch;
  justify-content: space-between;
  flex-direction: column;
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  padding: calc(${th('gridUnit')} * 2);
  background-color: ${th('colorBackgroundHue')};
`
const FlexP = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-between;
`
class ResolveDuplicates extends React.Component {
  state = { error: null, success: null }
  componentDidUpdate() {
    const { duplicates, close } = this.props
    const { success } = this.state
    if (success && this.portal) {
      const redirect = duplicates && duplicates.find(d => d.id === success)
      setTimeout(() => {
        this.setState({ success: null })
        if (duplicates.length < 1) {
          close()
        }
        if (redirect) {
          this.props.history.replace(
            `/submission/${redirect.id}/${States.admin[redirect.status].url}`,
          )
        }
      }, 2500)
    }
  }
  setRef = portal => {
    this.portal = portal
  }
  unsetDupe = dupe => {
    const { manuscript: current, note } = this.props
    const dList = dupe.meta.articleIds ? dupe.meta.articleIds : []
    const cList = current.meta.articleIds ? current.meta.articleIds : []
    const badId = cList.find(cid => dList.some(did => did.id === cid.id))
    if (badId) {
      this.setState({ error: badId.pubIdType })
      this.portal.scrollTop = 0
    } else {
      if (note) {
        const array = JSON.parse(note.content)
        array.push(dupe.id)
        this.props.changeNote({
          id: note.id,
          content: JSON.stringify(array),
          notesType: 'notDuplicates',
        })
      } else {
        this.props.newNote({
          manuscriptId: current.id,
          content: JSON.stringify([dupe.id]),
          notesType: 'notDuplicates',
        })
      }
      const dNote = dupe.notes
        ? dupe.notes.find(n => n.notesType === 'notDuplicates')
        : null
      if (dNote) {
        const array = JSON.parse(dNote.content)
        array.push(current.id)
        this.props.changeNote({
          id: dNote.id,
          content: JSON.stringify(array),
          notesType: 'notDuplicates',
        })
      } else {
        this.props.newNote({
          manuscriptId: dupe.id,
          content: JSON.stringify([current.id]),
          notesType: 'notDuplicates',
        })
      }
    }
  }
  render() {
    const { close, manuscript: current, duplicates } = this.props
    const { error, success } = this.state
    return (
      <Mutation
        mutation={REPLACE_MANUSCRIPT}
        refetchQueries={result => {
          if (result.data.replaceManuscript) {
            return [
              {
                query: CHECK_DUPES,
                fetchPolicy: 'cache-and-network',
                variables: {
                  id: current.id,
                  articleIds: current.meta.articleIds
                    ? current.meta.articleIds.map(aid => aid.id)
                    : null,
                  title: current.meta.title,
                },
              },
            ]
          }
          return []
        }}
      >
        {(replaceManuscript, { data }) => {
          const replace = (keepId, throwId, e) => {
            e.currentTarget.parentNode.classList.add('scaled')
            setTimeout(async () => {
              await replaceManuscript({
                variables: { keepId, throwId },
              })
              this.setState({ success: keepId })
            }, 500)
          }
          return (
            <Portal ref={this.setRef} transparent>
              <CloseModal onClick={close} />
              <Columns>
                <H3>This manuscript</H3>
                <H3>Duplicate(s)</H3>
                {error ||
                  (success && (
                    <div style={{ flex: '0 0 100%' }}>
                      {error && (
                        <Notification type="error">
                          Two articles cannot have the same{' '}
                          {error.toUpperCase()}.
                        </Notification>
                      )}
                      {success && (
                        <Notification type="success">
                          Duplicate merged and removed.
                        </Notification>
                      )}
                    </div>
                  ))}
                <Current>
                  <H4>{current.id}</H4>
                  <Citation
                    journal={current.journal}
                    metadata={current.meta}
                    version={current.version}
                  />
                  <FlexP>
                    <span>
                      <B>Status:</B> {current.status}
                    </span>
                    <span>
                      <B>Updated:</B> {timeSince(current.updated)} ago
                    </span>
                  </FlexP>
                  <p>
                    <B>Grants: </B>
                    {current.meta.fundingGroup &&
                      current.meta.fundingGroup.map((f, t) => (
                        <span key={f.awardId}>
                          {`${f.fundingSource} ${f.awardId}`}
                          {t !== current.meta.fundingGroup.length - 1 && ', '}
                        </span>
                      ))}
                  </p>
                  <Button
                    disabled={current.decision === 'accepted'}
                    onClick={e => replace(duplicates[0].id, current.id, e)}
                    primary
                  >
                    Remove this &amp; transfer grants
                    <Icon color="currentColor">arrow-right</Icon>
                  </Button>
                  {current.decision === 'accepted' && (
                    <ErrorText style={{ marginTop: 0 }}>
                      Manuscript has been sent to the repository and must be
                      withdrawn to be removed.
                    </ErrorText>
                  )}
                </Current>
                {duplicates.length < 1 && <div />}
                {duplicates.map((dupe, i) => (
                  <React.Fragment key={dupe.id}>
                    <Dupes>
                      <H4>
                        {dupe.id}
                        <Action
                          onClick={() => this.unsetDupe(dupe)}
                          style={{ float: 'right' }}
                        >
                          <Icon color="currentColor">x</Icon>
                          Not a duplicate!
                        </Action>
                      </H4>
                      <Link
                        target="_blank"
                        to={`/submission/${dupe.id}/${
                          States.admin[dupe.status].url
                        }`}
                      >
                        <Citation
                          journal={dupe.journal}
                          metadata={dupe.meta}
                          version={dupe.version}
                        />
                      </Link>
                      <FlexP>
                        <span>
                          <B>Status:</B> {dupe.status}
                        </span>
                        <span>
                          <B>Updated:</B> {timeSince(dupe.updated)} ago
                        </span>
                      </FlexP>
                      <p>
                        <B>Grants: </B>
                        {dupe.meta.fundingGroup &&
                          dupe.meta.fundingGroup.map((f, t) => (
                            <span key={f.awardId}>
                              {`${f.fundingSource} ${f.awardId}`}
                              {t !== dupe.meta.fundingGroup.length - 1 && ', '}
                            </span>
                          ))}
                      </p>
                      <Button
                        disabled={dupe.decision === 'accepted'}
                        onClick={e => replace(current.id, dupe.id, e)}
                        primary
                      >
                        <Icon color="currentColor">arrow-left</Icon>
                        Transfer grants &amp; remove this
                      </Button>
                      {dupe.decision === 'accepted' && (
                        <ErrorText style={{ marginTop: 0 }}>
                          Manuscript has been sent to the repository and must be
                          withdrawn to be removed.
                        </ErrorText>
                      )}
                    </Dupes>
                    {i !== duplicates.length - 1 && <div />}
                  </React.Fragment>
                ))}
              </Columns>
              <Buttons>
                <Button onClick={close}>Exit</Button>
              </Buttons>
            </Portal>
          )
        }}
      </Mutation>
    )
  }
}

const DuplicatesWithRouter = withRouter(ResolveDuplicates)

export default NoteMutations(DuplicatesWithRouter)
