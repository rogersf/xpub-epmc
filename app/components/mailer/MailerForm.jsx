import React from 'react'
import { Field } from 'formik'
import { capitalize } from 'lodash'
import styled from 'styled-components'
import { th, override } from '@pubsweet/ui-toolkit'
import {
  Action,
  Button,
  H2,
  Icon,
  TextField,
  Checkbox,
  CheckboxGroup,
  ErrorText,
} from '@pubsweet/ui'
import {
  Buttons,
  TextArea,
  Portal,
  CloseModal,
  Fieldset,
  Notification,
} from '../ui'

const Recipients = styled.div`
  margin-bottom: calc(${th('gridUnit')} * 3);
  display: flex;
  align-items: flex-end;
  justify-content: space-between;
`
const Recipient = styled(Fieldset)`
  border: 0;
  padding: 0;
  & > legend {
    margin: 0;
    ${override('ui.Label')};
  }
`
const Input = styled.div`
  position: relative;
`
const InputError = styled(ErrorText)`
  margin: 0;
  position: absolute;
  top: 0;
  right: 0;
`

const SubjectInput = props => (
  <TextField
    disabled={props.disabled}
    invalidTest={props.invalidTest}
    label="Subject"
    {...props.field}
  />
)
const MessageInput = props => (
  <TextArea
    disabled={props.disabled}
    invalidTest={props.invalidTest}
    label="Message"
    rows={15}
    {...props.field}
  />
)
const CCInput = props => (
  <TextField
    disabled={props.disabled}
    invalidTest={props.invalidTest}
    label="CC (enter comma-separated email addresses)"
    {...props.field}
  />
)
const CCMeInput = props => (
  <Checkbox
    checked={props.field.value}
    invalidTest={props.invalidTest}
    label="Send me a copy"
    name={props.field.name}
    onChange={e => {
      props.form.setFieldValue('bcc', e.target.checked)
    }}
  />
)

const DefaultSuccess = ({ close }) => {
  setTimeout(() => close(), 3200)
  return <Notification type="success">Email message sent</Notification>
}

class MailerForm extends React.Component {
  state = { showCC: false }
  // Assumption: there is at most one submitter and one reviewer
  setRef = elem => {
    this.el = elem
  }
  render() {
    const { props, state } = this
    const { showCC } = state
    const { errors, touched, showSuccess: ShowSuccess = DefaultSuccess } = props
    const people = props.currentUser.admin
      ? []
      : [
          {
            value: 'helpdesk',
            label: 'Europe PMC Helpdesk',
          },
        ]
    props.manuscript.teams.forEach(t => {
      const [person] = t.teamMembers
      if (person && person.user && person.user.id !== props.currentUser.id) {
        const i = people.findIndex(p => p.value === person.user.id)
        if (i >= 0) {
          people[i].label = `${people[i].label}/${capitalize(t.role)}`
        } else {
          const { title, givenNames, surname } = person.alias.name
          people.push({
            value: person.user.id,
            label: `${
              title ? `${title} ` : ''
            } ${givenNames} ${surname}, ${capitalize(t.role)}`,
          })
        }
      }
    })
    props.recipients &&
      props.recipients.forEach(r => {
        if (!people.some(e => e.value === r)) {
          people.push({
            value: r,
            label: r,
          })
        }
      })
    const success = props.status === 'success'
    return (
      <Portal transparent>
        <CloseModal onClick={() => props.close()} />
        <H2>Send an email</H2>
        <form onSubmit={props.handleSubmit} ref={this.setRef}>
          <Input>
            {errors.recipients && touched.recipients && (
              <InputError>{errors.recipients}</InputError>
            )}
            <Recipients>
              <Recipient disabled={success}>
                <legend>Recipient(s)</legend>
                <CheckboxGroup
                  invalidTest={errors.recipients && touched.recipients}
                  name="recipients"
                  onChange={checked => {
                    props.setFieldValue('recipients', checked)
                  }}
                  options={people}
                  value={props.values.recipients}
                />
                {!props.currentUser.admin && (
                  <Field component={CCMeInput} name="bcc" />
                )}
              </Recipient>
              <Action
                disabled={success}
                onClick={() => this.setState({ showCC: true })}
                style={{
                  display: showCC ? 'none' : 'inline-flex',
                  alignItems: 'center',
                }}
              >
                <Icon color="currentColor" size={2.5}>
                  plus
                </Icon>
                CC
              </Action>
            </Recipients>
          </Input>
          <div
            className={!showCC ? 'hidden' : ''}
            style={{ marginBottom: '1em' }}
          >
            <Field component={CCInput} disabled={success} name="cc" />
          </div>
          <Input>
            {errors.subject && touched.subject && (
              <InputError>{errors.subject}</InputError>
            )}
            <Field
              component={SubjectInput}
              disabled={success}
              invalidTest={errors.subject && touched.subject}
              name="subject"
            />
          </Input>
          <Input>
            {errors.message && touched.message && (
              <InputError>{errors.message}</InputError>
            )}
            <Field
              component={MessageInput}
              disabled={success}
              invalidTest={errors.message && touched.message}
              name="message"
            />
          </Input>
          {success ? (
            <ShowSuccess close={props.close} />
          ) : (
            <Buttons right>
              <Button disabled={success} primary type="submit">
                Send
              </Button>
              <Button disabled={success} onClick={() => props.close()}>
                Cancel
              </Button>
            </Buttons>
          )}
        </form>
      </Portal>
    )
  }
}

export default MailerForm
