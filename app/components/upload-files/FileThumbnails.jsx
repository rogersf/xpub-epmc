import React from 'react'
import styled, { css } from 'styled-components'
import { H4, Icon } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import { B } from '../ui'
import FileLightbox from './FileLightbox'
import { AllTypes, ImageTypes, fileSort } from './uploadtypes'

const FileThumbs = styled.div`
  h4 {
    margin-bottom: 0;
    &:first-child {
      margin-top: 0;
    }
  }
`
const Thumbnails = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: flex-start;
  margin: 0 calc(${th('gridUnit')} / -2);
  padding: 0;
`
const Thumbnail = styled.div`
  display: flex;
  flex: 0 1 33%;
  min-width: 200px;
  align-items: center;
  flex-wrap: wrap;
  padding: calc(${th('gridUnit')} / 2);
`
const Thumb = styled.div`
  flex: 1 1 100%;
  max-width: 250px;
`
const ImageBox = styled.div`
  height: 0;
  padding-top: 45%;
  color: ${th('colorTextReverse')};
  background-color: ${th('colorBorder')};
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  ${props => props.image && `background-image: ${props.image};`}
  background-size: cover;
  margin: calc(${th('gridUnit')} / 2) ${th('gridUnit')};
  margin-left: 0;
  p {
    margin: -35% 0 0;
    text-align: center;
  }
`
const FileInfo = styled.p`
  max-width: 190px;
  margin: 0;
  font-size: ${th('fontSizeBaseSmall')};
  word-break: break-all;
  span {
    display: inline-block;
    margin-right: ${th('gridUnit')};
  }
`
const error = css`
  display: inline-flex;
  align-items: flex-start;
  color: ${th('colorError')};
`
const Bold = styled(B)`
  ${props => props.error && error}
`
const FileThumbnails = ({ files }) => {
  const grouped = fileSort(files).reduce((group, file) => {
    if (group[file.type]) {
      group[file.type].push(file)
    } else {
      group[file.type] = []
      group[file.type].push(file)
    }
    return group
  }, {})
  return (
    <FileThumbs>
      {Object.keys(grouped).map(group => (
        <React.Fragment key={group}>
          <H4>
            {(!group && <Bold error>Files without types</Bold>) ||
              (group === 'manuscript' && 'Manuscript file') ||
              (AllTypes.find(t => t.value === group) &&
                `${AllTypes.find(t => t.value === group).label}s`)}
          </H4>
          <Thumbnails>
            {grouped[group].map(file => (
              <Thumbnail key={file.url}>
                <Thumb>
                  <ImageBox
                    image={
                      ImageTypes.includes(file.mimeType) && `url('${file.url}')`
                    }
                  >
                    {!ImageTypes.includes(file.mimeType) && (
                      <p>
                        <Icon color="currentColor" size={5}>
                          file-text
                        </Icon>
                      </p>
                    )}
                  </ImageBox>
                </Thumb>
                <FileInfo>
                  <FileLightbox file={file} withTime />
                  <br />
                  {file.type !== 'manuscript' && (
                    <span>
                      <Bold error={!file.label}>
                        Label:
                        {!file.label && (
                          <Icon color="currentColor" size={2}>
                            x-circle
                          </Icon>
                        )}
                      </Bold>
                      {file.label && ` ${file.label}`}
                    </span>
                  )}
                </FileInfo>
              </Thumbnail>
            ))}
          </Thumbnails>
        </React.Fragment>
      ))}
    </FileThumbs>
  )
}

export default FileThumbnails
