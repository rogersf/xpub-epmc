import React from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'
import moment from 'moment'
import { Action, Icon } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import PDFViewer from '../../component-pdf-viewer'
import { A, Page, Center, Cover, CloseModal } from '../ui'
import { ImageTypes } from './uploadtypes'

const Image = styled.img`
  max-width: 100%;
`
const Time = styled.em`
  font-size: ${th('fontSizeBaseSmall')};
  font-style: italic;
  display: inline-block;
`
const PDF = styled.div`
  .pdf-viewer {
    height: calc(90vh - (${th('gridUnit')} * 17));
    .pdf-viewer-pane {
      height: calc(90vh - (${th('gridUnit')} * 19));
    }
  }
`

class FilePreview extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      preview: null,
    }
  }
  render() {
    const { file, withTime } = this.props
    const { preview } = this.state
    return (
      <React.Fragment>
        {file.type !== 'manuscript' &&
        (ImageTypes.some(t => t === file.mimeType) ||
          file.mimeType === 'application/pdf') ? (
          <Action
            onClick={() => this.setState({ preview: file })}
            style={{ wordBreak: 'break-all', fontSize: 'inherit' }}
            title="Preview file"
          >
            {file.filename}
          </Action>
        ) : (
          <A
            download={file.filename}
            href={file.url}
            style={{ wordBreak: 'break-all' }}
            title="Download this file"
          >
            {file.filename}
            <Icon
              color="currentColor"
              size={1.6}
              style={{ verticalAlign: 'text-bottom' }}
            >
              download
            </Icon>
          </A>
        )}
        {withTime && (
          <React.Fragment>
            <span style={{ marginRight: '5px' }} />
            <Time>
              {moment(file.updated || file.created).format('DD/MM/YYYY HH:mm')}
            </Time>
          </React.Fragment>
        )}
        {preview &&
          ReactDOM.createPortal(
            <Cover>
              <Page>
                <CloseModal onClick={() => this.setState({ preview: null })} />
                {preview.mimeType === 'application/pdf' ? (
                  <PDF>
                    <PDFViewer url={preview.url} />
                    <Center>
                      <p>
                        <b>{preview.label ? `${preview.label}: ` : ''}</b>
                        {preview.filename}
                      </p>
                    </Center>
                  </PDF>
                ) : (
                  <Center>
                    <Image src={preview.url} />
                    <p>
                      <b>{preview.label ? `${preview.label}: ` : ''}</b>
                      {preview.filename}
                    </p>
                  </Center>
                )}
              </Page>
            </Cover>,
            document.getElementById('root').firstChild,
          )}
      </React.Fragment>
    )
  }
}

export default FilePreview
