export const TypesWithNames = {
  manuscriptTypes: [
    {
      mime:
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      name: 'Microsoft Word (OpenXML)',
    },
    { mime: 'application/msword', name: 'Microsoft Word' },
    { mime: 'application/pdf', name: 'Adobe Portable Document Format (PDF)' },
  ],
  xmlTypes: [
    { mime: 'application/xml', name: 'XML' },
    { mime: 'text/html', name: 'HyperText Markup Language (HTML)' },
    { mime: 'text/xml', name: 'XML' },
    { mime: 'application/xhtml+xml', name: 'XHTML' },
    { mime: 'text/xsl', name: 'XSLT' },
  ],
  fileTypes: [
    { mime: 'text/csv', name: 'Comma-separated values (CSV)' },
    { mime: 'text/tab-separated-values', name: 'Tab-separated values (TSV)' },
    { mime: 'text/rtf', name: 'Rich Text Format (RTF)' },
    { mime: 'text/plain', name: 'Plain text' },
    { mime: 'text/html', name: 'HyperText Markup Language (HTML)' },
    { mime: 'text/xml', name: 'XML' },

    { mime: 'application/rtf', name: 'Rich Text Format (RTF)' },
    { mime: 'application/zip', name: '  ZIP archive' },
    { mime: 'application/x-zip-compressed', name: 'ZIP archive' },
    { mime: 'application/x-compressed', name: 'ZIP archive' },
    { mime: 'multipart/x-zip', name: 'ZIP archive' },
    { mime: 'application/ogg', name: 'OGG audio' },
    { mime: 'application/postscript', name: 'PostScript file' }, // ai, eps
    {
      mime: 'application/vnd.oasis.opendocument.text',
      name: 'OpenDocument text document',
    },
    {
      mime: 'application/vnd.oasis.opendocument.presentation',
      name: 'OpenDocument presentation document',
    },
    {
      mime: 'application/vnd.oasis.opendocument.spreadsheet',
      name: 'OpenDocument spreadsheet document',
    },
    { mime: 'application/vnd.ms-powerpoint', name: 'Microsoft PowerPoint' },
    {
      mime:
        'application/vnd.openxmlformats-officedocument.presentationml.presentation',
      name: 'Microsoft PowerPoint (OpenXML)',
    },
    { mime: 'application/vnd.ms-excel', name: 'Microsoft Excel' },
    {
      mime: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      name: 'Microsoft Excel (OpenXML)',
    },
    { mime: 'application/xml', name: 'XML' },
    { mime: 'application/gzip', name: 'GZip Compressed Archive' },
    { mime: 'application/x-gzip', name: 'GZip Compressed Archive' },
    { mime: 'application/x-gzip-compressed', name: 'GZip Compressed Archive' },

    { mime: 'audio/midi', name: 'Musical Instrument Digital Interface (MIDI)' },
    { mime: 'audio/wav', name: 'Waveform Audio Format' },
    { mime: 'audio/ogg', name: 'OGG audio' },
    { mime: 'audio/x-ms-wma', name: 'Windows Media audio' },
    { mime: 'audio/mpeg', name: 'MP3 audio' },

    { mime: 'chemical/x-cif', name: 'Chemical Information File' }, // cif

    { mime: 'image/png', name: 'Portable Network Graphics' },
    { mime: 'image/jpeg', name: 'JPEG images' },
    { mime: 'image/bmp', name: 'Windows OS/2 Bitmap Graphics' },
    { mime: 'image/gif', name: 'Graphics Interchange Format (GIF)' },
    { mime: 'image/svg+xml', name: 'Scalable Vector Graphics (SVG)' },
    { mime: 'image/tiff', name: 'Tagged Image File Format (TIFF)' },

    { mime: 'video/mpeg', name: 'MPEG Video' },
    { mime: 'video/ogg', name: 'OGG' },
    { mime: 'video/quicktime', name: 'Quicktime video' }, // mov
    { mime: 'video/mp4', name: 'MPEG video' },
    { mime: 'video/x-flv', name: 'Flash video' }, // flv
    { mime: 'video/x-matroska', name: 'Matroska Multimedia' }, // mkv
    { mime: 'video/x-ms-asf', name: 'Windows Media' }, // wmv
    { mime: 'video/x-ms-wmv', name: 'Windows Media' }, // wmv
    {
      mime: 'application/x-troff-msvideo',
      name: 'AVI: Audio Video Interleave',
    },
    { mime: 'video/avi', name: 'AVI: Audio Video Interleave' },
    { mime: 'video/msvideo', name: 'AVI: Audio Video Interleave' },
    { mime: 'video/x-msvideo', name: 'AVI: Audio Video Interleave' }, // avi
  ],
}

export const ManuscriptTypes = TypesWithNames.manuscriptTypes.map(
  obj => obj.mime,
)

export const XMLTypes = TypesWithNames.xmlTypes.map(obj => obj.mime)

export const FileTypes = TypesWithNames.manuscriptTypes
  .map(obj => obj.mime)
  .concat(TypesWithNames.fileTypes.map(obj => obj.mime))

export const SubmissionTypes = [
  {
    label: '',
    value: '',
  },
  {
    label: 'Figure',
    value: 'figure',
  },
  {
    label: 'Table',
    value: 'table',
  },
  {
    label: 'Supplementary file',
    value: 'supplement',
  },
  {
    label: 'Manuscript addenda or alternate',
    value: 'manuscript_secondary',
  },
]

export const ReviewTypes = [
  {
    label: '',
    value: '',
  },
  {
    label: 'XML from tagger',
    value: 'PMC',
  },
  {
    label: 'GIF image',
    value: 'IMGsmall',
  },
  {
    label: 'JPG image',
    value: 'IMGview',
  },
  {
    label: 'TIF image',
    value: 'IMGprint',
  },
  {
    label: 'Supplementary file from tagger',
    value: 'supplement_tag',
  },
  {
    label: 'Converted NXML',
    value: 'PMCfinal',
  },
  /* {
    label: 'PDF for load to PMC',
    value: 'pdf4load',
  }, */
  {
    label: 'PDF preview',
    value: 'pdf4print',
  },
  /* {
    label: 'Movie in text',
    value: 'movie_in_text',
  },
  {
    label: 'Movie figure',
    value: 'movie_figure',
  }, */
  {
    label: 'Web preview',
    value: 'tempHTML',
  },
]

export const AllTypes = SubmissionTypes.concat(
  ReviewTypes.reduce((all, t) => {
    if (!SubmissionTypes.some(existing => existing.value === t.value)) {
      all.push(t)
    }
    return all
  }, []),
)

export const ImageTypes = [
  'image/png',
  'image/jpeg',
  'image/bmp',
  'image/gif',
  'image/svg+xml',
]

export const fileSort = files =>
  files.sort((a, b) => {
    if (a.type !== b.type) {
      if (a.type === 'manuscript') {
        return -1
      }
      if (b.type === 'manuscript') {
        return 1
      }
      if (a.type === 'manuscript_secondary') {
        return -1
      }
      if (b.type === 'manuscript_secondary') {
        return 1
      }
      if (!a.type) {
        return 1
      }
      if (!b.type) {
        return -1
      }
      if (a.type > b.type) {
        return 1
      }
    } else if (a.type === b.type) {
      if (!a.label) {
        return 1
      }
      if (!b.label) {
        return -1
      }
      const astring = a.label.replace(/\d+/g, '').toLowerCase()
      const bstring = b.label.replace(/\d+/g, '').toLowerCase()
      if (astring === bstring) {
        if (/\d/.test(a.label) && /\d/.test(b.label)) {
          if (
            parseInt(a.label.match(/\d+/)[0], 10) >
            parseInt(b.label.match(/\d+/)[0], 10)
          ) {
            return 1
          }
          return -1
        }
      }
      if (a.label.toLowerCase() > b.label.toLowerCase()) {
        return 1
      }
      return -1
    }
    return -1
  })
