import gql from 'graphql-tag'

export const UPLOAD_MUTATION = gql`
  mutation UploadFiles(
    $id: ID!
    $version: Float
    $files: Upload!
    $type: String
  ) {
    uploadFiles(id: $id, version: $version, files: $files, type: $type)
  }
`

export const REPLACE_MANUSCRIPT_FILE_MUTATION = gql`
  mutation ReplaceManuscriptFile($fileId: ID!, $files: Upload!) {
    replaceManuscriptFile(fileId: $fileId, files: $files)
  }
`

export const DELETE_FILE_MUTATION = gql`
  mutation DeleleFile($id: ID!) {
    deleteFile(id: $id)
  }
`

export const COPY_FILE_MUTATION = gql`
  mutation CopyFile($id: ID!, $type: String, $label: String) {
    copyFile(id: $id, type: $type, label: $label)
  }
`

export const UPDATE_FILE_MUTATION = gql`
  mutation UpdateFile($id: ID!, $type: String, $label: String) {
    updateFile(id: $id, type: $type, label: $label)
  }
`
