import gql from 'graphql-tag'
import { ManuscriptFragment } from '../operations'

export const CURRENT_USER = gql`
  query($email: String) {
    epmc_currentUser(email: $email) {
      id
    }
  }
`

export const CREATE_MANUSCRIPT = gql`
  mutation CreateManuscript($data: CreateManuscriptInput!) {
    createManuscript(data: $data) {
      manuscript {
        id
        status
        version
        formState
        meta {
          title
          articleIds {
            pubIdType
            id
          }
          publicationDates {
            type
            date
          }
          unmatchedJournal
        }
      }
      errors {
        type
        message
      }
    }
  }
`

export const SUBMIT_MANUSCRIPT = gql`
  mutation SubmitManuscript($data: ManuscriptInput!) {
    submitManuscript(data: $data) {
      manuscript {
        ...ManuscriptFragment
      }
      errors {
        type
        message
      }
    }
  }
  ${ManuscriptFragment}
`
