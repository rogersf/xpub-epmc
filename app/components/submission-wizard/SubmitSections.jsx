import React from 'react'
import styled from 'styled-components'
import { Button, H4, Icon } from '@pubsweet/ui'
import { th, darken } from '@pubsweet/ui-toolkit'
import { B } from '../ui'
import { SubmissionTypes, FileThumbnails, FileLightbox } from '../upload-files'
import { CrossRefCheck } from '../citation-search'
import Citation from '../Citation'
import SubmitHighlights from './SubmitHighlights'
import SubmitEdit, { EditCitation, EditFiles, ErrorMessage } from './SubmitEdit'
import EditFundingAccess from './EditFundingAccess'
import EditReviewer from './EditReviewer'

const CitationEdit = SubmitEdit(EditCitation)
const FundingEdit = SubmitEdit(EditFundingAccess)
const FileEdit = SubmitEdit(EditFiles)
const ReviewerEdit = SubmitEdit(EditReviewer)

const ManuscriptDiv = styled.div`
  h4 {
    margin: 0;
    display: inline-block;
  }
  p {
    margin-bottom: calc(${th('gridUnit')} * 3);
  }
  em {
    font-size: ${th('fontSizeBaseSmall')};
    font-style: italic;
    display: inline-block;
    margin-left: calc(${th('gridUnit')} * 2);
  }
  a {
    font-size: ${th('fontSizeBaseSmall')};
  }
`
const Warning = styled.p`
  color: ${darken('colorWarning', 30)};
  font-size: ${th('fontSizeBaseSmall')};
  display: flex;
  align-items: flex-start;
`

const formatName = name =>
  `${name.title ? `${name.title} ` : ''}${
    name.givenNames ? `${name.givenNames} ` : ''
  }${name.surname}`

const submitSections = (
  manuscript,
  checkDupes,
  pruneDupes,
  currentUser,
  highlights,
) => {
  const {
    meta,
    version,
    journal,
    files: allfiles,
    status,
    teams,
    organization,
  } = manuscript
  const preprint = organization.name === 'Europe PMC Preprints'
  if (teams && allfiles) {
    const {
      fundingGroup,
      releaseDelay = '',
      unmatchedJournal,
      notes,
      articleIds,
    } = meta
    const files = allfiles
      ? allfiles.filter(
          file =>
            !file.type || SubmissionTypes.some(t => t.value === file.type),
        )
      : []
    const manFile = allfiles.find(f => f.type === 'manuscript')
    const reviewerNote = notes
      ? notes.find(n => n.notesType === 'selectedReviewer')
      : null
    let reviewer = null
    if (teams && teams.find(team => team.role === 'reviewer')) {
      const [rev] = teams.find(team => team.role === 'reviewer').teamMembers
      reviewer = {
        id: rev.user.id,
        name: rev.alias.name,
      }
    }
    const selectedReviewer = reviewerNote
      ? JSON.parse(reviewerNote.content)
      : reviewer
    const { teamMembers } =
      (teams && teams.find(team => team.role === 'submitter')) || {}
    const [submitter = null] = teamMembers || []
    const dupeNote = notes
      ? notes.find(n => n.notesType === 'notDuplicates')
      : null
    const notDupes = dupeNote ? JSON.parse(dupeNote.content) : []
    const duplicates = checkDupes.filter(d => !notDupes.includes(d.id))
    const pmid = articleIds && articleIds.find(a => a.pubIdType === 'pmid')
    const doi = articleIds && articleIds.find(a => a.pubIdType === 'doi')
    const planS = notes && notes.find(n => n.notesType === 'planS')

    const citationInformation = {
      title: 'Citation',
      content: (
        <React.Fragment>
          <Citation journal={journal} metadata={meta} version={version} />
          {currentUser.admin && journal && (
            <React.Fragment>
              {journal.meta.pubmedStatus && !pmid && (
                <Warning>
                  <Icon color="currentColor" size={2}>
                    alert-triangle
                  </Icon>
                  This journal is indexed, but no PMID is attached
                </Warning>
              )}
              {journal.meta.pmcStatus && (
                <Warning>
                  <Icon color="currentColor" size={2}>
                    alert-triangle
                  </Icon>
                  This journal may be PMC participating
                </Warning>
              )}
            </React.Fragment>
          )}
        </React.Fragment>
      ),
      edit: CitationEdit,
      error:
        (currentUser.admin &&
          status === 'submitted' &&
          !preprint &&
          unmatchedJournal) ||
        duplicates.length > 0 ? (
          <React.Fragment>
            {unmatchedJournal && (
              <ErrorMessage>
                <Icon color="currentColor" size={2}>
                  alert_circle
                </Icon>
                Journal is not in the NLM Catalog.
              </ErrorMessage>
            )}
            {duplicates.length > 0 && (
              <React.Fragment>
                <Button onClick={() => pruneDupes()} primary>
                  Resolve duplicates
                </Button>
                <ErrorMessage>
                  <Icon color="currentColor" size={2}>
                    alert_circle
                  </Icon>
                  Submission is likely a duplicate.
                </ErrorMessage>
              </React.Fragment>
            )}
          </React.Fragment>
        ) : null,
    }

    const manuscriptFiles = {
      title: 'Files',
      content: (
        <React.Fragment>
          {manFile && (
            <ManuscriptDiv>
              <H4>Manuscript file</H4>
              {currentUser.admin && manFile.mimeType === 'application/pdf' && (
                <em>
                  {allfiles.find(f => f.type === 'pdf4load')
                    ? 'This PDF will be shown in PMC'
                    : 'PMC PDF will be generated'}
                </em>
              )}
              <p>
                <FileLightbox file={manFile} withTime />
              </p>
            </ManuscriptDiv>
          )}
          <FileThumbnails files={files} />
        </React.Fragment>
      ),
      edit: FileEdit,
      error:
        files &&
        files
          .filter(
            file =>
              !file.type || SubmissionTypes.some(s => s.value === file.type),
          )
          .some(file => !file.label || !file.type) ? (
          <ErrorMessage>
            <Icon color="currentColor" size={2}>
              alert_circle
            </Icon>
            All files must have a type and a label.
          </ErrorMessage>
        ) : null,
    }

    const fundingInfo = {
      title: 'Funding & Access',
      content: (
        <React.Fragment>
          <p>
            <B>Grants: </B>
            {fundingGroup &&
              fundingGroup.length > 0 &&
              fundingGroup.map((f, t) => (
                <span key={f.awardId}>
                  {`${f.fundingSource} ${f.awardId} `}({f.pi.surname})
                  {t !== fundingGroup.length - 1 && ', '}
                </span>
              ))}
          </p>
          <p>
            <B>Embargo: </B>
            {(releaseDelay || typeof releaseDelay === 'number') &&
              `${releaseDelay} month${
                parseInt(releaseDelay, 10) === 1 ? '' : 's'
              }`}
          </p>
          {currentUser.admin && (
            <React.Fragment>
              {!planS &&
                highlights &&
                /for[\s*]the[\s*]purposes?[\s*]of[\s*]open/gi.test(
                  highlights,
                ) && (
                  <Warning>
                    <Icon color="currentColor" size={2}>
                      alert-triangle
                    </Icon>
                    No Plan S funding, but open access purpose statement is
                    present.
                  </Warning>
                )}
              {planS && releaseDelay && releaseDelay !== '0' && (
                <Warning>
                  <Icon color="currentColor" size={2}>
                    alert-triangle
                  </Icon>
                  Has Plan S funding + embargo. Delete the embargo from the
                  activity page &amp; send for author agreement to Plan S
                  requirements.
                </Warning>
              )}
            </React.Fragment>
          )}
        </React.Fragment>
      ),
      edit: FundingEdit,
      error:
        (currentUser.admin ||
          (selectedReviewer &&
            selectedReviewer.id &&
            currentUser.id === selectedReviewer.id)) &&
        (!fundingGroup || fundingGroup.length === 0 || !releaseDelay) ? (
          <ErrorMessage>
            <Icon color="currentColor" size={2}>
              alert_circle
            </Icon>
            {(!fundingGroup || fundingGroup.length === 0) &&
              'Grants from Europe PMC Funders must be listed.'}
            {(!fundingGroup || fundingGroup.length === 0) && !releaseDelay && (
              <br />
            )}
            {!releaseDelay && 'Embargo period must be set.'}
          </ErrorMessage>
        ) : null,
    }

    const reviewerSelect = {
      title: `${reviewerNote ? 'Invited r' : 'R'}eviewer`,
      content: selectedReviewer && (
        <p>
          {selectedReviewer.id && submitter.user.id === selectedReviewer.id ? (
            `Manuscript Submitter (${formatName(submitter.alias.name)})`
          ) : (
            <React.Fragment>
              {formatName(selectedReviewer.name)}
              {selectedReviewer.id &&
                currentUser.id === selectedReviewer.id &&
                ' (Me)'}
            </React.Fragment>
          )}
        </p>
      ),
      edit: !reviewer || currentUser.id !== reviewer.id ? ReviewerEdit : null,
      error: !selectedReviewer && (
        <ErrorMessage>
          <Icon color="currentColor" size={2}>
            alert_circle
          </Icon>
          Reviewer must be indicated.
        </ErrorMessage>
      ),
    }

    const sections = [citationInformation, manuscriptFiles, reviewerSelect]

    const highlightTerms = {
      title: 'Referenced attachments',
      content: <SubmitHighlights highlights={highlights} />,
      edit: '',
      error: '',
    }

    const DOIcheck = {
      title: 'DOI check',
      content: <CrossRefCheck doi={doi && doi.id} />,
      edit: '',
      error: '',
    }

    if (currentUser.admin && status === 'submitted') {
      sections.splice(2, 0, highlightTerms)
    }

    if (!preprint) {
      sections.splice(1, 0, fundingInfo)
    }

    if (currentUser.admin && status === 'submitted' && doi && !pmid) {
      sections.splice(1, 0, DOIcheck)
    }

    return sections
  }
}

export default submitSections
