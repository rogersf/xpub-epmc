import React from 'react'
import { withRouter } from 'react-router'
import { Query } from 'react-apollo'
import { omit } from 'lodash'
import { createBrowserHistory } from 'history'
import styled, { createGlobalStyle } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Icon, Action, Button, H2 } from '@pubsweet/ui'
import { UserContext } from '../App'
import {
  Page,
  Buttons,
  InfoPanel,
  Loading,
  LoadingIcon,
  Notification,
  SplitPage,
  StepPanel,
  BoldCheck,
} from '../ui/'
import PubMedSearch, {
  PreprintSearch,
  FullTextChecks,
} from '../citation-search'
import UploadFiles, { SubmissionTypes } from '../upload-files'
import SubmissionHeader from '../SubmissionHeader'
import {
  NoteMutations,
  ManuscriptMutations,
  WithFunders,
} from '../SubmissionMutations'
import SubmissionCancel from '../SubmissionCancel'
import { GET_MANUSCRIPT } from '../operations'
import GrantSearch, { AccessSelect, mailFunders } from '../grant-search'
import SelectReviewer from '../SelectReviewer'
import Citation from '../Citation'
import CreatePageHeader from './CreateHeader'
import CreateInfo from './CreateInfo'
import { isJournalSubscribedToPMC } from '../../../server/utils/JournalUtil'

const FadeIn = createGlobalStyle`
  @keyframes fadeIn {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }
  .createInfo {
    opacity: 1;
    animation-name: fadeIn;
    animation-delay: 0s;
    animation-duration: .75s;
  }
`
const Confirm = styled.div`
  display: flex;
  align-items: flex-start;
  & > p {
    margin-top: ${th('gridUnit')};
  }
  & > span {
    margin-left: calc(${th('gridUnit')} * 2);
    color: ${th('colorSuccess')};
  }
`
const PubMedWithMutations = NoteMutations(PubMedSearch)
const PreprintWithMutations = NoteMutations(PreprintSearch)

export const ExitButton = ({ callback, deleteMan }) => (
  <Button onClick={() => deleteMan(callback)} primary>
    End submission
  </Button>
)

export const LinkGrantButton = ({ setStatus, id, history }) => (
  <Button
    onClick={() => {
      setStatus('link-existing', () => {
        history.push(`/submission/${id}/submit`)
      })
    }}
    primary
  >
    Submit grant links
  </Button>
)

class Created extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      currentStep: this.getCurrentStep(),
      showSearch: false,
      checked: false,
      showInfo: false,
      error: '',
      cancel: false,
      newReviewer: null,
      isPlanS: false,
      funderMailer: null,
      cancelCheck: null,
      inEPMC: false,
    }
  }
  static getDerivedStateFromProps(props, state) {
    const { isPlanS, currentStep } = state
    const { meta, journal, decision } = props.manuscript
    const { articleIds } = meta
    const pmcid =
      articleIds && articleIds.find(aid => aid.pubIdType === 'pmcid')
    const pmid = articleIds && articleIds.find(aid => aid.pubIdType === 'pmid')
    const obj = {}
    if (currentStep > 1) {
      obj.cancelCheck =
        (!isPlanS || decision === 'extant') &&
        (pmcid ||
          (pmid &&
            journal &&
            journal.meta.pmcStatus &&
            isJournalSubscribedToPMC(journal.meta.pmcStatus)))
    }
    return obj
  }
  componentDidMount() {
    const { notes } = this.props.manuscript.meta
    const planS = notes && notes.find(n => n.notesType === 'planS')
    this.setState({ isPlanS: !!planS })
  }
  componentDidUpdate(prevProps, prevState) {
    const { currentStep, isPlanS, funderMailer } = this.state
    if (currentStep === 2 && isPlanS && !funderMailer) {
      this.getMailer()
    }
    if (prevState.currentStep === 2 && currentStep !== 2) {
      this.getMailer(true)
    }
  }
  static contextType = UserContext
  getCurrentStep = () => {
    const { meta, files } = this.props.manuscript
    const planS = meta.notes && meta.notes.find(n => n.notesType === 'planS')
    const test1 = meta.fundingGroup && meta.fundingGroup.length > 0
    const test2 = meta.releaseDelay || (planS && planS.content)
    const test3 = files && files.length > 0
    if (test1 && test2 && test3) {
      return 3
    } else if (test1 && test2) {
      return 2
    } else if (test1) {
      return 1
    }
    return 0
  }
  getMailer = reset => {
    if (reset) {
      this.setState({ funderMailer: null })
    } else {
      const [iden] = this.props.currentUser.identities
      const { funders, manuscript } = this.props
      const { fundingGroup: grants, title } = manuscript.meta
      const funderData = { funders, grants, title, username: iden.name }
      this.setState({ funderMailer: mailFunders(funderData) })
    }
  }
  checkFiles = () => {
    const { checked } = this.state
    const { files } = this.props.manuscript
    const newState = {}

    // hacky way
    let hasEmptyInputs = false
    const inputs = document.querySelectorAll('input[type=text]')
    for (let i = 0; i < inputs.length; i += 1) {
      if (!inputs[i].value.trim()) {
        hasEmptyInputs = true
        break
      }
    }
    if (
      !checked &&
      files &&
      hasEmptyInputs &&
      files
        .filter(
          file =>
            !file.type || SubmissionTypes.some(t => t.value === file.type),
        )
        .some(file => !file.label || !file.type)
    ) {
      newState.checked = false
      newState.error =
        'Error: All indicated files must have a type and a label.'
    } else {
      newState.checked = !checked
      newState.error = ''
    }
    this.setState(newState)
  }
  goPrev = () => {
    const preprint =
      this.props.manuscript.organization.name === 'Europe PMC Preprints'
    const newState = { nextDisabled: false }
    if (preprint && this.state.currentStep === 3) {
      newState.currentStep = 0
    } else if (this.state.currentStep > 0) {
      newState.currentStep = this.state.currentStep - 1
    } else {
      newState.showSearch = true
      newState.cancelCheck = null
    }
    this.setState(newState)
    window.scrollY = 0
    window.pageYOffset = 0
    document.scrollingElement.scrollTop = 0
  }
  goNext = () => {
    const preprint =
      this.props.manuscript.organization.name === 'Europe PMC Preprints'
    const newState = { nextDisabled: false }
    if (this.state.showSearch) {
      newState.showSearch = false
    } else if (preprint && this.state.currentStep === 0) {
      newState.currentStep = 3
    } else if (this.state.currentStep < 4) {
      newState.currentStep = this.state.currentStep + 1
    } else {
      const { id } = this.props.manuscript
      this.props.setStatus('READY', () => {
        this.props.history.push(`/submission/${id}/submit`)
      })
    }
    this.setState(newState)
    window.scrollY = 0
    window.pageYOffset = 0
    document.scrollingElement.scrollTop = 0
  }
  async changeCitation(citationData) {
    const {
      data: { updateManuscript },
    } = await this.props.changeCitation(citationData)
    if (updateManuscript.message) {
      this.setState({ error: updateManuscript.message }, () => {
        window.scrollY = 0
        window.pageYOffset = 0
        document.scrollingElement.scrollTop = 0
        setTimeout(() => this.setState({ error: '', showSearch: false }), 4000)
      })
    } else {
      this.setState({ showSearch: false })
    }
  }
  render() {
    const currentUser = this.context
    const {
      currentStep,
      error,
      isPlanS,
      checked,
      newReviewer,
      cancelCheck,
      inEPMC,
    } = this.state
    const { manuscript, history, changeNote, deleteNote, newNote } = this.props
    const { meta, journal, files: allfiles, teams, organization } = manuscript
    const preprint = organization.name === 'Europe PMC Preprints'
    const { notes, fundingGroup: grants, articleIds } = meta
    const files = allfiles
      ? allfiles.filter(
          file =>
            !file.type ||
            file.type === 'manuscript' ||
            SubmissionTypes.some(t => t.value === file.type),
        )
      : []
    const pdfSend = allfiles && allfiles.find(f => f.type === 'pdf4load')
    const fundingGroup = grants
      ? grants.map(g => {
          const n = omit(g, '__typename')
          n.pi = omit(g.pi, '__typename')
          return n
        })
      : []
    const reviewerNote = notes
      ? notes.find(n => n.notesType === 'selectedReviewer')
      : null
    const planS = notes && notes.find(n => n.notesType === 'planS')
    const submitter =
      teams && teams.find(team => team.role === 'submitter')
        ? teams.find(team => team.role === 'submitter').teamMembers[0]
        : null
    const pmid = articleIds && articleIds.find(aid => aid.pubIdType === 'pmid')
    const pmcid =
      articleIds && articleIds.find(aid => aid.pubIdType === 'pmcid')
    let { nextDisabled } = this.state
    if (currentStep === 4 && !newReviewer) {
      nextDisabled = true
    } else if (currentStep === 3) {
      if (!files || !this.state.checked) {
        nextDisabled = true
      } else if (
        files &&
        files
          .filter(
            file =>
              !file.type || SubmissionTypes.some(t => t.value === file.type),
          )
          .some(file => !file.label || !file.type)
      ) {
        nextDisabled = true
      }
    } else if (currentStep === 2 && !meta.releaseDelay) {
      nextDisabled = true
    } else if (currentStep === 1) {
      if (!meta.fundingGroup || !meta.fundingGroup.length) {
        nextDisabled = true
      }
    }
    const CancelSub = () => (
      <SubmissionCancel
        callback={() => history.push('/')}
        manuscriptId={manuscript.id}
        showComponent={ExitButton}
      />
    )
    if (manuscript.status === 'INITIAL') {
      return (
        <SplitPage withHeader>
          <StepPanel>
            <div>
              <CreatePageHeader
                currentStep={currentStep}
                preprint={preprint}
                removeLast={currentStep === 2 && cancelCheck}
              />
              {currentStep === 0 && <H2>Citation information</H2>}
              {currentStep === 3 && <H2>Files</H2>}
              {error && <Notification type="error">{error}</Notification>}
              {currentStep === 0 && (
                <React.Fragment>
                  {this.state.showSearch ? (
                    <React.Fragment>
                      {preprint ? (
                        <PreprintWithMutations
                          citationData={v => this.changeCitation(v)}
                          manuscript={manuscript}
                          toggle
                        />
                      ) : (
                        <PubMedWithMutations
                          changeInfo={v => this.setState(v)}
                          citationData={v => this.changeCitation(v)}
                          endSubmission={CancelSub}
                          manuscript={manuscript}
                          toggle
                        />
                      )}
                    </React.Fragment>
                  ) : (
                    <Confirm>
                      <Citation
                        journal={journal}
                        metadata={meta}
                        version={manuscript.version}
                      />
                      <Icon color="currentColor" size={5}>
                        check
                      </Icon>
                    </Confirm>
                  )}
                </React.Fragment>
              )}
              {currentStep === 1 && (
                <GrantSearch
                  changedGrants={this.props.updateGrants}
                  decision={manuscript.decision}
                  deleteNote={async v => {
                    await deleteNote(v)
                    await this.props.updateEmbargo(null)
                    this.setState({ isPlanS: false })
                  }}
                  disableNext={v => this.setState({ nextDisabled: v })}
                  funders={this.props.funders}
                  isPlanS={isPlanS}
                  newNote={async v => {
                    await newNote(v)
                    await this.props.updateEmbargo(null)
                    this.setState({ isPlanS: true })
                  }}
                  planS={planS}
                  selectedGrants={fundingGroup}
                />
              )}
              {currentStep === 2 && (
                <React.Fragment>
                  {cancelCheck ? (
                    <FullTextChecks
                      fundingGroup={fundingGroup}
                      inEPMC={v => this.setState({ inEPMC: v })}
                      journal={journal}
                      pmcid={pmcid && pmcid.id}
                      pmid={pmid && pmid.id}
                      title={manuscript.meta.title}
                    />
                  ) : (
                    <AccessSelect
                      changedEmbargo={this.props.updateEmbargo}
                      exempt={planS && planS.content}
                      funderMailer={this.state.funderMailer}
                      grants={fundingGroup}
                      isPlanS={isPlanS}
                      openMailer={this.props.openMailer}
                      selectedEmbargo={meta.releaseDelay}
                    />
                  )}
                </React.Fragment>
              )}
              {currentStep === 3 && (
                <React.Fragment>
                  <UploadFiles
                    checked={checked}
                    files={files}
                    manuscript={manuscript.id}
                    pdfSend={pdfSend}
                    types={SubmissionTypes}
                    version={manuscript.version}
                  />
                  {files.length > 0 && (
                    <React.Fragment>
                      <p>
                        All files that are part of the manuscript, including
                        figures, tables, appendices, and all supplemental files,
                        must be provided. The processing of the manuscript will
                        be delayed if any of the manuscript files are missing.
                      </p>
                      <BoldCheck
                        checked={checked}
                        label="I have provided all required manuscript files."
                        onChange={() => this.checkFiles()}
                      />
                    </React.Fragment>
                  )}
                </React.Fragment>
              )}
              {currentStep === 4 && (
                <SelectReviewer
                  currentUser={currentUser}
                  funding={fundingGroup}
                  reviewer={null}
                  reviewerNote={reviewerNote}
                  setReviewerNote={v => this.setState({ newReviewer: v })}
                  submitter={submitter}
                />
              )}
              <Buttons>
                <Buttons right>
                  {currentStep === 2 && cancelCheck ? (
                    <LinkGrantButton
                      history={this.props.history}
                      id={this.props.manuscript.id}
                      setStatus={this.props.setStatus}
                    />
                  ) : (
                    <Button
                      disabled={nextDisabled}
                      onClick={async () => {
                        if (currentStep === 4) {
                          if (reviewerNote && newReviewer) {
                            await changeNote({
                              id: reviewerNote.id,
                              notesType: 'selectedReviewer',
                              content: JSON.stringify(newReviewer),
                            })
                          } else {
                            await newNote({
                              notesType: 'selectedReviewer',
                              content: JSON.stringify(newReviewer),
                            })
                          }
                        }
                        this.goNext()
                      }}
                      primary={!this.state.showSearch}
                      title={
                        this.state.showSearch
                          ? 'Click to proceed with previously selected citation'
                          : ''
                      }
                    >
                      {(this.state.showSearch && 'Cancel citation change') ||
                        (currentStep === 4 && 'Save & preview') ||
                        'Next'}
                    </Button>
                  )}
                  {!this.state.showSearch && (
                    <Button onClick={this.goPrev}>
                      {currentStep === 0 ? 'Change citation' : 'Previous'}
                    </Button>
                  )}
                </Buttons>
                <Buttons left>
                  <Action
                    onClick={() => this.setState({ cancel: true })}
                    style={{ display: 'inline-flex', alignItems: 'center' }}
                  >
                    <Icon color="currentColor" size={2.5}>
                      trash-2
                    </Icon>
                    Cancel submission
                  </Action>
                </Buttons>
              </Buttons>
            </div>
          </StepPanel>
          <InfoPanel>
            <FadeIn />
            <div
              className={`hidden ${this.state.showInfo && 'showInfo'}`}
              id="more-info"
              onClick={() => this.setState({ showInfo: !this.state.showInfo })}
            >
              More Info
              <Icon>{`chevron-${this.state.showInfo ? 'down' : 'right'}`}</Icon>
            </div>
            <CreateInfo
              cancelCheck={cancelCheck}
              currentStep={currentStep}
              funderMailer={this.state.funderMailer}
              inEPMC={cancelCheck && inEPMC && pmcid && pmcid.id}
              openMailer={this.props.openMailer}
              planS={planS}
              preprint={preprint}
            />
          </InfoPanel>
          {this.state.cancel && (
            <SubmissionCancel
              callback={() => this.props.history.push('/')}
              close={() => this.setState({ cancel: false })}
              manuscriptId={manuscript.id}
            />
          )}
        </SplitPage>
      )
    }
    history.push(`/submission/${manuscript.id}/submit`)
    return null
  }
}

const CreatedWithMutatons = ManuscriptMutations(
  NoteMutations(WithFunders(Created)),
)
const CreatedWithHeader = SubmissionHeader(CreatedWithMutatons)

const CreatePage = ({ match, ...props }) => (
  <Query
    fetchPolicy="cache-and-network"
    query={GET_MANUSCRIPT}
    variables={{ id: match.params.id }}
  >
    {({ data, loading }) => {
      if (loading) {
        return (
          <Page>
            <Loading>
              <LoadingIcon />
            </Loading>
          </Page>
        )
      }
      if (!data) {
        createBrowserHistory({ forceRefresh: true }).go()
        return null
      }
      if (!data.manuscript) {
        createBrowserHistory().push('/')
        return null
      }
      return (
        <CreatedWithHeader
          manuscript={data.manuscript}
          match={match}
          saved={new Date()}
          {...props}
        />
      )
    }}
  </Query>
)

export default withRouter(CreatePage)
