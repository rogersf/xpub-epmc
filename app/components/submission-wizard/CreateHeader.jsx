import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Steps, H1 } from '@pubsweet/ui'

const Title = styled(H1)`
  text-align: center;
`
const Progress = styled.div`
  margin: 0 auto;
  padding: 0 calc(${th('gridUnit')} * 6) calc(${th('gridUnit')} * 9);

  div {
    min-width: 0 !important;
  }
`
const Header = ({ currentStep, preprint, removeLast }) => (
  <div>
    <Title>Start a new submission</Title>
    <Progress>
      {preprint ? (
        <Steps currentStep={currentStep > 0 ? currentStep - 2 : 0}>
          <Steps.Step title="Citation" />
          <Steps.Step title="Files" />
          <Steps.Step title="Reviewer" />
        </Steps>
      ) : (
        <Steps currentStep={currentStep}>
          <Steps.Step title="Citation" />
          <Steps.Step title="Funding" />
          <Steps.Step title="Access" />
          {!removeLast && <Steps.Step title="Files" />}
          {!removeLast && <Steps.Step title="Reviewer" />}
        </Steps>
      )}
    </Progress>
  </div>
)

export default Header
