import React from 'react'
import { omit } from 'lodash'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Button, Steps } from '@pubsweet/ui'
import { Buttons, Notification } from '../ui/'
import { NoteMutations, WithFunders } from '../SubmissionMutations'
import GrantSearch, { AccessSelect, mailFunders } from '../grant-search'
import { FullTextChecks } from '../citation-search'
import { LinkGrantButton } from './CreatePage'

const Progress = styled.div`
  margin: calc(${th('gridUnit')} * -6) auto calc(${th('gridUnit')} * 6);
  max-width: 250px;
  div {
    min-width: 0 !important;
  }
`

class EditFundingAccess extends React.Component {
  state = {
    step: 1,
    isPlanS: false,
    inEPMC: false,
    newNote: null,
    deleteNote: null,
    updateGrants: undefined,
    deleteEmbargo: false,
    funderMailer: null,
    nextDisabled: false,
    cancelCheck: false,
  }
  static getDerivedStateFromProps(props, state) {
    const { isPlanS } = state
    const { meta, journal } = props.manuscript
    const { articleIds } = meta
    const pmcid =
      articleIds && articleIds.find(aid => aid.pubIdType === 'pmcid')
    const pmid = articleIds && articleIds.find(aid => aid.pubIdType === 'pmid')
    const cancelCheck =
      !isPlanS && (pmcid || (pmid && journal && journal.meta.pmcStatus))
    return { cancelCheck }
  }
  componentDidMount() {
    const { notes } = this.props.manuscript.meta
    const planS = notes && notes.find(n => n.notesType === 'planS')
    this.setState({ isPlanS: !!planS })
  }
  getMailer = () => {
    const [iden] = this.props.currentUser.identities
    const { manuscript, funders } = this.props
    const { fundingGroup, title } = manuscript.meta
    const grants = this.state.updateGrants || fundingGroup || []
    const funderData = { funders, grants, title, username: iden.name }
    const funderMailer = mailFunders(funderData)
    this.setState({ funderMailer })
    this.props.addMailer(funderMailer)
  }
  changeStep = v => {
    this.setState({ step: v })
    const { cancelCheck, inEPMC } = this.state
    const { articleIds } = this.props.manuscript.meta
    const pmcid =
      articleIds && articleIds.find(aid => aid.pubIdType === 'pmcid')
    this.props.changeInfo({
      currentStep: v,
      cancelCheck,
      inEPMC: inEPMC && pmcid.id,
    })
  }
  async saveSelections() {
    const checkArray = ['newNote', 'deleteNote', 'updateGrants']
    await checkArray.reduce(async (promise, func) => {
      await promise
      const funcVal = this.state[func]
      if (funcVal) {
        await this.props[func](funcVal)
      }
      return Promise.resolve()
    }, Promise.resolve())

    if (this.state.deleteEmbargo) {
      await this.props.updateEmbargo(null)
    }

    this.getMailer()
  }
  render() {
    const {
      step,
      isPlanS,
      funderMailer,
      updateGrants,
      cancelCheck,
      deleteEmbargo,
    } = this.state
    const {
      manuscript,
      openMailer,
      updateEmbargo,
      history,
      currentUser,
    } = this.props
    const { journal, meta, status } = manuscript
    const {
      articleIds,
      releaseDelay,
      title,
      fundingGroup: grants,
      notes,
    } = meta
    const fundingGroup = grants
      ? grants.map(g => {
          const n = omit(g, '__typename')
          n.pi = omit(g.pi, '__typename')
          return n
        })
      : []
    const planS = notes && notes.find(n => n.notesType === 'planS')
    const pmid = articleIds && articleIds.find(aid => aid.pubIdType === 'pmid')
    const pmcid =
      articleIds && articleIds.find(aid => aid.pubIdType === 'pmcid')
    let { nextDisabled } = this.state
    if (step === 1) {
      if (updateGrants ? !updateGrants.length : !fundingGroup.length) {
        nextDisabled = true
      }
    } else if (!releaseDelay) {
      nextDisabled = true
    }
    return (
      <React.Fragment>
        <Progress>
          <Steps currentStep={step - 1}>
            <Steps.Step title="Funding" />
            <Steps.Step title="Access" />
          </Steps>
        </Progress>
        {step === 1 && (
          <GrantSearch
            changedGrants={v => this.setState({ updateGrants: v })}
            deleteNote={v =>
              this.setState({
                deleteNote: v,
                newNote: null,
                deleteEmbargo: true,
                isPlanS: false,
              })
            }
            disableNext={v => this.setState({ nextDisabled: v })}
            funders={this.props.funders}
            isPlanS={isPlanS}
            newNote={v =>
              this.setState({
                newNote: v,
                deleteNote: null,
                deleteEmbargo: true,
                isPlanS: true,
              })
            }
            planS={planS}
            selectedGrants={fundingGroup}
          />
        )}
        {step === 2 && (
          <React.Fragment>
            {cancelCheck ? (
              <FullTextChecks
                fundingGroup={fundingGroup}
                inEPMC={v => this.setState({ inEPMC: v })}
                journal={journal}
                pmcid={pmcid && pmcid.id}
                pmid={pmid && pmid.id}
                title={title}
              />
            ) : (
              <AccessSelect
                changedEmbargo={updateEmbargo}
                exempt={planS && planS.content}
                funderMailer={funderMailer}
                grants={fundingGroup}
                isPlanS={isPlanS}
                openMailer={openMailer}
                selectedEmbargo={!deleteEmbargo && releaseDelay}
              />
            )}
            {currentUser.admin &&
              status === 'submitted' &&
              isPlanS &&
              !releaseDelay && (
                <Notification type="info">
                  {`Plan S status changed. Missing embargo has been auto-selected in Report Errors`}
                </Notification>
              )}
          </React.Fragment>
        )}
        <Buttons>
          <Buttons right>
            {step === 2 && cancelCheck ? (
              <LinkGrantButton
                history={history}
                id={manuscript.id}
                setStatus={this.props.setStatus}
              />
            ) : (
              <Button
                disabled={nextDisabled}
                onClick={async () => {
                  if (step === 1) {
                    await this.saveSelections()
                    this.changeStep(2)
                  } else {
                    this.props.close()
                  }
                }}
                primary
              >
                {step === 1 ? (
                  <React.Fragment>
                    {currentUser.admin ? 'Save grant selections' : 'Next'}
                  </React.Fragment>
                ) : (
                  'Save'
                )}
              </Button>
            )}
            {step === 2 && (
              <Button onClick={() => this.changeStep(1)}>Previous</Button>
            )}
          </Buttons>
          {currentUser.admin && (
            <Button onClick={() => this.props.close()}>
              {step === 1 ? 'Cancel' : 'Exit'}
            </Button>
          )}
        </Buttons>
      </React.Fragment>
    )
  }
}

export default NoteMutations(WithFunders(EditFundingAccess))
