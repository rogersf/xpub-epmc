import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Button, H2, H3, Icon } from '@pubsweet/ui'
import {
  SplitPage,
  StepPanel,
  InfoPanel,
  Buttons,
  CloseModal,
  Notification,
} from '../ui'
import UploadFiles, { SubmissionTypes } from '../upload-files'
import PubMedSearch, { PreprintSearch } from '../citation-search'
import { NoteMutations } from '../SubmissionMutations'
import Citation from '../Citation'
import CreateInfo from './CreateInfo'

const PubMedWithMutations = NoteMutations(PubMedSearch)
const PreprintWithMutations = NoteMutations(PreprintSearch)

export const EditCitation = ({ manuscript, citationError, ...props }) => {
  const { journal, meta, version, organization } = manuscript
  const preprint = organization.name === 'Europe PMC Preprints'
  if (citationError) {
    window.scrollY = 0
    window.pageYOffset = 0
    document.scrollingElement.scrollTop = 0
  }
  return (
    <div>
      {citationError && (
        <Notification type="error">{citationError}</Notification>
      )}
      {!props.cancelCheck && (
        <React.Fragment>
          <H2>Citation information</H2>
          <H3>Citation to replace:</H3>
          <Citation journal={journal} metadata={meta} version={version} />
        </React.Fragment>
      )}
      {preprint ? (
        <PreprintWithMutations manuscript={manuscript} {...props} />
      ) : (
        <PubMedWithMutations manuscript={manuscript} {...props} />
      )}
      <Buttons right>
        <Button onClick={() => props.close()}>Cancel</Button>
      </Buttons>
    </div>
  )
}

export const EditFiles = ({ manuscript, ...props }) => {
  const { files: allfiles } = manuscript
  const files = allfiles
    ? allfiles.filter(
        file =>
          !file.type ||
          file.type === 'manuscript' ||
          SubmissionTypes.some(t => t.value === file.type),
      )
    : []
  const pdfSend = allfiles.find(f => f.type === 'pdf4load')
  return (
    <div>
      <H2>Files</H2>
      <UploadFiles
        files={files}
        manuscript={manuscript.id}
        pdfSend={pdfSend}
        types={SubmissionTypes}
        version={manuscript.version}
        {...props}
      />
      <Buttons>
        <Button onClick={() => props.close()} primary>
          Save
        </Button>
      </Buttons>
    </div>
  )
}

const CloseEdit = styled(CloseModal)`
  margin: calc(${th('gridUnit')} * 6) auto calc(${th('gridUnit')} * 2);
`

export const ErrorMessage = styled.p`
  color: ${th('colorError')};
  font-size: ${th('fontSizeBaseSmall')};
  display: flex;
  align-items: flex-start;
  margin-left: calc(-${th('gridUnit')} / 2);
`

const SubmitEdit = BaseComponent =>
  class extends React.Component {
    state = {
      currentStep: this.props.currentStep,
      funderMailer: null,
      cancelCheck: false,
      inEPMC: false,
    }
    render() {
      const {
        close,
        error,
        manuscript,
        currentUser,
        openMailer,
        citationError,
        ...props
      } = this.props
      const { currentStep, funderMailer, cancelCheck, inEPMC } = this.state
      const { meta, organization } = manuscript
      const { notes } = meta
      const preprint = organization.name === 'Europe PMC Preprints'
      const planS = notes && notes.find(n => n.notesType === 'planS')
      return (
        <SplitPage>
          <StepPanel>
            <div>
              <CloseEdit onClick={() => close()} />
              <BaseComponent
                addMailer={v => this.setState({ funderMailer: v })}
                cancelCheck={cancelCheck}
                changeInfo={v => this.setState(v)}
                checked
                citationError={citationError}
                close={close}
                currentUser={currentUser}
                manuscript={manuscript}
                openMailer={openMailer}
                {...props}
              />
              {error && (
                <ErrorMessage>
                  <Icon color="currentColor" size={2}>
                    alert_circle
                  </Icon>
                  {error}
                </ErrorMessage>
              )}
            </div>
          </StepPanel>
          <InfoPanel>
            <CreateInfo
              cancelCheck={cancelCheck}
              currentStep={currentStep}
              funderMailer={funderMailer}
              inEPMC={cancelCheck && inEPMC}
              openMailer={openMailer}
              planS={planS}
              preprint={preprint}
            />
          </InfoPanel>
        </SplitPage>
      )
    }
  }

export default SubmitEdit
