import { states, epmcOrcidWebhookToolUrl } from 'config'
import React from 'react'
import { Action, Icon, Button, H1, H2, H3 } from '@pubsweet/ui'
import {
  A,
  B,
  Buttons,
  Callout,
  SplitPage,
  InfoPanel,
  StepPanel,
  Notification,
} from '../ui'
import { SubmissionTypes, FileThumbnails } from '../upload-files'
import { NoteMutations } from '../SubmissionMutations'
import Citation from '../Citation'
import PreprintSelections from '../PreprintSelections'
import OrcidClaim from '../orcid-claim'
import CreateInfo from './CreateInfo'

class SubmitComplete extends React.Component {
  state = {
    success: null,
    error: null,
  }
  render() {
    const { currentUser, cancel, manuscript, history, openMailer } = this.props

    const {
      status,
      meta,
      files,
      journal,
      teams,
      version,
      organization,
    } = manuscript

    const { articleIds, fundingGroup, releaseDelay, notes, title } = meta
    const claimed = notes && notes.some(n => n.notesType === 'orcidClaim')

    const workExternalIdentifiers =
      articleIds &&
      articleIds.reduce((acc, curr) => {
        if (!['pmcid', 'pre-pmc'].includes(curr.pubIdType)) {
          if (curr.pubIdType === 'pprid') curr.pubIdType = 'ppr'
          const obj = {
            workExternalIdentifierType: curr.pubIdType,
            workExternalIdentifierId: curr.id,
          }
          acc.push(obj)
        }
        return acc
      }, [])

    const showFiles = files
      ? files.filter(
          file =>
            !file.type ||
            file.type === 'manuscript' ||
            SubmissionTypes.some(t => t.value === file.type),
        )
      : []
    const preprint = organization.name === 'Europe PMC Preprints'
    const term = preprint ? 'preprint' : 'submission'
    const orcidObject = {
      title,
      workType: preprint ? 'preprint' : 'journal-article',
      workExternalIdentifiers,
    }

    const submitter =
      teams && teams.find(team => team.role === 'submitter')
        ? teams.find(team => team.role === 'submitter').teamMembers[0]
        : null
    const reviewerNote = notes
      ? notes.find(n => n.notesType === 'selectedReviewer')
      : null
    let reviewer = null
    if (teams && teams.find(team => team.role === 'reviewer')) {
      const [rev] = teams.find(team => team.role === 'reviewer').teamMembers
      reviewer = {
        id: rev.user.id,
        name: rev.alias.name,
      }
    }
    const selectedReviewer = reviewerNote
      ? JSON.parse(reviewerNote.content)
      : reviewer
    const seperateReviewer =
      selectedReviewer &&
      (!selectedReviewer.id ||
        (selectedReviewer.id && selectedReviewer.id !== submitter.user.id))
    const otherUserRole =
      selectedReviewer &&
      selectedReviewer.id &&
      currentUser.id === selectedReviewer.id
        ? 'submitter'
        : 'reviewer'
    const curr = states.indexOf(status)
    const done = states.indexOf('xml-complete')
    const sent = states.indexOf('repo-ready')
    const subState =
      (['submitted', 'in-review', 'xml-review'].includes(status) && 'review') ||
      (['being-withdrawn', 'withdrawal-triage'].includes(status) &&
        'withdraw') ||
      'process'
    const doi = articleIds && articleIds.find(aid => aid.pubIdType === 'doi')

    return (
      <SplitPage withHeader>
        <StepPanel>
          <div>
            <H1>{preprint ? 'Summary' : 'Thank you for your submission'}</H1>
            <p>
              {`The ${term} `}
              {curr >= done ? (
                <React.Fragment>
                  {status === 'link-existing' ? (
                    'is complete and grant links have been submitted. '
                  ) : (
                    <React.Fragment>
                      {preprint
                        ? 'is approved for display. '
                        : 'is complete and accepted for archiving. '}
                    </React.Fragment>
                  )}
                  {curr === 'xml-complete' &&
                    'The release of your submission full text is dependant on final publication of your article and the availability of full citation details, including volume. '}
                  {`If you have any questions, or require changes to be made, please `}
                  <Action onClick={() => openMailer(true)}>
                    contact the Helpdesk
                  </Action>
                  .
                </React.Fragment>
              ) : (
                <React.Fragment>
                  {subState === 'process' && 'is being processed.'}
                  {subState === 'withdraw' &&
                    'is being withdrawn from Europe PMC and PubMed Central (PMC).'}
                  {subState === 'review' && (
                    <React.Fragment>
                      {`will now be reviewed by `}
                      {seperateReviewer && otherUserRole === 'reviewer'
                        ? `the ${otherUserRole} and `
                        : ''}
                      {`the Europe PMC Helpdesk team.`}
                    </React.Fragment>
                  )}
                  {` You `}
                  {seperateReviewer ? `and the ${otherUserRole} ` : ''}
                  {`will receive email updates as it is processed. You can also log into Europe PMC plus at any time to check the status of your ${term}.`}
                </React.Fragment>
              )}
            </p>
            {otherUserRole === 'submitter' && doi && (
              <Callout hue>
                <H3>How do I get credit for my work?</H3>
                <p>
                  {`You can link the submission to your ORCID iD. An ORCID iD makes it easier for authors to get recognition for their work, by distinguishing themselves from other researchers. Claiming articles to an ORCID iD builds a body of articles associated with that iD. `}
                  <A href="https://orcid.org/" target="_blank">
                    Find out more about ORCID
                  </A>
                  .
                </p>
                {claimed || this.state.success ? (
                  <Notification type="success">
                    The submission has been successfully linked to your ORCID
                    iD!
                  </Notification>
                ) : (
                  <React.Fragment>
                    <p>
                      <OrcidClaim
                        citation={orcidObject}
                        errorCallback={event => {
                          this.setState({ error: true })
                        }}
                        successCallback={async data => {
                          await fetch(
                            `${epmcOrcidWebhookToolUrl}/${data.orcId}/EPMC`,
                          )
                          await this.props.newNote({
                            notesType: 'orcidClaim',
                            content: data.orcId,
                          })
                          this.setState({ success: true })
                        }}
                      />
                    </p>
                    <div>
                      {this.state.error && (
                        <Notification type="warning">
                          There was an issue linking the submission to your
                          ORCID iD. Please make another attempt, and contact the
                          Helpdesk if it continues to fail.
                        </Notification>
                      )}
                    </div>
                  </React.Fragment>
                )}
              </Callout>
            )}
            <H2>{preprint ? 'Preprint' : 'Submission'} details</H2>
            <H3>Citation</H3>
            <Citation journal={journal} metadata={meta} version={version} />
            {preprint ? (
              <React.Fragment>
                <H3>Settings</H3>
                <PreprintSelections manuscript={manuscript} />
              </React.Fragment>
            ) : (
              <React.Fragment>
                {showFiles.length > 0 && (
                  <React.Fragment>
                    <H3>Files</H3>
                    <FileThumbnails files={showFiles} />
                  </React.Fragment>
                )}
                <H3>Funding</H3>
                {fundingGroup && fundingGroup.length > 0 && (
                  <p>
                    <B>Grants: </B>
                    {fundingGroup &&
                      fundingGroup.length > 0 &&
                      fundingGroup.map((f, t) => (
                        <span key={f.awardId}>
                          {`${f.fundingSource} ${f.awardId} `}({f.pi.surname})
                          {t !== fundingGroup.length - 1 && ', '}
                        </span>
                      ))}
                  </p>
                )}
                {(releaseDelay || typeof releaseDelay === 'number') && (
                  <p>
                    <B>Embargo: </B>
                    {`${releaseDelay} month${
                      parseInt(releaseDelay, 10) === 1 ? '' : 's'
                    }`}
                  </p>
                )}
              </React.Fragment>
            )}
            <p style={{ textAlign: 'right' }}>
              {`If you require any changes to the submission, please `}
              <Action onClick={() => openMailer(true)}>
                contact the Helpdesk
              </Action>
              .
            </p>
            <Buttons>
              <Button onClick={() => history.push('/')} primary>
                Done
              </Button>
              {submitter &&
                submitter.user.id === currentUser.id &&
                curr < sent && (
                  <Action
                    onClick={() => cancel()}
                    style={{ display: 'inline-flex', alignItems: 'center' }}
                  >
                    <Icon color="currentColor" size={2.5}>
                      trash-2
                    </Icon>
                    Cancel submission
                  </Action>
                )}
            </Buttons>
          </div>
        </StepPanel>
        <InfoPanel>
          <CreateInfo
            currentStep={5}
            doi={doi}
            inEPMC={
              articleIds &&
              articleIds.find(aid => aid.pubIdType === 'pmcid') &&
              articleIds.find(aid => aid.pubIdType === 'pmcid').id
            }
            preprint={preprint}
            reviewer={otherUserRole === 'submitter'}
            status={status}
          />
        </InfoPanel>
      </SplitPage>
    )
  }
}

export default NoteMutations(SubmitComplete)
