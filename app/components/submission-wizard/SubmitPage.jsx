import React from 'react'
import { withRouter } from 'react-router'
import { Query } from 'react-apollo'
import { createBrowserHistory } from 'history'
import { Page, Loading, LoadingIcon } from '../ui'
import SubmissionHeader from '../SubmissionHeader'
import { ManuscriptMutations } from '../SubmissionMutations'
import { GET_MANUSCRIPT, CHECK_DUPES } from '../operations'
import Submit from './Submit'

const SubmitWithMutations = ManuscriptMutations(Submit)
const SubmitWithHeader = SubmissionHeader(SubmitWithMutations)

const DupeCheckSubmitPage = ({ manuscript, saved, ...props }) => {
  const { id, meta } = manuscript
  const { title, articleIds: aids } = meta
  const articleIds = aids ? aids.map(aid => aid.id) : null
  return (
    <Query
      fetchPolicy="cache-and-network"
      query={CHECK_DUPES}
      variables={{ id, articleIds, title }}
    >
      {({ data, loading }) => {
        if (loading || !data || !data.checkDuplicates) {
          return (
            <Page>
              <Loading>
                <LoadingIcon />
              </Loading>
            </Page>
          )
        }
        const { manuscripts } = data.checkDuplicates
        return (
          <SubmitWithHeader
            duplicates={manuscripts}
            manuscript={manuscript}
            saved={saved}
            {...props}
          />
        )
      }}
    </Query>
  )
}

const SubmitPage = ({ match, currentUser, ...props }) => (
  <Query
    fetchPolicy="cache-and-network"
    query={GET_MANUSCRIPT}
    variables={{ id: match.params.id }}
  >
    {({ data, loading }) => {
      if (loading) {
        return (
          <Loading>
            <LoadingIcon />
          </Loading>
        )
      }
      if (!data) {
        createBrowserHistory({ forceRefresh: true }).go()
        return null
      }
      if (!data.manuscript) {
        createBrowserHistory().push('/')
        return null
      }
      if (currentUser.admin) {
        return (
          <DupeCheckSubmitPage
            currentUser={currentUser}
            manuscript={data.manuscript}
            saved={new Date()}
            {...props}
          />
        )
      }
      return (
        <SubmitWithHeader
          currentUser={currentUser}
          manuscript={data.manuscript}
          saved={new Date()}
          {...props}
        />
      )
    }}
  </Query>
)

export default withRouter(SubmitPage)
