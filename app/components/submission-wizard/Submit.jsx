import React from 'react'
import styled, { withTheme } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Button, Action, CheckboxGroup, H1, H2, H3, Icon } from '@pubsweet/ui'
import {
  Buttons,
  CloseModal,
  PreviewPage,
  PreviewFooter,
  PreviewPanel,
  EditPanel,
  PanelHeader,
  PanelContent,
  SectionContent as Content,
  SectionHeader as Header,
  Portal,
  Notification,
  Page,
  NoClick,
  Fieldset,
} from '../ui'
import ManuscriptPreview from '../ManuscriptPreview'
import SubmissionCancel from '../SubmissionCancel'
import ResolveDuplicates from '../ResolveDuplicates'
import Mailer from '../mailer'
import SubmitText from './SubmitText'
import SubmitComplete from './SubmitComplete'
import SubmitApprove from './SubmitApprove'
import submitSections from './SubmitSections'
import SubmissionErrorReport, { adminOptions } from '../SubmissionErrorReport'
import ReviewerErrorReport from './ReviewerErrorReport'

const Alert = withTheme(({ children, theme }) => (
  <Icon color={theme.colorError} size={2.75}>
    {children}
  </Icon>
))

const ErrorReport = styled.p`
  white-space: pre-wrap;
  overflow-wrap: break-word;
  font-style: italic;
  margin-top: 0;
  color: ${th('colorError')};
`
class Submit extends React.Component {
  state = {
    approve: false,
    cancel: false,
    checkedBoxes: [],
    currentStep: 0,
    editing: -1,
    highlights: '',
    error: '',
    prune: false,
    reject: false,
    mailer: false,
    message: '',
    notif: false,
  }
  static getDerivedStateFromProps(props, state) {
    const { currentUser, manuscript } = props
    let { checkedBoxes } = state
    if (currentUser.admin && manuscript.status === 'submitted') {
      if (manuscript.meta.releaseDelay) {
        checkedBoxes = checkedBoxes.filter(c => c !== 'embargo')
      } else if (!checkedBoxes.includes('embargo')) {
        checkedBoxes.push('embargo')
      }
    }
    return { checkedBoxes }
  }
  async componentDidMount() {
    this._mounted = true
    this.notifTimer = null
    const { currentUser, manuscript } = this.props
    if (
      currentUser.admin &&
      manuscript.status === 'submitted' &&
      manuscript.files &&
      manuscript.files.some(
        f =>
          f.type === 'manuscript' &&
          f.mimeType ===
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      )
    ) {
      const source = manuscript.files.find(f => f.type === 'source')
      if (source) {
        const response = await fetch(source.url)
        const html = await response.text()
        const fake = document.createElement('div')
        fake.innerHTML = html
        if (this.refdiv) {
          this.setState({ highlights: fake.textContent })
        }
      } else if (this.refdiv) {
        this.setState({ highlights: 'empty' })
      }
    }
  }
  componentWillUnmount() {
    this._mounted = false
    clearTimeout(this.notifTimer)
  }
  setRef = refdiv => {
    this.refdiv = refdiv
  }
  setResponseRef = refnote => {
    this.refnote = refnote
  }
  async changeCitation(citation) {
    const {
      data: { updateManuscript },
    } = await this.props.changeCitation(citation)
    if (updateManuscript.message) {
      this.setState({ citationError: updateManuscript.message }, () =>
        setTimeout(
          () => this.setState({ citationError: null, editing: -1 }),
          4000,
        ),
      )
    } else {
      this.setState({ editing: -1 })
    }
  }
  pruneDupes = () => this.setState({ prune: true })
  showResponse = (type = 'success', message, timeout = true) => {
    const { id, status } = this.props.manuscript
    return new Promise((resolve, reject) => {
      if (this.refnote && this._mounted) {
        window.scrollY = 0
        window.pageYOffset = 0
        document.scrollingElement.scrollTop = 0
        this.setState({ notif: { type, message } })
        if (timeout) {
          if (this.notifTimer) {
            clearTimeout(this.notifTimer)
          }
          this.notifTimer = setTimeout(() => {
            if (this._mounted) {
              if (type === 'success' && status === 'tagging') {
                this.props.history.push(`/submission/${id}/review`)
                resolve()
              } else {
                this.setState({ notif: null }, resolve())
              }
            }
          }, 3000)
        }
        resolve()
      }
    })
  }
  render() {
    const { currentUser, manuscript, duplicates: checkDupes = [] } = this.props
    const {
      meta,
      files: allfiles,
      status,
      teams,
      formState,
      journal,
      organization,
      decision,
    } = manuscript
    if (teams && allfiles) {
      const {
        editing,
        currentStep,
        highlights,
        prune,
        cancel,
        approve,
        checkedBoxes,
        reject,
        message,
        mailer,
        notif,
      } = this.state
      const sections = submitSections(
        manuscript,
        checkDupes,
        this.pruneDupes,
        currentUser,
        highlights,
      )
      const Editing = sections[editing] && sections[editing].edit
      const { notes, releaseDelay, unmatchedJournal } = meta
      const preprint = organization.name === 'Europe PMC Preprints'
      const xml = allfiles.find(f => f.type === 'PMC') || null
      let reviewer = null
      if (teams && teams.find(team => team.role === 'reviewer')) {
        const rev = teams.find(team => team.role === 'reviewer').teamMembers[0]
        reviewer = {
          id: rev.user.id,
          name: rev.alias.name,
        }
      }
      const reviewerNote = notes
        ? notes.find(n => n.notesType === 'selectedReviewer')
        : null
      const submitter =
        teams && teams.find(team => team.role === 'submitter')
          ? teams.find(team => team.role === 'submitter').teamMembers[0]
          : null
      const dupeNote = notes
        ? notes.find(n => n.notesType === 'notDuplicates')
        : null
      const notDupes = dupeNote ? JSON.parse(dupeNote.content) : []
      const duplicates = checkDupes.filter(d => !notDupes.includes(d.id))
      if (!submitter) {
        return (
          <Page withHeader>
            <Notification type="error">
              Unable to load page: Manuscript has no submitter.
            </Notification>
          </Page>
        )
      }
      const { title, givenNames, surname } = submitter.alias.name
      const submitterName = `${title && `${title} `}${givenNames} ${surname}`
      const subReviewer =
        reviewer && submitter && reviewer.id === submitter.user.id
      const reviewerAccess =
        reviewer &&
        reviewer.id === currentUser.id &&
        ['in-review', 'submission-error'].includes(status)
      const submitterAccess =
        submitter &&
        submitter.user.id === currentUser.id &&
        ['INITIAL', 'READY', 'submission-error'].includes(status)
      const adminAccess = currentUser.admin && status !== 'link-existing'
      return (
        <div ref={this.setResponseRef}>
          {notif && (
            <React.Fragment>
              <NoClick />
              <Notification fullscreen type={notif.type}>
                {!notif.message
                  ? `Submission was successfully ${
                      ['tagging', 'in-review'].includes(status)
                        ? `sent for ${
                            status === 'tagging' ? 'tagging' : 'review'
                          }`
                        : 'submitted'
                    }`
                  : notif.message}
              </Notification>
            </React.Fragment>
          )}
          {(() => {
            if (
              !adminAccess &&
              (subReviewer
                ? !submitterAccess
                : !submitterAccess && !reviewerAccess)
            ) {
              window.scrollY = 0
              window.pageYOffset = 0
              document.scrollingElement.scrollTop = 0
              return (
                <SubmitComplete
                  cancel={() => this.setState({ cancel: true })}
                  currentUser={currentUser}
                  history={this.props.history}
                  manuscript={manuscript}
                  openMailer={this.props.openMailer}
                />
              )
            } else if (editing >= 0) {
              return (
                <Editing
                  citationData={v => this.changeCitation(v)}
                  citationError={this.state.citationError}
                  close={() => this.setState({ editing: -1 })}
                  currentStep={currentStep}
                  currentUser={currentUser}
                  error={this.state.error}
                  history={this.props.history}
                  manuscript={manuscript}
                  openMailer={this.props.openMailer}
                  pruneDupes={this.pruneDupes}
                  setStatus={this.props.setStatus}
                  updateEmbargo={this.props.updateEmbargo}
                  updateGrants={this.props.updateGrants}
                />
              )
            }
            return (
              <React.Fragment>
                <PreviewPage ref={this.setRef}>
                  <PreviewPanel>
                    <div>
                      <PanelHeader>
                        <H1>Check submitted manuscript file</H1>
                      </PanelHeader>
                      <PanelContent>
                        <ManuscriptPreview
                          file={
                            allfiles.find(file => file.type === 'source') ||
                            allfiles.find(file => file.type === 'manuscript')
                          }
                          original={allfiles.find(
                            file => file.type === 'manuscript',
                          )}
                          textContent={highlights =>
                            currentUser.admin && status === 'submitted'
                              ? this.setState({ highlights })
                              : false
                          }
                        />
                      </PanelContent>
                    </div>
                  </PreviewPanel>
                  <EditPanel>
                    <div>
                      <PanelHeader>
                        <H2>Check submission details</H2>
                      </PanelHeader>
                      <PanelContent>
                        <SubmitText
                          currentUser={currentUser}
                          status={status}
                          teams={teams}
                        />
                        {status === 'submission-error' && formState && (
                          <Content>
                            <ErrorReport>{formState}</ErrorReport>
                          </Content>
                        )}
                        {sections.map((sec, i) => (
                          <React.Fragment key={sec.title}>
                            <Header error={!!sec.error}>
                              <H3>{sec.title}</H3>
                              {sec.edit && (
                                <Action
                                  id={`edit-${sec.title}`}
                                  onClick={() => {
                                    const d = preprint ? 1 : 5
                                    const a = preprint ? 3 : 2
                                    this.setState({
                                      editing: i,
                                      currentStep:
                                        (i === d && i + 2) ||
                                        (i === a && i + 1) ||
                                        i,
                                    })
                                  }}
                                  style={{
                                    display: 'inline-flex',
                                    alignItems: 'center',
                                  }}
                                >
                                  {!!sec.error && <Alert>alert_circle</Alert>}
                                  <Icon color="currentColor" size={2.5}>
                                    edit
                                  </Icon>
                                  Edit
                                </Action>
                              )}
                            </Header>
                            <Content>
                              <div>
                                {sec.content}
                                {sec.error}
                              </div>
                            </Content>
                          </React.Fragment>
                        ))}
                        {currentUser.admin &&
                          status === 'submitted' &&
                          !reviewerNote && (
                            <React.Fragment>
                              <Header>
                                <H3>Report Errors</H3>
                              </Header>
                              <Content>
                                <div>
                                  <Fieldset>
                                    <legend>
                                      {`Check the following and select anything that is missing:`}
                                    </legend>
                                    <CheckboxGroup
                                      name="admin-checklist"
                                      onChange={c =>
                                        this.setState({ checkedBoxes: c })
                                      }
                                      options={adminOptions}
                                      value={checkedBoxes}
                                    />
                                  </Fieldset>
                                </div>
                              </Content>
                            </React.Fragment>
                          )}
                      </PanelContent>
                    </div>
                  </EditPanel>
                  {duplicates.length > 0 && prune && (
                    <ResolveDuplicates
                      close={() => this.setState({ prune: false })}
                      duplicates={duplicates}
                      manuscript={manuscript}
                      note={dupeNote}
                    />
                  )}
                </PreviewPage>
                <PreviewFooter>
                  <div>
                    <div>
                      {currentUser.admin && status === 'submitted' ? (
                        <SubmitApprove
                          adminButton={reviewerNote ? 'review' : 'tagging'}
                          disabled={sections.some(sec => sec.error)}
                          manuscriptId={manuscript.id}
                          preprint={preprint}
                          showResponse={this.showResponse}
                        />
                      ) : (
                        <React.Fragment>
                          {preprint ? (
                            <SubmitApprove
                              currentUser={currentUser}
                              manuscriptId={manuscript.id}
                              preprint={preprint}
                              showResponse={this.showResponse}
                              xml={xml}
                            />
                          ) : (
                            <Button
                              disabled={sections.some(sec => sec.error)}
                              onClick={() => this.setState({ approve: true })}
                              primary
                            >
                              Submit manuscript
                            </Button>
                          )}
                        </React.Fragment>
                      )}
                      {((currentUser.admin &&
                        ['in-review', 'submitted'].includes(status)) ||
                        (reviewer &&
                          reviewer.id === currentUser.id &&
                          status === 'in-review')) && (
                        <Button onClick={() => this.setState({ reject: true })}>
                          Reject submission
                        </Button>
                      )}
                    </div>
                    {submitter &&
                      submitter.user.id === currentUser.id &&
                      decision !== 'accepted' && (
                        <Action
                          onClick={() => this.setState({ cancel: true })}
                          style={{
                            display: 'inline-flex',
                            alignItems: 'center',
                          }}
                        >
                          <Icon color="currentColor" size={2.5}>
                            trash-2
                          </Icon>
                          Cancel submission
                        </Action>
                      )}
                  </div>
                </PreviewFooter>
              </React.Fragment>
            )
          })()}
          {cancel && (
            <SubmissionCancel
              callback={() => this.props.history.push('/')}
              close={() => this.setState({ cancel: false })}
              manuscriptId={manuscript.id}
            />
          )}
          {approve && (
            <SubmitApprove
              checkedBoxes={checkedBoxes}
              close={() => this.setState({ approve: false })}
              currentUser={currentUser}
              journal={journal ? journal.meta.nlmta : unmatchedJournal}
              manuscriptId={manuscript.id}
              releaseDelay={releaseDelay}
              setChecked={c => this.setState({ checkedBoxes: c })}
              showResponse={this.showResponse}
              xml={xml}
            />
          )}
          {reject && (
            <Portal transparent>
              <CloseModal onClick={() => this.setState({ reject: false })} />
              {currentUser.admin ? (
                <React.Fragment>
                  {status === 'submitted' ? (
                    <SubmissionErrorReport
                      checkedBoxes={checkedBoxes}
                      close={() => this.setState({ reject: false })}
                      currentUser={currentUser}
                      manuscript={manuscript}
                      teams={teams}
                    />
                  ) : (
                    <React.Fragment>
                      <H2>Cancel author review</H2>
                      <p>
                        The manuscript may be sent back to the submitter stage
                        before author review&mdash;for example, if the selected
                        reviewer cannot accept the manuscript and the submitter
                        must select a new one. An email form will open for you
                        to notify the submitter.
                      </p>
                      <Buttons right>
                        <Button
                          onClick={() =>
                            this.props.setStatus(
                              'READY',
                              this.setState({ reject: false, mailer: true }),
                            )
                          }
                          primary
                        >
                          Send back to submitter
                        </Button>
                        <Button
                          onClick={() => this.setState({ reject: false })}
                        >
                          Cancel
                        </Button>
                      </Buttons>
                    </React.Fragment>
                  )}
                </React.Fragment>
              ) : (
                <ReviewerErrorReport
                  close={() => this.setState({ reject: false })}
                  manuscript={manuscript}
                  message={message}
                  onChange={message => this.setState({ message })}
                  submitter={submitterName}
                />
              )}
            </Portal>
          )}
          {mailer && (
            <Mailer
              close={() => this.setState({ mailer: false })}
              currentUser={currentUser}
              manuscript={manuscript}
              recipients={[submitter.user.id]}
            />
          )}
        </div>
      )
    }
    return null
  }
}

export default Submit
