import React from 'react'
import { ApolloConsumer } from 'react-apollo'
import styled from 'styled-components'
import moment from 'moment'
import { th } from '@pubsweet/ui-toolkit'
import { Action, Button, H3, H4, Link } from '@pubsweet/ui'
import {
  Buttons,
  Loading,
  LoadingIcon,
  Notification,
  SearchForm,
  Toggle,
  ZebraList,
  NoClick,
} from '../ui'
import { JOURNAL_INFO } from './operations'
import PubMedSearchResult from './PubMedSearchResult'
import UnmatchedCitation from './UnmatchedCitation'
import FullTextChecks from './FullTextChecks'

const LoadMore = styled(Button)`
  margin: 15px auto;
`
const Notice = styled(H4)`
  margin: 0 auto;
  & + div {
    margin-bottom: calc(${th('gridUnit')} * 3);
  }
`
const FlexP = styled.p`
  display: flex;
  align-items: center;
  & > button {
    flex: 0 0 auto;
  }
  & > span {
    flex: 1 1 50%;
    margin-left: ${th('gridUnit')};
  }
`
const isDate = d => {
  const obj = {
    jatsDate: {},
  }
  let date = moment.utc(d, 'YYYY MMM DD')
  if (date.isValid()) {
    obj.jatsDate.day = moment(date).format('DD')
  } else {
    date = moment.utc(d, 'YYYY MMM')
    if (!date.isValid) {
      return false
    }
  }
  obj.jatsDate.year = moment(date).format('YYYY')
  obj.jatsDate.month = moment(date).format('MM')
  obj.date = date
  return obj
}
const headers = new Headers({
  Authorization: `Bearer ${window.localStorage.getItem('token')}`,
  'Content-Type': 'application/json',
})
class PubMedSearch extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      query: this.props.search || '',
      retstart: 0,
      results: [],
      enabled: !!this.props.search,
      loading: false,
      hitcount: null,
      inPMC: null,
      inEPMC: false,
      unmatched: false,
      cannotLoad: false,
      submitting: false,
    }
    this.onSearch = this.onSearch.bind(this)
    this.onLoad = this.onLoad.bind(this)
    this.onQueryChange = this.onQueryChange.bind(this)
    this.onSelected = this.onSelected.bind(this)
    this.selectResult = this.selectResult.bind(this)
    this.handleUnmatched = this.handleUnmatched.bind(this)
  }
  componentDidMount() {
    if (
      this.props.manuscript &&
      (!this.props.manuscript.meta.articleIds ||
        this.props.manuscript.meta.articleIds.length === 0)
    ) {
      this.setState({ unmatched: true })
    }
    if (this.props.search) {
      this.onSearch()
    }
  }
  componentDidUpdate(prevProps, prevState) {
    const { inPMC, inEPMC } = this.state
    if ((inPMC && !prevState.inPMC) || (inEPMC && !prevState.inEPMC)) {
      this.props.changeInfo({
        cancelCheck: !!inPMC,
        inEPMC: inEPMC && inPMC.pmcid,
      })
    }
    if (!inPMC && prevState.inPMC) {
      this.props.changeInfo({ cancelCheck: null })
    }
  }
  handleUnmatched(unm) {
    const citationData = unm
    if (this.props.newNote && unm.note) {
      const { notes } = this.props.manuscript.meta
      const note = notes
        ? notes.find(n => n.notesType === 'userCitation')
        : null
      if (note) {
        this.props.changeNote({ id: note.id, ...unm.note })
      } else {
        this.props.newNote({
          manuscriptId: this.props.manuscript.id,
          ...unm.note,
        })
      }
      delete citationData.note
    }
    this.props.citationData(citationData)
  }
  async selectResult(result, removePMCID) {
    const response = await fetch(`/citation`, {
      method: 'POST',
      headers,
      body: JSON.stringify(result),
    })
    const citationData = await response.json()
    citationData.decision = result.decision || null
    this.props.citationData(citationData, removePMCID)
  }
  keyPressed = e => {
    const list = document.getElementById('resultList')
    const item = document.activeElement
    if (list.contains(item)) {
      switch (e.keyCode) {
        case 13:
          e.target.click()
          break
        case 38:
          e.preventDefault()
          if (item.previousElementSibling) {
            item.tabIndex = -1
            item.previousElementSibling.tabIndex = 0
            item.previousElementSibling.focus()
          }
          break
        case 40:
          e.preventDefault()
          if (item.nextElementSibling) {
            item.tabIndex = -1
            item.nextElementSibling.tabIndex = 0
            item.nextElementSibling.focus()
          }
          break
        case 9:
          e.target.tabIndex = -1
          list.firstChild.tabIndex = 0
          break
        default:
          break
      }
    }
  }
  onQueryChange(event) {
    this.setState({
      enabled: event ? !!event.target.value : false,
      retstart: 0,
      results: [],
      hitcount: null,
      cannotLoad: false,
      query: event ? event.target.value : '',
    })
  }
  onLoad(event) {
    this.onSearch(event)
  }
  async onSearch(event) {
    event && event.preventDefault()
    this.setState({
      enabled: false,
      loading: true,
    })
    const { query, retstart, results } = this.state

    if (retstart >= results.length && query) {
      const SearchUrl = `/eutils/esearch?term=${query
        .replace(/http.*doi\.org\//gi, '')
        .replace(/(.*)\W$/g, '$1')}&retstart=${retstart}&db=pubmed`
      try {
        const hydrares = await fetch(`/hydra?query=${query}`, { headers })
        const hj = await hydrares.json()
        const response = await fetch(SearchUrl, { headers })
        const json = await response.json()
        const ids = [...new Set([...hj, ...json.esearchresult.idlist])]
        const hitcount = parseInt(json.esearchresult.count, 10) || ids.length
        const SummaryUrl = `/eutils/esummary?db=pubmed&id=${ids.join()}`
        const summary = await fetch(SummaryUrl, { headers })
        const summaryResponse = await summary.json()
        const { result } = summaryResponse
        const newResults = this.state.results.slice()
        ids.forEach(id => {
          newResults.push(result[id])
        })
        this.setState({
          hitcount,
          results: newResults,
          retstart: newResults.length > 0 ? newResults.length : 0,
        })
      } catch (e) {
        this.setState({
          cannotLoad: true,
          loading: false,
        })
      }
    }
    this.setState({ loading: false })
  }
  async onSelected(result, journal) {
    let pmcid =
      result.articleids.find(id => id.idtype === 'pmc') &&
      result.articleids.find(id => id.idtype === 'pmc').value
    if (!pmcid) {
      const idCheck = await fetch(`/idconv?pmid=${result.uid}`, { headers })
      const idJson = await idCheck.json()
      const [ids] = idJson.records
      ;({ pmcid } = ids)
    }
    // Embargo check
    const releaseDate =
      result.history.find(d => d.pubstatus === 'pmc-release') &&
      result.history.find(d => d.pubstatus === 'pmc-release').date
    const rDate = releaseDate && moment.utc(releaseDate, 'YYYY/MM/DD kk:mm')
    const pDate = isDate(result.pubdate) && isDate(result.pubdate).date
    const eDate = isDate(result.epubdate) && isDate(result.epubdate).date
    // If the article has a release date that is not a pubdate it has an embargo
    const embargo = rDate && !rDate.isSame(pDate) && !rDate.isSame(eDate)
    if (pmcid) {
      result.articleids = result.articleids.filter(i => i.idtype !== 'pmc')
      result.articleids.push({ idtype: 'pmc', idtypen: 8, value: pmcid })
      this.setState({ loading: true })
      const obj = {
        loading: false,
        submitting: false,
      }
      // License check
      const response = await fetch(`/ebisearch?pmid=${result.uid}`, { headers })
      const json = await response.json()
      // On initial search stop submissions at grants for CC BY and 0 embargo
      const { license } = json.resultList.result[0] || {}
      const planSReq = !!(license === 'cc by' && !embargo)
      if (this.props.lenientChecks || this.props.adminRemove) {
        obj.inPMC = { pmcid, result, journal, planSReq }
        this.setState(obj)
      } else {
        if (planSReq) result.decision = 'extant'
        this.selectResult(result)
      }
    } else {
      this.selectResult(result)
    }
  }
  render() {
    const {
      results,
      hitcount,
      loading,
      query,
      inPMC,
      unmatched,
      cannotLoad,
      submitting,
    } = this.state
    const {
      manuscript,
      toggle,
      adminRemove,
      lenientChecks,
      endSubmission: EndSub,
    } = this.props
    const { meta, journal } = manuscript || {}
    const { notes, articleIds } = meta || {}
    const note = notes ? notes.find(n => n.notesType === 'userCitation') : null
    const doi = articleIds ? articleIds.find(a => a.pubIdType === 'doi') : null
    let journalInfo = journal
    if (!journalInfo && meta && meta.unmatchedJournal) {
      journalInfo = {
        journalTitle: meta.unmatchedJournal,
      }
    }
    return (
      <React.Fragment>
        {submitting && <NoClick />}
        {toggle && (
          <Toggle>
            <Action
              className={!unmatched && 'current'}
              onClick={() => this.setState({ unmatched: false })}
            >
              Populate from PubMed
            </Action>
            <Action
              className={unmatched && 'current'}
              onClick={() => this.setState({ unmatched: true })}
            >
              Enter manually
            </Action>
          </Toggle>
        )}
        {unmatched ? (
          <UnmatchedCitation
            citation={unm => {
              this.setState({ submitting: true }, () =>
                this.handleUnmatched(unm),
              )
            }}
            close={!toggle ? () => this.setState({ unmatched: false }) : false}
            doi={doi ? doi.id : ''}
            journal={journalInfo}
            note={note ? note.content : ''}
            title={meta && meta.title}
          />
        ) : (
          <React.Fragment>
            {inPMC ? (
              <React.Fragment>
                <FullTextChecks
                  inEPMC={v => this.setState({ inEPMC: v })}
                  journal={lenientChecks ? inPMC.journal : null}
                  pmcid={inPMC.pmcid}
                  pmid={inPMC.result.uid}
                  title={inPMC.result.title}
                />
                {adminRemove && (
                  <React.Fragment>
                    <H4>Admin options</H4>
                    {!inPMC.planSReq && (
                      <Notification type="warning">
                        Check that grants and Plan S status are correct before
                        canceling submission!
                      </Notification>
                    )}
                    <FlexP>
                      <Button
                        onClick={() =>
                          this.setState({ submitting: true }, () =>
                            this.selectResult(inPMC.result),
                          )
                        }
                      >
                        Force submission
                      </Button>
                      <span>
                        Force manuscript submission under this citation data
                        regardless of manuscript being already in PMC.
                      </span>
                    </FlexP>
                    <FlexP>
                      <Button
                        onClick={() => {
                          this.setState({ submitting: true }, () =>
                            this.selectResult(inPMC.result, inPMC.pmcid),
                          )
                        }}
                      >
                        Link funding only
                      </Button>
                      <span>
                        Cancel the file submission, and change to a grant link
                        submission. An email will be sent to the submitter.
                      </span>
                    </FlexP>
                  </React.Fragment>
                )}
                <Buttons>
                  {EndSub ? (
                    <EndSub />
                  ) : (
                    <Link to="/">
                      <Button primary>End submission</Button>
                    </Link>
                  )}
                  <Button onClick={() => this.setState({ inPMC: null })}>
                    Back to Search
                  </Button>
                </Buttons>
              </React.Fragment>
            ) : (
              <React.Fragment>
                <H3>Populate from PubMed</H3>
                <SearchForm
                  buttonLabel="Search"
                  disabled={!this.state.enabled}
                  label="Search by article title, PMID, DOI, etc."
                  name="Search"
                  onChange={this.onQueryChange}
                  onSubmit={this.onSearch}
                  placeholder="Search for your article"
                  value={query}
                />
                {cannotLoad && (
                  <Notification type="warning">
                    {`Cannot load results. Please try a more specific query. Or, `}
                    <Action onClick={() => this.setState({ unmatched: true })}>
                      enter your citation manually
                    </Action>
                    .
                  </Notification>
                )}
                {hitcount !== null && (
                  <React.Fragment>
                    <Notice>Select your citation from results</Notice>
                    {(hitcount === 0 || results.length > 0) && (
                      <React.Fragment>
                        {this.props.changeNote ? (
                          <Notification type="info">
                            {hitcount === 0 && 'No results found. '}
                            {results.length > 0 &&
                              'Manuscript not in results? '}
                            <Action
                              onClick={() => this.setState({ unmatched: true })}
                            >
                              Click to enter citation manually.
                            </Action>
                          </Notification>
                        ) : (
                          <React.Fragment>
                            {hitcount === 0 && (
                              <Notification type="info">
                                No results found.
                              </Notification>
                            )}
                          </React.Fragment>
                        )}
                      </React.Fragment>
                    )}
                  </React.Fragment>
                )}
                {results.length > 0 && (
                  <ApolloConsumer>
                    {client => (
                      <ZebraList
                        id="resultList"
                        onKeyDown={this.keyPressed}
                        style={{ textAlign: 'center' }}
                      >
                        {results.map(
                          (result, i) =>
                            result.fulljournalname && (
                              <PubMedSearchResult
                                key={result.uid}
                                onClick={async () => {
                                  const { data } = await client.query({
                                    query: JOURNAL_INFO,
                                    variables: { nlmId: result.nlmuniqueid },
                                  })
                                  this.setState({ submitting: true }, () =>
                                    this.onSelected(result, data.selectWithNLM),
                                  )
                                }}
                                result={result}
                                tabIndex={i === 0 ? 0 : -1}
                              />
                            ),
                        )}
                        {loading && (
                          <Loading>
                            <LoadingIcon />
                          </Loading>
                        )}
                        {results.length < hitcount && !loading && (
                          <LoadMore onClick={this.onLoad} secondary>
                            Load More Results
                          </LoadMore>
                        )}
                      </ZebraList>
                    )}
                  </ApolloConsumer>
                )}
                {results.length === 0 && loading && (
                  <Loading>
                    <LoadingIcon />
                  </Loading>
                )}
              </React.Fragment>
            )}
          </React.Fragment>
        )}
      </React.Fragment>
    )
  }
}

export default PubMedSearch
