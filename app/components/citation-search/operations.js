import gql from 'graphql-tag'

export const JOURNAL_INFO = gql`
  query($nlmId: String!) {
    selectWithNLM(nlmId: $nlmId) {
      id
      journalTitle
      meta {
        nlmta
        pmcStatus {
          startDate
          endDate
        }
        pubmedStatus
      }
    }
  }
`

export const SEARCH_JOURNALS = gql`
  query($query: String!) {
    alphaJournals(query: $query) {
      id
      journalTitle
      meta {
        nlmta
        pmcStatus {
          startDate
          endDate
        }
        pubmedStatus
      }
    }
  }
`
