import React from 'react'
import styled from 'styled-components'
import { Icon } from '@pubsweet/ui'
import { th, darken } from '@pubsweet/ui-toolkit'
import { Loading, LoadingIcon } from '../ui'

const Small = styled.span`
  font-size: ${th('fontSizeBaseSmall')};
  display: block;
`

const Warning = styled.p`
  color: ${darken('colorWarning', 30)};
  font-size: ${th('fontSizeBaseSmall')};
  display: flex;
  align-items: flex-start;
`

class CrossRefCheck extends React.Component {
  state = { loading: true, title: null, journal: null, authors: null }
  async componentDidMount() {
    if (this.props.doi) {
      const headers = new Headers({
        Authorization: `Bearer ${window.localStorage.getItem('token')}`,
      })
      const response = await fetch(
        `/crossref?filter=doi:${this.props.doi}&select=title,container-title,author`,
        {
          headers,
        },
      )
      const json = await response.json()
      const [item] = json.message.items
      this.setState({
        title: item && item.title,
        journal: item && item['container-title'],
        authors: item && item.author,
        loading: false,
      })
    }
  }
  render() {
    const { loading, title, journal, authors } = this.state
    if (loading) {
      return (
        <Loading>
          <LoadingIcon />
        </Loading>
      )
    }
    return (
      <React.Fragment>
        {title ? (
          <p>
            {title}
            {journal && <Small>{journal}</Small>}
          </p>
        ) : (
          <Warning>
            <Icon color="currentColor" size={2}>
              alert-triangle
            </Icon>
            DOI not found
          </Warning>
        )}
        {authors && (
          <p>
            <Small>
              {authors.map(a => `${a.given} ${a.family}`).join(', ')}
            </Small>
          </p>
        )}
      </React.Fragment>
    )
  }
}

export default CrossRefCheck
