import React from 'react'
import { europepmcUrl } from 'config'
import { H2, H3, Icon } from '@pubsweet/ui'
import { A, B, HTMLString } from '../ui'
import { isJournalSubscribedToPMC } from '../../../server/utils/JournalUtil'

class FullTextChecks extends React.Component {
  state = { inEPMC: false }
  async componentDidMount() {
    if (this.props.pmid) {
      const headers = new Headers({
        Authorization: `Bearer ${window.localStorage.getItem('token')}`,
      })
      const response = await fetch(`/ebisearch?pmid=${this.props.pmid}`, {
        headers,
      })
      const json = await response.json()
      const { inEPMC } = json.resultList.result[0] || {}
      if (inEPMC === 'Y') {
        this.setState({ inEPMC: true })
        this.props.inEPMC(true)
      }
    }
  }
  render() {
    const { pmcid, title, journal, fundingGroup } = this.props
    const { inEPMC } = this.state
    return (
      <React.Fragment>
        <H2>File submission not required</H2>
        {pmcid ? (
          <React.Fragment>
            <p>
              {`A full text version of the article `}
              <B>
                <HTMLString string={title} />
              </B>
              {` that meets your funding requirements has already been sent to Europe PMC.`}
            </p>
            {inEPMC && (
              <p>
                <A
                  href={`${europepmcUrl}/article/PMC/${pmcid}`}
                  style={{ display: 'inline-flex', alignItems: 'center' }}
                  target="_blank"
                >
                  <Icon color="currentColor">arrow-right-circle</Icon>
                  View this article on Europe PMC
                </A>
              </p>
            )}
            <p>
              {`Please use `}
              <B>{pmcid}</B>
              {` for grant reporting purposes.`}
            </p>
          </React.Fragment>
        ) : (
          <React.Fragment>
            {journal.meta.pmcStatus &&
              isJournalSubscribedToPMC(journal.meta.pmcStatus)(
                <p>
                  {`Your article will be published in `}
                  <B>{journal.journalTitle}</B>
                  {`. This journal participates in PMC at a level that meets your funding requirements. Your article will be provided to PMC and made available in Europe PMC by the publisher. Please contact the publisher regarding any delays in this process.`}
                </p>,
              )}
          </React.Fragment>
        )}
        {fundingGroup && (
          <React.Fragment>
            <H3>Submit grant links</H3>
            <p>
              The funding information you have selected will be linked to the
              existing record:
            </p>
            <ul>
              {fundingGroup.map(f => (
                <li key={f.awardId}>
                  {`${f.fundingSource} ${f.awardId} (${f.pi.surname}): ${f.title}`}
                </li>
              ))}
            </ul>
          </React.Fragment>
        )}
      </React.Fragment>
    )
  }
}

export default FullTextChecks
