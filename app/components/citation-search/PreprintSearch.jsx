import React from 'react'
import styled from 'styled-components'
import moment from 'moment'
import { th } from '@pubsweet/ui-toolkit'
import { Action, Button, H3, H4 } from '@pubsweet/ui'
import {
  Loading,
  LoadingIcon,
  Notification,
  SearchForm,
  Toggle,
  ZebraList,
} from '../ui'
import PreprintSearchResult from './PreprintSearchResult'
import UnmatchedCitation from './UnmatchedCitation'

const LoadMore = styled(Button)`
  margin: 15px auto;
`
const Notice = styled(H4)`
  margin: 0 auto;
  & ~ ul {
    margin-top: calc(${th('gridUnit')} * 2);
  }
`
class PreprintSearch extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      manual: false,
      enabled: !!this.props.search,
      loading: false,
      query: this.props.search || '',
      results: [],
      hitcount: null,
      pageSize: 25,
      cursorMark: '',
      cannotLoad: false,
    }
    this.onSearch = this.onSearch.bind(this)
  }
  componentDidMount() {
    if (this.props.search) {
      this.onSearch()
    }
  }
  handleUnmatched = unm => {
    const citationData = unm
    if (this.props.newNote && unm.note) {
      const { notes } = this.props.manuscript.meta
      const note = notes
        ? notes.find(n => n.notesType === 'userCitation')
        : null
      if (note) {
        this.props.changeNote({ id: note.id, ...unm.note })
      } else {
        this.props.newNote({
          manuscriptId: this.props.manuscript.id,
          ...unm.note,
        })
      }
      delete citationData.note
    }
    this.props.citationData(citationData)
  }
  keyPressed = e => {
    const list = document.getElementById('resultList')
    const item = document.activeElement
    if (list.contains(item)) {
      switch (e.keyCode) {
        case 13:
          e.target.click()
          break
        case 38:
          e.preventDefault()
          if (item.previousElementSibling) {
            item.tabIndex = -1
            item.previousElementSibling.tabIndex = 0
            item.previousElementSibling.focus()
          }
          break
        case 40:
          e.preventDefault()
          if (item.nextElementSibling) {
            item.tabIndex = -1
            item.nextElementSibling.tabIndex = 0
            item.nextElementSibling.focus()
          }
          break
        case 9:
          e.target.tabIndex = -1
          list.firstChild.tabIndex = 0
          break
        default:
          break
      }
    }
  }
  onQueryChange = event => {
    this.setState({
      enabled: event ? !!event.target.value : false,
      results: [],
      hitcount: null,
      cursorMark: '',
      query: event ? event.target.value : '',
    })
  }
  onSelected = result => {
    const jatsDate = result.firstPublicationDate.split('-')
    const citationData = {
      meta: {
        unmatchedJournal: result.bookOrReportDetails.publisher,
        title: result.title,
        publicationDates: [
          {
            type: 'epub',
            date: moment.utc(result.firstPublicationDate, 'YYYY-MM-DD'),
            jatsDate: {
              year: jatsDate[0],
              month: jatsDate[1],
              day: jatsDate[2],
            },
          },
        ],
        articleIds: [
          {
            pubIdType: 'doi',
            id: result.doi,
          },
          {
            pubIdType: 'pprid',
            id: result.id,
          },
        ],
      },
    }
    if (result.versionNumber) {
      citationData.version = result.versionNumber
    }
    this.props.citationData(citationData)
  }
  async onSearch(event) {
    event && event.preventDefault()
    const { query, results, pageSize, cursorMark } = this.state
    if (query) {
      const epmcURL = `/ebisearch?query=SRC%3APPR%20AND%20${query}&resulttype=lite&format=json&pageSize=${pageSize}&cursorMark=${cursorMark}`
      this.setState({ enabled: false, loading: true })
      try {
        const headers = new Headers({
          Authorization: `Bearer ${window.localStorage.getItem('token')}`,
        })
        const response = await fetch(epmcURL, { headers })
        const json = await response.json()
        this.setState({
          hitcount: json.hitCount,
          results: results.concat(json.resultList.result),
          cursorMark: json.nextCursorMark,
          loading: false,
        })
      } catch (e) {
        this.setState({
          cannotLoad: true,
          loading: false,
        })
      }
    }
  }
  render() {
    const {
      manual,
      query,
      enabled,
      results,
      hitcount,
      loading,
      pageSize,
      cannotLoad,
    } = this.state
    const { manuscript, toggle } = this.props
    const { meta, journal } = manuscript || {}
    const { notes } = meta || {}
    const note = notes ? notes.find(n => n.notesType === 'userCitation') : null
    let journalInfo = journal
    if (!journalInfo && meta && meta.unmatchedJournal) {
      journalInfo = {
        journalTitle: meta.unmatchedJournal,
      }
    }
    return (
      <React.Fragment>
        {toggle && (
          <Toggle>
            <Action
              className={!manual && 'current'}
              onClick={() => this.setState({ manual: false })}
            >
              Find in Europe PMC
            </Action>
            <Action
              className={manual && 'current'}
              onClick={() => this.setState({ manual: true })}
            >
              Enter manually
            </Action>
          </Toggle>
        )}
        {manual ? (
          <React.Fragment>
            <H3>Enter manually</H3>
            <UnmatchedCitation
              citation={this.handleUnmatched}
              close={!toggle ? () => this.setState({ manual: false }) : false}
              journal={journalInfo}
              note={note ? note.content : ''}
              title={meta && meta.title}
            />
          </React.Fragment>
        ) : (
          <React.Fragment>
            <H3>Find in Europe PMC</H3>
            <SearchForm
              buttonLabel="Search"
              disabled={!enabled}
              label="Search by article title, PPRID, or DOI"
              name="Search"
              onChange={this.onQueryChange}
              onSubmit={this.onSearch}
              placeholder="Search for your article"
              value={query}
            />
            {cannotLoad && (
              <Notification type="warning">
                {`Cannot load results. Please try a more specific query. Or, `}
                <Action onClick={() => this.setState({ manual: true })}>
                  enter your citation manually
                </Action>
                .
              </Notification>
            )}
            {hitcount !== null && (
              <React.Fragment>
                <Notice>Select your citation from results</Notice>
                {hitcount === 0 && (
                  <Notification type="info">No results found.</Notification>
                )}
              </React.Fragment>
            )}
            {results.length > 0 && (
              <ZebraList
                id="resultList"
                onKeyDown={this.keyPressed}
                style={{ textAlign: 'center' }}
              >
                {results.map((result, i) => (
                  <PreprintSearchResult
                    key={result.id}
                    onClick={() => this.onSelected(result)}
                    result={result}
                    tabIndex={i === 0 ? 0 : -1}
                  />
                ))}
                {loading && (
                  <Loading>
                    <LoadingIcon />
                  </Loading>
                )}
                {pageSize < hitcount && !loading && (
                  <LoadMore onClick={this.onSearch} secondary>
                    Load More Results
                  </LoadMore>
                )}
              </ZebraList>
            )}
          </React.Fragment>
        )}
      </React.Fragment>
    )
  }
}

export default PreprintSearch
