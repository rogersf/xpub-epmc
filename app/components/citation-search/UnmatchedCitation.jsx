import React from 'react'
import { Field, withFormik } from 'formik'
import * as yup from 'yup'
import { Button, ErrorText, H3, TextField } from '@pubsweet/ui'
import { Buttons, TextArea } from '../ui'
import JournalSearch from './JournalSearch'

const Title = props => (
  <TextField
    invalidTest={props.invalidTest}
    label="Enter manuscript title"
    placeholder="The title of your manuscript"
    {...props.field}
  />
)

const Journal = props => (
  <JournalSearch
    error={props.invalidTest}
    journal={props.field.value}
    setJournal={j => props.form.setFieldValue('journal', j)}
  />
)

const DOI = props => (
  <TextField
    invalidTest={props.invalidTest}
    label="DOI (optional)"
    placeholder="Begins with 10..."
    rows={3}
    {...props.field}
  />
)

const Other = props => (
  <TextArea
    label="Other journal information (optional)"
    placeholder="URL, Volume/Issue, Page numbers, etc."
    {...props.field}
  />
)

const UnmatchedCitation = ({ values, errors, touched, ...props }) => (
  <form onSubmit={props.handleSubmit}>
    <H3>Enter manually</H3>
    <Field
      component={Title}
      invalidTest={errors.title && touched.title}
      name="title"
    />
    {errors.title && touched.title && <ErrorText>{errors.title}</ErrorText>}
    <Field
      component={Journal}
      invalidTest={errors.journal && touched.journal}
      name="journal"
    />
    {errors.journal && touched.journal && (
      <ErrorText>{JSON.stringify(errors.journal)}</ErrorText>
    )}
    <Field component={DOI} invalidTest={errors.doi && touched.doi} name="doi" />
    {errors.doi && touched.doi && <ErrorText>{errors.doi}</ErrorText>}
    <Field component={Other} name="note" />
    <Buttons right>
      <Button
        // disabled={!values.title || !values.journal}
        primary
        type="submit"
      >
        Submit
      </Button>
      {props.close && (
        <Button onClick={props.close}>Back to citation search</Button>
      )}
    </Buttons>
  </form>
)

const handleSubmit = async (
  values,
  { props, setSubmitting, setErrors, resetForm, setStatus },
) => {
  const citationData = {
    meta: {
      title: values.title,
      articleIds: null,
    },
  }
  if (values.journal) {
    if (values.journal.id) {
      citationData.meta.unmatchedJournal = null
      citationData.journalId = values.journal.id
    } else {
      citationData.meta.unmatchedJournal = values.journal.journalTitle
      citationData.journalId = null
    }
  }
  if (values.doi) {
    citationData.meta.articleIds = [
      {
        pubIdType: 'doi',
        id: values.doi,
      },
    ]
  }
  if (values.note) {
    citationData.note = {
      notesType: 'userCitation',
      content: values.note,
    }
  }
  resetForm()
  props.citation(citationData)
}

const enhancedFormik = withFormik({
  initialValues: {
    title: '',
    journal: '',
    doi: '',
    note: '',
  },
  mapPropsToValues: ({ title, journal, note, doi }) => ({
    title,
    journal,
    note,
    doi,
  }),
  validationSchema: props =>
    yup.object().shape({
      title: yup.string().required('Title is required'),
      journal: yup.object().shape({
        id: yup.string(),
        journalTitle: yup.string().required('Journal is required'),
        meta: yup.object().shape({
          pmcStatus: yup
            .object()
            .shape({
              startDate: yup.date(),
              endDate: yup.date().nullable(true),
            })
            .nullable(true),
        }),
      }),
      note: yup.string(),
      doi: yup
        .string()
        .matches(
          /^10.\d{4,9}\/[-._;()/:A-Z0-9]+$/i,
          'DOI is not valid as entered',
        ),
    }),
  displayName: 'user-citation',
  handleSubmit,
})(UnmatchedCitation)

export default enhancedFormik
