import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { HTMLString, ZebraListItem } from '../ui'

export const Result = styled(ZebraListItem)`
  text-align: left;
  &:hover {
    cursor: pointer;
    color: ${th('colorPrimary')};
  }
`
export const Citation = styled.span`
  font-size: ${th('fontSizeBaseSmall')};
  margin-right: ${th('gridUnit')};
`

const PubMedSearchResult = ({ result, ...props }) => (
  <Result {...props}>
    <HTMLString string={result.title} />
    <br />
    <Citation>
      <HTMLString
        string={`${result.sortfirstauthor}${
          result.authors.length > 1 ? `, ... ${result.lastauthor}` : ''
        }. ${result.fulljournalname}. ${result.pubdate}${
          result.volume ? `;${result.volume}` : ''
        }${result.issue ? `(${result.issue})` : ''}${
          result.pages ? `:${result.pages}` : ''
        }`}
      />
    </Citation>
  </Result>
)

export default PubMedSearchResult
