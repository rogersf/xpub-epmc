import React from 'react'
import { H2, RadioGroup, Button, Icon } from '@pubsweet/ui'
import { europepmcUrl } from 'config'
import { Buttons, A } from './ui'
import { NoteMutations } from './SubmissionMutations'

class PreprintReview extends React.Component {
  state = {
    show: this.props.show || 'versionApproval',
    versionApprovalSelected: this.props.singleView || null,
    addLicenseSelected: this.props.singleView || null,
  }
  setNoteValue = type => {
    const value = this.state[`${type}Selected`]
    const { notes } = this.props.manuscript.meta
    const exists = notes && notes.find(n => n.notesType === type)
    if (exists) {
      this.props.changeNote({
        id: exists.id,
        content: value,
        notesType: type,
      })
    } else {
      this.props.newNote({
        content: value,
        notesType: type,
      })
    }
  }
  render() {
    const { versionApprovalSelected, addLicenseSelected, show } = this.state
    const { singleView } = this.props
    const { journal, meta } = this.props.manuscript
    const { notes, unmatchedJournal } = meta
    const license = notes && notes.find(n => n.notesType === 'openAccess')
    const licenseLink = license && license.content
    const openAccess =
      licenseLink && licenseLink.search(/\/(by-nd|by-nc-nd)\//i) < 0
    return (
      <React.Fragment>
        {show === 'versionApproval' && (
          <React.Fragment>
            <H2>
              {singleView
                ? 'Would you like this preprint automatically updated?'
                : 'Thank you for approving the display of your preprint!'}
            </H2>
            <p>
              {`We will be tracking new versions of this preprint submitted to ${
                journal ? journal.journalTitle : unmatchedJournal
              }, to keep the copy on Europe PMC up to date. Would you like updates to happen automatically, or would you like to check each version?`}
            </p>
            <RadioGroup
              name="preapprove"
              onChange={v => this.setState({ versionApprovalSelected: v })}
              options={[
                {
                  value: 'approved',
                  label:
                    'You have my approval to display future versions automatically.',
                },
                {
                  value: 'refused',
                  label:
                    'Please contact me to approve each version before displaying it.',
                },
              ]}
              value={versionApprovalSelected}
            />
          </React.Fragment>
        )}
        {show === 'addLicense' && (
          <React.Fragment>
            {singleView === 'approved' || openAccess ? (
              <React.Fragment>
                <H2>Your preprint is available for text and data mining</H2>
                <p>
                  {openAccess
                    ? 'Your preprint has an open Creative Commons license. '
                    : 'You have added an open license to your preprint. '}
                  Thank you for making your preprint available for deep
                  scientific analysis.
                </p>
              </React.Fragment>
            ) : (
              <React.Fragment>
                <H2>
                  {licenseLink
                    ? `Would you like to see text and data mining results for this preprint?`
                    : `Would you like this preprint to be available for text and data mining?`}
                </H2>
                <p>
                  {licenseLink
                    ? `Your preprint has a no-derivatives Creative Commons license. This means when it is displayed on Europe PMC, we will be unable to show the results of programmatic text and data mining.`
                    : `Your preprint has no specific license. This means that when displayed on Europe PMC, it will not be available for programmatic text and data mining.`}
                </p>
                <p>
                  {`We would like to add the following license to your preprint, to apply to this and any later versions. If you agree, your preprint ${
                    licenseLink
                      ? 'and annotations and text-mined terms derived from it '
                      : ''
                  }can be made available to a wider audience, and its content can be used for deeper scientific analysis.`}
                </p>
                <blockquote style={{ display: 'flex' }}>
                  <span>&quot;</span>
                  <span>
                    {'This preprint is made available via the '}
                    <A
                      href={`${europepmcUrl}/downloads/openaccess`}
                      target="_blank"
                    >
                      Europe PMC open access subset
                    </A>
                    {`, for unrestricted research re-use and secondary analysis in any form or by any means with acknowledgement of the original preprint source."`}
                  </span>
                </blockquote>
                <p>Would you like to add the license?</p>
                <RadioGroup
                  name="addlicense"
                  onChange={v => this.setState({ addLicenseSelected: v })}
                  options={[
                    {
                      value: 'approved',
                      label: licenseLink
                        ? 'Yes, add the license and make text and data mining results available for this preprint.'
                        : 'Yes, add the license and make my preprint available for text and data mining.',
                    },
                    {
                      value: 'refused',
                      label: licenseLink
                        ? 'No, do not add the license. My preprint will be displayed, but highlighted text and data mined terms will not be.'
                        : 'No, do not add the license. My preprint will be displayed, but not available for text or data mining via bulk download or via Europe PMC open APIs.',
                    },
                  ]}
                  value={addLicenseSelected}
                />
                <p
                  style={{
                    fontStyle: 'italic',
                    display: 'flex',
                    alignItems: 'center',
                  }}
                >
                  <Icon size={2}>help-circle</Icon>
                  <A
                    href={`${europepmcUrl}/downloads/openaccess`}
                    target="_blank"
                  >
                    Find out more about making your preprint available for text
                    and data mining.
                  </A>
                </p>
              </React.Fragment>
            )}
          </React.Fragment>
        )}
        <Buttons right>
          {singleView || openAccess || show === 'addLicense' ? (
            <React.Fragment>
              {show === 'addLicense' &&
              (singleView === 'approved' || openAccess) ? (
                ''
              ) : (
                <Button
                  disabled={!this.state[`${show}Selected`]}
                  onClick={() => {
                    this.setNoteValue(show)
                    this.props.complete(true)
                  }}
                  primary
                >
                  Submit
                </Button>
              )}
              {singleView && (
                <Button
                  onClick={() => {
                    this.props.complete(true)
                  }}
                >
                  Exit
                </Button>
              )}
            </React.Fragment>
          ) : (
            <Button
              disabled={!versionApprovalSelected}
              onClick={() => {
                this.setNoteValue(show)
                this.setState({ show: 'addLicense' })
              }}
              primary
            >
              Next
            </Button>
          )}
          {!singleView && show === 'addLicense' && (
            <Button onClick={() => this.setState({ show: 'versionApproval' })}>
              Back
            </Button>
          )}
        </Buttons>
      </React.Fragment>
    )
  }
}

const PreprintWithMutations = NoteMutations(PreprintReview)

export default PreprintWithMutations
