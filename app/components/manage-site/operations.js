import gql from 'graphql-tag'

export const JOB_LIST = gql`
  query ListJobs {
    getJobs {
      name
      description
      running
      lastStatus
      lastPass
      lastRun
    }
  }
`

export const JOB_LOG = gql`
  query JobLog($name: String!) {
    jobLog(name: $name) {
      id
      created
      dataCenter
      originalData
      changes
    }
  }
`

export const PROPS = gql`
  query ListProps {
    getProps {
      name
      description
      schema
      value
    }
  }
`

export const SAVE_PROP = gql`
  mutation updateProp($data: PropInput!) {
    updateProp(data: $data) {
      name
      value
    }
  }
`

export const COUNT_SET = gql`
  query CountSetByStatus($preprint: Boolean, $set: String) {
    countSetByStatus(preprint: $preprint, set: $set) {
      type
      count
    }
  }
`

export const PRIORITY_PREPRINTS = gql`
  query PreprintPriority {
    preprintPriority {
      type
      count
    }
  }
`

export const PRIORITY_LIST = gql`
  query PreprintPriorityList($tier: Int!, $page: Int, $pageSize: Int) {
    preprintPriorityList(tier: $tier, page: $page, pageSize: $pageSize) {
      total
      manuscripts {
        id
        version
        updated
      }
    }
  }
`

export const METRICS = gql`
  query METRICS(
    $startMonth: Int
    $endMonth: Int
    $preprint: Boolean
    $set: String
  ) {
    getMetrics(
      startMonth: $startMonth
      endMonth: $endMonth
      preprint: $preprint
      set: $set
    ) {
      display_mth
      submitted
      xml_review
      xml_review_within_10_days
      xml_review_within_10_days_perc
      xml_review_within_3_days
      xml_review_within_3_days_perc
      published
      ncbi_ready_median
      external_qa
      xml_tagging
      xml_tagging_within_3_days
      xml_tagging_within_3_days_perc
      xml_errors
    }
  }
`

export const STATS = gql`
  query STATS($startMonth: Int, $endMonth: Int) {
    publisherMetrics(startMonth: $startMonth, endMonth: $endMonth) {
      name
      user_id
      display_mth
      count
    }
  }
`

export const WEEK_METRICS = gql`
  query METRICS($preprint: Boolean, $set: String) {
    weeklyMetrics(preprint: $preprint, set: $set) {
      display_mth
      submitted
      xml_review
      published
      external_qa
      xml_tagging
      xml_errors
    }
  }
`

export const USERS_BY_NAME = gql`
  query($name: String!) {
    usersByName(name: $name) {
      id
      identities {
        type
        email
        name {
          title
          givenNames
          surname
        }
      }
    }
  }
`

export const USERS_BY_EMAIL = gql`
  query($email: String!) {
    usersByEmail(email: $email) {
      id
      identities {
        type
        email
        name {
          title
          givenNames
          surname
        }
      }
    }
  }
`
