import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Link } from '@pubsweet/ui'
import { withApollo } from 'react-apollo'
import {
  SearchForm,
  ZebraList,
  ZebraListItem,
  Loading,
  LoadingIcon,
  Notification,
} from '../ui/'
import ManagementBase from './ManagementBase'
import { USERS_BY_NAME, USERS_BY_EMAIL } from './operations'

const SearchArea = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-between;
  form {
    width: 45%;
    min-width: 200px;
    input {
      height: calc(${th('gridUnit')} * 5);
    }
  }
  @media screen and (max-width: 800px) {
    justify-content: stretch;
  }
`
const Separator = styled.div`
  margin: 0 calc(${th('gridUnit')} * 2);
`
const ListItem = styled(ZebraListItem)`
  padding: calc(${th('gridUnit')} * 2) ${th('gridUnit')};
  display: flex;
  justify-content: space-between;
  * {
    width: 280px;
    padding-right: ${th('gridUnit')};
    min-width: 0;
    word-break: break-all;
    &:first-child {
      width: 380px;
    }
  }
  @media screen and (max-width: 800px) {
    flex-wrap: wrap;
  }
`
class UserSearch extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      searchByName: props.searchByName ? props.searchByName : '',
      searchByEmail: props.searchByEmail ? props.searchByEmail : '',
      results: [],
      error: '',
      emptyResults: false,
    }
    this.onSearchValChanged = this.onSearchValChanged.bind(this)
    this.onSearchValSubmitted = this.onSearchValSubmitted.bind(this)
  }
  onSearchValChanged(e) {
    this.setState({ [e.target.name]: e.target.value })
  }
  onSearchValSubmitted(searchForm, e) {
    e.preventDefault()
    let options
    if (searchForm === 'usersByName') {
      options = {
        query: USERS_BY_NAME,
        variables: { name: this.state.searchByName },
        fetchPolicy: 'network-only',
      }
      this.setState({ searchByEmail: '', loading: true })
    } else {
      options = {
        query: USERS_BY_EMAIL,
        variables: { email: this.state.searchByEmail },
        fetchPolicy: 'network-only',
      }
      this.setState({ searchByName: '', loading: true })
    }
    this.props.client
      .query(options)
      .then(response =>
        this.setState({
          results: response.data[searchForm],
          loading: false,
          error: '',
          emptyResults: response.data[searchForm].length === 0,
        }),
      )
      .catch(e =>
        this.setState({
          loading: false,
          error: 'Error occured, please try again.',
        }),
      )
  }
  render() {
    const {
      searchByName,
      searchByEmail,
      results,
      loading,
      emptyResults,
      error,
    } = this.state
    return (
      <div>
        <div>
          <SearchArea>
            <SearchForm
              disabled={!searchByName}
              label="Search by given or last name"
              name="searchByName"
              noButton="true"
              onChange={this.onSearchValChanged}
              onSubmit={e => this.onSearchValSubmitted('usersByName', e)}
              value={searchByName}
            />
            <Separator> OR </Separator>
            <SearchForm
              disabled={!searchByEmail}
              label="Search by email address"
              name="searchByEmail"
              noButton="true"
              onChange={this.onSearchValChanged}
              onSubmit={e => this.onSearchValSubmitted('usersByEmail', e)}
              value={searchByEmail}
            />
          </SearchArea>
          <ZebraList>
            {results.map(user => {
              let [localIdentity] = user.identities.filter(
                identity =>
                  identity.type === 'local' || identity.type === 'ftp',
              )
              if (!localIdentity) {
                ;[localIdentity] = user.identities
              }
              if (!localIdentity) {
                return (
                  <ListItem>
                    <Notification key={user.id} type="warning">
                      Error in users table
                    </Notification>
                  </ListItem>
                )
              }
              return (
                <ListItem key={user.id}>
                  <Link to={`/manage-account/${user.id}`}>
                    <span>{user.id}</span>
                  </Link>
                  <span>
                    {`
                      ${
                        localIdentity.name.title ? localIdentity.name.title : ''
                      }
                      ${localIdentity.name.givenNames}
                      ${localIdentity.name.surname}
                    `}
                  </span>
                  <span>{localIdentity.email}</span>
                </ListItem>
              )
            })}
          </ZebraList>
          {loading && (
            <Loading>
              <LoadingIcon />
            </Loading>
          )}
          {emptyResults && <div> No results found for this search.</div>}
          <div>{error}</div>
        </div>
      </div>
    )
  }
}

const UserTitle = ManagementBase(UserSearch)

const UserSearchPage = ({ ...props }) => (
  <UserTitle pageTitle="Manage users" {...props} />
)

export default withApollo(UserSearchPage)
