import React from 'react'
import { Query } from 'react-apollo'
import moment from 'moment'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { JOB_LOG } from './operations'
import { Loading, LoadingIcon, Table, Notification } from '../ui'
import ManagementBase from './ManagementBase'

const DetailsTable = styled(Table)`
  width: 100%;
  @media screen and (max-width: 600px) {
    th {
      display: none;
    }
    tr {
      border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
    }
  }
`
const TD = styled.td`
  font-size: ${th('fontSizeBaseSmall')};
  word-break: break-word;
  @media screen and (max-width: 600px) {
    display: inline-block;
    width: 100%;
    border: 0 !important;
  }
`
const TdDate = styled(TD)`
  vertical-align: top;
  width: 1%;
  white-space: nowrap;
  @media screen and (max-width: 600px) {
    width: 50%;
    white-space: normal;
  }
`

const ParseUpdate = ({ col, val }) => {
  switch (val) {
    case null:
    case '':
      return `Removed previous ${col}.`
    case 't':
      return `Set job ${col}`
    case 'f':
      return `Set job not ${col}`
    default:
      if (moment(val).isValid())
        return `Job ${col} at: ${moment
          .utc(val)
          .local()
          .format('DD/MM/YYYY HH:mm:ss.SS')}`
      return `Job ${col} is: ${val}`
  }
}

const EventList = ({ entry: { id, originalData, changes } }) => {
  let rowData = changes
  if (!changes) {
    rowData = originalData
    Object.keys(rowData).forEach(k => rowData[k] === null && delete rowData[k])
  }
  const updateList = Object.keys(rowData)
  return updateList.map(key => (
    <React.Fragment key={id + key}>
      <ParseUpdate col={key.replace(/_/g, ' ')} val={rowData[key]} />
      <br />
    </React.Fragment>
  ))
}

const JobLog = ({ match, ...props }) => (
  <Query
    fetchPolicy="no-cache"
    query={JOB_LOG}
    variables={{ name: match.params.name }}
  >
    {({ data, loading }) => {
      if (loading) {
        return (
          <Loading>
            <LoadingIcon />
          </Loading>
        )
      }
      if (!data) {
        return (
          <Notification type="error">
            Error loading job log from database
          </Notification>
        )
      }
      const { jobLog } = data
      return (
        <DetailsTable>
          <tbody>
            <tr>
              <th>Date</th>
              <th>DC</th>
              <th>Event</th>
            </tr>
            {jobLog.map(entry => (
              <tr key={entry.id}>
                <TdDate>
                  {moment(entry.created).format('DD/MM/YYYY HH:mm:ss.SS')}
                </TdDate>
                <TdDate>{entry.dataCenter}</TdDate>
                <TD>
                  <EventList entry={entry} />
                </TD>
              </tr>
            ))}
          </tbody>
        </DetailsTable>
      )
    }}
  </Query>
)

const JobLogTitle = ManagementBase(JobLog)

const JobLogPage = ({ ...props }) => (
  <JobLogTitle pageTitle={`Job log: ${props.match.params.name}`} {...props} />
)

export default JobLogPage
