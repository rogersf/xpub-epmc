import React from 'react'
import styled from 'styled-components'
import { Action, Link, Icon } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import {
  HTMLString,
  FlexBar,
  LeftSide,
  RightSide,
  Loading,
  LoadingIcon,
  Notification,
  PreprintLabel,
} from './ui'
import Mailer from './mailer'
import Claimer from './AdminClaim'
import { States } from './dashboard'

const ManuscriptHead = styled(FlexBar)`
  border-bottom: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  font-weight: 600;
`
const Left = styled.div`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`
const FlexDiv = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
`
const Progress = styled.span`
  margin-left: calc(${th('gridUnit')} * 3);
  font-weight: normal;
  font-style: italic;
  color: ${th('colorTextPlaceholder')};
  &.saved {
    color: ${th('colorText')};
  }
`

let savedTimer
const addSaved = () => {
  document.getElementById('progress').classList.add('saved')
}
const removeSaved = () => {
  document.getElementById('progress').classList.remove('saved')
}

const SubmissionHeader = BaseComponent =>
  class extends React.Component {
    state = { mailer: false }
    componentDidMount() {
      const { manuscript } = this.props
      const { id, meta } = manuscript
      document.title = `${id}: ${meta.title.substring(
        0,
        30,
      )}... - Europe PMC plus`
    }
    componentWillUnmount() {
      document.title = `Europe PMC plus`
    }
    render() {
      const { currentUser, manuscript, children, saved, ...props } = this.props
      const { version, meta } = manuscript
      const { subjects = [], title } = meta
      if (manuscript) {
        document.title = `${manuscript.id} - Europe PMC plus`
        if (saved && document.getElementById('progress')) {
          addSaved()
        }
        if (savedTimer) {
          clearTimeout(savedTimer)
        }
        savedTimer = setTimeout(() => {
          if (document.getElementById('progress')) {
            removeSaved()
          }
        }, 2000)
        if (manuscript.deleted && !currentUser.admin) {
          props.history.push('/')
          return null
        }
        return (
          <React.Fragment>
            <ManuscriptHead>
              <LeftSide>
                <Left>
                  {manuscript.organization.name === 'Europe PMC Preprints' && (
                    <PreprintLabel subjects={subjects} version={version} />
                  )}{' '}
                  {manuscript.id}: <HTMLString string={title} />
                </Left>
              </LeftSide>
              <RightSide>
                <FlexDiv>
                  {currentUser.admin ? (
                    <React.Fragment>
                      {window.location.pathname ===
                      `/submission/${manuscript.id}/activity` ? (
                        <React.Fragment>
                          {States.admin[manuscript.status].url !==
                            'activity' && (
                            <Link
                              to={`/submission/${manuscript.id}/${
                                States.admin[manuscript.status].url
                              }`}
                            >
                              <Icon color="currentColor" size={2.5}>
                                x-square
                              </Icon>
                              Exit activity
                            </Link>
                          )}
                        </React.Fragment>
                      ) : (
                        <Link to={`/submission/${manuscript.id}/activity`}>
                          <Icon color="currentColor" size={2.5}>
                            check-square
                          </Icon>
                          Activity
                        </Link>
                      )}
                    </React.Fragment>
                  ) : (
                    <React.Fragment>
                      {currentUser.external || currentUser.tagger ? (
                        <Claimer
                          claiming={manuscript.claiming}
                          currentUser={currentUser}
                          manId={manuscript.id}
                        />
                      ) : (
                        <Action
                          onClick={() => this.setState({ mailer: true })}
                          style={{ fontWeight: 600 }}
                        >
                          <Icon color="currentColor" size={2.5}>
                            mail
                          </Icon>
                          Helpdesk
                        </Action>
                      )}
                    </React.Fragment>
                  )}
                  <Progress className="saved" id="progress">
                    Progress saved
                  </Progress>
                </FlexDiv>
              </RightSide>
            </ManuscriptHead>
            {this.state.mailer && (
              <Mailer
                cc={this.state.mailer.cc}
                close={() => this.setState({ mailer: false })}
                currentUser={this.props.currentUser}
                manuscript={this.props.manuscript}
                message={this.state.mailer.message}
                recipients={this.state.mailer.recipients || ['helpdesk']}
                showSuccess={this.state.mailer.showSuccess}
                subject={this.state.mailer.subject}
              />
            )}
            {manuscript.deleted && (
              <Notification fullscreen type="error">
                This submission has been deleted.
              </Notification>
            )}
            <BaseComponent
              currentUser={currentUser}
              manuscript={manuscript}
              openMailer={v => this.setState({ mailer: v })}
              {...props}
            >
              {children}
            </BaseComponent>
          </React.Fragment>
        )
      }
      return (
        <Loading>
          <LoadingIcon />
        </Loading>
      )
    }
  }

export default SubmissionHeader
