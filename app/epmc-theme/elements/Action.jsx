import { css } from 'styled-components'
import { th, darken } from '@pubsweet/ui-toolkit'

export default css`
  text-align: inherit;
  border-radius: ${th('borderRadius')};
  &:hover,
  &:focus {
    color: ${darken('colorPrimary', 30)};
  }
  &:focus {
    outline: none;
    box-shadow: none;
  }
  &.current {
    color: ${th('colorText')};
    border-bottom: ${th('borderWidth')} ${th('borderStyle')}
      ${th('colorSecondary')};
    &:link,
    &:hover,
    &:focus,
    &:visited {
      color: ${th('colorText')};
      text-decoration: none;
    }
    cursor: default;
  }
  &:focus-visible {
    outline-color: transparent;
    box-shadow: ${th('dropShadowSecondary')};
  }
`
