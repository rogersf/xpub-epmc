import { css } from 'styled-components'
import { th, darken } from '@pubsweet/ui-toolkit'

export default css`
  color: ${th('colorTextPlaceholder')};
  cursor: not-allowed;
  &:link,
  &:visited {
    color: ${th('colorPrimary')};
    text-decoration: none;
    cursor: pointer;
  }
  &:hover,
  &:focus {
    text-decoration: underline;
    color: ${darken('colorPrimary', 30)};
  }
  &:focus {
    outline: none;
  }
  &.current {
    border-bottom: ${th('borderWidth')} ${th('borderStyle')}
      ${th('colorSecondary')};
    &:link,
    &:hover,
    &:focus,
    &:visited {
      color: ${th('colorText')};
      text-decoration: none;
    }
    cursor: default;
  }
`
