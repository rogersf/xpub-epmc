import { css } from 'styled-components'
import { lighten, th } from '@pubsweet/ui-toolkit'

export default {
  Dropdown: css`
    *:focus-within,
    &.focus {
      outline-offset: -2px;
      outline: 2px ${th('borderStyle')} rgba(32, 105, 156, 0.8);
    }
  `,
  SelectedContainer: css`
    min-height: 0;
    margin-bottom: ${th('gridUnit')};
  `,
  SelectedLabel: css`
    display: none;
  `,
  Selected: css`
    border-color: ${th('colorSuccess')};
    color: ${th('colorSuccess')};
    &:hover {
      border-color: ${lighten('colorError', 20)};
      color: ${lighten('colorError', 20)} !important;
    }
  `,
  Option: css`
    &:focus {
      box-shadow: none;
      border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorPrimary')};
      border-right: 0;
      border-left: 0;
    }
    &.neededAction {
      color: ${th('colorPrimary')};
      &:hover,
      &:focus {
        color: ${th('colorTextReverse')};
      }
    }
  `,
  OptionList: css`
    max-height: calc(${th('gridUnit')} * 30);
  `,
}
