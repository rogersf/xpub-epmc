import { css } from 'styled-components'
import { th, lighten } from '@pubsweet/ui-toolkit'

export default {
  Root: css`
    cursor: default;
    align-items: flex-start;
    margin-bottom: ${th('gridUnit')};

    &:hover span {
      color: ${th('colorPrimary')};
    }
  `,
  Input: css`
    margin-right: ${th('gridUnit')};
    height: calc(${th('gridUnit')} * 3);

    &:disabled + span,
    &.disabled + span {
      color: ${lighten('colorText', 75)};
    }
    &:focus {
      outline: initial;
      outline: unset;
      outline: revert;
    }
  `,
}
