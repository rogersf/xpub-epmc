# xpub-epmc

xpub-epmc is a manuscript submission system for [Europe PMC](https://europepmc.org/). It is currently being developed by Europe PMC staff based on three open source projects: [XPub](https://gitlab.coko.foundation/xpub/xpub), [PubSweet](https://gitlab.coko.foundation/pubsweet/pubsweet), and [XSweet](https://gitlab.coko.foundation/XSweet/XSweet), which are maintained by the [Coko Foundation](https://coko.foundation/).

## Workflow

The ongoing development aims to support the following workflow:

![EPMC Workflow](https://gitlab.coko.foundation/xpub/xpub-epmc/raw/master/static/epmcworkflow.png)

The complete workflow implementation consists of four main components:

* User Dashboard
* Submission Wizard
* XML Wizard
* FTP Integration

For details of our workflow, please contact us via <plusadmin@ebi.ac.uk>.

## QuickStart

xpub-epmc involves a range of tools including [Docker](https://www.docker.com/), [MongoDB](https://www.mongodb.com), and [Node.js](https://nodejs.org/en/) with [yarn](https://yarnpkg.com/lang/en/). It is necessary to understand these tools before running this project. This QuickStart aims to let you quickly get the app running and try the basic features such as manuscript submission. Please contact us via <plusdev-shared@ebi.ac.uk> for the complete guide that enables other features such as manuscript bulk upload via FTP.

Before getting the app running, make sure Docker and Node.js (with yarn) have been installed. Then, go to the project root directory, create the following files: *.env* and *.env.postgres*, which are used for necessary setup in the build/run process:

*.env*
```
POSTGRES_PASSWORD=xxxx
PUBSWEET_TEST_EMAIL=your email here
```

*.env.postgres*
```
POSTGRES_PASSWORD=xxxx
POSTGRES_USER=username
DATABASE_URL=postgres://username:xxxx@postgres:5432/dbname
```

In the above files, xxxx can be any valid and consistent value. "username" should be your username.

### Running the app in the demo/production mode

Run `yarn start`. This will run docker containers required by the app. Then you can access the app via http://localhost:3000.

### Running the app in the development mode

First, install the dependencies by running `yarn install`.

Then, copy the following file: *local-development.json*, into the config folder.

```json
{
  "pubsweet-server": {
    "baseUrl": "http://localhost:3000",
    "secret": "xxxx",
    "db": {
      "port": 5432
    }
  },
  "users" : [
    {
      "givenNames": "xxxx",
      "surname": "xxxx",
      "defaultIdentity": "local",
      "identities": [
        {
          "email": "xxxx",
          "type": "local",
          "password": "xxxxxxxx"
        }
      ],
      "roles": [
        {
          "name": "admin"
        }
      ]
    }
  ]
}
```

The above file is used to connect to the server and create an admin user as a starting point when the app is running. xxxx can be any valid value.

Next, run the docker container for the database by running `yarn start:services`.

In a separate terminal, run `yarn migrate`, to migrate to the latest version of the database, followed by `yarn seed`, to seed items including the admin user and the journal list.

Now, run the app by running `yarn server`.

Finally, you should be able to access http://localhost:3000.

## Installing SSL Certificate

To install SSL certificate, copy the certificate (*xxxx.crt*) and its private key (*xxxx.key*) to directory config/certs. After that, rerun `yarn start`, then you can access the application via https://your_domain.

## Continuous Integration

CI requires a Kubernetes cluster. Check [pubsweet/infra](https://gitlab.coko.foundation/pubsweet/infra) for the instructions for setting up a Kubernetes cluster on AWS.

Check [pubsweet/deployer](https://gitlab.coko.foundation/pubsweet/deployer) and [xpub/deployment-config](https://gitlab.coko.foundation/xpub/deployment-config) for tools and templates for deployment to this cluster.

## Community

Join the [Coko Mattermost channel](https://mattermost.coko.foundation/coko/channels/xpub) for discussion on XPub/PubSweet/XSweet.