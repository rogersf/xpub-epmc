#!/bin/bash

echo "Running cron service"
# Add the env variables at the beginning of the cron file
env | cat - /etc/cron.d/crontab > temp && crontab temp
service cron start
echo 'Running migrations'
yarn migrate

echo 'Installing epmc-pwd...'
[ -f epmc-pwd.sh ] && sh epmc-pwd.sh
echo 'epmc-pwd installed'

echo "Running Seed"
yarn seed

# echo "Running ftp upload monitors"
# pm2 start pm2.ftp.config.js
