const pm2Config = {
  apps: [
    {
      name: 'pubsweet',
      script: './node_modules/pubsweet/src/startup/start.js',
      exec_mode: 'cluster_mode',
      instances: 1,
    },
  ],
}

module.exports = pm2Config
