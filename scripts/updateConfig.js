const rfr = require('rfr')
const logger = require('@pubsweet/logger')
const Configstore = require('configstore')

if (process.argv.length < 4) {
  logger.info('Usage: node updateConfig.js <name> <value>')
  process.exit(1)
}

const packageJson = rfr('./package.json')

const config = new Configstore(packageJson.name)

if (process.argv[3]) {
  config.set(process.argv[2], process.argv[3])
  logger.info(`Config ${process.argv[2]} set to ${process.argv[3]}`)
} else {
  config.delete(process.argv[2])
  logger.info(`Config ${process.argv[2]} removed`)
}

logger.info('Updated config: ', config.all)
