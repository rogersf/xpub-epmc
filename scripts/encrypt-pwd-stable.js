const logger = require('@pubsweet/logger')
const BaseModel = require('@pubsweet/base-model')
const Identity = require('../server/xpub-model/entities/identity/data-access')

const knex = BaseModel.knex()

async function run() {
  try {
    const identities = await knex('identity')
      .select(['id', 'password_hash'])
      .whereRaw('length(password_hash)=?', [7])
    /* eslint-disable no-await-in-loop */
    /* eslint-disable no-restricted-syntax */
    for (const identity of identities) {
      await knex('identity')
        .where({ id: identity.id })
        .update({
          password_hash: await Identity.hashPassword(identity.passwordHash),
        })
    }
  } catch (e) {
    logger.info(e)
  }
}

;(async () => {
  await run()
  await knex.destroy()
})()
