const logger = require('@pubsweet/logger')
const { addUser } = require('@pubsweet/db-manager')

if (process.argv.length !== 6) {
  logger.info(
    'Usage  : node scripts/adduser.js <username> <email> <password> <isAdmin>',
  )
  logger.info(
    'Example: node scripts/adduser.js admin1 admin@example.com password123 true',
  )
  process.exit(1)
}

const args = process.argv.slice(2)
const user = {
  username: args[0],
  email: args[1],
  password: args[2],
  admin: args[3] === 'true',
}
addUser(user)
