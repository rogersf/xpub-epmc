CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE OR REPLACE FUNCTION trigger_set_timestamp()
RETURNS TRIGGER AS $$
BEGIN
  NEW.updated_at = NOW();
  RETURN NEW;
END;
$$ language 'plpgsql';

CREATE OR REPLACE FUNCTION trigger_set_manuscript_id() RETURNS TRIGGER AS $$
BEGIN
  NEW.id = CONCAT('EMS', nextval('manuscript_emsid_seq'));
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

-- merge users
CREATE OR REPLACE FUNCTION merge(secondary UUID, main UUID, updatedBy UUID) RETURNS BOOLEAN AS $$
  BEGIN
    update users set deleted = now(), updated_by = updatedBy where id = secondary;
    update annotation set user_id = main where user_id = secondary;
    update review set user_id = main where user_id = secondary;
    update manuscript set updated_by = main where updated_by = secondary;
    update file set updated_by = main where updated_by = secondary;
    update note set updated_by = main where updated_by = secondary;
    update review set updated_by = main where updated_by = secondary;
    update team set updated_by = main where updated_by = secondary; 
    delete from team where user_id = secondary and manuscript_id is null;
    delete from identity where user_id = secondary;
    update team set user_id = main where user_id = secondary and manuscript_id is not null;
    RETURN true;
  END;
  $$ LANGUAGE plpgsql;

-------------DB Schema-------------

CREATE TABLE organization (
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
    updated TIMESTAMP WITH TIME ZONE DEFAULT current_timestamp,
    name TEXT NOT NULL
);

CREATE TABLE privacy_notice (
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    version TEXT NOT NULL UNIQUE,
    effective_date TIMESTAMP NOT NULL
);


CREATE TABLE journal (
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    -- organization_id UUID NOT NULL REFERENCES organization,
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
    updated TIMESTAMP WITH TIME ZONE,
    date_revised TIMESTAMP WITH TIME ZONE,
    journal_title TEXT NOT NULL,
    "meta,publisher_name" TEXT,
    "meta,issn" JSONB[],
    "meta,nlmta" TEXT,
    "meta,pmc_status" BOOLEAN,
    "meta,nlmuniqueid" TEXT UNIQUE,
    "meta,pubmed_status" BOOLEAN,
    "meta,first_year" TEXT,
    "meta,end_year" TEXT
);

CREATE INDEX journalTitle_idx ON journal (journal_title);
CREATE INDEX journalNlmta_idx ON journal ("meta,nlmta");

CREATE TABLE manuscript (
    id TEXT PRIMARY KEY, -- emsid
    journal_id UUID REFERENCES journal,
    organization_id UUID NOT NULL REFERENCES organization,
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
    updated TIMESTAMP WITH TIME ZONE,
    deleted TIMESTAMP WITH TIME ZONE,
    status TEXT NOT NULL,
    form_state TEXT,
    decision TEXT,
    "meta,title" TEXT,
    "meta,article_type" TEXT,
    "meta,article_ids" JSONB,
    "meta,abstract" TEXT,
    "meta,subjects" TEXT[],
    "meta,publication_dates" JSONB,
    "meta,volume" TEXT,
    "meta,issue" TEXT,
    "meta,location" JSONB,
    "meta,funding_group" JSONB,
    "meta,release_delay" TEXT,
    "meta,unmatched_journal" TEXT,
    pdf_deposit_id TEXT,
    pdf_deposit_state TEXT,
    ncbi_state TEXT,
    updated_by UUID NOT NULL,
    claimed_by TEXT
);

-- ALTER SEQUENCE manuscript_id_seq RESTART WITH 80000;
CREATE SEQUENCE IF NOT EXISTS manuscript_emsid_seq START 90000;

CREATE INDEX pdfState_idx ON manuscript (pdf_deposit_state);

CREATE TABLE file (
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    manuscript_id TEXT NOT NULL REFERENCES manuscript,
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
    updated TIMESTAMP WITH TIME ZONE,
    deleted TIMESTAMP WITH TIME ZONE,
    type TEXT,
    label TEXT,
    filename TEXT NOT NULL,
    url TEXT NOT NULL,
    mime_type TEXT,
    size INTEGER,
    updated_by UUID NOT NULL
);

CREATE INDEX file_manid_idx ON file (manuscript_id) ;

-- user is a reserved word so we use users instead
CREATE TABLE users (
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    title TEXT,
    given_names TEXT,
    surname TEXT,
    privacy_notice_version TEXT NOT NULL REFERENCES privacy_notice(version),
    privacy_notice_agreed_date TIMESTAMP WITH TIME ZONE NOT NULL,
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
    updated TIMESTAMP WITH TIME ZONE,
    default_identity TEXT,
    deleted TIMESTAMP WITH TIME ZONE,
    updated_by UUID
);

CREATE TABLE review (
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
    updated TIMESTAMP WITH TIME ZONE,
    deleted TIMESTAMP WITH TIME ZONE,
    comments JSONB[],
    recommendation TEXT,
    open BOOLEAN,
    user_id UUID NOT NULL REFERENCES users,
    manuscript_id TEXT NOT NULL REFERENCES manuscript,
    updated_by UUID NOT NULL
);

CREATE TABLE note (
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
    updated TIMESTAMP WITH TIME ZONE,
    deleted TIMESTAMP WITH TIME ZONE,
    content TEXT,
    notes_type TEXT,
    manuscript_id TEXT NOT NULL REFERENCES manuscript,
    updated_by UUID NOT NULL
);

CREATE TABLE role (
    name TEXT PRIMARY KEY,
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
    updated TIMESTAMP WITH TIME ZONE,
    organization BOOLEAN NOT NULL
    -- object_id UUID NOT NULL,
    -- object_type TEXT NOT NULL
);

CREATE TABLE team (
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
    updated TIMESTAMP WITH TIME ZONE,
    deleted TIMESTAMP WITH TIME ZONE,
    manuscript_id TEXT REFERENCES manuscript,
    user_id UUID NOT NULL REFERENCES users,
    role_name TEXT NOT NULL REFERENCES role(name),
    UNIQUE (manuscript_id, user_id, role_name),
    updated_by UUID
);

CREATE UNIQUE INDEX mtu_m_uni_idx ON team (user_id, role_name) WHERE manuscript_id IS NULL;

CREATE TABLE identity (
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    user_id UUID NOT NULL REFERENCES users,
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
    updated TIMESTAMP WITH TIME ZONE,
    type TEXT NOT NULL, -- e.g. local, orcid
    identifier TEXT, -- e.g. orcid ID
    email TEXT UNIQUE, -- used for contacting user
    password_hash TEXT,
    password_reset_token TEXT,
    deleted TIMESTAMP WITH TIME ZONE,
    updated_by UUID,
    meta JSONB -- anything else, e.g. password, oauth tokens
);

-- ALTER TABLE users
--   ADD COLUMN title TEXT,
--   ADD COLUMN given_names TEXT,
--   ADD COLUMN surname TEXT;

-- annotation --
CREATE TABLE annotation
(
  id uuid DEFAULT uuid_generate_v4() NOT NULL,
  quote varchar,
  "text" varchar,
  ranges JSONB,
  created TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  updated TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  deleted TIMESTAMP WITH TIME ZONE,
  user_id UUID NOT NULL REFERENCES users,
  review_id UUID NOT NULL REFERENCES review,
  file_id UUID NOT NULL REFERENCES file,
  CONSTRAINT annotation_pkey PRIMARY KEY (id)
);

-- CREATE TRIGGER set_timestamp
-- BEFORE UPDATE ON annotations
-- FOR EACH ROW
-- EXECUTE PROCEDURE trigger_set_timestamp();

CREATE TRIGGER set_manuscript_id
BEFORE INSERT ON manuscript
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_manuscript_id();
