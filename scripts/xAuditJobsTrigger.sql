create table audit.job_log
(
  id            uuid default uuid_generate_v4()                    not null
    constraint job_log_pkey
      primary key,
  created       timestamp with time zone default CURRENT_TIMESTAMP not null,
  action        text                                               not null,
  original_data jsonb,
  changes       jsonb,
  job_name      text,
  data_center   text
);


CREATE OR REPLACE FUNCTION audit.job_modified_func() RETURNS TRIGGER AS $body$
DECLARE
  audit_row audit.job_log;
  include_values boolean;
  log_diffs boolean;
  h_old_data hstore;
  h_changes hstore;
  excluded_cols text[] = ARRAY[]::text[];
BEGIN
  IF TG_WHEN <> 'AFTER' THEN
    RAISE EXCEPTION 'audit.job_modified_func() may only run as an AFTER trigger';
  END IF;

  audit_row = ROW(
    uuid_generate_v4(), -- id
    current_timestamp,                            -- created
    substring(TG_OP,1,1),                         -- action
    NULL,                                         -- original_data
    NULL,                                         -- changes
    NULL,                                         -- job_name
    NULL                                         -- data_center
    );

  IF TG_ARGV[1] IS NOT NULL THEN
    excluded_cols = TG_ARGV[1]::text[];
  ELSE
    excluded_cols = ARRAY['created', 'updated']::text[];
  END IF;

  IF (TG_OP = 'UPDATE' AND TG_LEVEL = 'ROW') THEN
    h_old_data = hstore(OLD.*) - excluded_cols;
    audit_row.original_data = to_json(h_old_data);
    h_changes =  (hstore(NEW.*) - h_old_data) - excluded_cols;
    audit_row.data_center = NEW.data_center;
    audit_row.job_name = NEW.name;
    IF h_changes = hstore('') THEN
      -- All changed fields are ignored. Skip this update.
      RETURN NULL;
    ELSE
      audit_row.changes = hstore_to_jsonb(h_changes);
    END IF;
  ELSIF (TG_OP = 'DELETE' AND TG_LEVEL = 'ROW') THEN
    h_old_data = hstore(OLD.*) - excluded_cols;
    audit_row.data_center = OLD.data_center;
    audit_row.job_name = OLD.name;
    audit_row.original_data = hstore_to_jsonb(h_old_data);
  ELSIF (TG_OP = 'INSERT' AND TG_LEVEL = 'ROW') THEN
    h_changes =  hstore(NEW.*) - excluded_cols;
    audit_row.changes = hstore_to_jsonb(h_changes);
    audit_row.data_center = NEW.data_center;
    audit_row.job_name = NEW.name;
  ELSE
    RAISE EXCEPTION '[audit.job_modified_func] - Trigger func added as trigger for unhandled case: %, %',TG_OP, TG_LEVEL;
    RETURN NULL;
  END IF;
  INSERT INTO audit.job_log VALUES (audit_row.*);
  RETURN NULL;
END;
$body$
  LANGUAGE plpgsql
  SECURITY DEFINER
  SET search_path = pg_catalog, public;


COMMENT ON FUNCTION audit.job_modified_func() IS $body$
Track changes to config.job table at the statement and/or row level.

Optional parameters to trigger in CREATE TRIGGER call:

param 0: boolean, whether to log the query text. Default 't'.

param 1: text[], columns to ignore in updates. Default [].

         Updates to ignored cols are omitted from changed_fields.

         Updates with only ignored cols changed are not inserted
         into the audit log.

         Almost all the processing work is still done for updates
         that ignored. If you need to save the load, you need to use
         WHEN clause on the trigger instead.

         No warning or error is issued if ignored_cols contains columns
         that do not exist in the target table. This lets you specify
         a standard set of ignored columns.

There is no parameter to disable logging of values. Add this trigger as
a 'FOR EACH STATEMENT' rather than 'FOR EACH ROW' trigger if you do not
want to log row values.
$body$;

DROP TRIGGER IF EXISTS audit_trigger_row ON config.job;
CREATE TRIGGER audit_trigger_row AFTER INSERT OR UPDATE OR DELETE ON config.job FOR EACH ROW EXECUTE PROCEDURE audit.job_modified_func();

CREATE OR REPLACE VIEW audit.tableslist AS
SELECT DISTINCT triggers.trigger_schema AS schema,
                triggers.event_object_table AS auditedtable
FROM information_schema.triggers
WHERE triggers.trigger_name::text IN ('audit_trigger_row'::text, 'audit_trigger_stm'::text)
ORDER BY schema, auditedtable;

COMMENT ON VIEW audit.tableslist IS $body$
View showing all tables with auditing set up. Ordered by schema, then table.
$body$;
