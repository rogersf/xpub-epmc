const { deferConfig } = require('config/defer')

module.exports = {
  'pubsweet-server': {
    baseUrl: deferConfig(
      cfg => `http://localhost:${cfg['pubsweet-server'].port}`,
    ),
    secret: 'secret-string',
    internalBaseUrl: 'http://localhost:3000',
    internalServiceUrl: 'http://localhost:3000',
  },
  'epmc-email': {
    testAddress: process.env.PUBSWEET_TEST_EMAIL || 'plusdev-shared@ebi.ac.uk',
  },
  dbManager: {
    username: 'admin',
    password: 'password',
    email: 'admin@example.com',
    admin: true,
  },
}
