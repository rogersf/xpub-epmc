require('dotenv').config()

module.exports = {
  transport: {
    host: process.env.MAILER_HOST,
    port: Number(process.env.MAILER_PORT),
  },
}
